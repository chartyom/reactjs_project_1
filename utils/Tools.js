const config = require('../config')
const UsersModel = require('../models/UsersModel')
const bluebird = require('bluebird')
const bcrypt = bluebird.promisifyAll(require('bcryptjs'))
const tool = module.exports

const MIN_CODE = 10000
const MAX_CODE = 99999

const ABC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

tool.generateCode = async function () {
    const code = (Math.floor(Math.random() * (MAX_CODE - MIN_CODE + 1)) + MIN_CODE).toString()
    let salt = await bcrypt.genSaltAsync(10)
    let hash = await bcrypt.hashAsync(code, salt)
    return { code, hash }
}

tool.generateReferrerCode = async function () {
    let code = []
    for (let i = 0; i < config.REFERRER_CODE_LENGTH; i++) {
        code.push(ABC.charAt(Math.floor(Math.random() * ABC.length)))
    }
    code = code.join('')
    let user = await UsersModel.getOneByRefCode(code)
    if (user) {
        return await generateReferrerCode()
    }
    return code
}