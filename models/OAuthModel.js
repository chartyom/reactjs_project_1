const util = require('util'),
  redis = require('../db/redis'),
  crypto = require('crypto'),
  randomBytes = require('bluebird').promisify(require('crypto').randomBytes),
  usersModel = require('./UsersModel'),
  clientsModel = require('./ClientsModel'),
  accessAuthorizationsModel = require('./AccessAuthorizationsModel'),
  refreshTokensModel = require('./RefreshTokensModel'),
  InvalidRequestError = require('../own_modules/oauth2-server/errors/invalid-request-error'),
  bluebird = require('bluebird'),
  bcrypt = bluebird.promisifyAll(require('bcryptjs')),
  CacheModel = require('../models/CacheModel')

const model = {
  /**
   * Generate access token
   * 
   * Вызывается во время:
   * 
   * authorization_code grant
   * client_credentials grant
   * refresh_token grant
   * password grant
   * 
   * @param {Object} client           Клиент для которого генерируется access token
   * @param {Object} user             Пользователь для которого генерируется access token
   * @param {Object} scope            Области, связанные с токеном доступа. Может быть нулевым.

   * 
   * @returns {String}
   */
  generateAccessToken: async function (client, user, scope) {
    if (DEVELOPMENT) console.log('OAuthModel: generateAccessToken');

    // можно подключить JWT
    return await randomBytes(256).then(function (buffer) {
      return crypto
        .createHash('sha1')
        .update(buffer)
        .digest('hex');
    });
  },

  /**
   * Generate refresh token
   * 
   * Вызывается во время:
   * 
   * authorization_code grant
   * refresh_token grant
   * password grant
   * 
   * @param {Object} client           Клиент для которого генерируется access token
   * @param {Object} user             Пользователь для которого генерируется access token
   * @param {Object} scope            Области, связанные с токеном доступа. Может быть нулевым.

   * 
   * @returns {String}
   */
  generateRefreshToken: async function (client, user, scope) {
    if (DEVELOPMENT) console.log('OAuthModel: generateRefreshToken');

    // можно подключить JWT
    return await randomBytes(256).then(function (buffer) {
      return crypto
        .createHash('sha1')
        .update(buffer)
        .digest('hex');
    });
  },


  /**
   * Generate authorization code
   * 
   * Вызывается во время:
   * 
   * authorization_code grant
   * 
   * 
   * @returns {String}
   * 
   */
  generateAuthorizationCode: async function () {
    if (DEVELOPMENT) console.log('OAuthModel: generateAuthorizationCode');

    return await randomBytes(256).then(function (buffer) {
      return crypto
        .createHash('sha1')
        .update(buffer)
        .digest('hex');
    });
  },

  /**
   * getAccessToken вызывается для получения существующего access token, ранее сохраненного через Model#saveToken().
   * 
   * Вызывается во время:
   * 
   * request authentication
   * 
   * @param {String} accessToken      access token

   * 
   * @returns {Object}
   * 
   */
  getAccessToken: async function (accessToken) {
    if (DEVELOPMENT) console.log('OAuthModel: getAccessToken');
    // EXAMPLE
    // db.queryAccessToken({ access_token: accessToken })
    //   .then(function (token) {
    //     return Promise.all([
    //       token,
    //       db.queryClient({ id: token.client_id }),
    //       db.queryUser({ id: token.user_id })
    //     ]);
    //   })
    //   .spread(function (token, client, user) {
    //     return {
    //       accessToken: token.access_token,
    //       accessTokenExpiresAt: token.expires_at,
    //       scope: token.scope,
    //       client: client, // with 'id' property
    //       user: user
    //     };
    //   });
    return await redis.hgetallAsync('AccessTokens:' + accessToken).then((res) => {
      if (!res) return null
      return {
        accessToken: res.token,
        accessTokenExpiresAt: new Date(res.expires_at),
        scope: res.scope,
        client: { id: res.client_id }, // with 'id' property
        user: { id: res.user_id }
      };
    })

  },

  /**
   * getRefreshToken Вызывается для получения существующего refresh token, сохраненного ранее Model#saveToken().
   * 
   * Вызывается во время:
   * 
   * refresh_token grant
   * 
   * @param {String} refreshToken     refresh token

   * 
   * @returns {Object}
   * 
   */
  getRefreshToken: async function (refreshToken) {
    if (DEVELOPMENT) console.log('OAuthModel: getRefreshToken');
    // EXAMPLE
    // db.queryRefreshToken({refresh_token: refreshToken})
    //   .then(function(token) {
    //     return Promise.all([
    //       token,
    //       db.queryClient({id: token.client_id}),
    //       db.queryUser({id: token.user_id})
    //     ]);
    //   })
    //   .spread(function(token, client, user) {
    //     return {
    //       refreshToken: token.refresh_token,
    //       refreshTokenExpiresAt: token.expires_at,
    //       scope: token.scope,
    //       client: client, // with 'id' property
    //       user: user
    //     };
    //   });
    var token = await refreshTokensModel.getOneByToken(refreshToken)
    if (!token) return null

    var client = await clientsModel.getOneById(token.client_id)
    if (!client) return null

    var user = await usersModel.getOneById(token.user_id).then((user) => {
      if (!user || !user._id) return null
      return { id: user._id }
    })
    if (!user) return null

    return await {
      refreshToken: token.token,
      refreshTokenExpiresAt: token.expires_at,
      scope: token.scope,
      client: client,
      user: user
    };
  },

  /**
   * getAuthorizationCode Вызывается для получения существующего кода авторизации, сохраненного ранее Model#saveAuthorizationCode().
   * 
   * Вызывается во время:
   * 
   * authorization_code grant
   * 
   * @param {String} authorizationCode      authorization code

   * 
   * @returns {Object}
   * 
   */
  getAuthorizationCode: async function (authorizationCode) {
    if (DEVELOPMENT) console.log('OAuthModel: getAuthorizationCode');
    // EXAMPLE
    // db.queryAuthorizationCode({ authorization_code: authorizationCode })
    //   .then(function (code) {
    //     return Promise.all([
    //       code,
    //       db.queryClient({ id: code.client_id }),
    //       db.queryUser({ id: code.user_id })
    //     ]);
    //   })
    //   .spread(function (code, client, user) {
    //     return {
    //       code: code.authorization_code,
    //       expiresAt: code.expires_at,
    //       redirectUri: code.redirect_uri,
    //       scope: code.scope,
    //       client: client, // with 'id' property
    //       user: user
    //     };
    //   });

    var auth = await accessAuthorizationsModel.getOneByCode(authorizationCode)
    if (!auth) return null

    var client = await clientsModel.getOneById(auth.client_id)
    if (!client) return null

    var user = await usersModel.getOneById(auth.user_id).then((user) => {
      if (!user || !user._id) return null
      return { id: user._id }
    })
    if (!user) return null

    return await {
      code: auth.code, // {String}
      expiresAt: auth.expires_at, // {Date}
      redirectUri: auth.redirect_uri, // {String} Перенаправный URI кода авторизации.
      scope: auth.scope, // {String}
      client: { id: client.id }, // {Object}
      user: { id: user.id } // {Object}
    };
  },

  /**
   * getClient получение клиента с использованием client id или комбинации client id/client secret, в зависимости от типа гранта.
   * 
   * Вызывается во время:
   * 
   * authorization_code grant
   * client_credentials grant
   * refresh_token grant
   * password grant
   * 
   * @param {String} clientId       clientId
   * @param {String} [clientSecret]   clientSecret

   * 
   * @returns {Object}
   * 
   */
  getClient: async function (clientId, clientSecret) {
    if (DEVELOPMENT) console.log('OAuthModel: getClient', clientId, clientSecret);
    var client, userOwner;
    if (clientSecret) {
      client = await clientsModel.getOneByIdSecret(clientId, clientSecret)
      userOwner = true
    } else {
      client = await clientsModel.getOneById(clientId)
      userOwner = false
    }
    if (!client) return null;
    return {
      id: client.id,// {String}
      redirectUris: client.redirect_uris, // {Array<String>} not required 
      grants: client.grants,  // {Array<String>} Предоставляемые клиенту типы грантов.
      scope: client.scope, // {String}
      userId: client.user_id, // {String}
      //accessTokenLifetime: ... // {Number}  not required 
      //refreshTokenLifetime: ... // {Number}  not required 
      userOwner: userOwner
    }
  },

  /**
   * getUser получение пользователя с использованием комбинации имени пользователя и пароля.
   * 
   * Вызывается во время:
   * 
   * password grant
   * 
   * @param {String} username       идентификатор пользователя
   * @param {String} password       password

   * 
   * @returns {Object}
   * 
   */
  getUser: async function (username, password) {

    if (DEVELOPMENT) console.log('OAuthModel: getUser', username, password);

    let phone = username
    let PNF = require('google-libphonenumber').PhoneNumberFormat
    let phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance()
    phone = phoneUtil.format(phoneUtil.parse(phone, 'RU'), PNF.E164)

    let userFromCache = await CacheModel.get(phone) // Ищем текущего пользователя в кэше

    if (!userFromCache)
      return null

    const isMatch = await bcrypt.compareAsync(password, userFromCache.code)

    if (!isMatch)
      return null

    await CacheModel.del(phone)

    return await usersModel.getOneByPhone(phone).then((user) => {
      if (!user || !user._id) return null
      return { id: user._id }
    })
  },

  /**
   * getUserFromClient получение пользователя, связанного с указанным клиентом.
   * 
   * Вызывается во время:
   * 
   * client_credentials grant
   * 
   * @param {object} client       username

   * 
   * @returns {Object}
   * 
   */
  getUserFromClient: async function (client) {

    if (DEVELOPMENT) console.log('OAuthModel: getUserFromClient');

    return await usersModel.getOneById(client.userId).then(function (user) {
      if (!user || !user._id) return null
      return {
        id: user._id, // {String}
      };
    });
  },

  /**
   * saveToken 
   * Вызывается для сохранения access token и необязательного refresh token в зависимости от типа гранта.
   * 
   * Вызывается во время:
   * 
   * authorization_code grant
   * client_credentials grant
   * refresh_token grant
   * password grant
   * 
   * @param {object} token       
   * @param {String} token.accessToken 
   * @param {Date} token.accessTokenExpiresAt
   * @param {String} [token.refreshToken]
   * @param {Date} [token.refreshTokenExpiresAt]
   * @param {String} [token.scope]
   * @param {object} client  
   * @param {object} user  

   * 
   * @returns {Object}
   * 
   */
  saveToken: async function (token, client, user) {
    if (DEVELOPMENT) console.log('OAuthModel: saveToken');

    await redis.hmsetAsync('AccessTokens:' + token.accessToken, {
      token: token.accessToken,
      expires_at: token.accessTokenExpiresAt,
      scope: token.scope,
      client_id: client.id,
      user_id: user.id.toString()
    });
    if (token.refreshToken) {
      await refreshTokensModel.create({
        token: token.refreshToken,
        expiresAt: token.refreshTokenExpiresAt,
        scope: token.scope,
        clientId: client.id,
        userId: user.id.toString()
      })
      return {
        accessToken: token.accessToken, // {String}
        accessTokenExpiresAt: token.accessTokenExpiresAt, // {Date}
        refreshToken: token.refreshToken, // {String}
        refreshTokenExpiresAt: token.refreshTokenExpiresAt, // {Date}
        scope: token.scope, // {String}
        client: { id: client.id }, // {Object}
        user: { id: user.id } // {Object}
      };
    }
    return {
      accessToken: token.accessToken, // {String}
      accessTokenExpiresAt: token.accessTokenExpiresAt, // {Date}
      scope: token.scope, // {String}
      client: { id: client.id }, // {Object}
      user: { id: user.id } // {Object}
    };
  },

  /**
   * saveAuthorizationCode 
   * Вызывается для сохранения кода авторизации.
   * 
   * Вызывается во время:
   * 
   * authorization_code grant
   * 
   * @param {object} code       
   * @param {String} code.authorizationCode 
   * @param {Date} code.expiresAt
   * @param {String} code.redirectUri
   * @param {String} [code.scope]
   * @param {object} client  
   * @param {object} user  

   * 
   * @returns {Object}
   * 
   */
  saveAuthorizationCode: async function (code, client, user) {
    if (DEVELOPMENT) console.log('OAuthModel: saveAuthorizationCode');
    // EXAMPLE
    // let authCode = {
    //   authorization_code: code.authorizationCode,
    //   expires_at: code.expiresAt,
    //   redirect_uri: code.redirectUri,
    //   scope: code.scope,
    //   client_id: client.id,
    //   user_id: user.id
    // };
    // return db.saveAuthorizationCode(authCode)
    //   .then(function (authorizationCode) {
    //     return {
    //       authorizationCode: authorizationCode.authorization_code,
    //       expiresAt: authorizationCode.expires_at,
    //       redirectUri: authorizationCode.redirect_uri,
    //       scope: authorizationCode.scope,
    //       client: { id: authorizationCode.client_id },
    //       user: { id: authorizationCode.user_id }
    //     };
    //   });

    await accessAuthorizationsModel.create({
      code: code.authorizationCode,
      redirectUri: code.redirectUri,
      clientId: client.id,
      expiresAt: code.expiresAt,
      scope: code.scope,
      userId: user.id
    })

    return await {
      authorizationCode: code.authorizationCode, // {String}
      expiresAt: code.expiresAt, // {Date}
      redirectUri: code.redirectUri, // {String}
      scope: code.scope, // {String}
      client: { id: client.id }, // {Object}
      user: { id: user.id } // {Object}
    };
  },

  /**
   * revokeToken 
   * Вызывается для отзыва refresh token.
   * 
   * Вызывается во время:
   * 
   * refresh_token grant
   * 
   * @param {object} token       
   * @param {String} token.refreshToken 
   * @param {Date} [token.refreshTokenExpiresAt]
   * @param {String} [token.scope]
   * @param {object} token.client  
   * @param {String} token.client.id 
   * @param {object} token.user  

   * 
   * @returns {Boolean} Возвращает true, если отзыв был успешным или false, если refresh token не найден.
   * 
   */
  revokeToken: async function (token) {
    if (DEVELOPMENT) console.log('OAuthModel: revokeToken');
    // EXAMPLE
    // return db.deleteRefreshToken({ refresh_token: token.refreshToken })
    //   .then(function (refreshToken) {
    //     return !!refreshToken;
    //   });
    return await refreshTokensModel.removeOneByToken(token)
  },

  /**
   * revokeAuthorizationCode 
   * Вызывается для отзыва authorization code.
   * 
   * Вызывается во время:
   * 
   * authorization_code grant
   * 
   * @param {object} code       
   * @param {String} code.code 
   * @param {Date} code.expiresAt
   * @param {Date} [code.redirectUri]
   * @param {String} [code.scope]
   * @param {object} code.client  
   * @param {String} code.client.id 
   * @param {object} code.user  

   * 
   * @returns {Boolean} Возвращает true, если отзыв был успешным или false, если authorization code не найден.
   * 
   */
  revokeAuthorizationCode: async function (code) {
    if (DEVELOPMENT) console.log('OAuthModel: revokeAuthorizationCode');
    // EXAMPLE
    // return db.deleteAuthorizationCode({ authorization_code: code.authorizationCode })
    //   .then(function (authorizationCode) {
    //     return !!authorizationCode;
    //   });
    return await accessAuthorizationsModel.updateOneByCodeIsDeleted(code.code)
  },

  /**
   * validateScope 
   * Вызывается, чтобы проверить, действительна ли scope удовлетворяет конкретной комбинации клиент / пользователь.
   * 
   * Вызывается во время:
   * 
   * authorization_code grant
   * client_credentials grant
   * password grant
   * 
   * @param {object} user       
   * @param {object} client  
   * @param {String} client.id 
   * @param {String} scope  

   * 
   * @returns {Bool || String} Запрашиваемые scope могут использоваться или если таких прав не существует вернет false
   * 
   */
  validateScope: async function (user, client, scope) {
    if (DEVELOPMENT) console.log('OAuthModel: validateScope');
    // EXAMPLE
    // To reject invalid or only partially valid scopes:

    // // list of valid scopes
    // const VALID_SCOPES = ['read', 'write'];

    // function validateScope(user, client, scope) {
    // if (!scope.split(' ').every(s => VALID_SCOPES.indexOf(s) >= 0)) {
    //   return false;
    // }
    // return scope;
    // }
    // To accept partially valid scopes:

    // // list of valid scopes
    // const VALID_SCOPES = ['read', 'write'];

    // function validateScope(user, client, scope) {
    //   return scope
    //     .split(' ')
    //     .filter(s => VALID_SCOPES.indexOf(s) >= 0)
    //     .join(' ');
    // }

    if (client) {
      if (scope) {
        let clientScope = client.scope.split(' ')
        if (!scope.split(' ').every(s => clientScope.indexOf(s) >= 0)) return await false
        // if (!scope.split(' ').every(s => client.scope.split(' ').indexOf(s) >= 0)) return await false

        return await scope
      }
      return await client.scope
    }
    return await false
  },

  /**
   * verifyScope 
   * Вызывается во время аутентификации запроса, чтобы проверить, разрешен ли предоставленный access token запрошенному scope.
   * 
   * Вызывается во время:
   * 
   * request authentication
   * 
   * @param {object} token  
   * @param {String} token.accessToken 
   * @param {Date} [token.accessTokenExpiresAt] 
   * @param {String} token.scope    
   * @param {object} token.client  
   * @param {String} token.client.id 
   * @param {object} token.user 
   * @param {String} scope  

   * 
   * @returns {Bool} 
   * 
   */
  verifyScope: async function (token, scope) {
    if (DEVELOPMENT) console.log('OAuthModel: verifyScope');
    if (!token.scope) {
      return false;
    }
    let requestedScopes = scope.split(' ');
    let authorizedScopes = token.scope.split(' ');
    return requestedScopes.every(s => authorizedScopes.indexOf(s) >= 0);
  },
};

module.exports = model;