const Memcached = require('../db/memcached')
const model = module.exports
const config = require('../config')

model.get = async function (id) {
    return await new Promise((resolve, reject) => {
        Memcached.get(id.toString(), (err, data) => {
            if (err) reject(err)
            resolve(data)
        })
    })
}

model.set = async function (id, data) {
    return await new Promise((resolve, reject) => {
        Memcached.set(id.toString(), data, config.DEFAULT_CACHE_TIME, (err, data) => {
            if (err) reject(err)
            resolve(data)
        })
    })
}

model.del = async function (id) {
    return await new Promise((resolve, reject) => {
        Memcached.del(id.toString(), (err) => {
            if (err) reject(err)
            resolve({ success: true })
        })
    })
}
