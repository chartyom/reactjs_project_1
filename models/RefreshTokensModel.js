/*
Schema RefreshTokens   

id: { type: Integer },
token: { type: String, required: true, size: 255 },
client_id: { type: Integer, required: true },
expires_at: { type: Date, required: true },
user_id: { type: String, required: true, size: 80 },
created_at: { type: Date }
*/

var db = require('../db/postgres');

/**
 * getOneByToken
 * 
 * @param {Int} id
 * 
 * @return {Promise<Object>||Object}
 */
async function getOneByToken(token) {
    return await db.oneOrNone(
        'SELECT token, client_id, expires_at, scope, user_id FROM refresh_tokens WHERE token=${token} LIMIT 1',
        { token }
    ).catch(e => { console.log("pg_error", e) })
}

/**
 * create
 * 
 * @param {Object} data
 * @param {String} data.token
 * @param {Integer} data.clientId
 * @param {Date} data.expiresAt
 * @param {String} data.userId
 * 
 * @return {Promise<Object>||<Integer>}
 */
async function create({ token, clientId, expiresAt, userId, scope }) {
    return await db.one(
        'INSERT INTO refresh_tokens(token, client_id, expires_at, user_id, scope) ' +
        'VALUES(${token}, ${clientId}, ${expiresAt}, ${userId}, ${scope}) ' +
        'RETURNING id',
        { token, clientId, expiresAt, userId, scope }
    ).catch(e => { console.log("pg_error", e) });
}

/**
 * removeOneByToken
 * 
 * @param {Integer} id
 * 
 * @return {Promise<Object>}
 */
async function removeOneByToken(token) {
    return await db.oneOrNone(
        'DELETE FROM refresh_tokens WHERE token=${token} RETURNING *',
        { token }
    ).catch(e => { console.log("pg_error", e) });
}


module.exports = {
    getOneByToken: getOneByToken,
    create: create,
    removeOneByToken: removeOneByToken,
}