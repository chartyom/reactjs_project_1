const model = module.exports

const rq = require('request')
const config = require('../config')

/**
 * 
 * 
 */
model.send = async function (phone, params, slug, userId, send = true) {
    return await rq.post({
        url: `${config.url.sms}/makesms`,
        json: {
            phone,
            params,
            slug,
            send,
            userId
        }
    })
}