/*
Schema access_authentifications   

id: { type: Integer },
code: { type: String, required: true, size: 40 },
redirect_uri: { type: String, required: true, size: 100 },
scope: { type: String, required: true, size: 255 },
client_id: { type: Integer, required: true },
expires_at: { type: Date, required: true },
user_id: { type: String, required: true, size: 80 },
created_at: { type: Date }
deleted: { type: Boolean }
*/

var db = require('../db/postgres')
var model = module.exports

/**
 * getOneByCode
 * 
 * @param {String} code
 * 
 * @return {Promise<Object>||Object}
 */
model.getOneByCode = async function (code) {
    return await db.oneOrNone(
        'SELECT id, code, redirect_uri, client_id, expires_at, scope, user_id, deleted FROM access_authorizations WHERE code=${code} LIMIT 1',
        { code }
    )
        .then(res => {
            if (res.deleted) return null
            return res
        })
        .catch(e => { console.log("pg_error", e) })
}

/**
 * create
 * 
 * @param {Object} data
 * @param {String} data.code
 * @param {String} data.redirectUri
 * @param {Integer} data.clientId
 * @param {Date} data.expiresAt
 * @param {String} data.scope
 * @param {String} data.userId
 * 
 * @return {Promise<Object>||<Integer>}
 */
model.create = async function ({ code, redirectUri, clientId, expiresAt, scope, userId }) {
    return await db.one(
        'INSERT INTO access_authorizations(code, redirect_uri, client_id, expires_at, scope, user_id) ' +
        'VALUES(${code}, ${redirectUri}, ${clientId}, ${expiresAt}, ${scope}, ${userId}) ' +
        'RETURNING id',
        { code, redirectUri, clientId, expiresAt, scope, userId }
    ).catch(e => { console.log("pg_error", e) });
}

/**
 * removeOneByCode
 * 
 * @param {String} code
 * 
 * @return {Promise<Object>}
 */
model.removeOneByCode = async function (code) {
    return await db.oneOrNone(
        'DELETE FROM access_authorizations WHERE code=${code} RETURNING *',
        { code }
    ).catch(e => { console.log("pg_error", e) });
}

/**
 * updateOneByCodeIsDeleted
 * Устанавливает значение по которому определяется завершенным авторизационный код
 * 
 * @param {String} code
 * @param {Boolean} [deleted]
 * 
 * @return {Promise<Object>}
 */
model.updateOneByCodeIsDeleted = async function (code, deleted) {
    if (typeof deleted !== 'boolean') deleted = true
    return await db.oneOrNone(
        'UPDATE access_authorizations SET deleted=${deleted} ' +
        'WHERE code=${code} RETURNING *',
        { code, deleted }
    ).catch(e => { console.log("pg_error", e) });
}
