/*
Schema access_authentifications   

id: { type: Integer },
name: { type: String, required: true, size: 60 },
description: { type: String, size: 255 },
content: { type: String, size: 255 },
view: { type: Boolean, required: true }
*/

var db = require('../db/postgres')
var model = module.exports

/**
 * getWithView
 * 
 * @return {Promise<Object>||Object}
 */
model.getWithView = async function () {
    return await db.manyOrNone(
        'SELECT id, name, content FROM access_rules WHERE view=TRUE'
    )
        .then(res => {
            return res
        })
        .catch(e => { console.log("pg_error", e) })
}
