//var mongoose = require('mongoose')
//const mongoDB = mongoose.model('User',{ phone: String, email: String, emailVerified: Boolean })
// var Schema = mongoose.Schema

// var usersSchema = new Schema(
//     {
//         username: { type: String, required: true },
//         password: { type: String, required: true },
//     },
//     {
//         timestamps: true
//     }
// )

// module.exports = mongoose.model('Users', usersSchema)

const rq = require('request')
const config = require('../config')
const model = module.exports
const CacheModel = require('./CacheModel')
const bluebird = require('bluebird')
const bcrypt = bluebird.promisifyAll(require('bcryptjs'))

/**
 * @param {String} userId
 * 
 * @return {Promise<>} 
 */
model.getOneById = async function (userId) {
    return new Promise((resolve, reject) => {
        rq.post({
            url: `${config.url.user}/users/getOne`,
            json: {
                id: userId
            },
        }, function (error, response, body) {
            if (error) {
                console.error('Upload failed:', error);
                return reject(error)
            }
            let user = body.user && body.user._id ? body.user : null
            resolve(user)
        })
    })
}


/**
 * @param {String} phone
 * 
 * @return {Promise<>} 
 */
model.getOneByPhone = async function (phone) {
    return new Promise((resolve, reject) => {
        rq.post({
            url: `${config.url.user}/users/get`,
            json: {
                filter: {
                    phone: phone,
                    verified: true //искомый пользователь должен быть подтвержденным
                }
            },
        }, function (error, response, body) {
            if (error) {
                console.error('Upload failed:', error);
                return reject(error)
            }
            let user = body.users && body.users[0] && body.users[0]._id ? body.users[0] : null
            resolve(user)
        })
    })
}

/**
 * @param {String} email
 * 
 * @return {Promise<>} 
 */
model.getOneByEmail = function (email) {
    return new Promise((resolve, reject) => {
        rq.post({
            url: `${config.url.user}/users/get`,
            json: {
                filter: {
                    email: email,
                    emailVerified: true //искомый пользовательский e-mail должен быть подтвержденным
                }
            },
        }, function (error, response, body) {
            if (error) {
                console.error('Upload failed:', error);
                return reject(error)
            }
            let user = body.users && body.users[0] && body.users[0]._id ? body.users[0] : null
            resolve(user)
        })
    })
}

/**
 * @param {String} code
 * 
 * @return {Promise<>} 
 */
model.getOneByRefCode = async function (code) {
    return await new Promise((resolve, reject) => {
        rq.post({
            url: `${config.url.user}/users/get`,
            json: {
                filter: {
                    refCode: code,
                    verified: true //искомый пользователь должен быть подтвержденным
                }
            },
        }, function (error, response, body) {
            if (error) {
                console.error('Upload failed:', error);
                return reject(error)
            }
            let user = body.users && body.users[0] && body.users[0]._id ? body.users[0] : null
            resolve(user)
        })
    })
}



/**
 * @param {Object} user
 * 
 * @return {Promise<>} 
 */
model.create = async function (user) {
    return new Promise((resolve, reject) => {
        rq.post({
            url: `${config.url.user}/users/create`,
            json: user,
        }, function (error, response, body) {
            if (error) {
                console.error('Upload failed:', error);
                return reject(error)
            }
            resolve(body)
        })
    })
}

/**
 * @param {Object} user
 * 
 * @return {Promise<>} 
 */
model.authorize = async function (phone, code) {
    return new Promise((resolve, reject) => {
        rq.post({
            url: `${config.server.url}/token`,
            form: {
                grant_type: 'password',
                client_id: 1,
                username: phone,
                password: code,
                scope: 'oauth user_profile'
            },
            json: true
        }, function (error, response, body) {
            if (error) {
                console.error('Upload failed:', error);
                return reject(error)
            }
            resolve(body)
        })
    })
}
