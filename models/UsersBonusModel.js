const rq = require('request')
const config = require('../config')
const model = module.exports

/**
 * @param {String} referrerId
 * @param {String} userId
 * 
 * @return {Promise<>} 
 */
model.create = async function (referrerId, userId) {
    return new Promise((resolve, reject) => {
        rq.post({
            url: `${config.url.bonus}/refill/referralInvitation/create`,
            json: { referrerId: referrerId, referralId: userId },
        }, function (error, response, body) {
            if (error) {
                console.error('Upload failed:', error);
                return reject(error)
            }
            resolve({ success: true })
        })
    })
}
