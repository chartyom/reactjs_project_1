/*
Schema Clients   

id: { type: Integer },
name: { type: String, required: true, size: 255 },
secret: { type: String, required: true, size: 60 },
scope: { type: String, required: true, size: 255 },
user_id: { type: String, required: true, size: 80 },
redirect_uris: [{ type: String, required: true, size: 255 }],
grants: [{ type: String, required: true, size: 255 }],
description: { type: String, required: true, size: 255 },
confirmed: { type: Boolean, default: false },
confirmed_at: { type: Date },
blocked: { type: Boolean, default: false },
blocked_description: { type: String, default: '', size: 255 },
blocked_at: { type: Date },
created_at: { type: Date }
*/

var db = require('../db/postgres');

/**
 * getOneById
 * 
 * @param {Int} id
 * 
 * @return {Promise<Object>||Object}
 */
async function getOneById(id) {
    return await db.oneOrNone(
        'SELECT id, name, redirect_uris, grants, scope, user_id FROM clients WHERE id=${id} LIMIT 1',
        { id }
    ).catch(e => { console.log("pg_error", e) })
}

/**
 * getOneById
 * 
 * @param {Int} id
 * @param {String} secret
 * 
 * @return {Promise<Object>||Object}
 */
async function getOneByIdSecret(id, secret) {
    return await db.oneOrNone(
        'SELECT id, redirect_uris, grants, scope, user_id FROM clients WHERE id=${id} AND secret=${secret} LIMIT 1',
        { id, secret }
    ).catch(e => { console.log("pg_error", e) })
}

/**
 * create
 * 
 * @param {Object} data
 * @param {String} data.name
 * @param {String} data.secret
 * @param {String} data.description
 * @param {String} data.userId
 * @param {String} data.scope
 * @param {String} data.redirectUris
 * @param {String} data.grants
 * 
 * Example:
 * await clientsModel.create({ name: 'name', secret: 'secret', scope: 'user.read', userId: 'userid', redirectUris: ['http://sitesite.site/callback'], grants: ['password', 'authentification_code'], description: 'application' })
 * 
 * @return {Promise<Object>||<Integer>}
 */
async function create({ name, secret, description, userId, scope, redirectUris, grants }) {
    return await db.one(
        'INSERT INTO clients(name, secret, description, user_id, scope, redirect_uris, grants) ' +
        'VALUES(${name}, ${secret}, ${description}, ${userId}, ${scope}, ${redirectUris}, ${grants}) ' +
        'RETURNING id',
        { name, secret, description, userId, scope, redirectUris, grants }
    ).catch(e => { console.log("pg_error", e) });
}

/**
 * removeOneById
 * 
 * @param {Integer} id
 * 
 * Example:
 * await clientsModel.removeOneById(2)
 * 
 * @return {Promise<Object>}
 */
async function removeOneById(id) {
    return await db.none(
        'DELETE FROM clients WHERE id=${id}',
        { id }
    ).catch(e => { console.log("pg_error", e) });
}

async function updateOneById({ id, name, secret, description, userId, scope, redirectUris, grants }) {
    return await db.one(
        'UPDATE clients SET name=${name}, secret=${secret}, description=${description}, user_id=${userId}, scope=${scope}, redirect_uris=${redirectUris}, grants=${grants}' +
        'WHERE id=${redirectUris} ',
        { id, name, secret, description, userId, scope, redirectUris, grants }
    ).catch(e => { console.log("pg_error", e) });
}

module.exports = {
    getOneById: getOneById,
    create: create,
    removeOneById: removeOneById,
    updateOneById: updateOneById,
    getOneByIdSecret: getOneByIdSecret,
}