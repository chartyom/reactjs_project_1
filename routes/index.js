const bodyParser = require('body-parser');
const request = require('request')
const UserController = require('../controllers/UserController')
const ClientController = require('../controllers/ClientController')
const RulesController = require('../controllers/RulesController')

var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })
var htmlParser = bodyParser.text({ type: 'text/html' });

const CacheModel = require('../models/CacheModel')
/*
 GET — получение ресурса
 POST — создание ресурса
 PUT — обновление ресурса
 DELETE — удаление ресурса
 */

module.exports = function (app) {
    // app.use(bodyParser.json({ type: 'application/*+json' }))
    // app.use(bodyParser.text({ type: 'text/html' }))

    /**
     * 
     * grant_type: authorization_code || password || refresh_token || client_credentials
     * 
     */

    app.all('/token', urlencodedParser, app.oauth.token());
    app.post('/token/revoke', urlencodedParser, app.oauth.authenticate(), UserController.revokeToken);
    app.get('/client/:id', jsonParser, app.oauth.authenticate({ scope: 'oauth' }), ClientController.get);
    app.post('/rules', urlencodedParser, app.oauth.authenticate({ scope: 'oauth' }), RulesController.getByScope);

    app.post('/check/phone', urlencodedParser, UserController.checkPhone);
    app.post('/check/email', urlencodedParser, UserController.checkEmail);
    app.post('/check/referrerCode', urlencodedParser, UserController.checkReferrerCode);
    app.post('/registration/sendSMS', urlencodedParser, UserController.registartionBegin);
    app.post('/registration', urlencodedParser, UserController.registartionEnd);

    app.post('/authenticate/sendSMS', urlencodedParser, UserController.authenticateBegin);
    app.post('/authenticate', urlencodedParser, UserController.authenticateEnd);
    app.post('/regenerateCode', urlencodedParser, UserController.regenerateCode);

    /**
     * 
     * Authorization Code Flow
     * request
     * /authorize?client_id=1&redirect_uri=http://example.com/callback&scope=user&response_type=code
     * response
     * 
     */
    app.post('/authorize', urlencodedParser, app.oauth.authorize({ scope: 'oauth' }));

    app.get('/secret/user', jsonParser, app.oauth.authenticate({ scope: 'user_profile' }), function (req, res) {
        res.json({ response: 'success' });
    });

    // app.get('/blank.html', htmlParser, function (req, res) {
    //     res.send('empty page');
    // });

    app.get('/callback', jsonParser, function (req, res) {

        if (DEVELOPMENT) console.log(req.originalUrl);

        if (req.query && req.query.code) {
            return request.post({
                url: 'http://localhost:3001/token',
                form: {
                    client_id: 9,
                    client_secret: 'secret',
                    grant_type: 'authorization_code',
                    redirect_uri: 'http://localhost:3001/callback',
                    code: req.query.code
                }
            }, function (err, response, body) {
                if (err) return console.error('Error:', err);
                res.status(response.statusCode).format({
                    'application/json': function () {
                        res.send(body);
                    }
                })
            })
        }
        res.status(200).json({ error: req.query.error })
    });

    app.get('*', htmlParser, async function (req, res) {
        const filesApp = await require('../config/files.app.json')
        res.render('main', {
            title: 'Битком 24',
            files: {
                style: filesApp['app.css'],
                manifest: filesApp['manifest.app.js'],
                vendor: filesApp['vendor.app.js'],
                app: filesApp['app.js'],
            }
        });
    });

}

