const _ = require('lodash'),
	RulesModel = require('../models/RulesModel')

/**
 * method:	POST
 * uri:		/rules
 * params:
 * headers:	Authenticate => Bearer ..., 
 * body:	scope
 * query:
 * 
 * response:
 * 400 { error: 'invalid_request', error_description: 'Missing parameter: `scope`' }
 * 400 { error: 'invalid_request', error_description: 'incorrect scope' }
 * 200 { rules: [..., ..., ...]}
 */
module.exports.getByScope = async function (req, res) {
	let scope = req.body && req.body.scope ? req.body.scope : null
	if (_.isEmpty(scope))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Missing parameter: `scope`' })
	let scopeArray = scope.split(' ');

	let rules = await RulesModel.getWithView()

	let result = []
	rules.map((i) => scopeArray.map((name) => {
		if (i.name === name)
			result.push(i.content)
	}))
	if (result.length === 0) {
		return res.status(200).json({ error: 'invalid_request', error_description: 'incorrect scope' })
	} else {
		return res.status(200).json({ rules: result })
	}

};