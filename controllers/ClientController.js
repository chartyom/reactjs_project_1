var clientsModel = require('../models/clientsModel'),
  _ = require('lodash')

/**
 * method: GET
 * uri: /client/:id
 * params: id
 * headers: Authenticate => Bearer ..., 
 * body: 
 * query:
 * 
 * response:
 * 404 { error: 'not_found', error_description: 'Client is not found' }
 * 200 { name: '...' }
 */
module.exports.get = async function (req, res) {
  const id = req.params.id
  const client = await clientsModel.getOneById(id)
  if (!client) {
    return res.status(404).json({ error: 'not_found', error_description: 'Client is not found' })
  }
  return res.status(200).json({ name: client.name })
};