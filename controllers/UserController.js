const redis = require('../db/redis'),
	refreshTokensModel = require('../models/RefreshTokensModel'),
	_ = require('lodash'),
	UsersModel = require('../models/UsersModel'),
	UsersBonusModel = require('../models/UsersBonusModel'),
	CacheModel = require('../models/CacheModel'),
	validator = require('validator'),
	uuid = require('uuid'),
	Tools = require('../utils/Tools'),
	SMSModel = require('../models/SMSModel'),
	bluebird = require('bluebird'),
	bcrypt = bluebird.promisifyAll(require('bcryptjs'))


// /**
//  * method: POST
//  * uri: /token/revoke_access_token
//  * params:
//  * headers: Authenticate => Bearer ..., 
//  * body:  
//  * query:
//  */
// module.exports.revokeAccessToken = async function (req, res) {
// 	return await redis.del(token)
// };

/**
 * method: POST
 * uri: /token/revoke
 * params:
 * headers: Authenticate => Bearer ..., 
 * body:  refreshToken, accessToken
 * query:
 */
module.exports.revokeToken = async function (req, res) {

	if (!req.body) req.body = {}
	if (_.isEmpty(req.body.refreshToken)) await refreshTokensModel.removeOneByToken(req.body.refreshToken)
	if (_.isEmpty(req.body.accessToken)) await redis.delAsync(req.body.accessToken)

	return res.status(200).json({ success: true })
};

/**
 * method: POST
 * uri: /check/phone
 * params:
 * headers: 
 * body:  phone
 * query:
 * 
 */
module.exports.checkPhone = async function (req, res) {
	let phone = req.body && req.body.phone ? req.body.phone : null

	if (_.isEmpty(phone))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Missing parameter: `phone`' })

	let PNF = require('google-libphonenumber').PhoneNumberFormat
	let phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance()
	phone = phoneUtil.format(phoneUtil.parse(phone, 'RU'), PNF.E164)
	let user = await UsersModel.getOneByPhone(phone).catch((err) => {
		return res.status(503).json({ error: 'server_error', error_description: 'Service Temporarily Unavailable' })
	})

	if (_.isEmpty(user))
		return res.status(200).json({ phoneExist: false })

	res.status(200).json({ phoneExist: true })
};

/**
 * method: POST
 * uri: /check/email
 * params:
 * headers: 
 * body:  email
 * query:
 */
module.exports.checkEmail = async function (req, res) {
	let email = req.body && req.body.email ? req.body.email : null

	if (_.isEmpty(email))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Missing parameter: `email`' })

	if (!validator.isEmail(email))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Incorrect parameter: `email`' })

	let user = await UsersModel.getOneByEmail(email).catch((err) => {
		return res.status(503).json({ error: 'server_error', error_description: 'Service Temporarily Unavailable' })
	})

	if (_.isEmpty(user))
		return res.status(200).json({ emailExist: false })

	return res.status(200).json({ emailExist: true })

};

/**
 * method: POST
 * uri: /check/referrerCode
 * params:
 * headers: 
 * body:  code
 * query:
 */
module.exports.checkReferrerCode = async function (req, res) {
	let code = req.body && req.body.code ? req.body.code : null

	if (_.isEmpty(code))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Missing parameter: `code`' })

	let user = await UsersModel.getOneByRefCode(code).catch((err) => {
		return res.status(503).json({ error: 'server_error', error_description: 'Service Temporarily Unavailable' })
	})

	if (_.isEmpty(user))
		return res.status(200).json({ referrerExist: false })

	return res.status(200).json({ referrerExist: true, firstName: user.firstName, lastName: user.lastName })

};


/**
 * method: POST
 * uri: /authenticate/sendSMS
 * params:
 * headers: 
 * body:  phone
 * query:
 * 
 * 
 */
module.exports.authenticateBegin = async function (req, res) {
	let phone = req.body && req.body.phone ? req.body.phone : null

	if (_.isEmpty(phone))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Incorrect parameters' })

	//Check Phone
	let PNF = require('google-libphonenumber').PhoneNumberFormat
	let phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance()
	phone = phoneUtil.format(phoneUtil.parse(phone, 'RU'), PNF.E164)
	let phoneCheck = await UsersModel.getOneByPhone(phone).catch((err) => {
		return res.status(503).json({ error: 'server_error', error_description: 'Service Temporarily Unavailable' })
	})

	if (_.isEmpty(phoneCheck))
		return res.status(400).json({ error: 'phone_exist', error_description: 'A user with this phone number not exists' })

	const { code, hash } = await Tools.generateCode() // Генерируем новый код
	await CacheModel.set(phone, {
		code: hash,
		_id: phoneCheck._id, //need on regenerateCode
		phone: phone		//need on regenerateCode
	})

	await SMSModel.send(phone, { code }, 'code', phoneCheck._id)
		.catch(e => console.error('Send SMS error', e));
	res.status(200).json({ success: true })
};



/**
 * method: POST
 * uri: /authenticate
 * params:
 * headers: 
 * body:  phone, code
 * query:
 * 
 */
module.exports.authenticateEnd = async function (req, res) {
	let code = req.body && req.body.code ? req.body.code : null
	let phone = req.body && req.body.phone ? req.body.phone : null

	if (_.isEmpty(phone))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Missing parameter: `phone`' })

	if (_.isEmpty(code))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Missing parameter: `code`' })

	//Check Phone
	// let PNF = require('google-libphonenumber').PhoneNumberFormat
	// let phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance()
	// phone = phoneUtil.format(phoneUtil.parse(phone, 'RU'), PNF.E164)

	// if (_.isEmpty(code))
	// 	return res.status(400).json({ error: 'invalid_request', error_description: 'Missing parameter: `code`' })

	// const userFromCache = await CacheModel.get(phone)

	// if (_.isEmpty(userFromCache))
	// 	return res.status(400).json({ error: 'key_not_found', error_description: 'Key not found in the cache' })

	// const isMatch = await bcrypt.compareAsync(code, userFromCache.code)

	// if (!isMatch)
	// 	return res.status(400).json({ error: 'invalid_code', error_description: 'Invalid code' })

	//Аутентификация пользователя, выдача access, refresh tokens 
	var userAuth = await UsersModel.authorize(phone, code).catch((err) => {
		return res.status(503).json({ error: 'server_error', error_description: 'Service Temporarily Unavailable' })
	})

	if (_.isEmpty(userAuth) || _.isEmpty(userAuth.access_token))
		return res.status(400).json({ error: 'invalid_user_auth', error_description: 'Invalid user' })

	let result = userAuth
	result.success = true

	console.log('=> result', result)

	return res.status(200).json(result)
};





/**
 * method: POST
 * uri: /registration/sendSMS
 * params:
 * headers: 
 * body:  phone, code, firstName, lastName, email
 * query:
 * 
 * 
 */
module.exports.registartionBegin = async function (req, res) {
	let phone = req.body && req.body.phone ? req.body.phone : null
	let refCode = req.body && req.body.code ? req.body.code : ''
	let firstName = req.body && req.body.firstName ? req.body.firstName : null
	let lastName = req.body && req.body.lastName ? req.body.lastName : null
	let email = req.body && req.body.email ? req.body.email : ''

	if (_.isEmpty(phone) || _.isEmpty(firstName) || firstName.length < 2 || _.isEmpty(lastName) || lastName.length < 2)
		return res.status(400).json({ error: 'invalid_request', error_description: 'Incorrect parameters' })

	//Check Phone
	let PNF = require('google-libphonenumber').PhoneNumberFormat
	let phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance()
	phone = phoneUtil.format(phoneUtil.parse(phone, 'RU'), PNF.E164)
	let phoneCheck = await UsersModel.getOneByPhone(phone).catch((err) => {
		return res.status(503).json({ error: 'server_error', error_description: 'Service Temporarily Unavailable' })
	})

	if (!_.isEmpty(phoneCheck))
		return res.status(400).json({ error: 'phone_exist', error_description: 'A user with this phone number exists' })

	//Check Email
	if (validator.isEmail(email)) {
		let emailCheck = await UsersModel.getOneByEmail(email).catch((err) => {
			return res.status(503).json({ error: 'server_error', error_description: 'Service Temporarily Unavailable' })
		})

		if (!_.isEmpty(emailCheck))
			return res.status(400).json({ error: 'email_exist', error_description: 'A user with this email address exists' })
	} else {
		email = null
	}

	const { code, hash } = await Tools.generateCode() // Генерируем новый код
	let key = uuid.v1()
	await CacheModel.set(key, {
		firstName,
		lastName,
		phone,
		refCode,
		email,
		code: hash
	})

	await SMSModel.send(phone, { code }, 'code', 'registration')
	res.status(200).json({ success: true, key: key })
};



/**
 * method: POST
 * uri: /registration
 * params:
 * headers: 
 * body:  key, code
 * query:
 * 
 */
module.exports.registartionEnd = async function (req, res) {
	let key = req.body && req.body.key ? req.body.key : null
	let code = req.body && req.body.code ? req.body.code : null

	if (_.isEmpty(key))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Missing parameter: `key`' })

	if (_.isEmpty(code))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Missing parameter: `code`' })

	const userFromCache = await CacheModel.get(key)

	if (_.isEmpty(userFromCache))
		return res.status(400).json({ error: 'key_not_found', error_description: 'Key not found in the cache' })

	const isMatch = await bcrypt.compareAsync(code, userFromCache.code)

	if (!isMatch)
		return res.status(400).json({ error: 'invalid_code', error_description: 'Invalid code' })

	await CacheModel.del(key)

	console.log('=> userFromCache', userFromCache)

	let invitedBy = '', referrer = {}

	if (!_.isEmpty(userFromCache.refCode)) {
		let referrer = await UsersModel.getOneByRefCode(userFromCache.refCode)
		console.log('=> referrer', referrer)
		if (referrer && referrer._id) {
			invitedBy = referrer._id
		}
	}

	// кеш для получения access, refresh tokens
	await CacheModel.set(userFromCache.phone, userFromCache)

	let user = userFromCache
	user.verified = true
	user.invitedBy = invitedBy
	user.refCode = await Tools.generateReferrerCode()

	console.log('=> user', user)

	user = await UsersModel.create(user).catch((err) => {
		return res.status(503).json({ error: 'server_error', error_description: 'Service Temporarily Unavailable' })
	})

	console.log('=> user serialize', user)

	if (referrer && referrer._id) {
		await UsersBonusModel.create(referrer._id, user._id).catch((err) => {
			return res.status(503).json({ error: 'server_error', error_description: 'Service Temporarily Unavailable' })
		})

		console.log('=> create bonus for referrer')
	}


	//Аутентификация пользователя, выдача access, refresh tokens 
	var userAuth = await UsersModel.authorize(userFromCache.phone, code).catch((err) => {
		return res.status(503).json({ error: 'server_error', error_description: 'Service Temporarily Unavailable' })
	})

	if (_.isEmpty(userAuth) || _.isEmpty(userAuth.access_token))
		return res.status(400).json({ error: 'invalid_user_auth', error_description: 'Invalid user' })

	let result = userAuth
	result.success = true

	console.log('=> result', result)

	return res.status(200).json(result)
};


/**
 * method: POST
 * uri: /regenerateCode
 * params:
 * headers: 
 * body:  key || phone
 * query:
 * 
 * response: 
 * 	400 { error: 'invalid_request', error_description: 'Missing parameter: `key`' }
 * 	400 { error: 'key_not_found', error_description: 'Key not found in the cache' }
 * 	400 { error: 'limit_exceeded', error_description: 'Code retry limit exceeded, maximum 3' }
 *  200 { success: true }
 */
module.exports.regenerateCode = async function (req, res) {
	let key = req.body && req.body.key ? req.body.key : null
	let phone = req.body && req.body.phone ? req.body.phone : null

	if (_.isEmpty(key) || _.isEmpty(phone))
		return res.status(400).json({ error: 'invalid_request', error_description: 'Missing parameter: `key`' })

	let id = phone;
	if (!_.isEmpty(key)) {
		id = key
	}

	let userFromCache = await CacheModel.get(id) // Ищем текущего пользователя в кэше

	if (_.isEmpty(userFromCache))
		return res.status(400).json({ error: 'key_not_found', error_description: 'Key not found in the cache' })

	if (userFromCache.tryCount && userFromCache.tryCount >= 3)  // Если нашли в кэше и количество попыток >= 3
		return res.status(400).json({ error: 'limit_exceeded', error_description: 'Code retry limit exceeded, maximum 3' })


	let { code, hash } = await Tools.generateCode() // Генерируем код
	await CacheModel.del(id)
	userFromCache.code = hash
	userFromCache.tryCount++
	await CacheModel.set(id, userFromCache)
	await SMSModel.send(userFromCache.phone, { code }, 'code', userFromCache._id)

	return res.status(200).json({ success: true })
};