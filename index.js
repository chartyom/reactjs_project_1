global.ENVIRONMENT = process.env.NODE_ENV || 'development';
global.DEVELOPMENT = process.env.NODE_ENV === 'development' ? true : false;

const express = require('express');
const oauthServer = require('./own_modules/oauth2-server');
const config = require('./config');
const routes = require('./routes');
const helmet = require('helmet');
//const MongoDBClient = require('./db/mongo');
const hbs = require('hbs')
const path = require('path')
require('isomorphic-fetch')

const app = express();

app.use(helmet({
    frameguard: {
        action: 'deny'
    },
    hsts: false,
    ieNoOpen: false
}));

app.oauth = new oauthServer({
    model: require('./models/OAuthModel'),
    authorizationCodeLifetime: config.settings.oauth.authorizationCodeLifetime,
    accessTokenLifetime: config.settings.oauth.accessTokenLifetime,
    refreshTokenLifetime: config.settings.oauth.refreshTokenLifetime,
    addAuthorizedScopesHeader: true,
    addAcceptedScopesHeader: true,
    allowEmptyState: true, //Произвольная строка, которая будет возвращена вместе с результатом авторизации.
    requireClientAuthentication: { password: false, refresh_token: false },
    scopeDefault: 'user_profile',
    uriDefault: config.server.url + '/blank.html'
});

//Add view engine
app.set('view engine', 'hbs');
app.set('views', path.join(process.cwd(), 'views'));

//Public files
app.use(express.static('public/'));

routes(app)

app.use(function (req, res, next) {
    console.error('Not found URL: ' + req.url);
    res.status(404).json({ statusCode: 404, error: 'Not found' });
});

app.use(function (err, req, res, next) {
    console.error('Server error: ', err);
    res.status(err.code).json({
        statusCode: err.code,
        error: err.error,
        error_description: err.error_description
    });
});

// Start listening for requests.
const port = config.server.port;
const host = config.server.host;
const url = config.server.url;
app.listen(port, (err) => {
    if (err) return console.error(err);
    console.info(`⚡ Server running on http://${host}:${port}/ ⚡ environment:`, ENVIRONMENT, `siteUrl: ${url}`);
});