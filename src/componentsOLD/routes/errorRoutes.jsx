import React from 'react';
import { Route } from 'react-router';
import Application from '../Application';
import MainTheme from '../themes/MainTheme';
import ErrorPageView from '../widgets/ErrorPageView/ErrorPageView';

export default (
    <Route component={Application}>
        <Route component={MainTheme}>
            <Route path="*" component={ErrorPageView} />
        </Route>
    </Route>
);

