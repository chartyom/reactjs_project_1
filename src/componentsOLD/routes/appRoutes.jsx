import React from 'react';
import { Route } from 'react-router';
import Application from '../Application';
import MainTheme from '../themes/MainTheme';
import OnlyHeaderTheme from '../themes/OnlyHeaderTheme';
import MainPage, { LINK_MASK as MAIN_LINK_MASK } from '../pages/main/MainPage';
import SelectionPage, { LINK_MASK as SELECTION_LINK_MASK } from '../pages/selection/SelectionPage';
import SelectionsPage, { LINK_MASK as SELECTIONS_LINK_MASK } from '../pages/selections/SelectionsPage';
import SearchPage, { LINK_MASK as SEARCH_LINK_MASK } from '../pages/search/SearchPage';
import EstablishmentsPage, { LINK_MASK as ESTABLISMENTS_LINK_MASK } from '../pages/establishments/EstablishmentsPage';
import EstablishmentPage, { LINK_MASK as ESTABLISMENT_LINK_MASK } from '../pages/establishment/EstablishmentPage';
import MapPage, { LINK_MASK as MAP_LINK_MASK } from '../pages/map/MapPage';
/*import AboutPage, { LINK_MASK as ABOUT_LINK_MASK } from '../pages/about/AboutPage';*/
import ContactsPage, { LINK_MASK as CONTACTS_LINK_MASK } from '../pages/contacts/ContactsPage';

export default (
    <Route component={Application}>
        <Route component={OnlyHeaderTheme}>
            <Route path={MAP_LINK_MASK} component={MapPage} />
        </Route>
        <Route component={MainTheme}>
            <Route path={MAIN_LINK_MASK} component={MainPage} />
            <Route path={SELECTION_LINK_MASK} component={SelectionPage} />
            <Route path={SELECTIONS_LINK_MASK} component={SelectionsPage} />
            <Route path={SEARCH_LINK_MASK} component={SearchPage} />
            <Route path={ESTABLISMENTS_LINK_MASK} component={EstablishmentsPage} />
            <Route path={ESTABLISMENT_LINK_MASK} component={EstablishmentPage} />
            {/*<Route path={ABOUT_LINK_MASK} component={AboutPage} />*/}
            <Route path={CONTACTS_LINK_MASK} component={ContactsPage} />
        </Route>
    </Route>
);

