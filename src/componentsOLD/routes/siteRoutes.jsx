import React from 'react';
import { Route } from 'react-router';
import Application from '../Application';
import MainTheme from '../themes/MainTheme';
import AgreementPage, { LINK_MASK as AGREEMENT_LINK_MASK } from '../pages/agreement/AgreementPage';

export default (
    <Route component={Application}>
        <Route component={MainTheme}>
            <Route path={AGREEMENT_LINK_MASK} component={AgreementPage} />
        </Route>
    </Route>
);

