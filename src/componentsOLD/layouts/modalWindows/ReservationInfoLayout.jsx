import React from 'react';

export default ({ className, place, date, time, people, budget, format, paramsBasis, paramsAlcohol }) => {
    var budgetView, formatView, paramsView;
    if (budget) {
        budgetView = (
            <div className={className + "__info-item " + className + "__info-item_second"}>
                <div className={className + "__info-item__title"}>
                    Бюджет на человека
                </div>
                <div className={className + "__info-item__description"}>
                    {budget}
                </div>
            </div>
        );
        formatView = (
            <div className={className + "__info-item " + className + "__info-item_second"}>
                <div className={className + "__info-item__title"}>
                    Формат мероприятия
                </div>
                <div className={className + "__info-item__description"}>
                    {format}
                </div>
            </div>
        );
        paramsView = (
            <div className={className + "__info-item " + className + "__info-item_second"}>
                <div className={className + "__info-item__title"}>
                    Параметры
                </div>
                <div className={className + "__info-item__description"}>
                    <p>Праздник под ключ: {paramsBasis ? 'Да' : 'Нет'}</p>
                    <p>Свой алкоголь: {paramsAlcohol ? 'Да' : 'Нет'}</p>
                </div>
            </div>
        );
    }
    return (
        <div className={className + "__info-list"}>
            <div className={className + "__info-item"}>
                <div className={className + "__info-item__title"}>
                    Заведение
                </div>
                <div className={className + "__info-item__description"}>
                    {place}
                </div>
            </div>
            <div className={className + "__info-item"}>
                <div className={className + "__info-item__title"}>
                    Дата
                </div>
                <div className={className + "__info-item__description"}>
                    {date}
                </div>
            </div>
            <div className={className + "__info-item"}>
                <div className={className + "__info-item__title"}>
                    Время
                </div>
                <div className={className + "__info-item__description"}>
                    {time}
                </div>
            </div>
            <div className={className + "__info-item"}>
                <div className={className + "__info-item__title"}>
                    Персон
                </div>
                <div className={className + "__info-item__description"}>
                    {people}
                </div>
            </div>
            <div className={className + "__info-separator"}></div>
            {budgetView}
            {formatView}
            {paramsView}
        </div>
    );
};