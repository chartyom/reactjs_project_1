import React from 'react';


/**
 * 
 * @param {string} classes общий стилевой класс компонента
 * @param {array} scheduleTimeList выводимый список времени
 * @param {string} reservationTime выбранное время, записанное в redux
 * @param {func} handleSetTime функция обработки после нажатия на кнопку со временем
 */
export default ({classes, scheduleTimeList, reservationTime, handleSetTime }) => (
    <div className={classes}>
        {
            scheduleTimeList.map((time, index) => {
                let classes = '';
                if (reservationTime == time.time)
                    classes = ' time-point--active';


                var sC = 'green';
                switch (time.croud) {
                    case 'Загружено':
                        sC = 'orange';
                        break;
                    case 'Битком':
                        sC = 'red';
                        break;
                }

                if (time.bonus && (time.bonus.rBonusValue || time.bonus.rBonusPercent)) {
                    var bonusType = time.bonus.rBonusValue ? 'value' : 'percent';
                    var bonus = time.bonus.rBonusValue ? time.bonus.rBonusValue : time.bonus.rBonusPercent;
                    var bonusTypeValue = bonusType === 'percent' ? ' %' : '';
                    return (
                        <div key={index} onClick={() => handleSetTime(time.time, bonus, bonusType)} className={"time-point  time-point_" + sC + " time-point_with-bonus fl fl-jc-c fl-ai-c" + classes}>
                            <p className="time-point__time">{time.time}</p>
                            <p className="time-point__bonus-note fl fl-ai-c fl-jc-c">{bonus}{bonusTypeValue}</p>
                        </div>
                    );
                } 

                return (
                    <div key={index} onClick={() => handleSetTime(time.time)} className={"time-point  time-point_" + sC + " fl fl-jc-c fl-ai-c" + classes}>
                        <p className="time-point__time">{time.time}</p>
                    </div>
                );
            })
        }
    </div>
);