import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
/**
 * 
 * @param {function} handleButton обработчик кнопки "обновить"
 * @param {string} titleButton текст кнопки 
 * 
 * <div className="badConnection__main__action" >
 *  <button className="badConnection__main__action__button" onClick={()=>location.reload()}>Обновить</button>
 * </div>
 * 
 */
class BadConnection extends PureComponent {
  // renderUpdateUrl() {
  //   let link = "/";
  //   if (
  //     typeof window !== 'undefined' &&
  //     window.document
  //   )
  //     link = window.location.pathname;

  //   return link;
  // }

  renderAction() {
    if (this.props.handleButton) {
      return (
        <div className="error-notify__main__action" >
          <button className="error-notify__main__action__button" onClick={() => this.props.handleButton()}>{this.props.titleButton || 'Обновить'}</button>
        </div>
      );
    }
  }

  render() {
    return (
      <div className={"badConnection error-notify--grey"}>
        <div className="error-notify__main" >
          <div className="error-notify__main__images" ><i className="fa fa-wifi" aria-hidden="true"></i></div>
          <div className="error-notify__main__description" >Упс! Похоже нет<br /> интернет соединения</div>
          {this.renderAction()}
        </div>
      </div>
    );
  }
};

export default BadConnection;
