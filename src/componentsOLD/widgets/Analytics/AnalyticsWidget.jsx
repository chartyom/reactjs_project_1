import React from 'react';
import { loadScript } from '../../../utils/Tools';

/**
 * Инициализация яндекс метрики
 */
export default function () {
    if (typeof window === 'object' && !DEVELOPMENT) {
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter44452363 = new Ya.Metrika({
                        id: 44452363,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true,
                        trackHash: true
                    });
                } catch (e) { }
            });

            var f = function () {
                loadScript('https://mc.yandex.ru/metrika/watch.js')
                    .catch(e => { if (DEVELOPMENT) console.log(e); });
            };

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
        return (
            <noindex>
                <div>
                    <img src="https://mc.yandex.ru/watch/44452363" style={{ position: 'absolute', left: '-9999px' }} alt="metrica" />
                </div>
            </noindex>
        );
    }
    return <div></div>;

};