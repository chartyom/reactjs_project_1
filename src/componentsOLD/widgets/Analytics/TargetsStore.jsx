/**
 * Кнопка бронирования столика в шапке заведения
 */
export function reservationTablePlaceHeader() {
    if (typeof yaCounter44452363 === 'object')
        yaCounter44452363.reachGoal('reservation_table_place_header');
}

/**
 * Кнопка бронирование столика по кнопке в зоне отзывы
 */
export function reservationTablePlaceBottom() {
    if (typeof yaCounter44452363 === 'object')
        yaCounter44452363.reachGoal('reservation_table_place_bottom');
}

/**
 * Кнопка забронировать во всплывающем окне после ввода номера телефона и имени
 */
export function reservationTableConfirmInfoPopup() {
    if (typeof yaCounter44452363 === 'object')
        yaCounter44452363.reachGoal('reservation_table_confirm_info_popup');
}

/**
 * Событие подтверждения SMS при бронировании столика
 */
export function reservationTableConfirmSMSPopup() {
    if (typeof yaCounter44452363 === 'object')
        yaCounter44452363.reachGoal('reservation_table_confirm_sms_popup');
}

/**
 * Событие завершения бронирования столика
 */
export function reservationTableCompletePopup() {
    if (typeof yaCounter44452363 === 'object')
        yaCounter44452363.reachGoal('reservation_table_complete_popup');
}

/**
 * Кнопка забронировать банкет под шапкой заведения справа
 */
export function reservationBanquetPlaceHeader() {
    if (typeof yaCounter44452363 === 'object')
        yaCounter44452363.reachGoal('reservation_banquet_place_header');
}

/**
 * Кнопка забронировать банкет под шапкой заведения справа
 */
export function reservationBanquetPlaceBottom() {
    if (typeof yaCounter44452363 === 'object')
        yaCounter44452363.reachGoal('reservation_banquet_place_bottom');
}

/**
 * Кнопка "забронировать" банкет в модальном окне
 */
export function reservationBanquetConfirmInfoPopup() {
    if (typeof yaCounter44452363 === 'object')
        yaCounter44452363.reachGoal('reservation_banquet_confirm_info_popup');
}

/**
 * Событие подтверждения SMS при бронировании банкета в заведении
 */
export function reservationBanquetConfirmSMSPopup() {
    if (typeof yaCounter44452363 === 'object')
        yaCounter44452363.reachGoal('reservation_banquet_confirm_sms_popup');
}

/**
 * Событие завершения бронирования банкета
 */
export function reservationBanquetCompletePopup() {
    if (typeof yaCounter44452363 === 'object')
        yaCounter44452363.reachGoal('reservation_banquet_complete_popup');
}
