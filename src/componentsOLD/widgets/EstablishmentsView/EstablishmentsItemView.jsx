import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { LINK_PAGE as ESTABLISHMENT_LINK_PAGE } from '../../pages/establishment/EstablishmentPage';
import LazyLoad from '../../elements/LazyLoad';

const EstablishmentsItemView = ({ establishment }) => {
  var place = establishment;
  var id = place.id,
    image = place.img,
    label = place.label,
    link = ESTABLISHMENT_LINK_PAGE + '/' + place.mapType + '/' + place.name + '/' + place.id,
    rating = place.rating,
    shortDescription = place.shortDesc,
    name = place.name,
    price = place.price,
    priceName = '₽', // TODO нужно исправлять на сервере
    underground = place.address && place.address.metro && place.address.metro.name ? place.address.metro.name : null,
    trend = place.trend,
    isActive = place.isActive, // true || false - Работа Битком24 с заведением
    crowd = place.croud, // Битком || Загружено || Свободно
    currentWork = place.currentWork, // с 12:00 || до 20:00 || Закрыто || 24
    status = place.status,
    statusName = place.statusName;

  /*
    LABEL
  */
  var
    labelView = '';

  if (label && label.color && label.name) {
    var labelColor = label.color,
      labelName = label.name,
      labelType = label.type,
      labelCC = '', //COLOR CLASS
      labelTC = ''; //TYPE CLASS

    if (labelColor == 'greenAlert') {
      labelCC = " label_green";
    } else if (labelColor == 'redAlert') {
      labelCC = " label_red";
    } else if (labelColor == 'yellowAlert') {
      labelCC = " label_yellow";
    }

    if (labelType == 'check') {
      labelTC = " label_type--check";
    } else if (labelType == 'alert') {
      labelTC = " label_type--alert";
    }

    labelView = (
      <p className={"label" + labelCC + labelTC}>
        <span>{labelName}</span>
      </p>
    );
  }


  /*
    STATUS
  */

  var status = currentWork == 'Закрыто' ? currentWork : crowd,
    statusClass = '';
  if (currentWork == 'Закрыто') {
    statusClass = 'status__load-closed';
  } else if (crowd == 'Загружено') {
    statusClass = 'status_load-middle';
  } else if (crowd == 'Битком') {
    statusClass = 'status_load-hight';
  } else if (crowd == 'Свободно') {
    statusClass = 'status_load-low';
  }


  /*
    ADDRESS
  */

  var undergroundView = '';
  if (underground) {
    undergroundView = (
      <p className="metro">{underground}</p>
    );
  }

  /*
    TREND
  */
  var trendClass = '';
  if (trend == "up") {
    trendClass = " place-attributes__rating_up";
  } else if (trend == "down") {
    trendClass = "  place-attributes__rating_down";
  }


  /*
    RENDER
  */

  return (
    <Link to={link} className="place">
      <div className="place__image-block">
        <LazyLoad className="place__thumbnail" dataSrc={image} alt={name} />
        {labelView}
        <p className={"status  " + statusClass}><span>{status}</span></p>
      </div>
      <div className="place-attributes">
        <div className="fl fl-in-di nwr">
          <div>
            <div className="place-attributes__header">{name}</div>
            <p className="place-attributes__subheader">{shortDescription}</p>
          </div>
          <div>
            <p className={"place-attributes__rating" + trendClass}><span>{rating}</span></p>
          </div>
        </div>
        <div className="fl fl-in-di">
          <div>
            <p className="place-attributes__middle-check fl fl-it-cnt"><span>{price} <i className="currency currency_rub currency_regular ">{priceName}</i></span></p>
            {undergroundView}
          </div>
        </div>
      </div>
    </Link>
  );
};

// EstablishmentsItemView.propTypes = {
//   establishment: PropTypes.shape({
//     name: PropTypes.string.isRequired,
//     username: PropTypes.string.isRequired,
//     email: PropTypes.string.isRequired,
//     phone: PropTypes.string.isRequired,
//     website: PropTypes.string.isRequired,
//   }).isRequired,
// };

export default EstablishmentsItemView;