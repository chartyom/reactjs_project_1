import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import EstablishmentsItemView from './EstablishmentsItemView';

const EstablishmentsListView = ({ establishments, begin, amount, classes }) => {

  return (
    <div className={classes || ''}>
      {establishments.map((establishment, index) => {
        if ((amount && (index + 1) > amount) || (begin && (index + 1) < begin)) return false;
        return <EstablishmentsItemView establishment={establishment} key={establishment.id} />;
      })}
    </div>
  );

};

EstablishmentsListView.propTypes = {
  establishments: PropTypes.array.isRequired,
  amount: PropTypes.number,
  classes: PropTypes.string,
  begin: PropTypes.number,
};

export default EstablishmentsListView;