import React from 'react';

export default () => (
  <div className="loader loader--default">
    <div className="loader__main" ></div>
  </div>
);
