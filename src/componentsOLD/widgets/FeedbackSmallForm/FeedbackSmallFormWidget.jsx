import React, { PureComponent } from 'react';
import isEmailValidator from 'validator/lib/isEmail';
import * as uiAction from '../../../actions/uiAction';

class FeedbackSmallFrom extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      usernameValid: null,
      emailValid: null,
      emailValidDescription: null,
      messageValid: null,
      feedbackSend: null,
      feedbackSendStatus: null,
    };

    this.handleCallback = this.handleCallback.bind(this);
  }
   
  /*
    ===== BEGIN SEND FEEDBACK =====
  */
  handleCallback(err){
      if(err) {
        return this.setState({feedbackSend: false, feedbackSendStatus: null});
      }
      this.setState({feedbackSend: true, messageValid: null, usernameValid: null, emailValid: null, feedbackSendStatus: null});
      this.refs.inputMessage.value = '';
      this.refs.inputEmail.value = '';
      this.refs.inputUsername.value = '';
  }


  handleSendFeedbackMessage() {
    this.setState({feedbackSendStatus: 'sending'});
    uiAction.sendFeedbackMessage({
      message: this.refs.inputMessage.value,
      email: this.refs.inputEmail.value,
      username: this.refs.inputUsername.value
    }, this.handleCallback);
  }


  handleMessageValid(e) {
    const v = e.target.value;
    if (v.length > 4 ) {
        if(this.state.messageValid != true)
          return this.setState({ messageValid: true});
        return;
    }
      if(this.state.messageValid === true)
        return this.setState({ messageValid: null});
  }

    handleEmailValid(e) {
        const v = e.target.value;
        if (v.length > 0) {
            if (isEmailValidator(v)) {
                return this.setState({ emailValid: true, emailValidDescription: null});
            }
            if (this.state.emailValid !== false)
                return this.setState({ emailValid: false, emailValidDescription: "Email адрес введен не верно" });
            return;
        }
        if (this.state.emailValid !== null)
            this.setState({ emailValid: null, emailValidDescription: null });
    }

  handleUsernameValid(e) {
    const v = e.target.value;
    if (v.length > 1) {
        if(this.state.usernameValid != true)
          return this.setState({ usernameValid: true});
        return;
    } 
    if(this.state.usernameValid === true)
      return this.setState({ usernameValid: null});
  }

  render(){
    const usernameValidClass = this.state.usernameValid
        ? " has-success"
        : this.state.usernameValid === null
            ? ""
            : " has-error";

    const emailValidClass = this.state.emailValid
        ? " has-success"
        : this.state.emailValid === null
            ? ""
            : " has-error";

    const messageValidClass = this.state.messageValid
        ? " has-success"
        : this.state.messageValid === null
            ? ""
            : " has-error";

    const submitEnable = (
        this.state.usernameValid
        && this.state.emailValid
        && this.state.messageValid
        && this.state.feedbackSendStatus === null
    );

    var submitLoadingClass = this.state.feedbackSendStatus === 'sending' ? ' btn-loading' : '';

    const notice = this.state.feedbackSend === null
    ? ''
    : this.state.feedbackSend 
      ? (
        <div className="alert alert-success">Сообщение отправлено</div>
      )
      : (
        <div className="alert alert-danger">Во время отправки возникла ошибка</div>
      );

    return (
      <div className="contact-blck contact-blck_form">
        <div className="header-container header-container--paragraph">
          <h3 className="header-container__header">Напишите нам</h3>
        </div>
        {notice}
        <div className={"form-group" + usernameValidClass}>
          <label className="control-label" htmlFor="inputUsername">Имя <span className="require" title="Обязательное поле">*</span></label>
          <input type="text" ref="inputUsername" onChange={(e)=>this.handleUsernameValid(e)} className="form-control" id="inputUsername" placeholder="Введите ваше имя" />
        </div>
        <div className={"form-group" + emailValidClass}>
          <label className="control-label" htmlFor="inputEmail">Email <span className="require" title="Обязательное поле">*</span></label>
          <input type="text" ref="inputEmail" onChange={(e)=>this.handleEmailValid(e)} className="form-control" id="inputEmail" placeholder="Введите ваш email" />
          <div className="help-block">{this.state.emailValidDescription}</div>
        </div>
        <div className={"form-group" + messageValidClass}>
          <label className="control-label" htmlFor="inputMessage">Сообщение <span className="require" title="Обязательное поле">*</span></label>
          <textarea ref="inputMessage" onChange={(e)=>this.handleMessageValid(e)} className="form-control contact-blck_form__textarea" id="inputMessage" placeholder="Введите ваше сообщение"></textarea>
        </div>
        <button type="button" disabled={!submitEnable} onClick={()=>this.handleSendFeedbackMessage()} className={"btn btn-success" + submitLoadingClass} >Отправить</button>
      </div>
    );
  }
  /*
    ===== END SEND FEEDBACK =====
  */
};

export default FeedbackSmallFrom;