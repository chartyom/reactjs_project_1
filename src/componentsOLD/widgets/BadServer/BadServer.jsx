import React from 'react';

export default () => {
  /*
    TODO: Критическая ошибка
  */
  return (
    <div className={"badServer error-notify--grey"}>
      <div className="error-notify__main" >
        <div className="error-notify__main__images" ><i className="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
        <div className="error-notify__main__description" >Упс! Наш сервер не отвечает.<br />В кротчайшее время всё заработает!</div>
        {/*<div className="error-notify__main__action" >
          <button className="error-notify__main__action__button" onClick={() => location.reload()}>Обновить</button>
        </div>*/}
      </div>
    </div>
  );
};
