import React, { PureComponent } from 'react';


export default class BannerTop10Widget extends PureComponent {

  render() {

    if (DEVELOPMENT) console.log('BannerTop10Widget: Render');

    return (
      <section className="top-section">
        <div className="center">
          <div className="top-section__content-wrap">
            <p className="top-section__top-note"><span>ТОП-10</span></p>
            <h4 className="top-section__header">Лучшие мясные рестораны Москвы</h4>
            <a href="#" className="abs-link"></a>
          </div>
        </div>
      </section>
    );
  }
};
