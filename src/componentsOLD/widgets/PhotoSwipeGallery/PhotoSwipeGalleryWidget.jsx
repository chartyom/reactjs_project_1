import React, { PureComponent, PropTypes } from 'react';
import { loadScript } from '../../../utils/Tools';
import LoaderView from '../LoaderView';

/**
 * @param {string} className класс блока
 * @param {array} photos список фотографий
 * @param {string} role уникальная визуализация галереи
 * @param {number} count количество выводимых фотографий
 * @param {func} templateItem функция-шаблон отображения элемента списка
 */
class PhotoSwipeGallery extends PureComponent {

    gallaryInit() {
        if (PhotoGallery && PhotoGallery.init)
            PhotoGallery.init('.ps-gallery');
    }

    componentDidMount() {
        this.loadScripts();
    }

    loadScripts() {
        loadScript('/temp/js/PhotoGallery.js').then(() => {
            this.gallaryInit();
        })
            .catch(e => { if (DEVELOPMENT) console.log(e); });
    }

    renderVisual() {

            return (
                <div className="pswp" tabIndex="-1" role={(this.props.role || "dialog")} aria-hidden="true">

                    {/*<!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->*/}
                    <div className="pswp__bg"></div>

                    {/*<!-- Slides wrapper with overflow:hidden. -->*/}
                    <div className="pswp__scroll-wrap">

                        {/*<!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->*/}
                        <div className="pswp__container">
                            <div className="pswp__item"></div>
                            <div className="pswp__item"></div>
                            <div className="pswp__item"></div>
                        </div>

                        {/*<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->*/}
                        <div className="pswp__ui pswp__ui--hidden">

                            <div className="pswp__top-bar">

                                {/*<!--  Controls are self-explanatory. Order can be changed. -->*/}

                                <div className="pswp__counter"></div>

                                <button className="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                <button className="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                {/*<!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->*/}
                                <div className="pswp__preloader">
                                    <LoaderView />
                                </div>

                            </div>

                            <div className="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                <div className="pswp__share-tooltip"></div>
                            </div>

                            <button className="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                            </button>

                            <button className="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                            </button>

                            {/*<div className="pswp__caption">
                                <div className="pswp__caption__center"></div>
                            </div>*/}

                        </div>

                    </div>

                </div>
            );
    }

    renderPhotos() {

        let templateItem = this.props.templateItem ?
            this.props.templateItem :
            (photo) => (
                <figure itemProp="associatedMedia" itemScope itemType="http://schema.org/ImageObject">
                    <a href={photo.img} target="_blank" itemProp="contentUrl">
                        <img src={photo.img} itemProp="thumbnail" alt="Image description" />
                    </a>
                </figure>
            );


        return (
            <div className='ps-gallery' itemScope itemType="http://schema.org/ImageGallery">
                {
                    this.props.photos.map((photo, index) => {
                        if (this.props.count && index >= this.props.count) return false;
                        return templateItem(photo, index);
                    })
                }
            </div>
        );
    }

    render() {
        return (
            <div className={(this.props.className || 'ps')}>
                {this.renderPhotos()}
                {this.renderVisual()}
            </div>
        );
    }
};


PhotoSwipeGallery.propTypes = {
    photos: PropTypes.array.isRequired,
    className: PropTypes.string,
    role: PropTypes.string,
    count: PropTypes.number,
    templateItem: PropTypes.func
};

export default PhotoSwipeGallery;