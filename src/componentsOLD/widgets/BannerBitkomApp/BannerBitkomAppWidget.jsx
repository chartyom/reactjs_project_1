import React, { PureComponent } from 'react';
import LazyLoad from '../../elements/LazyLoad';

export default class BannerBitkomAppWidget extends PureComponent {

  render() {

    if (DEVELOPMENT) console.log('BannerBitkomAppWidget: Render');

    return (
      <section className="bitcom-app fl fl-ai-c">
        <div className="bitcom-app__content-section">
          <div className="bitcom-app__background"></div>
          <div className="center">
            <div className="fl fl-ai-c fl-in-di full-width">
              <div className="fl fl-in-di bitcom-app__content-wrap">
                <div>
                  <h4 className="bitcom-app__header">Приложение Битком24</h4>
                  <p className="bitcom-app__subheader">в вашем смартфоне</p>
                </div>
                <div>
                  <p className="bitcom-app__additional-info"><span>Персонилизированный подбор ресторанов по вкусу и онлайн загруженность заведения.</span></p>
                </div>
                <div>
                  <a className="download-app-link" target="_blank" href="https://redirect.appmetrica.yandex.com/serve/457394471315723481">
                    <LazyLoad dataSrc="/images/appstore.png" alt="Скачать приложение Битком24 в AppStore" />
                  </a>
                  <a className="download-app-link" target="_blank" href="https://redirect.appmetrica.yandex.com/serve/385337361753463199">
                    <LazyLoad dataSrc="/images/playstore.png" alt="Скачать приложение Битком24 в GooglePlay" />
                  </a>
                </div>
              </div>
              <div className="bitcom-app__image-wrap">
                <LazyLoad className="bitcom-app__image" dataSrc="/images/phones.png"  alt="Приложение Битком24" />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
};
