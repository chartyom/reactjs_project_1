import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { getNumEnding, cropText } from '../../../utils/Tools';
import { LINK_PAGE as SELECTION_LINK_PAGE } from '../../pages/selection/SelectionPage';
import LazyLoad from '../../elements/LazyLoad';
/*
  selections object
  amount int
  beginMini int
  classes доп классы (КАСТЫЛЬ ВЕРСТАЛЬЩИКА ОТРАЗИЛСЯ НА КОМПОНЕНТЕ)
*/

export default ({ selections, amount, beginMini, classes }) => (
  <div className={(classes || '')} >
    {selections.map(function (selection, index) {

      let _index = index + 1;

      if (amount && _index > amount) {
        return false;
      }

      if (beginMini && (_index == beginMini || _index == (beginMini + 1))) {

        if (_index == (beginMini + 1)) return false;

        const selectionsMini = [selections[beginMini - 1], selections[beginMini]];
        return (
          <div key={selection.id} className="mini-selection-wrapper without-flex-spacing__item selection__item fl fl-in-di">
            {selectionsMini.map(function (selectionMini) {
              let textRightEnding = getNumEnding(selectionMini.length, ['заведение', 'заведения', 'заведений']);
              let name = cropText(selectionMini.name, 40);
              const TYPE = selection.type ? cropText(selection.type, 16) : 'Выбор Битком';
              return (
                <div key={selectionMini.id} className="selection selection_mini fl fl-in-di" title={selectionMini.name}>
                  <LazyLoad className="selection__thumbnail" dataSrc={selectionMini.img} alt={selectionMini.name} />
                  <div className="selection__choice-like fl fl-in-di">
                    <p className="choice">{TYPE}</p>
                  </div>
                  <div className="selection__header">
                    <h4>{name}</h4>
                  </div>
                  <div className="quantity">
                    <p>{selectionMini.length} {textRightEnding}</p>
                  </div>
                  <Link to={SELECTION_LINK_PAGE + '/' + selectionMini.id} className="abs-link"></Link>
                </div>
              );
            })}
          </div>
        );


      }
      let textRightEnding = getNumEnding(selection.length, ['заведение', 'заведения', 'заведений']);
      let name = cropText(selection.name, 40);
      const TYPE = selection.type ? cropText(selection.type, 16) : 'Выбор Битком';
      return (
        <div key={selection.id} className="selection without-flex-spacing__item selection__item" title={selection.name}>
          <LazyLoad className="selection__thumbnail" dataSrc={selection.img} alt={selection.name} />
          <div className="fl fl-in-di fl-dr-col nwr full-width full-height">
            <div className="selection__choice-like fl fl-in-di">
              <p className="choice">{TYPE}</p>
            </div>
            <div className="selection__header">
              <h4>{name}</h4>
            </div>
            <div className="quantity">
              <p>{selection.length} {textRightEnding}</p>
            </div>
          </div>
          <Link to={SELECTION_LINK_PAGE + '/' + selection.id} className="abs-link"></Link>
        </div>
      );
    })}
  </div>
);
