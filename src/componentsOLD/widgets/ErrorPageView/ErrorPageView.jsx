import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import { LINK_PAGE as MAIN_LINK_PAGE } from '../../pages/main/MainPage';

export default class ErrorPageView extends PureComponent {
    render() {
        return (
            <div className='ErrorPageView error-notify--grey'>
                <div className="error-notify__main" >
                    <div className="error-notify__main__images" ><i className="fa fa-exclamation-circle" aria-hidden="true"></i></div>
                    <div className="error-notify__main__description" >Упс! Страница не найдена.<br />Возможно эта страница была удалена или никогда не создавалась!</div>
                    <div className="error-notify__main__action" >
                        <Link to={MAIN_LINK_PAGE} className="error-notify__main__action__button">Вернуться на главную</Link>
                    </div>
                </div>
            </div>
        );
    }
};