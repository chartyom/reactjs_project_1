import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import { LINK_PAGE as MAIN_LINK_PAGE } from '../../pages/main/MainPage';

export default class inDevPageView extends PureComponent {
    render() {
        return (
            <div className={'container error-page error-page--' + (this.props.theme || 'default')}>

                <div className="error-page__elem col-xs-12">
                    <h1>Страница в разработке</h1>
                    <h2>Эта страница находится на стадии разработки</h2>
                    <p><Link to={MAIN_LINK_PAGE} className="btn btn-default">Вернуться на главную</Link></p>
                </div>

            </div>
        );
    }
};