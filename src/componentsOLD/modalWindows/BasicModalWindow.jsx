import React, { PureComponent, PropTypes } from 'react';
import { Link } from 'react-router';

/**
 * BasicModalWindow
 * use checkbox and label combination
 * <BasicModalWindow
		ref="modal"
		title="Отзывы"
		actionId="revies-popup-launcher"
		onCancel={() => this.closeModal()}>
		<div>Content</div>
	</BasicModalWindow>
 * 
 * @param {string} ref require
 * @param {string} actionId require
 * @param {string} title 
 * @param {string} cancel button value
 * @param {string} confirm button value
 * @param {function} onCancel 
 * @param {function} onConfirm 
 * 
 */
class BasicModalWindow extends PureComponent {

	open() {
		$('body').addClass('fixed');
	}
	close() {
		$('body').removeClass('fixed');
	}
	handleCancel() {
		if (this.props.onCancel) {
			this.props.onCancel();
		} else {
			this.close();
		}
	}
	handleConfirm() {
		if (this.props.onConfirm) {
			this.props.onConfirm();
		} else {
			this.close();
		}
	}

	render() {
		var footerElement = null;
		if (this.props.confirm || this.props.cancel) {
			var confirmButton = null;
			var cancelButton = null;
			if (this.props.confirm) {
				confirmButton = (
					<label onClick={() => this.handleConfirm()} htmlFor={this.props.actionId} >
						{this.props.confirm}
					</label>
				);
			}
			if (this.props.cancel) {
				cancelButton = (
					<label onClick={() => this.handleCancel()} htmlFor={this.props.actionId} >
						{this.props.cancel}
					</label>
				);
			}
			footerElement = (
				<div className="full-reviews__footer">
					{cancelButton}
					{confirmButton}
				</div>
			);
		}
		return (
			<div className="dn full-reviews">
				<div className="full-reviews__inner-wrap">
					<label onClick={() => this.handleCancel()} htmlFor={this.props.actionId} className="abs-link"></label>
					<div className="full-reviews__inner">
						<div className="full-reviews__head fl fl-in-di">
							<h5 className="full-reviews__header">{this.props.title}</h5>
							<svg className="dn" width="21px" height="21px" viewBox="943 285 21 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
								<path id="modal-close" d="M954.202177,295.5 L963.648911,286.053266 L964,285.702177 L963.297823,285 L962.946734,285.351089 L953.5,294.797823 L944.053266,285.351089 L943.702177,285 L943,285.702177 L943.351089,286.053266 L952.797823,295.5 L943.351089,304.946734 L943,305.297823 L943.702177,306 L944.053266,305.648911 L953.5,296.202177 L962.946734,305.648911 L963.297823,306 L964,305.297823 L963.648911,304.946734 L954.202177,295.5 Z" id="modal-close" stroke="none" fill="#000000" fillRule="evenodd"></path>
							</svg>
							<label onClick={() => this.handleCancel()} className="revies-popup-close" htmlFor={this.props.actionId}>
								<svg width="21px" height="21px" viewBox="943 285 21 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
									<use xlinkHref="#modal-close"></use>
								</svg>
							</label>
						</div>
						<div className="full-reviews__body">
							{this.props.children}
						</div>
						{footerElement}
					</div>
				</div>
			</div>
		);
	}
};

export default BasicModalWindow;