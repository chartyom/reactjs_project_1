import React, { PureComponent } from 'react';
import { loadScript } from '../../utils/Tools';
/**
 * Модальное окно
 * @param {string} ref 
 * @param {string} modalId id form
 * @param {string} confirm 
 * @param {string} cancel 
 * @param {string} title 
 * @param {string} headerClass (modal-header--no-border || ...)
 * @param {string} bodyClass (modal-body--no-padding || modal-body--no-padding-top  || modal-body--no-padding-left || modal-body--no-padding-right || modal-body--no-padding-bottom)
 * @param {string} sizeClass (modal-lg || modal-sm || modal-size--md-sm)
 * @param {function} onCancel 
 * @param {function} onConfirm
 * @param {function} onHidden
 * @param {function} onShown
 */
export default class BootstrapModalWindow extends PureComponent {
  // The following two methods are the only places we need to
  // integrate Bootstrap or jQuery with the components lifecycle methods.
  componentDidMount() {
    loadScript('/js/jquery.slim.min.js')
      .then(() => {
        loadScript('/temp/js/bootstrap.js')
          .then(() => this.initDidMount())
          .catch(e => { if (DEVELOPMENT) console.log(e); });
      })
      .catch(e => { if (DEVELOPMENT) console.log(e); });
  }

  initDidMount() {
    // When the component is added, turn it into a modal
    $(this.refs.root).modal({ keyboard: true, show: false });
    // Bootstrap's modal class exposes a few events for hooking into modal
    // functionality. Lets hook into one of them:
    $(this.refs.root).on('hidden.bs.modal', () => this.handleHidden());
    $(this.refs.root).on('shown.bs.modal', () => this.handleShown());
  }
  componentWillUnmount() {
    loadScript('/js/jquery.slim.min.js')
      .then(() => this.initWillUnmount())
      .catch(e => { if (DEVELOPMENT) console.log(e); });
  }
  initWillUnmount() {
    $(this.refs.root).off('hidden.bs.modal', () => this.handleHidden());
    $(this.refs.root).off('shown.bs.modal', () => this.handleShown());
  }



  close() {
    $(this.refs.root).modal('hide');
  }
  open() {
    $(this.refs.root).modal('show');
  }

  renderHeader() {

    const buttonClose = (
      <button
        type="button"
        className="modal-close"
        onClick={() => this.handleCancel()}>
        <svg width="21px" height="21px" viewBox="943 285 21 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
          <use xlinkHref="#modal-close"></use>
        </svg>
      </button>
    );

    if (this.props.title) {
      return (
        <div className={"modal-header " + (this.props.headerClass || '')}>
          {buttonClose}
          <h3 className="modal-title">{this.props.title}</h3>
        </div>
      );
    }
    return buttonClose;
  }

  renderFooter() {
    if (this.props.confirm || this.props.cancel) {
      var confirmButton = null;
      var cancelButton = null;
      if (this.props.confirm) {
        confirmButton = (
          <button onClick={() => this.handleConfirm()} >
            {this.props.confirm}
          </button>
        );
      }
      if (this.props.cancel) {
        cancelButton = (
          <button onClick={() => this.handleCancel()}  >
            {this.props.cancel}
          </button>
        );
      }
      return (
        <div className="full-reviews__footer">
          {cancelButton}
          {confirmButton}
        </div>
      );
    }
  }

  render() {
    return (
      <div id={this.props.modalId || "bs-modal"} className="modal fade" ref="root">
        <div className={"modal-dialog " + (this.props.sizeClass || '')}>
          <div className="modal-content">
            {this.renderHeader()}
            <div className={"modal-body " + (this.props.bodyClass || '')}>
              {this.props.children}
            </div>
            {this.renderFooter()}
          </div>
        </div>
        <svg className="dn" width="21px" height="21px" viewBox="943 285 21 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
          <path id="modal-close" d="M954.202177,295.5 L963.648911,286.053266 L964,285.702177 L963.297823,285 L962.946734,285.351089 L953.5,294.797823 L944.053266,285.351089 L943.702177,285 L943,285.702177 L943.351089,286.053266 L952.797823,295.5 L943.351089,304.946734 L943,305.297823 L943.702177,306 L944.053266,305.648911 L953.5,296.202177 L962.946734,305.648911 L963.297823,306 L964,305.297823 L963.648911,304.946734 L954.202177,295.5 Z" id="modal-close" stroke="none" fill="#000000" fillRule="evenodd"></path>
        </svg>
      </div>
    );
  }
  handleCancel() {
    if (this.props.onCancel) {
      this.props.onCancel();
    } else {
      this.close();
    }
  }
  handleConfirm() {
    if (this.props.onConfirm) {
      this.props.onConfirm();
    }
  }
  handleHidden() {
    if (this.props.onHidden) {
      this.props.onHidden();
    }
  }
  handleShown() {
    if (this.props.onShown) {
      this.props.onShown();
    }
  }
};
