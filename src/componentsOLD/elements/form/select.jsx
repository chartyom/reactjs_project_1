import React from 'react';

/**
 * HTML TAG: select
 * @param {string} className стилевые классы компонента
 * @param {array} list список элементов
 * @param {string} value выбранноый элемент
 * @param {func} handleOnChange обработчик нажатия
 * @param {string} disabledValue в списке будет присутствовать первый элемент со значением disabled
 * @param {string} selectedValue в списке будет присутствовать первый элемент имеющий пустое значение value
 * example: 
 * <SelectElement className={} list={} value={} onChange={} disabledValue={} selectedValue={}/>
 */
export default ({ className, list, value, onChange, disabledValue, selectedValue }) => {
    var d = disabledValue ? <option disabled >{disabledValue}</option> : '';
    var s = selectedValue ? <option value='default' >{selectedValue}</option> : '';
    value = value ? value : 'default';
    return (
        <select className={"form-control" + (className ? ' ' + className : '')} value={value} onChange={(e) => onChange(e)}>
            {d}
            {s}
            {list.map((o, i) => (
                <option key={i} value={o}>{o}</option>
            ))}
        </select>
    );
};