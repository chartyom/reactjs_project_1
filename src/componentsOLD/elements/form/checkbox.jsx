import React from 'react';

/**
 * HTML TAG: input checkbox
 * @param {string} className стилевые классы компонента
 * @param {bool} checked статус
 * @param {string} value текст
 * @param {func} handleOnChange обработчик нажатия
 * 
 * example: 
 * <CheckboxElement className={} checked={} onChange={} />
 */
export default ({ className, checked, value, onChange }) => (
    <div className={"checkbox" + (className ? ' ' + className : '')} >
        <label>
            <input type="checkbox" onChange={(e) => onChange(e)} checked={checked} /> {value || ''}
        </label>
    </div>
);