import React, { Component } from 'react';
import { getNumEnding } from '../../../utils/Tools';
import { Link } from 'react-router';
/**
 * Author: Chernyaev Artyom
 * style: pgnElem
 * @param {number} count количество всех элементов в списке
 * @param {number} amount количество элементов на странице
 * @param {number} thisPageId список используемых элементов
 * @param {object} query query параметры страницы (пагинация изменяет url, для правильного изменения нужна передача query)
 */
class PaginationElement extends Component {

  render() {
    var { count, amount, thisPageId, query} = this.props;
    thisPageId = parseInt(thisPageId) || 1;
    var isShowClass = '';
    var leftDisableClass = '';
    var rightDisableClass = '';
    var prevPageId, nextPageId;

    var e = Math.ceil(count / amount);
    var eView = 5; // количество центральных кнопок, где активная имеет индекс 3
    var eAllView = eView + 4;
    var eAllViewMiddle = Math.ceil(eAllView / 2);

    if (1 == thisPageId) {
      prevPageId = thisPageId;
      leftDisableClass = ' pgnElem__item_disable';
    } else {
      prevPageId = thisPageId - 1;
    }
    if (e == thisPageId) {
      nextPageId = thisPageId;
      rightDisableClass = ' pgnElem__item_disable';
    } else {
      nextPageId = thisPageId + 1;
    }

    var a = [];
    if (e <= eAllView) {
      //без сепаратора (9 элементов)
      for (var i = 1; i <= e; i++) {
        a.push({ id: i, type: 'link' });
      }
      if(e==1) {
        isShowClass = ' dn';
      }
    } else {
      //есть сепаратор
      if (eAllViewMiddle >= thisPageId) {
        //Добавление сепаратора справа
        for (var i = 1; i <= eView + 2; i++) {
          a.push({ id: i, type: 'link' });
        }
        //начало следующего элемента (eView + 1)
        //a.push({ id: (e - 1), type: 'separator' });
        //a.push({ id: e, type: 'link' });
      } else if ((e - eAllViewMiddle) <= thisPageId) {
        //Добавление сепаратора слева
        //a.push({ id: 1, type: 'link' });
        //a.push({ id: 2, type: 'separator' });

        for (var i = (e - eView - 2); i <= e; i++) {
          a.push({ id: i, type: 'link' });
        }
      } else {
        var eViewMiddle = Math.floor(eAllView / 2);
        //Добавление сепаратора слева и справа
        //a.push({ id: 1, type: 'link' });
        //a.push({ id: 2, type: 'separator' });
        for (var i = (thisPageId - eViewMiddle); i <= thisPageId + eViewMiddle; i++) {
          a.push({ id: i, type: 'link' });
        }
        //a.push({ id: (e - 1), type: 'separator' });
        //a.push({ id: e, type: 'link' });
      }
    }

    return (
      <div className={"pgnElem" + this.handleClassName() + isShowClass}>
        <Link to={{pathname: this.props.link + '/' + 1, query: query}} className={"pgnElem__item pgnElem__item_first" + leftDisableClass}>В начало</Link>
        <Link to={{pathname: this.props.link + '/' + prevPageId, query: query}} className={"pgnElem__item pgnElem__item_prev" + leftDisableClass}>Назад</Link>

        {a.map((i) => {
          {/*if (i.type === 'separator')
            return <span key={i.id} className="pgnElem__item pgnElem__item_separator">...</span>;*/}
            var link = i.id == 1 ? this.props.link : this.props.link + '/' + i.id;
          return <Link key={i.id} to={{pathname: link, query: query}} activeClassName="pgnElem__item_active" className="pgnElem__item">{i.id}</Link>;
        })}

        <Link to={{pathname: this.props.link + '/' + nextPageId, query: query}} className={"pgnElem__item pgnElem__item_next" + rightDisableClass}>Вперед</Link>
        <Link to={{pathname: this.props.link + '/' + e, query: query}} className={"pgnElem__item pgnElem__item_last" + rightDisableClass}>В конец</Link>
      </div>
    );
  }

  handleClassName() {
    if (this.props.className)
      return ' ' + this.props.className;
    return '';
  }
}

export default PaginationElement;