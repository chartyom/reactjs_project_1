import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';

const canUseDOM = !!(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
);

let lazySizes = null;

if (canUseDOM) {
  lazySizes = require('lazysizes');
}
/**
 * 
 * <LazyLoad width='1920' height='1024' dataSrc='http://lorempixel.com/1920/1024/sports/1'/>
 */
class LazyLoad extends React.Component {

  static propTypes = {
    src: React.PropTypes.string,
    dataSizes: React.PropTypes.string,
    dataSrc: React.PropTypes.string,
    dataSrcSet: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object, React.PropTypes.array]),
    className: React.PropTypes.string
  };

  static defaultProps = {
    src: 'data:image/gif;base64,R0lGODdhEAAJAIAAAMLCwsLCwiwAAAAAEAAJAAACCoSPqcvtD6OclBUAOw==',
    dataSizes: 'auto'
  };

  componentWillUpdate = (nextProps) => {
    let propsChanged = false;
    for (let propName of ['src', 'dataSizes', 'dataSrc', 'dataSrcSet', 'className']) {
      let prop = propName === 'dataSrcSet' ? this.handleSrcSet(this.props[propName]) : this.props[propName];
      let nextProp = propName === 'dataSrcSet' ? this.handleSrcSet(nextProps[propName]) : nextProps[propName];
      if (prop !== nextProp) {
        propsChanged = true;
        break;
      }
    }
    if (propsChanged && lazySizes) {
      let lazyElement = ReactDOM.findDOMNode(this);
      if (lazySizes.hC(lazyElement, 'lazyloaded')) {
        lazySizes.rC(lazyElement, 'lazyloaded');
      }
    }
  };

  componentDidUpdate = () => {
    if (!lazySizes) {
      return;
    }
    let lazyElement = ReactDOM.findDOMNode(this);
    if (!lazySizes.hC(lazyElement, 'lazyloaded') && !lazySizes.hC(lazyElement, 'lazyload')) {
      lazySizes.aC(lazyElement, 'lazyload');
    }
  };

  handleSrcSet = (srcSet) => {
    let result = srcSet;
    if (typeof srcSet === 'object') {
      if (!Array.isArray(srcSet)) {
        result = [];
        for (let variant in srcSet) {
          if (srcSet.hasOwnProperty(variant)) {
            result.push({
              variant: variant,
              src: srcSet[variant]
            });
          }
        }
      }
      result = result.map(item => {
        return `${item.src} ${item.variant}`;
      }).join(', ');
    }
    return result;
  };

  render() {
    let { src, dataSizes, dataSrc, dataSrcSet, className, ...other } = this.props;
    dataSrcSet = this.handleSrcSet(dataSrcSet);
    className = 'lazyload ' + (className || '');

    if(!canUseDOM) {
      src = dataSrc;
    }

    return (
      <img {...other}
        src={src}
        data-src={dataSrc}
        data-sizes={dataSizes}
        data-srcset={dataSrcSet}
        className={className} />
    );
  }
}

export default LazyLoad;