import React, { PureComponent } from 'react';
import { getNumEnding } from '../../../utils/Tools';
/**
 * Author: Chernyaev Artyom
 * style: ssElem
 * @param {array} data список элементов
 * @param {array} selectData список используемых элементов
 * @param {string} value название поля
 * @param {array} subValues массив парвильных названий типов
 * hundleButtonRemove
 * hundleButtonAdd
 * hundleButtonClear
 */
class SelectSearchElement extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      selectData: [],
      data: []
    };
    this.globalClick = this.globalClick.bind(this);
  }

  componentDidMount() {
    //var obj = {};
    // if (!this.state.selectData && this.props.selectData) {
    //   obj.selectData = this.props.selectData;
    // }
    if (this.props.data) {
      this.setState({ data: this.props.data });
    }
    // if (this.props.selectData || this.props.data) {
    //   this.setState(obj);
    // }
    document.addEventListener('click', this.globalClick, true);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.globalClick, true);
  }

  handleToggleList() {
    if (this.state.isOpen) {
      return this.setState({ isOpen: false });
    }
    this.setState({ isOpen: true });
  }

  handleCloseList() {
    return this.setState({ isOpen: false });
  }

  handleDataSearch(e) {

    const value = e.target.value.toLowerCase();
    var filter;
    //TODO custom underground theme
    if (this.props.theme && this.props.theme === 'underground') {
      filter = this.props.data.filter(v => {
        return !!v.name.toLowerCase().match(value);
      });
    } else {
      filter = this.props.data.filter(v => {
        return !!v.toLowerCase().match(value);
      });
    }
    this.setState({
      data: filter
    });
  };

  globalClick(ev) {
    const r = this.refs.selectSearchElement;
    if (r && ev && !r.contains(ev.target)) {
      this.handleCloseList();
    }
  }

  handleSelectData(v) {
    //var a = this.state.selectData;
    var a = this.props.selectData;
    var i = a.indexOf(v);
    if (i === -1) {
      if (this.props.hundleButtonAdd)
        this.props.hundleButtonAdd(v);
      //a.push(v);
    } else {
      if (this.props.hundleButtonRemove)
        this.props.hundleButtonRemove(v);
      //a.splice(i, 1);
    }
    //this.setState({ selectData: a });
  }

  handleClearData() {
    if (this.props.hundleButtonRemove)
      this.props.hundleButtonRemove(false);

    //this.setState({ selectData: [] });

  }

  rednerList() {
    //if (this.state.data) {
    if (this.state.data) {
      //if (this.state.data.length === 0)
      if (this.state.data.length === 0)
        return (
          <div className="ssElem__secondary__list__item_empty">
            Ничего не найдено
            </div>
        );

      //TODO custom underground theme
      if (this.props.theme && this.props.theme === 'underground')
        //return this.state.data.map((e, i) => {
        return this.state.data.map((e, i) => {
          var ckeckView = (
            <i className="fa fa-check-circle-o" aria-hidden="true"></i>
          );
          var checkClass = ' ssElem__secondary__list__item--active';
          //const a = this.state.selectData;
          const a = this.props.selectData;
          if (a.indexOf(e.name) === -1) {
            ckeckView = (
              <div className="ssElem__secondary__list__item__click__circle" style={{ backgroundColor: e.color }}></div>
            );
            checkClass = '';
          }
          return (
            <div key={i} className={"ssElem__secondary__list__item" + checkClass} onClick={() => this.handleSelectData(e.name)}>
              <div className="ssElem__secondary__list__item__click" style={{ color: e.color }}>
                {ckeckView}
              </div>
              <div className="ssElem__secondary__list__item__value">{e.name}</div>
            </div>
          );
        });

      //return this.state.data.map((e, i) => {
      return this.state.data.map((e, i) => {
        var ckeckView = (
          <i className="fa fa-check-circle-o" aria-hidden="true"></i>
        );
        var checkClass = ' ssElem__secondary__list__item--active';
        //const a = this.state.selectData;
        const a = this.props.selectData;
        if (a.indexOf(e) === -1) {
          ckeckView = (
            <i className="fa fa-circle-o" aria-hidden="true"></i>
          );
          checkClass = '';
        }
        return (
          <div key={i} className={"ssElem__secondary__list__item" + checkClass} onClick={() => this.handleSelectData(e)}>
            <div className="ssElem__secondary__list__item__click">
              {ckeckView}
            </div>
            <div className="ssElem__secondary__list__item__value">{e}</div>
          </div>
        );
      });
    }
    return;
  }

  rednerListSearch() {
    if (this.props.data && this.props.data.length > 10) {
      return (
        <div className="ssElem__secondary__search">
          <input className="ssElem__secondary__search__input" type="search" onChange={(e) => this.handleDataSearch(e)} placeholder="Введите название" />
        </div>
      );
    }
    return;
  }

  render() {
    var removeButtonActiveClass = '';
    var removeButton = '';

    var openListClass = this.state.isOpen ? ' ssElem_secondary--active' : '';

    var r = this.props.value;
    //const d = this.state.selectData.length;
    const d = this.props.selectData.length;
    if (d > 0) {
      r = this.props.value + ": " + d + " " + getNumEnding(d, this.props.subValues);
      removeButtonActiveClass = " ssElem__primary_remove--active";
      removeButton = (<div className="ssElem__primary__remove" title="Очистить фильтр" onClick={() => this.handleClearData()}></div>);
    }
    var theme = this.props.theme || 'default';

    return (
      <div ref="selectSearchElement" className={"ssElem" + openListClass}>
        <div className={"ssElem__primary" + removeButtonActiveClass}>
          {removeButton}
          <div className="ssElem__primary__select" onClick={() => this.handleToggleList()}>
            {r}
          </div>
        </div>
        <div className="ssElem__secondary">
          {this.rednerListSearch()}
          <div className={"ssElem__secondary__list ssElem__secondary__list_theme_" + theme}>
            {this.rednerList()}
          </div>
        </div>
      </div>
    );
  }
}

export default SelectSearchElement;