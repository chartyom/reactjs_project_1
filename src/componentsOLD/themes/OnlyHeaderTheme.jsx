import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import MainHeader from '../headers/MainHeader';
import MainFooter from '../footers/MainFooter';
//import * as authorizationAction from '../../actions/authorizationAction';
import CONFIG from '../../components/config';
import AnalytiWidget from '../widgets/Analytics';

class OnlyHeaderTheme extends PureComponent {
  render() {
    return (
      <div className="app__main-theme">
        <MainHeader
          numberPhone={this.props.locationCity.numberPhone}
          numberPhoneMask={this.props.locationCity.numberPhoneMask}
        />
        {this.props.children}
        <AnalytiWidget />
      </div>
    );
  }
};


OnlyHeaderTheme.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = ({ authorization }) => ({
  locationCity: authorization.locationCity
});

export default connect(mapStateToProps)(OnlyHeaderTheme);