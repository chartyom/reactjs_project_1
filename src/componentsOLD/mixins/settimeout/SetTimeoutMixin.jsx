// sampleComponent.js:
// import SetTimeoutMixin from 'mixins/settimeout'; 

// var SampleComponent = React.createClass({

//     //mixins:
//     mixins: [SetTimeoutMixin],

//     // sample usage
//     componentWillReceiveProps: function(newProps) {
//         if (newProps.myValue != this.props.myValue) {
//             this.clearTimeouts();
//             this.setTimeout(function(){ console.log('do something'); }, 2000);
//         }
//     },
// }


// constructor(props) {
//     super(props);
//     this.shouldComponentUpdate = SetTimeoutMixin.shouldComponentUpdate.bind(this);
// }

var SetTimeoutMixin = {
    componentWillMount: function () {
        this.timeouts = [];
    },
    setTimeout: function () {
        this.timeouts.push(setTimeout.apply(null, arguments));
    },

    clearTimeouts: function () {
        this.timeouts.forEach(clearTimeout);
    },

    componentWillUnmount: function () {
        this.clearTimeouts();
    }
};

export default SetTimeoutMixin;