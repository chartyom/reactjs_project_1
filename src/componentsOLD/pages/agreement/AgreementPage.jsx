import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';

export const LINK_PAGE = '/terms';
export const LINK_MASK = '/terms';

class AgreementPage extends PureComponent {

  render() {

    if (DEVELOPMENT) console.log('AgreementPage: Render');

    const PAGE_NAME = 'Пользовательское соглашение';
    const PAGE_DESCRIPTION = 'Битком24 - Умный сервис по выбору и бронированию ресторанов, баров и кафе. Пользовательское соглашение.';
    const PAGE_KEYWORDS = 'битком, битком24, bitkom, bitkom24, bitcom, bitcom24, лучшие, рестораны, бары, кафе, москва, заказ, столик, стол, бронирование, банкет, меню, отзывы, адрес, телефон, договор, оферта, пользовательское, соглашение';

    return (
      <section className="agrp">
        <Helmet
          title={PAGE_NAME}
          meta={[
            { name: 'description', content: PAGE_DESCRIPTION },
            { name: 'Keywords', content: PAGE_KEYWORDS },
          ]}
        />
        <div className="agrp__header">
          <div className="center">
            <h1>ПОЛЬЗОВАТЕЛЬСКОЕ СОГЛАШЕНИЕ</h1>
          </div>
        </div>
        <div className="center">
          <p>Настоящее Пользовательское Соглашение Общество c ограниченной ответственностью «Битком24» (далее - Сервис) считается заключенным с момента совершения Пользователем одного из следующих конклюдентных действий:</p>
          <ul>
            <li>регистрации на сайте www.bitkom24.ru;</li>
            <li>оформления заказа Пользователем в мобильном приложении;</li>
            <li>оформления заказа Пользователем без авторизации на Сайте;</li>
            <li>оформления заказа по телефону Контактного Центра «Битком 24».</li>
          </ul>
          <p className="agrp__end-paragraph">Настоящее Пользовательское Соглашение приравнивается к договору, составленному в письменной форме. Заключение Пользовательского соглашения означает, что Пользователь в необходимой для него степени ознакомился с условиями настоящего Пользовательского соглашения и полностью и безоговорочно согласен с ними, в том числе в части предоставления согласия Сервису на обработку персональных данных Пользователя на условиях, указанных в пункте 7 настоящего Пользовательского соглашения, и в части предоставления согласия Сервиса на получение Е-mail, sms и иных видов рассылок информационного и рекламного содержания, в том числе от Партнеров Сервиса.</p>


          <h2>1. Предмет соглашения</h2>

          <p>1.1. Отношения Сторон по настоящему Соглашению являются безвозмездными. Законодательство о защите прав потребителя к отношениям Сторон не применяется.</p>

          <p>1.2. К отношением между Обществом c ограниченной ответственностью «Битком24» и незарегистрированными пользователями Сервиса положения настоящего Соглашения применяются в том объеме, в котором они могут быть применимы в условиях отсутствия регистрации на сайте <a href="https://www.bitkom24.ru" title="Битком 24">https://www.bitkom24.ru</a> и, соответственно, функциональных возможностей, предоставляемых Сервисом зарегистрированному Пользователю.</p>

          <p>1.3. Сервис предлагает Вам свои услуги на условиях, являющихся предметом настоящего Пользовательского Соглашения (ПС).</p>

          <p className="agrp__end-paragraph">1.4. Соглашение может быть изменено Сервисом в одностороннем порядке без какого-либо специального уведомления, новая редакция Соглашения вступает в силу с момента ее размещения, если иное не предусмотрено новой редакцией Соглашения. Действующая редакция ПС всегда находится на странице по адресу: <a href="https://www.bitkom24.ru/terms" title="Пользовательское соглашение - Битком 24">https://www.bitkom24.ru/terms</a></p>

          <h2>2. Термины и определения</h2>


          <p>2.1. В настоящем пользовательском соглашении, если из текста прямо не вытекает иное, следующие термины будут иметь указанные ниже значения:</p>

          <p>2.1.1. <b>Пользователь</b> – дееспособное физическое лицо, добровольно прошедшее регистрацию на Сайте или в мобильном приложении, являющееся одной из сторон настоящего Соглашения.</p>

          <p>2.1.2. <b>Заведение</b> – указанное в Системе место общественного питания, в котором Исполнитель оказывает услуги общественного питания.</p>

          <p>2.1.3. <b>Исполнитель</b> – Юридическое лицо или индивидуальный предприниматель, оказывающий услуги общественного питания, информация о котором доступна в Системе</p>

          <p>2.1.4. <b>Система</b> – База данных исполнителей и Заведений, а также программа для ЭВМ, предназначенная для обеспечения взаимодействия Пользователя и Исполнителя, в частности, для автоматизации процесса бронирования мест в Заведении, доставки из заведений, и предварительного заказа блюд и напитков. Система является составной частью Сайта и мобильного приложения.</p>

          <p>2.1.5. <b>Сервис</b> – Общество с ограниченной ответственностью «Битком24», юридическое лицо, зарегистрированное по законодательству Российской Федерации, ОГРН 1167746719546, ИНН/КПП97001046553/770101001, адрес местонахождения Москва, улица 2-ая Бауманская, д. 5, строение 1 помещение 506А.</p>

          <p>2.1.6. <b>Соглашение</b> – Настоящее пользовательское соглашение.</p>
          <p>2.1.7. <b>Стороны</b> – Сервис и Пользователь.</p>
          <p>2.1.8. <b>Мобильное приложение</b> – Программа для ЭВМ, правообладателем которой является Сервис, использующая в наименовании слово «Битком» и предназначенная для функционирования ЭВМ и других компьютеров устройств (смартфонов, планшетных компьютеров), включающая в себя Систему.</p>

          <p>2.1.9. <b>Сайт</b> – Составное произведение, представляющее собой совокупность системы, базы данных, содержащей информацию, тексты, графические элементы, дизайн, изображения, фото- и видеоматериалы и иные результаты интеллектуальной деятельности, и программ для ЭВМ, которые позволяют найти и обработать материалы, содержащиеся в базе данных. Данное составное произведение содержится в информационной системе, обеспечивающей доступность такой информации в сети Интернет, размещенные в доменах по адресу bitkom24.ru, bitcom24.ru, а также прочих доменах, входящих в домены bitkom24.ru и/или bitcom24.ru.</p>

          <p>2.1.10. <b>Учетные данные</b> – логин (номер телефона) и код высылаемый на номер телефона пользователя, служат для индивидуализации пользователя и доступа к Личному кабинету.</p>

          <p>2.1.11. <b>Личный кабинет</b> – Защищенная часть Системы, созданная в результате регистрации Пользователя в Системе и доступная при вводе Пользователем Учетных данных.</p>

          <p>2.1.12. <b>Сеть Интернет</b> – World Wide Web - всемирная глобальная компьютерная сеть передачи и обмена данных посредством протоколов TCPIP, HTTP.</p>

          <p>2.1.13. <b>Услуги</b> – услуги по обслуживанию в местах общественного питания, в частности, бронирование мест в Заведениях, доставка готовых блюд и осуществление предварительного заказа блюд, доступные для заказа посредством Сервиса.</p>

          <p>2.1.14. <b>Посетитель</b> – лицо, непосредственно получающее обслуживание в Заведении Исполнителя (Заказчик и/или Клиент иили Несовершеннолетний клиент).</p>

          <p>2.1.15. <b>Бронирование</b> – заказ определенного столика в Заведении Исполнителя на определенную дату и определенное время с целью получить обслуживание в Заведении Исполнителя.</p>

          <p>2.1.16. <b>Предварительный заказ</b> – заказ еды, напитков и т.п. из Онлайн меню Заведения, которые будут приготовлены заранее, и в назначенное время прибытия Посетителя в Заведение.</p>

          <p>2.1.17. <b>Онлайн меню Заведения</b> – блюда и напитки из Меню Заведения, доступные для заказа на Сайте или в Мобильном приложении, с указанием их стоимости. Онлайн меню Заведения публикуется на Сайте или в Мобильном приложении и может содержать дополнительные условия заказа/отказа от заказа конкретного блюда/напитка.</p>

          <p>2.1.18. <b>Бонусная программа</b> – мероприятие проводимое Сервисом в рамках Системы, участниками которого являются Пользователи и Заведение.</p>

          <p>2.1.19. <b>Виртуальная валюта</b> – биткомы, начисляемые на Баланс Пользователя Сервисом в рамках проведения Системой рекламных акций, где 1 (один) битком Виртуальной валюты равен 1 (одному) российскому рублю.</p>

          <p>2.1.20. <b>Баланс</b> – персональный счет Пользователя в Системе, отражающий в режиме реального времени биткомы.</p>

          <p>2.1.21. <b>Регистрация</b> – перечень последовательных действий Пользователя на Сайте или в Мобильном приложении для получения Учетных данных и доступа к Личному кабинету.</p>

          <p>2.2. Все остальные термины и определения встречающиеся в тексте Соглашения, толкуются Сторонами в соответствии с законодательством Российской Федерации и сложившимися в сети Интернет правилами толкования соответствующих терминов</p>

          <p className="agrp__end-paragraph">2.3. Название заголовков (статей) Соглашения предназначены исключительно для удобства пользования текстом Соглашения и буквального юридического значения не имеют.</p>

          <h2>3. Регистрация, пароль и безопасность</h2>

          <p>3.1. По завершении Регистрации Пользователь получает код на телефонный номер указанный в момент регистрации в системе.</p>

          <p>3.2. Пользователь ответственен за хранение учетных данных, обеспечивающих доступ к Личному кабинету Пользователя на Сайте и в Мобильном приложении, вне доступа третьих лиц и своевременную их смену в случае утери или при иной необходимости.</p>

          <p>3.3. Пользователь несет ответственность за безопасность своего кода, а также за все, что будет сделано на Сервисе под его номером телефона и кодом.</p>

          <p>3.4. Пользователь соглашается с тем, что он обязан немедленно уведомить Сервис о любом случае неавторизованного (не разрешенного) доступа со своим телефонным номером и кодом и/или о любом нарушении безопасности.</p>

          <p>3.5. Любые действия и выполнение обязательств сервиса перед пользователем не возможно, без получения кода от сервиса на мобильный телефон пользователя.</p>

          <p className="agrp__end-paragraph">3.6. Сервис не несет ответственность за хранение и безопасность высланного кода пользователю на его смартфоне.</p>

          <h2>4. Права и обязанности пользователя. Условия использования сервиса </h2>

          <p>4.1. Услуги Сервиса носят информационный характер и оказываются Пользователю безвозмездно.</p>

          <p>4.2. Пользователь соглашается с тем, что Услуги предоставляются «как есть» и что Сервис не несет ответственности за качество поставляемых продуктов за задержки, сбои, неверную или несвоевременную доставку блюд, удаление или не сохранность какой-либо пользовательской персональной информации, а также за сбои в работе платежных систем.</p>

          <p>4.3. Для того чтобы воспользоваться Услугами Сервиса, необходимо иметь компьютер или установленное Мобильное приложение и доступ в Сеть Интернет.</p>

          <p>4.4. Пользователь самостоятельно оформляет Заказ либо на Сайте, либо через Мобильное приложение, либо по телефону Контактного Центра «Битком24».</p>

          <p>4.5. Пользователь имеет право использовать Сервис в соответствии с настоящим Соглашением и не запрещенными законодательством РФ способами.</p>

          <p>4.6. Пользователь обязан использовать Сервис добросовестно, соблюдая законные права и интересы Сервиса, третьих лиц, положения действующего законодательства Российской Федерации.</p>

          <p>4.7. Пользователь обязан до момента Регистрации на Сайте или в Мобильном приложении ознакомиться с текстом данного Соглашения, а также периодически проверять наличие изменений Соглашения. Соглашение может быть изменено Сервисом в одностороннем порядке без какого-либо специального уведомления Пользователя, в связи с чем Сервис рекомендует Пользователям регулярно проверять условия Соглашения. Продолжение использования Сайта или мобильного приложения Пользователем после внесения изменений в настоящее Соглашение означает принятие и согласие Пользователя с такими изменениями.</p>

          <p>4.8. Пользователь признает и соглашается с тем, что Сервис и все необходимые программы, связанные с ними, содержат конфиденциальную информацию, защищенную в соответствии с действующим законодательством Российской Федерации и международным законодательством.</p>

          <p>4.9. Пользователь соглашается не модифицировать, не продавать, не распространять этот контент и программы, целиком либо по частям.</p>

          <p>4.10. Пользователь обязуется не размещать на Сайте и не направлять куда-либо через/посредством Сайта любые материалы следующего характера:</p>

          <p>4.10.1. нарушающие законодательство, содержащие угрозы и оскорбления, дискредитирующие других лиц, нарушающие права граждан на частную жизнь или публичный порядок, носящие характер непристойности;</p>

          <p>4.10.2. нарушающие в той или иной степени честь и достоинство, права и охраняемые законом интересы других лиц;</p>

          <p>4.10.3. способствующие или содержащие призывы к разжиганию религиозной, расовой или межнациональной розни, содержащие попытки разжигания вражды или призывы к насилию;</p>

          <p>4.10.4. а также иные материалы, которые побуждают других лиц на противоправное поведение, влекущее уголовную, гражданско-правовую и иную ответственность или каким-либо образом нарушающее положения законодательства.</p>

          <p>4.11. Пользователь вправе принимать участие в рекламных акциях, проводимых Сервисом в целях популяризации Сайта и Мобильного приложения, на условиях, публикуемых Сервисом при сообщении о проведении соответствующей рекламной акции.</p>

          <p>4.12. Пользователь соглашается с тем, что Сервис вправе по своему усмотрению направлять на предоставленный Пользователем электронный адрес и размещать на Сайте рекламные и информационные сообщения, а также уведомления в связи с работой Сервиса и/или Соглашением. Пользователь вправе в любое время отказаться от получения рассылки по электронной почте.</p>

          <p>4.13. Пользователь не имеет права вносить изменения, публиковать, передавать третьим лицам, участвовать в продаже или уступке, создавать производные продукты или иным образом использовать, частично или полностью, содержание Сайта.</p>

          <p>4.14. Пользователь обязуется не размещать в Сервисе и не направлять через/посредством Сайта материалы, являющиеся рекламой каких-либо товаров или услуг, без получения предварительного выраженного согласия Сервиса.</p>

          <p>4.15. Пользователь обязуется не использовать Сервис для рекламы или иного стимулирования сбыта любых товаров и услуг в любой форме, включая, но не ограничиваясь, стимулирование пользователей к подписке на другую систему интерактивного обслуживания, являющуюся конкурентом Сервиса.</p>

          <p className="agrp__end-paragraph">4.16. Пользователь обязуется не загружать, размещать или иным образом использовать на Сайте какие-либо материалы, охраняемые законодательством об интеллектуальной собственности (в том числе, авторским правом, законодательством о товарных знаках), и иные охраняемые законодательством материалы без получения выраженного разрешения обладателя прав на охраняемый материал. При этом бремя доказывания того, что размещение на Сайте пользователем материалов не нарушает авторские, смежные и иные права третьих лиц на размещаемые материалы, а также ответственность незаконное размещение лежит на Пользователе.</p>

          <h2>5. Права и обязанности Сервиса. Условия предоставления системы</h2>

          <p>5.1. Сервис не несет ответственности за соблюдение/несоблюдения службами доставки и Заведением своих обязательств перед Пользователем, а также за достоверность информации, предоставленной такими службами и заведениями.</p>

          <p>5.2. Сервис не несет ответственность за информацию, предоставленную Заведением, а именно фотографии, описание ресторана, график работы, меню заведения и все связанные материалы, связанные с Заведением, размещенные от имени Сервиса в сети интернет.</p>

          <p>5.3. Сервис обязуется запрашивать информацию у Заведений для своевременного обновления системы. Сервис не несет ответственность за Заведение, которое не предоставило информацию в установленный срок, что могло привести к ущербу пользователям Сервиса.</p>

          <p>5.4. Сервис является лишь информационным связующим звеном между Пользователем, службами доставки и Заведением, информация о которых размещена на сайте и/или в мобильном приложении Сервиса.</p>

          <p>5.5. Сервис не несет ответственность за отзывы, опубликованные Пользователем.</p>

          <p>5.6. Сервис не при каких обстоятельствах не несет перед Посетителем и Заведением материальной (финансовой) ответственности за ущерб, вынужденные перерывы в деловой активности, потерю деловых, либо иных данных или информации, претензии или расходы, любые убытки, а также упущенную выгоду и утерянные сбережения, вызванные использованием или связанные с использованием Системы и ее составных частей, а также за ущерб, вызванный возможными ошибками и опечатками в программном обеспечении, равно как и за любые претензии со стороны третьих лиц.</p>

          <p>5.7. Сервис оставляет за собой право размещать комментарии к отзыву Пользователя. Сервис оставляет за собой право не размещать и удалять отзывы Пользователя, нарушающие Российское законодательство и наносящие ущерб законным интересам третьих лиц.</p>

          <p>5.8. Сервис оставляет за собой право в любой момент изменить, приостановить или прекратить предоставление Системы и ее составных частей полностью или частично, предварительно уведомив Пользователя либо без предварительного уведомления, при условии возврата Пользователю денежных средств, полученных Сервисом от Пользователя и не перечисленных в счет оплаты Услуг. Пользователь соглашается с тем, что Сервис не несет какой-либо ответственности перед пользователем за изменение, приостановление или прекращение предоставления Системы и ее составных частей.</p>

          <p>5.9. Сервис вправе в любое время по своему усмотрению заблокировать доступ Пользователя к его Личному кабинету без уведомления Пользователя, если существует обоснованное предположение о том, что Пользователь нарушает положения настоящего Соглашения.</p>

          <p className="agrp__end-paragraph">5.10. Сервис может временно изъять Мобильное приложение или Сайт для решения технических проблем либо усовершенствования. Сервис обязуется связаться с Пользователем и заблаговременно сообщить о подобных мерах, за исключением случаев непредвиденных обстоятельств.</p>

          <h2>6. Правила участия в Бонусной программе</h2>

          <p>6.1. Общие правила участия в Бонусной программе.</p>

          <p>6.1.1. В случае участия Пользователя в поощрительной программе или акции в рамках правил участия в бонусной программе Сервиса настоящего соглашения, Пользователь принимает все условия проводимой акции или поощрительной программы, размещенной на ресурсах предоставляемых Сервисом.</p>

          <p>6.2. Правила начисления Виртуальной валюты и получение скидки.</p>

          <p>6.2.1 В рамках действия Системы Пользователи накапливают Виртуальную валюту, которая может быть использована в рамках Сервиса.</p>

          <p>6.2.2. Виртуальная валюта начисляется на Баланс Пользователя в случае проведения Сервисом рекламных акций или поощрительных программ, предлагаемых Заведением или Сервисом самостоятельно.</p>

          <p>6.2.3. Виртуальная валюта начисляется на Баланс Пользователя, только после получения информации Сервисом от Заведения, в котором проводится рекламная акция или иная поощрительная программа.</p>

          <p>6.2.4. Виртуальная валюта начисляется на Баланс Пользователя при полном выполнении всех условий акций и иных поощрительных программ, проводимых сервисом.</p>

          <p>6.2.5. 1 (один) битком Виртуальной валюты равен 1 (одному) российскому рублю.</p>

          <p>6.2.6. Пользователь уведомлен и согласен с тем, что в течение 1 (одного) календарного года на Баланс Пользователя может быть начислены биткомы Виртуальной валюты на сумму в пределах 10000 рублей. Превышение данного лимита не допускается.</p>

          <p>6.3. Правила списания Виртуальной валюты и получение скидки.</p>

          <p>6.3.1. Виртуальная валюта, начисленная Сервисом, может быть использована Пользователем в условиях проводимой акции или поощрительной программы в настоящих правилах бонусной программы</p>

          <p>6.3.2. Виртуальная валюта начисленная Сервисом, может быть использована Пользователем в период проведения рекламных акций, скидок или иных поощрительных программ.</p>

          <p>6.3.3. Виртуальная валюта начисленная Сервисом, может быть использована Пользователем для получения акций, скидок или иных поощрительных программ в которых участвует Заведение в рамках Сервиса.</p>

          <p>6.3.4. Виртуальная валюта и права, предоставляемые пользователю в рамках Системы, не могут быть проданы, переданы, уступлены другому пользователю или использованы иным образом кроме как в соответствии с настоящими Правилами участия в Бонусной программе.</p>

          <p>6.3.5. Пользователь не вправе требовать выплаты виртуальной валюты, начисленной на Баланс Пользователя, в денежной или иной прямо не предусмотренной настоящим Соглашением форме.</p>

          <p>6.4. Споры и разногласия по начислению и списанию виртуальной валюты.</p>

          <p>6.4.1. При возникновении споров, возникающих в связи с исполнением условий настоящих правил участия в бонусной программе, Стороны обязуются разрешить их путём проведения переговоров. В случае не достижения соглашения по спорным вопросам, они разрешаются в судебном порядке по месту нахождения Сервиса.</p>

          <p className="agrp__end-paragraph">6.4.2. Во всем что не урегулировано настоящим Соглашением стороны руководствуются действующем законодательством Российской Федерации.</p>

          <h2>7. Интеллектуальная собственность</h2>

          <p>7.1. Система и ее составные части являются результатом интеллектуальной деятельности.</p>

          <p>7.2. Исключительное право на Систему и документацию, имеющую отношение к ней, принадлежит Правообладателю.</p>

          <p>7.3. Сайт содержит материалы, охраняемые авторским правом, товарные знаки и иные охраняемые законом материалы, включая, но не ограничиваясь: тексты, фотографии, графические изображения.</p>

          <p>7.4. При этом все содержание Сайта охраняется авторским правом как произведение, созданное коллективным творческим трудом в соответствии с законодательством Российской Федерации об авторском праве и смежных правах.</p>

          <p className="agrp__end-paragraph">7.5. Сервису принадлежит авторское право на использование содержания Сайта (в том числе, право на подбор, расположение, систематизацию и преобразование данных, содержащихся на сайте Сервиса, а также на сами исходные данные), кроме случаев, отдельно отмеченных в содержании опубликованных на Сайте материалов.</p>

          <h2>8. Политика конфиденциальности</h2>

          <p>8.1. Персональные данные Пользователя обрабатываются в соответствии с требованиями федерального закона от 27 июля 2006 г. №152-ФЗ «О персональных данных».</p>

          <p>8.2. При регистрации в системе и доступу к бронированию, Пользователь предоставляет следующую информацию о себе: Имя, адрес электронной почты, номер контактного телефон, адрес доставки товара.</p>

          <p>8.3. Предоставляя свои персональные данные при регистрации/оформлении заказа в Сервисе Пользователь соглашается на их обработку Сервисом, в том числе и в целях выполнения Сервисом обязательств перед Пользователем в рамках настоящего Пользовательского соглашения, информирования Пользователей о своих услугах, продвижения Сервисом товаров и услуг, проведения электронных и sms опросов, контроля маркетинговых акций, клиентской поддержки, организации доставки товара Пользователям, проведение розыгрышей призов среди Пользователей, контроля удовлетворенности Пользователя, а также качества услуг, оказываемых Сервисом. Не считается нарушением предоставление Сервису информации партнерам, агентам и третьим лицам, действующим на основании договора с Сервисом, для исполнения обязательств перед Пользователем.</p>

          <p>8.4. Под обработкой персональных данных понимается любое действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение) извлечение, использование, передачу (в том числе передачу третьим лицам, не исключая трансграничную передачу, если необходимость в ней возникла в ходе исполнения обязательств), обезличивание, блокирование, удаление, уничтожение персональных данных.</p>

          <p>8.5. Пользователь соглашается, что Сервис имеет право отправлять информационные, в том числе рекламные сообщения, на электронную почту и мобильный телефон Пользователя. Пользователь вправе отказаться от получения рекламной и другой информации без объяснения причин отказа.</p>

          <p>8.6. Сервисные сообщения, информирующие Пользователя о заказе и этапах его обработки, отправляются автоматически и не могут быть отклонены Пользователем.</p>

          <p>8.7. Сервис вправе использовать технологию «cookies». «Cookies» не содержат конфиденциальную информацию, и Сервис вправе передавать информацию о «cookies» Партнерам, агентам и третьим лицам, имеющим заключенные с Сервисом договоры, для исполнения обязательств перед Пользователем и для целей статистики и оптимизации рекламных сообщений.</p>

          <p>8.8. Сервис получает информацию об ip-адресе посетителя Сайта <a href="https://bitkom24.ru" title="Битком 24">https://www.bitkom24.ru</a>. Данная информация не используется для установления личности посетителя.</p>

          <p>8.9. Сервис не несет ответственности за сведения, предоставленные Пользователем на Сайте или в Мобильном приложении в общедоступной форме.</p>

          <p>8.10. Сервис вправе осуществлять записи телефонных разговоров с Пользователем, в целях контроля качества обслуживания Сервиса. При этом Сервис обязуется предотвращать попытки несанкционированного доступа к информации, полученной в ходе телефонных переговоров, и/или передачу ее третьим лицам, не имеющим непосредственного отношения к исполнению заказов, в соответствии с п. 4 ст. 16 Федерального закона «Об информации, информационных технологиях и защите информации».</p>

          <p className="agrp__end-paragraph">8.11. Сервис обязуется удалить запись телефонного разговора по первому требованию пользователя.</p>

          <h2>9. Общие положения</h2>

          <p>9.1. Настоящее соглашение регулируется нормами российского законодательства.</p>

          <p>9.2. Все возможные споры по поводу Соглашения разрешаются согласно нормам действующего российского законодательства.</p>

          <p>9.3. Ввиду безвозмездности условий настоящего соглашения нормы о защите прав потребителей не могут быть применимыми к отношениям между Пользователем и Сервисом.</p>

          <p>9.4. Ничто в Соглашении не может пониматься как установление между Пользователем и Сервисом агентских отношений, отношений товарищества, отношений по совместной деятельности, отношений личного найма, либо каких-то иных отношений, прямо не предусмотренных настоящим Соглашением.</p>

          <p>9.4. Любые вопросы, комментарии и иная корреспонденция Пользователя должны направляться Сервису по электронной почте support@bitkom24.ru. Сервис не несет ответственности и не гарантирует ответ на запросы, вопросы, предложения и иную информацию, кроме случаев, предусмотренных действующим законодательством.</p>
          <div className="agrp__footer">
            Адрес размещения Пользовательского соглашения в сети <br />
            Интернет: <a href="https://bitkom24.ru/terms" title="Пользовательское соглашение - Битком 24">https://www.bitkom24.ru/terms</a><br />
            По состоянию на «03» апреля 2017 г.
          </div>
        </div>
      </section>
    );
  }
};

export default AgreementPage;