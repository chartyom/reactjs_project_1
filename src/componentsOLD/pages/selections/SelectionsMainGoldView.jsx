import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import * as selectionsAction from '../../../actions/selectionsAction';
import SelectionsListView from '../../widgets/SelectionsView/SelectionsListView';
import { LINK_PAGE as SELECTIONS_LINK_PAGE } from '../selections/SelectionsPage';
import LoaderView from '../../widgets/LoaderView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';

class SelectionsMainGoldView extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  componentDidMount() {
    this.props.dispatch(selectionsAction.getPopular());
  }

  handleToggleButton() {
    return this.setState({ isOpen: !this.state.isOpen });
  }

  renderSelectionsListView() {
    if (this.state.isOpen) {
      return <SelectionsListView selections={this.props.selections.list} amount={13} beginMini={3} classes={'content-container fl content-container_without-padd'} />;
    }
    return <SelectionsListView selections={this.props.selections.list} amount={5} beginMini={3} classes={'content-container fl content-container_without-padd'} />;
  }

  renderSelections() {
    const { selections } = this.props;

    if (
      selections.readyState === selectionsAction.SELECTIONS_INVALID ||
      selections.readyState === selectionsAction.SELECTIONS_POPULAR_FETCHING
    ) {
      return <LoaderView />;
    }

    if (selections.readyState === selectionsAction.SELECTIONS_POPULAR_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('Failed to fetch => show error: ', selections.error);
      switch (selections.error.error) {
        case 404:
          return <ErrorPageView />;
        case 500:
          return <BadServer />;
        default:
          return <BadConnection />;
      }
    }
    var btntext = 'Показать еще';
    if (this.state.isOpen) {
      btntext = "Свернуть";
    }
    return (
      <div className="center">
        {this.renderSelectionsListView()}
        <div className="button-wrapper button-wrapper_center">
          <button type="button" className="button-wrapper__button button-wrapper__button_gold" onClick={() => this.handleToggleButton()} >{btntext}</button>
        </div>
      </div>
    );
  };


  render() {

    if (DEVELOPMENT) console.log('SelectionsPage => SelectionsMainGoldView: Render');

    return (
      <section className="main-container main-container_gold">
        <div className="header-container">
          <div className="center">
            <h2 className="header-container__header header-container__header_star">Самые популярные</h2>
          </div>
        </div>
        {this.renderSelections()}
      </section>
    );
  }
};


SelectionsMainGoldView.propTypes = {
  selections: PropTypes.shape({
    readyState: PropTypes.string.isRequired,
    list: PropTypes.array
  }).isRequired
};

const mapStateToProps = ({ selections }) => ({
  selections: selections.popular
});

export default connect(mapStateToProps)(SelectionsMainGoldView);