import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as selectionsAction from '../../../actions/selectionsAction';
import SelectionsListView from '../../widgets/SelectionsView/SelectionsListView';
import LoaderView from '../../widgets/LoaderView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';
import { shuffleArray } from '../../../utils/Tools';

class SelectionsShuffleView extends PureComponent {

  renderShuffleSelections() {
    const { selections } = this.props;

    if (
      selections.readyState === selectionsAction.SELECTIONS_INVALID ||
      selections.readyState === selectionsAction.SELECTIONS_FETCHING
    ) {
      return <LoaderView />;
    }

    if (selections.readyState === selectionsAction.SELECTIONS_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('Failed to fetch => show error: ', selections.error);
      switch (selections.error.error) {
        case 404:
          return <ErrorPageView />;
        case 500:
          return <BadServer />;
        default:
          return <BadConnection />;
      }
    }
    let list = shuffleArray(selections.list);
    return <SelectionsListView selections={list} amount={5} beginMini={3} classes={'content-container fl content-container_without-padd'} />;
  };


  render() {

    if (DEVELOPMENT) console.log('SelectionsPage => SelectionsShuffleView: Render');

    return (
      <section className="main-container">
        <div className="header-container">
          <div className="center">
            <h1 className="header-container__header header-container__header_big-font">Подборки</h1>
          </div>
        </div>
        <div className="center">
          {this.renderShuffleSelections()}
        </div>
      </section>
    );
  }
};


SelectionsShuffleView.propTypes = {
  selections: PropTypes.shape({
    readyState: PropTypes.string.isRequired,
    list: PropTypes.array
  }).isRequired
};

const mapStateToProps = ({ selections }) => ({
  selections: selections.main
});

export default connect(mapStateToProps)(SelectionsShuffleView);