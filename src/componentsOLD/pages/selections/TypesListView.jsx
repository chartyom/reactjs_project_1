import React, { PureComponent, PropTypes } from 'react';
import { Link } from 'react-router';
import SelectionsListView from '../../widgets/SelectionsView/SelectionsListView';
import { LINK_PAGE as SELECTIONS_LINK_PAGE } from '../selections/SelectionsPage';


const TypesListView = ({ types, selections }) => {
  return (
    <div >
      {types.map((type, index) => {

        let _selections = selections.filter(function (selection) {
          if (
            (
              typeof selection.type == 'object' &&
              selection.type.indexOf(type.name) >= 0
            ) ||
            selection.type == type.name
          ) {
            return true;
          }
          return false;
        });

        let classTopLine = index != 0 ? ' header-container_top-line' : '';
        if (_selections.length > 0)
          return (
            <section key={index} className="main-container">
              <div className={"header-container" + classTopLine}>
                <div className="center">
                  <h3 className="header-container__header">{type.name}</h3>
                </div>
              </div>
              <div className="center">
                <SelectionsListView selections={_selections} classes={'content-container fl content-container_without-padd'} />
              </div>
            </section>
          );
        return <div key={index}></div>;
      })}
    </div>);
};


TypesListView.propTypes = {
  types: PropTypes.array.isRequired,
  selections: PropTypes.array.isRequired
};

export default TypesListView;