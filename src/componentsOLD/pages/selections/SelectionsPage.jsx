import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import SearchSectionMiniHeader from '../../headers/SearchSectionMiniHeader';
import TypesView from './TypesView';
import * as selectionsAction from '../../../actions/selectionsAction';
import SelectionsShuffleView from './SelectionsShuffleView';
import SelectionsMainGoldView from './SelectionsMainGoldView';

export const LINK_PAGE = '/selections';
export const LINK_MASK = '/selections';

class SelectionsPage extends PureComponent {

  static readyOnActions(dispatch, params) {
    return Promise.all([
      dispatch(selectionsAction.getWithTypes())
    ]);
  }

  componentDidMount() {
    SelectionsPage.readyOnActions(this.props.dispatch, this.props.params);
  }

  render() {

    if (DEVELOPMENT) console.log('SelectionsPage: Render');

    const PAGE_NAME = 'Подборки заведений';
    const PAGE_DESCRIPTION = 'Битком 24 - Лучшие рестораны, бары и кафе Москвы, заботливо собранные в удобные подборки. Лучшие суши, лучшие бургеры, лучшие веранды, скидки в день рождения';
    const PAGE_KEYWORDS = 'битком, битком24, bitkom, bitkom24, bitcom, bitcom24, лучшие, рестораны, бары, кафе, москва, заказ, столик, стол, бронирование, банкет, меню, отзывы, адрес, бургеры, суши, веранда, панорама, ревизорро, летучая, доставка, скидки, акции, мероприятия, заказать, телефон, суши-бар, день рождения';


    return (
      <div>
        <Helmet
          title={PAGE_NAME}
          meta={[
            { name: 'description', content: PAGE_DESCRIPTION },
            { name: 'Keywords', content: PAGE_KEYWORDS },
          ]}
        />
        <SearchSectionMiniHeader />
        <SelectionsShuffleView />
        <SelectionsMainGoldView />
        <TypesView />
      </div>
    );
  }
};


SelectionsPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect()(SelectionsPage);