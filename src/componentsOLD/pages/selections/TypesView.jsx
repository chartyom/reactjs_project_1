import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import * as selectionsAction from '../../../actions/selectionsAction';
import TypesListView from './TypesListView';
import { LINK_PAGE as SELECTIONS_LINK_PAGE } from '../selections/SelectionsPage';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';

class TypesView extends PureComponent {

  renderTypes() {
    const { selections, types } = this.props;
    if (
      (
        selections.readyState === selectionsAction.SELECTIONS_INVALID ||
        selections.readyState === selectionsAction.SELECTIONS_FETCHING
      )
    ) {
      return <LoaderView />;
    }

    if (
      selections.readyState === selectionsAction.SELECTIONS_FETCH_FAILED
    ) {
      if (selections.error) {
        if (DEVELOPMENT) console.log('Failed to fetch => show error: ', selections.error);
        switch (selections.error.errorCode) {
          case 404:
            return <ErrorPageView />;
          case 500:
            return <BadServer />;
          default:
            return <BadConnection />;
        }
      }
      if (DEVELOPMENT) console.log('Failed to fetch => local load');
    }
    return <TypesListView types={types.list} selections={selections.list} />;
  };


  render() {

    if (DEVELOPMENT) console.log('SelectionsPage => TypesView: Render');

    return (
      <section className="main-container">
        {this.renderTypes()}
      </section>
    );
  }
};


TypesView.propTypes = {
  selections: PropTypes.shape({
    readyState: PropTypes.string.isRequired,
    list: PropTypes.array
  }).isRequired,
  // types: PropTypes.shape({
  //   readyState: PropTypes.string.isRequired,
  //   list: PropTypes.array
  // }).isRequired
};

const mapStateToProps = ({ selections }) => ({
  selections: selections.main,
  types: selections.types
});

export default connect(mapStateToProps)(TypesView);