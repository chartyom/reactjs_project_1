import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router';
import FeedbackSmallFormWidget from '../../widgets/FeedbackSmallForm';

export const LINK_PAGE = '/about';
export const LINK_MASK = '/about';

class AboutPage extends PureComponent {

  // static readyOnActions(dispatch, params) {
  //   return Promise.all([
  //   ]);
  // }

  render() {

    if (DEVELOPMENT) console.log('AboutPage: Render');

    return (
      <section>
        <Helmet title="О нас" />
        <div className="header-container header-container_content-page">
          <div className="center">
            <div className="full-width fl fl-in-di">
              <div>
                <ul className="breadcrumbs breadcrumbs_banner">
                  <li><Link to="/">Главная</Link></li>
                  <li><span>/</span></li>
                  <li><span>О нас</span></li>
                </ul>
                <h3 className="header-container__header">О нас</h3>
              </div>
            </div>
          </div>
        </div>
        <div className="center about-cnt">
          <div className="ordinary-note">
            <p className="ordinary-note__text">Международный деловой центр «Москва-Сити» расположен на Пресненской набережной. Он объединил бизнес, офисы, жилой район и досуг. Где поесть в деловом центре Москва-Сити знают сотрудники здешних офисов, жители этого района и гости. Здесь можно
							в небольшой кофейне выпить чашку кофе, в пабе – бокал крафтового пива или перекусить в ближайшем фастфуде или пиццерии.</p>
          </div>
          <div className="full-width fl fl-in-di">
            <div className="left">
              <div className="about-blck about-blck_history">
                <div className="header-container header-container_bottom-line header-container--paragraph">
                  <h3 className="header-container__header">Наша история</h3>
                </div>
                <div className="about-blck__content">
                  <div className="about-blck_history__point">
                    <span className="about-blck_history__title">
                      <span>Step #1</span>
                      <span className="about-blck_history__label">2010 г.</span>
                    </span>
                    <div className="about-blck_history__text">
                      Along with conventional advertising and below the line activities, organizations and corporate bodies have come to realize that they need to invest in trade shows in order to create maximum recall for their product or brand name.
										</div>
                  </div>
                  <div className="about-blck_history__point">
                    <span className="about-blck_history__title">
                      <span>Step #1</span>
                      <span className="about-blck_history__label">2010 г.</span>
                    </span>
                    <div className="about-blck_history__text">
                      Along with conventional advertising and below the line activities, organizations and corporate bodies have come to realize that they need to invest in trade shows in order to create maximum recall for their product or brand name.
										</div>
                  </div>
                  <div className="about-blck_history__point about-blck_history__point_right">
                    <div className="about-blck_history__image">
                      <img src="" alt="" />
                    </div>
                    <span className="about-blck_history__title">
                      <span>Step #1</span>
                      <span className="about-blck_history__label">2010 г.</span>
                    </span>
                    <div className="about-blck_history__text">
                      Along with conventional advertising and below the line activities, organizations and corporate bodies have come to realize that they need to invest in trade shows in order to create maximum recall for their product or brand name.
										</div>
                  </div>
                  <div className="about-blck_history__point about-blck_history__point_right">
                    <div className="about-blck_history__image">
                      <img src="" alt="" />
                    </div>
                    <span className="about-blck_history__title">
                      <span>Step #1</span>
                      <span className="about-blck_history__label">2010 г.</span>
                    </span>
                    <div className="about-blck_history__text">
                      Along with conventional advertising and below the line activities, organizations and corporate bodies have come to realize that they need to invest in trade shows in order to create maximum recall for their product or brand name.
										</div>
                  </div>
                  <div className="about-blck_history__point about-blck_history__point_last">
                    <div className="about-blck_history__image">
                      <img src="" alt="" />
                    </div>
                    <span className="about-blck_history__title">
                      <span>Final Step</span>
                      <span className="about-blck_history__label">2010 г.</span>
                    </span>
                    <div className="about-blck_history__text">
                      Along with conventional advertising and below the line activities, organizations and corporate bodies have come to realize that they need to invest in trade shows in order to create maximum recall for their product or brand name.
										</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="right">
              <div className="about-blck about-blck_mission">
                <div className="header-container header-container--paragraph">
                  <h3 className="header-container__header">Миссия</h3>
                </div>
                <div className="about-blck__content">
                  <p>Along with conventional advertising and below the line activities, organizations and corporate bodies have come to realize that they need to invest in trade shows in order to create</p>
                </div>
                <div className="about-blck__person">
                  <span className="about-blck__person-image">
                    <img src="" alt="" />
                  </span>
                  <span className="about-blck__person-name">
                    <span className="about-blck__person-first-name">John Smith</span>
                    <span className="about-blck__person-position">Cheif Executive Officer</span>
                  </span>
                </div>
              </div>
              {/*<div className="about-blck about-blck_presentation">
                  <div className="about-blck__content">
                    <span className="about-blck_presentation__title">
                      <span>Презентация Битком24</span>
                      <span className="about-blck_presentation__size">PDF, 2 мб.</span>
                    </span>
                    <a href="#" className="about-blck_presentation__button">Скачать</a>
                  </div>
                </div>*/}
              <div className="about-blck about-blck_law-docs">
                <div className="header-container header-container--paragraph">
                  <h3 className="header-container__header">Юридические документы</h3>
                </div>
                <div className="about-blck__content">
                  <ul>
                    <li>
                      <a href="#">Пользовательское соглашение</a>
                    </li>
                  </ul>
                </div>
              </div>
              <FeedbackSmallFormWidget />
            </div>
          </div>
        </div>
      </section>
    );
  }
};

export default AboutPage;