import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import PaginationElement from '../../elements/pagination';
import SearchSectionMiniHeader from '../../headers/SearchSectionMiniHeader';
import EstablishmentsView from './EstablishmentsView';

export const LINK_PAGE = '/search';
export const LINK_MASK = '/search(/:pageId)';
/*
url query links

/search?kitchens=Итальянская
/search?types=Ресторан,Кафе,Клуб
/search?tags=Стейки
/search?underground=Бауманская,Авиамоторная
*/
class SearchPage extends PureComponent {

  getSearchValue() {
    return this.props.location.query.q;
  }

  render() {

    if (DEVELOPMENT) console.log('SearchPage: Render');

    const PAGE_NAME = 'Поиск заведений';
    const PAGE_DESCRIPTION = 'Битком24 - Лучшие рестораны, бары и кафе Москвы, поиск по чеку, кухне, метро, меню, детскому меню, времени работы, особенностям. Подберем самый близкий ресторан';
    const PAGE_KEYWORDS = 'битком, битком24, bitkom, bitkom24, bitcom, bitcom24, лучшие, рестораны, бары, кафе, москва, заказ, столик, стол, бронирование, банкет, меню, отзывы, адрес, найти, поиск, самый близкий, геолокация, быстрый';

    return (
      <div>
        <Helmet
          title={PAGE_NAME}
          meta={[
            { name: 'description', content: PAGE_DESCRIPTION },
            { name: 'Keywords', content: PAGE_KEYWORDS },
          ]}
        />
        <SearchSectionMiniHeader
          isOpenFilters={true}
          searchValue={this.getSearchValue()}
          query={this.props.location.query}
          handleFetch={true}
          params={this.props.params}
        />
        <EstablishmentsView params={this.props.params} query={this.props.location.query}/>
      </div>
    );
  }
};


export default SearchPage;