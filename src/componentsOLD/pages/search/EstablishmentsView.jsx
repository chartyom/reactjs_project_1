import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import EstablishmentsListView from '../../widgets/EstablishmentsView/EstablishmentsListView';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';
import PaginationElement from '../../elements/pagination';
import { LINK_PAGE } from './SearchPage';
import PlacesSortView from './PlacesSortView';

class EstablishmentsView extends PureComponent {


  renderPlaces() {

    if (DEVELOPMENT) console.log('SearchPage => EstablishmentsView: Render');

    const { places } = this.props;
    if (places.readyState === establishmentsAction.ESTABLISHMENTS_SEARCH_FETCHING) {
      return <LoaderView />;
    }

    if (places.readyState === establishmentsAction.ESTABLISHMENTS_SEARCH_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('FAILED FETCH => ERROR:', places.error);
      switch (places.error.statusCode) {
        case 404:
          return <ErrorPageView />;
        case 500:
          return <BadServer />;
        default:
          return <BadConnection />;
      }
    }
    if (places.list && places.list.length > 0)
      return <EstablishmentsListView establishments={places.list} amount={32} classes={'content-container content-container_without-padd fl place__list--col-4'} />;
    return (
      <div> По вашему запросу ничего не найдено </div>
    );
  }

  renderPagination() {
    const { places } = this.props;
    if (
      places.readyState === establishmentsAction.ESTABLISHMENTS_SEARCH_FETCHED &&
      places.count
    )
      return <PaginationElement link={LINK_PAGE} thisPageId={this.props.params.pageId} count={places.count} amount={32} query={this.props.query} />;
  };


  render() {
    return (
      <div className="main-container">
        <div className="header-container center">
          <div className="fl-in-di full-width">
            <h3 className="header-container__header header-container__header_big-font">Поиск заведений</h3>
            <PlacesSortView />
          </div>
        </div>
        <div className="center">
          {this.renderPlaces()}
        </div>
        {this.renderPagination()}
      </div>
    );
  }
};


EstablishmentsView.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = ({ establishments }) => ({
  places: establishments.search
});

export default connect(mapStateToProps)(EstablishmentsView);