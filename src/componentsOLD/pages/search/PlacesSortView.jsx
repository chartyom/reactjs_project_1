import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import CONFIG from '../../config';
import * as searchAction from '../../../actions/searchAction';
import * as establishmentsAction from '../../../actions/establishmentsAction';

class PlaceSortView extends PureComponent {


  handleSetSort(e) {
    var v = e.target.value;
    this.props.dispatch(searchAction.setSort({ by: v }));
    setTimeout(function(){
      this.handleFetch();
    }.bind(this),100);
  }

  handleFetch() {
    var { activeFilters, sort, value } = this.props;

    var obj = {};
    obj.q = value;
    obj.filter = {};
    obj.filter.tags = activeFilters.tags;
    obj.filter.types = activeFilters.types;
    obj.filter.kitchens = activeFilters.kitchens;
    obj.filter.metros = activeFilters.underground;
    obj.sort = sort;

    this.props.dispatch(establishmentsAction.getForSearch(1, 32, obj));
  }

  handleSetTypeSort() {
    if (this.props.sort.dir == 'asc') {
      this.props.dispatch(searchAction.setSort({ dir: 'desc' }));
    } else {
      this.props.dispatch(searchAction.setSort({ dir: 'asc' }));
    }
    setTimeout(function(){
      this.handleFetch();
    }.bind(this),100);
  }

  renderButtonTypeSort() {
    if (this.props.sort.dir == 'desc') {
      return (
        <button type="button" title="По возрастанию" className="btn btn-default_border" onClick={this.handleSetTypeSort.bind(this)}><i className="fa fa-sort-amount-asc" aria-hidden="true"></i>
        </button>
      );
    }
    return (
      <button type="button" title="По убыванию" className="btn btn-default_border" onClick={this.handleSetTypeSort.bind(this)}><i className="fa fa-sort-amount-desc" aria-hidden="true"></i>
      </button>
    );
  }

  handleGetText(v) {
    switch (v) {
      case 'croud':
        return 'По загруженности';
      case 'rating':
        return 'По популярности';
      case 'price':
        return 'По чеку';
    }
  }

  render() {
    return (
      <div className="header-container__sort fl nwr fl-ai-c">
        <select className="form-control" title="Выбор параметра сортировки" id="selectSort" value={this.props.sort.by} onChange={(e) => this.handleSetSort(e)}>
          {CONFIG.placesSort.by.map((a, i) => (
            <option key={i} value={a} >{this.handleGetText(a)}</option>
          ))}
        </select>
        {this.renderButtonTypeSort()}
      </div>
    );
  }
};


PlaceSortView.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = ({ search }) => ({
  value: search.main.value,
  activeFilters: search.main.activeFilters,
  sort: search.main.sort
});

export default connect(mapStateToProps)(PlaceSortView);