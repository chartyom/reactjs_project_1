import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { getNumEnding, cropText } from '../../../utils/Tools';
import PlaceInsideReservationView from './PlaceInsideReservationView';
import { LINK_PAGE as SEARCH_LINK_PAGE } from '../search/SearchPage';
import { LINK_PAGE as ESTABLISHMENT_LINK_PAGE } from './EstablishmentPage';

const PlaceInsideView = ({ establishment, params, handleAddToFavorites }) => {

	let bgImg = establishment.img,
		shortDesc = establishment.shortDesc,
		currentWork = establishment.currentWork,
		schedule = establishment.scheduleRes,
		availableDates = establishment.availableDates,
		name = establishment.name,
		type = establishment.mapType,
		placeId = establishment.id,
		label = establishment.label,
		price = establishment.price,
		rating = establishment.rating,
		feedbackCount = establishment.feedbacks.length,
		locationAdress = establishment.address.street,
		underground = establishment.address.metro && establishment.address.metro.name ? establishment.address.metro.name : null,
		//undergroundDistance = establishment.undergroundDistance,
		scheduleRes = establishment.scheduleRes,
		isReservation = establishment.pointsOfAgreement.reservation,
		undergroundView = '',
		orderView = '';



	if (currentWork && currentWork != "Закрыто") {
		currentWork = "открыто " + currentWork;
	}

	if (underground) {
		undergroundView = (
			<p className="metro metro_half-transparent-white">{underground} {/*({undergroundDistance})*/}</p>
		);
	}

	if (schedule && isReservation) {
		orderView = <PlaceInsideReservationView schedule={schedule} availableDates={availableDates} params={params} />;
	}


	/*
  		LABEL
	*/
	var
		labelView = '';

	if (label && label.color && label.name) {
		var labelColor = label.color,
			labelName = label.name,
			labelType = label.type,
			labelCC = '', //COLOR CLASS
			labelTC = ''; //TYPE CLASS

		if (labelColor == 'greenAlert') {
			labelCC = " label_green";
		} else if (labelColor == 'redAlert') {
			labelCC = " label_red";
		} else if (labelColor == 'yellowAlert') {
			labelCC = " label_yellow";
		}

		if (labelType == 'check') {
			labelTC = " label_type--check";
		} else if (labelType == 'alert') {
			labelTC = " label_type--alert";
		}

		{/*<span className="label label_orange-gradient label_in-full-height-main-block">Остался один столик</span>*/ }
		labelView = (
			<span className={"label label_in-full-height-main-block" + labelCC + labelTC}>
				<span>{labelName}</span>
			</span>
		);
	}

	/*
		<div className="place-inside-main-block__search">
							<div className="place-inside-main-block__search-form-wrapper fl fl-in-di nwr fl-ai-c">
								<input placeholder="Заведение, кухня или метро" className="place-inside-main-block__search-field" type="text" />
								<a className="place-inside-main-block__search-button" href="#">Найти</a>
							</div>
							<div className="fl fl-in-di fl-ai-c full-width">
								<div>
									<p className="place-inside-main-block__bread-crumbs-note">поиск / </p>
									<a className="place-inside-main-block__bread-crumbs-link" href="#">BB Burgers</a>
								</div>
								<div>
									<a className="place-inside-main-block__search-additional-button" href="#">Добавить в избранное</a>
									<a className="place-inside-main-block__search-additional-button place-inside-main-block__search-additional-button_share" href="#">Поделиться</a>
								</div>
							</div>
						</div>
						*/
	const LINK = ESTABLISHMENT_LINK_PAGE + '/' + establishment.mapType + '/' + establishment.name + '/' + establishment.id;

	return (
		<section className="place-inside-main-block" style={{ backgroundImage: 'url(' + bgImg + ')' }}>
			<div className="place-inside-main-block__background" />
			<div className="center full-height">
				<div className="full-height full-width fl fl-dr-col fl-in-di nwr">
					<div className="place-inside-main-block__search">
						<div className="fl fl-in-di fl-ai-c full-width">
							<div>
								<Link to={{ pathname: SEARCH_LINK_PAGE }} className="place-inside-main-block__bread-crumbs-link" >поиск</Link>
								<p className="place-inside-main-block__bread-crumbs-devider">/</p>
								<Link to={{ pathname: SEARCH_LINK_PAGE, query: { types: type } }} className="place-inside-main-block__bread-crumbs-link" >{type}</Link>
								<p className="place-inside-main-block__bread-crumbs-devider">/</p>
								<p className="place-inside-main-block__bread-crumbs-note">{name}</p>
							</div>
							<div>
								<Link to={LINK} rel="sidebar" className="place-inside-main-block__search-additional-button" onClick={() => handleAddToFavorites()} >Добавить в избранное</Link>
								{/*<a className="place-inside-main-block__search-additional-button place-inside-main-block__search-additional-button_share" href="#">Поделиться</a>*/}
							</div>
						</div>
					</div>
					<div className="fl fl-ai-c fl-in-di nwr">
						<div className="place-inside-main-block__general-info fl fl-dr-col fl-in-di">
							<div>
								<p className="place-inside-main-block__specialization">{shortDesc}</p>
								<p className="place-inside-main-block__work-hours"><span>{currentWork}</span></p>
							</div>
							<div>
								<h1 className="place-inside-main-block__name">{cropText(name, 45)} {labelView}</h1>
								<div>
									<span className="place-inside-main-block__rating fl fl-it-cnt" title="Рейтинг заведения">{rating}</span>
									<span className="place-inside-main-block__middle-check fl fl-it-cnt" title="Средний чек">{price} ₽</span>
									<span className="place-inside-main-block__feedback-count fl fl-it-cnt" title="Количество отзывов">{feedbackCount}</span>
								</div>
							</div>
							<div>
								<a className="place-inside-main-block__location" href="#goto-map">{locationAdress}</a>
								{undergroundView}
							</div>
						</div>
						{orderView}
					</div>
					<div className="mouse">
						<a className="mouse__link" href="#goto-about"><img src="/images/mouse-button.png" /></a>
					</div>
				</div>
			</div>
		</section>
	);
};


PlaceInsideView.propTypes = {
	establishment: PropTypes.shape({
		name: PropTypes.string.isRequired,
		id: PropTypes.string.isRequired,
	}).isRequired
};

export default PlaceInsideView;