import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as uiAction from '../../../actions/uiAction';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import { loadScript, getNumEnding } from '../../../utils/Tools';
import moment from 'moment';
import CONFIG from '../../config';
import InputElement from 'react-input-mask';
import SelectElement from '../../elements/form/select';
import CheckboxElement from '../../elements/form/checkbox';
import LoaderView from '../../widgets/LoaderView';
/**
 * 
 * @param {array} schedule
 * @param {number} banquetPrice
 */
class ReviewsReservationBanquetView extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            pikaday: null,
            budgetValidDescription: ''
        };
    }

    componentDidMount() {
        loadScript("/temp/js/pikaday.js")
            .then(() => this.initDatePicker())
            .catch(e => { if (DEVELOPMENT) console.log(e); });
    }

    getReservation() {
        return this.props.reservation;
    }

    //Получить хранимые значение выбранного времени из REDUX
    getScheduleTime() {
        return this.props.tempBanquetScheduleTime;
    }

    //Время бронирования
    getScheduleList() {
        //Вывод нового списка времени бронирования
        if (this.getScheduleTime() && this.getScheduleTime().list)
            return this.getScheduleTime().list;
        return this.props.schedule;
    }

    handleSetReservation(d) {
        this.props.dispatch(uiAction.setReservationBanquet(d));
    }

    /**
     * Задаёт дату
     * @param {date} date 
     */
    handleSetDate(date) {
        //Предотвращает излишние изменения состояния
        //Преобразование через moment потому, что date имеет значение начала дня, а this.getReservation().date текущее время дня
        if (moment(date).format('MMDD') != moment(this.getReservation().date).format('MMDD')) {
            this.handleSetReservation({ date: date, time: false });
            this.props.dispatch(establishmentsAction.setBanquetScheduleTime(this.props.placeId, moment(date).format('YYYY-MM-DD')));
        }
    }

    /**
     * Задаёт количество персон
     * @param {event} e
     */
    handleSetPeople(e) {
        //Предотвращает излишние изменения состояния
        if (e.target.value != this.getReservation().people)
            this.handleSetReservation({ people: e.target.value });
    }


    /**
     * Задаёт время
     * @param {event} e 
     */
    handleSetTime(e) {
        var v = e.target.value;
        if (v != this.getReservation().time) {
            v = v != 'default' ? v : false;
            var bonus = false;
            var bonusType = false;
            if (v) {
                for (var i = 0; i < this.getScheduleList().length; i++) {
                    if (
                        this.getScheduleList()[i].time == v &&
                        this.getScheduleList()[i].bonus &&
                        (this.getScheduleList()[i].bonus.rBonusValue || this.getScheduleList()[i].bonus.rBonusPercent)
                    ) {
                        bonusType = this.getScheduleList()[i].bonus.rBonusValue ? 'value' : 'percent';
                        bonus = this.getScheduleList()[i].bonus.rBonusValue
                            ? this.getScheduleList()[i].bonus.rBonusValue
                            : this.getScheduleList()[i].bonus.rBonusPercent;
                    }
                }
            }
            this.handleSetReservation({ time: v, bonus, bonusType });
        }
    }

    initPikaday(opt) {
        this.setState({
            pikaday: new Pikaday(opt)
        });
    }

    getPikaday() {
        return this.state.pikaday;
    }

    initDatePicker() {

        const c = document.getElementsByClassName('rcrbv-piker_window');
        while (c.length > 0) {
            c[0].parentNode.removeChild(c[0]);
        }

        const setDate = (date) => this.handleSetDate(date);
        const getDate = () => this.getReservation().date;
        //массив с датами начала брони
        var getAvailableDates = this.props.availableDates;
        const w = document.getElementById('rcrbv-piker_temp');
        const t = document.getElementById('rcrbv-piker_trigger');
        if (!w || !t) return;
        // Config
        var datepickerConfig = {
            field: w,
            trigger: t,
            format: 'MMMM',
            theme: 'rcrbv-piker_window',
            showDaysInNextAndPreviousMonths: true,
            defaultDate: this.getReservation().date,
            minDate: moment().toDate(),
            maxDate: moment().add(28, 'days').toDate(),
            firstDay: 1,
            onSelect: function () {
                if (!w) return;
                setDate(this._d);
            },
            disableDayFn: function (theDay) {
                var i = getAvailableDates.map(function (obj) {
                    return obj.value;
                }).indexOf(moment(theDay).format('YYYY-MM-DD'));
                return i === -1;
            },
            onOpen: function () {
                this.setDate(getDate());
            },
            i18n: {
                previousMonth: 'Предыдущий месяц',
                nextMonth: 'Следующий месяц',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
            }
        };
        this.initPikaday(datepickerConfig);

        //по дефолту устанавливаем на ближайшую дату
        this.getPikaday().setDate(getAvailableDates[0].value);
    }

    openReservationModal() {
        $('#reservationBanquetModal').modal('show');
    }

    //График
    renderSelectSchedule() {
        if (this.getScheduleList() && this.props.availableDates) {
            if (this.getScheduleTime().readyState === establishmentsAction.ESTABLISHMENT_BANQUET_SCHEDULE_TIME_FETCHING)
                return <LoaderView />;
            var s = (
                <option value='default'>Выберите время</option>
            );
            var value = this.getReservation().time ? this.getReservation().time : 'default';
            return (
                <select className="form-control" id="selectBanquetTime" value={value} onChange={(e) => this.handleSetTime(e)}>
                    {s}
                    {this.getScheduleList().map((time, i) => (
                        <option key={i} value={time.time} >{time.time}</option>
                    ))}
                </select>
            );
        }
        return;
    }

    //Output select
    renderSelectPersons() {
        let options = [];
        for (var i = CONFIG.reservation.banquetPeopleMin; i <= CONFIG.reservation.banquetPeopleMax; i++) {
            options.push(i);
        }
        return (
            <select className="form-control date-and-people-block__people " value={this.getReservation().people} onChange={(e) => this.handleSetPeople(e)}>
                {options.map((m, i) => (
                    <option key={i} value={m}>{m}</option>
                ))}
            </select>
        );

    }

    //Изменение бюджета
    handleSetBudget(e) {
        this.handleSetReservation({ budget: e.target.value });
    }

    //Изменение доп параметра "Праздник под ключ"
    handleSetBasis(e) {
        this.handleSetReservation({ paramsBasis: e.target.checked });
    }

    //Изменение доп параметра "Свой алкоголь"
    handleSetAlcohol(e) {
        this.handleSetReservation({ paramsAlcohol: e.target.checked });
    }

    /**
     * Задаёт формат мероприятия
     * @param {event} e
     */
    handleSetFormat(e) {
        //Предотвращает излишние изменения состояния
        var v = e.target.value;
        if (v != this.getReservation().format)
            this.handleSetReservation({ format: v != 'default' ? v : false });
    }

    render() {

        if (DEVELOPMENT) console.log('EstablishmentPage => ReviewsReservationView: Render');

        const date = moment(this.getReservation().date).format('dddd, D MMM');

        var banquetPrice = this.props.banquetPrice;
        var budget = this.getReservation().budget ? this.getReservation().budget : banquetPrice;
        return (
            <div className="banquet-wrap fl fl-dr-col fl-in-di">
                <div className="banquet-body">
                    <div className="banquet-form-list full-width fl fl-in-di">
                        <div className="banquet-form-item">
                            <div id="rcrbv-piker_trigger" className="form-group" >
                                <label className="control-label" htmlFor="rcrbv-piker">Дата</label>
                                <div id="rcrbv-piker" className="form-control form-control--date-right date-and-people-block__stable" >{date}</div>
                                <input id="rcrbv-piker_temp" type="text" hidden />
                            </div>
                        </div>
                        <div className="banquet-form-item">
                            <div className="form-group" >
                                <label className="control-label">Количество персон</label>
                                {this.renderSelectPersons()}
                            </div>
                        </div>
                        <div className="banquet-form-item">
                            <div className="form-group" >
                                <label className="control-label" htmlFor="selectBanquetTime">Время</label>
                                {this.renderSelectSchedule()}
                            </div>
                        </div>
                        <div className="banquet-form-item">
                            <div className="form-group" >
                                <label className="control-label" htmlFor="inputBanquetBudget">Бюджет на человека</label>
                                <input type="number" className="form-control" id="inputBanquetBudget" value={budget} onChange={(e) => this.handleSetBudget(e)} placeholder={banquetPrice} min={banquetPrice} step="100" />
                            </div>
                        </div>
                        <div className="banquet-form-item">
                            <div className="form-group" >
                                <label className="control-label">Формат мероприятия</label>
                                <SelectElement list={CONFIG.reservation.format} selectedValue='Выберите мероприятие' value={this.getReservation().format} onChange={this.handleSetFormat.bind(this)} />
                            </div>
                        </div>
                        <div className="banquet-form-item fl fl-dr-col fl-jc-r">
                            <CheckboxElement
                                checked={this.getReservation().paramsBasis}
                                value="Праздник под ключ"
                                onChange={this.handleSetBasis.bind(this)} />
                            <CheckboxElement
                                checked={this.getReservation().paramsAlcohol}
                                value="Свой алкоголь"
                                onChange={this.handleSetAlcohol.bind(this)} />
                        </div>
                    </div>

                </div>
                <a href="javascript:;" onClick={() => this.openReservationModal()} className="reservation-button">Забронировать</a>
            </div >
        );
    }
}

const mapStateToProps = ({ ui, establishments }) => ({
    reservation: ui.reservationBanquet,
    tempBanquetScheduleTime: establishments.tempBanquetScheduleTime
});

export default connect(mapStateToProps)(ReviewsReservationBanquetView);