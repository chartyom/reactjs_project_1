import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as uiAction from '../../../actions/uiAction';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import { loadScript, getNumEnding } from '../../../utils/Tools';
import ReservationInfoLayout from '../../layouts/modalWindows/ReservationInfoLayout';
import BootstrapModalWindow from '../../modalWindows/BootstrapModalWindow';
import moment from 'moment';
import CONFIG from '../../config';
import InputElement from 'react-input-mask';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import matchesValidator from 'validator/lib/matches';
import isEmailValidator from 'validator/lib/isEmail';
import ScheduleTimeListLayout from '../../layouts/ScheduleTimeListLayout';
import LoaderView from '../../widgets/LoaderView';
import SelectElement from '../../elements/form/select';
import CheckboxElement from '../../elements/form/checkbox';
/**
 * Модальное окно по бронированию столиков в заведении
 * @param {string} place наименование заведения
 * @param {string} placeId id заведения
 * @param {array} schedule график работы заведения
 * @param {number} banquetPrice
 * @param {array} availableDates
 */
class ReservationBanquetModalView extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            pikaday: null,
            timeValid: null,
            formatValid: null,
            phoneNumberValid: null,
            phoneNumberValue: null,
            phoneNumberCodeValid: null,
            phoneNumberCodeValue: null,
            phoneNumberCodeInput: 1,
            usernameValid: null,
            usernameValue: null,
            emailValid: null,
            emailValue: null,
            emailValidDescription: null,
            refCodeValid: null,
            refCodeValue: null,
            descriptionValue: null
        };
    }


    componentDidMount() {
        loadScript("/temp/js/pikaday.js")
            .then(() => this.initDatePicker())
            .catch(e => { if (DEVELOPMENT) console.log(e); });
    }

    /*
      ===== BEGIN GERRERS =====
    */

    getReservation() {
        return this.props.reservation;
    }

    //Получить хранимые значение выбранного времени из REDUX
    getScheduleTime() {
        return this.props.tempBanquetScheduleTime;
    }

    //Время бронирования
    getScheduleList() {
        //Вывод нового списка времени бронирования
        if (this.getScheduleTime() && this.getScheduleTime().list)
            return this.getScheduleTime().list;
        return this.props.schedule;
    }

    /*
      ===== END GERRERS =====
    */


    /*
      ===== BEGIN HANDLE ACTION =====
    */

    handleSetReservation(d) {
        this.props.dispatch(uiAction.setReservationBanquet(d));
    }

    /**
     * Задаёт дату
     * @param {date} date 
     */
    handleSetDate(date) {
        //Предотвращает излишние изменения состояния
        //Преобразование через moment потому, что date имеет значение начала дня, а this.getReservation().date текущее время дня
        if (moment(date).format('MMDD') != moment(this.getReservation().date).format('MMDD')) {
            this.handleSetReservation({ date: date, time: false });
            this.props.dispatch(establishmentsAction.setBanquetScheduleTime(this.props.placeId, moment(date).format('YYYY-MM-DD')));
        }
    }


    /**
     * Задаёт количество персон
     * @param {event} e
     */
    handleSetPeople(e) {
        //Предотвращает излишние изменения состояния
        if (e.target.value != this.getReservation().people)
            this.handleSetReservation({ people: e.target.value });
    }

    /**
    * Отправка запроса на подтверждение номера
    * @param {string} phoneNumber 
    * @param {number} phoneNumberCode 
    */
    handleSetPhoneNumberCode(phoneNumber, phoneNumberCode) {
        this.handleSetReservation({ phoneNumber: phoneNumber, phoneNumberCode: phoneNumberCode });
    }


    /**
    * Очищает все заданные поля
    */
    handleSetClear() {
        this.setState({
            phoneNumberCodeValid: null,
            phoneNumberCodeValue: null,
        });
        this.props.dispatch(uiAction.setReservationBanquetClear());
    }

    /*
      ===== END HANDLE ACTION =====
    */




    /*
        ===== BEGIN HANDLE PIKADAY =====
    */


    initPikaday(opt) {
        this.setState({
            pikaday: new Pikaday(opt)
        });
    }

    getPikaday() {
        return this.state.pikaday;
    }

    initDatePicker() {

        const c = document.getElementsByClassName('rcrbvm-piker_window');
        while (c.length > 0) {
            c[0].parentNode.removeChild(c[0]);
        }

        const setDate = (date) => this.handleSetDate(date);
        const getDate = () => this.getReservation().date;
        //массив с датами начала брони
        var getAvailableDates = this.props.availableDates;
        const w = document.getElementById('rcrbvm-piker_temp');
        const t = document.getElementById('rcrbvm-piker_trigger');
        if (!w || !t) return;
        // Config
        var datepickerConfig = {
            field: w,
            trigger: t,
            showDaysInNextAndPreviousMonths: true,
            theme: 'rcrbvm-piker_window',
            defaultDate: this.getReservation().date,
            minDate: moment().toDate(),
            maxDate: moment().add(28, 'days').toDate(),
            firstDay: 1,
            onSelect: function () {
                if (!w) return;
                setDate(this._d);
            },
            disableDayFn: function (theDay) {
                var i = getAvailableDates.map(function (obj) {
                    return obj.value;
                }).indexOf(moment(theDay).format('YYYY-MM-DD'));
                return i === -1;
            },
            onOpen: function () {
                this.setDate(getDate());
            },
            i18n: {
                previousMonth: 'Предыдущий месяц',
                nextMonth: 'Следующий месяц',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
            }
        };

        this.initPikaday(datepickerConfig);

        //по дефолту устанавливаем на ближайшую дату
        this.getPikaday().setDate(getAvailableDates[0].value);
    }

    /*
        ===== END HANDLE PIKADAY =====
    */


    /*
      ===== BEGIN RESERVATION MODAL =====
    */

    //actions
    openReservationModal() {
        this.refs.reservationBanquetModal.open();
    }
    confirmReservationModal() {
        this.refs.reservationBanquetModal.close();
    }

    //handle actions
    handleReservationModalCancel() {
        this.refs.reservationBanquetModal.close();
    }
    handleReservationModalHidden() {
        this.handleSetClear();
    }
    handleReservationModalShown() {
        //Предотвращает излишние изменения состояния
        if (this.props.placeId != this.getReservation().establishmentId && this.props.place != this.getReservation().establishment) {
            var budget = this.getReservation().budget ? this.getReservation().budget : this.props.banquetPrice;
            this.handleSetReservation({ establishmentId: this.props.placeId, establishment: this.props.place, budget: budget });
        }

    }

    /*
      ===== END RESERVATION MODAL =====
    */



	/*
	  ===== BEGIN RENDER RESERVATION =====
	*/

    renderSteps() {
        if (!this.getReservation().confirmSelectData) return;

        if (!this.getReservation().confirmUserInfo) {
            //TODO Показывать если пользователь не авторизован
            return this.renderInputUserInfo();
        }
        if (!this.getReservation().confirmPhoneNumber) {
            //TODO Показывать если пользователь не авторизован
            return this.renderUserConfirm();
        }
        if (!this.getReservation().confirmRefCode) {
            //TODO Показывать если пользователь не авторизован и он является новым пользователем
            return this.renderInputRefCode();
        }
        return this.renderSuccessReservation();

    }

    renderReservation() {

        //renderSelectData Необходимо отображать всегда, так как используется Pikaday, который инициализируется при загрузке
        //Если DOM обновится, то datepiker сломается...

        return (
            <div className="rnmlfm">
                {/*{this.renderInputRefCode()}*/}
                {this.renderSelectData()}
                {this.renderSteps()}
            </div>
        );
    }

    //STEP 1

    //Изменение бюджета
    handleSetBudget(e) {
        this.handleSetReservation({ budget: e.target.value });
    }

    //Изменение доп параметра "Праздник под ключ"
    handleSetBasis(e) {
        this.handleSetReservation({ paramsBasis: e.target.checked });
    }

    //Изменение доп параметра "Свой алкоголь"
    handleSetAlcohol(e) {
        this.handleSetReservation({ paramsAlcohol: e.target.checked });
    }

    /**
     * Задаёт формат мероприятия
     * @param {event} e
     */
    handleSetFormat(e) {
        //Предотвращает излишние изменения состояния
        var v = e.target.value;
        if (v != this.getReservation().format) {
            if (v != 'default') {
                this.setState({ formatValid: null });
            }
            this.handleSetReservation({ format: (v != 'default' ? v : false) });
        }
    }

    /**
    * Задаёт время
    * @param {string} e 
    */
    handleSetTime(e) {
        var v = e.target.value;
        if (v != this.getReservation().time) {
            v = v != 'default' ? v : false;
            var bonus = false;
            var bonusType = false;
            if (v) {
                this.setState({ timeValid: null });

                for (var i = 0; i < this.getScheduleList().length; i++) {
                    if (
                        this.getScheduleList()[i].time == v &&
                        this.getScheduleList()[i].bonus &&
                        (this.getScheduleList()[i].bonus.rBonusValue || this.getScheduleList()[i].bonus.rBonusPercent)
                    ) {
                        bonusType = this.getScheduleList()[i].bonus.rBonusValue ? 'value' : 'percent';
                        bonus = this.getScheduleList()[i].bonus.rBonusValue
                            ? this.getScheduleList()[i].bonus.rBonusValue
                            : this.getScheduleList()[i].bonus.rBonusPercent;
                    }
                }
            }
            this.handleSetReservation({ time: v, bonus, bonusType });
        }
    }



    //График
    renderSelectSchedule() {
        if (this.getScheduleList() && this.props.availableDates) {
            if (this.getScheduleTime().readyState === establishmentsAction.ESTABLISHMENT_BANQUET_SCHEDULE_TIME_FETCHING)
                return <LoaderView />;
            var s = (
                <option value='default'>Выберите время</option>
            );
            var value = this.getReservation().time ? this.getReservation().time : 'default';
            return (
                <select className="form-control" id="selectBanquetTimeModal" value={value} onChange={(e) => this.handleSetTime(e)}>
                    {s}
                    {this.getScheduleList().map((time, i) => (
                        <option key={i} value={time.time} >{time.time}</option>
                    ))}
                </select>
            );
        }
        return;
    }

    handleConfirmSelectData() {
        if (this.getReservation().time && this.getReservation().format && this.getReservation().budget >= this.props.banquetPrice)
            return this.handleSetReservation({ confirmSelectData: true });
        this.setState({ timeValid: false, formatValid: false });
    }


    //Output select
    renderSelectPersons() {
        let options = [];
        for (var i = CONFIG.reservation.banquetPeopleMin; i <= CONFIG.reservation.banquetPeopleMax; i++) {
            options.push(i);
        }
        return (
            <select className="form-control control-elements__item__people" value={this.getReservation().people} onChange={(e) => this.handleSetPeople(e)}>
                {options.map(option => (
                    <option key={option} value={option}>{option}</option>
                ))}
            </select>
        );

    }

    renderSelectData() {
        const date = moment(this.getReservation().date).format('dddd, D MMM');
        var banquetPrice = this.props.banquetPrice;
        var budget = this.getReservation().budget ? this.getReservation().budget : banquetPrice;
        const timeValidClass = this.state.timeValid
            ? " has-success"
            : this.state.timeValid === null
                ? ""
                : " has-error";
        const formatValidClass = this.state.formatValid
            ? " has-success"
            : this.state.formatValid === null
                ? ""
                : " has-error";
        return (
            <div className={"rnmlfm__select" + (this.getReservation().confirmSelectData ? " dn" : "")} >
                <div className="rnmlfm__header rnmlfm__header--no-border">
                    <h3 className="rnmlfm__header__title">Бронирование банкета</h3>
                </div>
                <div className="rnmlfm__select__item rnmlfmsi fl fl-in-di">
                    <div className="rnmlfmsi__item">
                        <div id="rcrbvm-piker_trigger" className="form-group" >
                            <label className="control-label" htmlFor="rcrbvm-piker">Дата</label>
                            <div id="rcrbvm-piker"className="form-control form-control--date-right date-and-people-block__datepicker date-and-people-block__stable">{date}</div>
                            <input id="rcrbvm-piker_temp" type="text" hidden />
                        </div>
                    </div>
                    <div className="rnmlfmsi__item">
                        <div className="form-group" >
                            <label className="control-label">Количество персон</label>
                            {this.renderSelectPersons()}
                        </div>
                    </div>
                    <div className="rnmlfmsi__item">
                        <div className={"form-group" + timeValidClass} >
                            <label className="control-label" htmlFor="selectBanquetTimeModal">Время</label>
                            {this.renderSelectSchedule()}
                        </div>
                    </div>
                    <div className="rnmlfmsi__item">
                        <div className="form-group" >
                            <label className="control-label" htmlFor="inputBanquetBudgetModal">Бюджет на человека</label>
                            <input type="number" className="form-control" id="inputBanquetBudgetModal" value={budget} onChange={(e) => this.handleSetBudget(e)} placeholder={banquetPrice} min={banquetPrice} step="100" />
                        </div>
                    </div>
                    <div className="rnmlfmsi__item">
                        <div className={"form-group" + formatValidClass} >
                            <label className="control-label" >Формат мероприятия</label>
                            <SelectElement list={CONFIG.reservation.format} selectedValue='Выберите мероприятие' value={this.getReservation().format} onChange={this.handleSetFormat.bind(this)} />
                        </div>
                    </div>
                    <div className="rnmlfmsi__item fl fl-dr-col fl-jc-r">
                        <CheckboxElement
                            checked={this.getReservation().paramsBasis}
                            value="Праздник под ключ"
                            onChange={this.handleSetBasis.bind(this)} />
                        <CheckboxElement
                            checked={this.getReservation().paramsAlcohol}
                            value="Свой алкоголь"
                            onChange={this.handleSetAlcohol.bind(this)} />
                    </div>
                    <div className="rnmlfmsi__footer">
                        <button type="button" onClick={() => this.handleConfirmSelectData()} className={"btn btn-success"}>Выбрать</button>
                        <button type="button" onClick={() => this.handleReservationModalCancel()} className="btn btn-default">Отмена</button>
                    </div>
                </div>

            </div>
        );
    }

    //HANDLE PHONE NUMBER

    handlePhoneValid(e) {
        const v = e.target.value;
        if (v.length > 0) {
            if (matchesValidator(v, CONFIG.form.phoneNumberRegExp)) {
                return this.setState({ phoneNumberValid: true, phoneNumberValue: v });
            }
        }
        if (this.state.phoneNumberValid !== null)
            this.setState({ phoneNumberValid: null });
    }

    //HANDLE USERNAME

    handleUsernameValid(e) {
        const v = e.target.value;
        if (v.length > 1) {
            return this.setState({ usernameValid: true, usernameValue: v });
        }
        if (this.state.usernameValid !== null)
            this.setState({ usernameValid: null });
    }

    //HANDLE EMAIL

    handleEmailValid(e) {
        const v = e.target.value;
        if (v.length > 0) {
            if (isEmailValidator(v)) {
                return this.setState({ emailValid: true, emailValidDescription: null, emailValue: v });
            }
            if (this.state.emailValid !== false)
                return this.setState({ emailValid: false, emailValidDescription: "Email адрес введен не верно" });
            return;
        }
        if (this.state.emailValid !== null)
            this.setState({ emailValid: null, emailValidDescription: null });
    }

    handleConfirmUserInfo() {
        if (this.state.phoneNumberValid && this.state.usernameValid && (this.state.emailValid === null || this.state.emailValid)) {
            var d = this.refs.inputDescription.value;
            this.setState({ descriptionValue: d });
            this.handleSetReservation({
                username: this.state.usernameValue,
                phoneNumber: this.state.phoneNumberValue,
                email: this.state.emailValue,
                description: d,
                confirmUserInfo: true
            });
        }
    }

    handleReturnToSelectData() {
        this.handleSetReservation({ confirmSelectData: false });
    }

    //STEP 2

    renderInputUserInfo() {

        var submitClass = '';

        if (this.getReservation().readyState === uiAction.UI_RESERVATION_BANQUET_FETCHING) {
            submitClass = ' btn-loading';
        }

        if (this.getReservation().readyState === uiAction.UI_RESERVATION_BANQUET_FETCH_FAILED) {
            switch (this.getReservation().error.statusCode) {
                case 500:
                    return <BadServer />;
                default:
                    return <BadConnection handleButton={this.handleConfirmUserInfo.bind(this)} titleButton={"Попробовать еще раз"} />;
            }
        }

        const phoneNumberValidClass = this.state.phoneNumberValid
            ? " has-success"
            : this.state.phoneNumberValid === null
                ? ""
                : " has-error";
        const phoneNumberValue = this.state.phoneNumberValue;

        const usernameValidClass = this.state.usernameValid
            ? " has-success"
            : this.state.usernameValid === null
                ? ""
                : " has-error";
        const usernameValue = this.state.usernameValue;

        const emailValidClass = this.state.emailValid
            ? " has-success"
            : this.state.emailValid === null
                ? ""
                : " has-error";
        const emailValue = this.state.emailValue;

        const descriptionValue = this.state.descriptionValue;

        const submitEnable = (
            this.state.phoneNumberValid
            && this.state.usernameValid
            && (this.state.emailValid === null || this.state.emailValid)
        );

        return (
            <div className="rnmlfm__confirm">
                <div className="rnmlfm__header rnmlfm__header--no-border">
                    <h3 className="rnmlfm__header__title">Бронирование банкета</h3>
                </div>
                <ReservationInfoLayout
                    className={'rnmlfm'}
                    place={this.getReservation().establishment}
                    date={moment(this.getReservation().date).format('D MMMM, \r dddd')}
                    time={this.getReservation().time}
                    people={this.getReservation().people}
                    budget={this.getReservation().budget}
                    format={this.getReservation().format}
                    paramsBasis={this.getReservation().paramsBasis}
                    paramsAlcohol={this.getReservation().paramsAlcohol}
                />
                <div className="rnmlfm__confirm__body">
                    <div className={"form-group" + usernameValidClass} onChange={(e) => this.handleUsernameValid(e)}>
                        <label className="control-label" htmlFor="inputUsername">Имя <span className="require" title="Обязательное поле">*</span></label>
                        <input type="text" className="form-control" id="inputUsername" defaultValue={usernameValue} placeholder="Введите ваше имя" />
                    </div>
                    <div className={"form-group" + phoneNumberValidClass}>
                        <label className="control-label" htmlFor="inputPhoneNumber">Номер телефона <span className="require" title="Обязательное поле">*</span></label>
                        <InputElement type="text" className="form-control" defaultValue={phoneNumberValue} id="inputPhoneNumber" onChange={(e) => this.handlePhoneValid(e)} placeholder={CONFIG.form.phoneNumberPlaceholder} mask={CONFIG.form.phoneNumberMask} />
                        <span className="form-group__description">На ваш номер придет SMS с кодом подтверждения</span>
                    </div>
                    <div className={"form-group" + emailValidClass} >
                        <label className="control-label" htmlFor="inputEmail">E-mail</label>
                        <input type="email" className="form-control" id="inputEmail" defaultValue={emailValue} onChange={(e) => this.handleEmailValid(e)} placeholder="Введите ваш адрес электронной почты" />
                        <div className="help-block">{this.state.emailValidDescription}</div>
                    </div>
                    <div className="form-group" >
                        <label className="control-label" htmlFor="inputDescription">Пожелания</label>
                        <textarea ref="inputDescription" className="form-control rnmlfm__confirm__body__form_textarea" id="inputDescription" placeholder="Укажите Ваши пожелания" >{descriptionValue}</textarea>
                    </div>
                </div>
                <div className="rnmlfm__confirm__footer">
                    <button type="button" disabled={!submitEnable} onClick={() => this.handleConfirmUserInfo()} className={"btn btn-success" + submitClass}>Забронировать</button>
                    <button type="button" onClick={() => this.handleReturnToSelectData()} className="btn btn-default">Изменить</button>
                </div>
            </div>

        );
    }

    //STEP 3 CONFIRM PHONE NUMBER


    //HANDLE PHONE CONFIRM CODE

    handlePhoneNumberCodeValid(e) {
        const v = e.target.value;
        if (matchesValidator(v, CONFIG.form.phoneNumberCodeRegExp)) {
            this.setState({ phoneNumberCodeValid: true, phoneNumberCodeValue: v });

            setTimeout(() => this.handlePhoneNumberConfirm(), 100);
        }
    }

    //HANDLE NEXT STEP

    handlePhoneNumberConfirm() {
        if (this.getReservation().phoneNumber && this.state.phoneNumberCodeValid && this.state.phoneNumberCodeValue) {
            this.handleSetPhoneNumberCode(this.getReservation().phoneNumber, this.state.phoneNumberCodeValue);
        }
    }


    handleGetNewCode() {
        if (this.state.phoneNumberCodeInput < 4) {
            if (this.state.phoneNumberCodeInput === 3)
                setTimeout(function () { this.setState({ phoneNumberCodeInput: 1 }); }.bind(this), 60000);
            this.setState({ phoneNumberCodeInput: this.state.phoneNumberCodeInput + 1 });

            //TODO go
            //console.log(this.getReservation().token, this.getReservation().phoneNumber);
            uiAction.getNewPhoneNumberCode(this.getReservation().token, this.getReservation().phoneNumber);
        }
    }
    //HANDLE BACK STEP

    handleGoToInputUserInfo() {
        this.handleSetReservation({ confirmUserInfo: false });
    }

    renderUserConfirm() {

        var submitClass = '';
        var submitSMSEnable = true;

        if (this.getReservation().readyState === uiAction.UI_RESERVATION_BANQUET_FETCHING) {
            submitSMSEnable = false;
            submitClass = ' btn-loading';
        }

        var phoneNumberCodeValidClass = '';
        var phoneNumberCodeDescription = '';

        if (this.getReservation().readyState === uiAction.UI_RESERVATION_BANQUET_FETCH_FAILED) {
            switch (this.getReservation().error.statusCode) {
                case 500:
                    phoneNumberCodeValidClass = ' has-error';
                    phoneNumberCodeDescription = 'Код подтверждения введен не верно';
                    break;
                default:
                    return <BadConnection handleButton={() => this.handlePhoneNumberConfirm()} titleButton={"Попробовать еще раз"} />;
            }
        }
        if (this.state.phoneNumberCodeInput > 3)
            submitSMSEnable = false;
        const submitEnable = this.state.phoneNumberCodeValid;

        return (
            <div className="rnmlfm__confirm">
                <div className="rnmlfm__header">
                    <h3 className="rnmlfm__header__title">Подтверждение номера</h3>
                </div>
                <div className="rnmlfm__confirm__body">
                    <div className="form-group  form-group--center" >
                        Вам выслан код подтверждения
                    </div>
                    <div className={"form-group form-group--center" + phoneNumberCodeValidClass}>
                        <label className="control-label" htmlFor="inputCode">Введите код из SMS</label>
                        <InputElement type="text" className="form-control" id="inputCode" onChange={(e) => this.handlePhoneNumberCodeValid(e)} placeholder={CONFIG.form.phoneNumberCodePlaceholder} mask={CONFIG.form.phoneNumberCodeMask} maskChar={CONFIG.form.phoneNumberCodeMaskChar} />
                        <div className="help-block">{phoneNumberCodeDescription}</div>
                    </div>
                    <div className="form-group  form-group--center" >
                        <button type="button" disabled={!submitSMSEnable} onClick={() => this.handleGetNewCode()} className={"btn btn-link " + submitClass}>Выслать новый код</button>
                    </div>
                </div>
                <div className="rnmlfm__confirm__footer">
                    <button type="button" disabled={!submitEnable} onClick={() => this.handlePhoneNumberConfirm()} className={"btn btn-success " + submitClass}>Продолжить</button>
                    <button type="button" onClick={() => this.handleGoToInputUserInfo()} className="btn btn-default">Назад</button>
                </div>

            </div>
        );
    }

    //STEP 4 REFERAL CODE

    //HANDLE PHONE CONFIRM CODE

    handleRefCodeConfirmValid(e) {
        const v = e.target.value;
        if (v.length > 5) {
            if (matchesValidator(v, '[0-9A-Za-z]{6}')) {
                this.setState({ refCodeValid: true, refCodeValue: v.toUpperCase() });
                setTimeout(() => this.handleRefCodeConfirm(), 100);
            }
        } else if (this.state.refCodeValid === true) {
            this.setState({ refCodeValid: false });
        }
    }

    //HANDLE CONFIRM STEP

    handleRefCodeConfirm() {
        if (this.state.refCodeValid && this.state.refCodeValue) {
            this.handleSetReservation({ refCode: this.state.refCodeValue });
        }
    }

    //HANDLE SKIP STEP

    handleRefCodeSkip() {
        this.handleSetReservation({ confirmRefCode: true });
    }


    renderInputRefCode() {

        var submitClass = '';

        if (this.getReservation().readyState === uiAction.UI_RESERVATION_BANQUET_FETCHING) {
            submitClass = ' btn-loading';
        }

        var refCodeValidClass = '';
        var refCodeDescription = '';

        if (this.getReservation().readyState === uiAction.UI_RESERVATION_BANQUET_FETCH_FAILED) {
            switch (this.getReservation().error.statusCode) {
                case 400:
                    refCodeValidClass = ' has-error';
                    refCodeDescription = 'Реферальный код введен не верно';
                    break;
                case 500:
                    return <BadServer />;
                default:
                    return <BadConnection />;
            }
        }

        const submitEnable = this.state.refCodeValid;

        return (
            <div className="rnmlfm__confirm">
                <div className="rnmlfm__header">
                    <h3 className="rnmlfm__header__title">Реферальный код</h3>
                </div>
                <div className="rnmlfm__confirm__body">
                    <div className={"form-group form-group--center" + refCodeValidClass}>
                        <label className="control-label" htmlFor="inputRefCode">Реферальный код</label>
                        <InputElement type="text" className="form-control" id="inputRefCode" onChange={(e) => this.handleRefCodeConfirmValid(e)} placeholder={CONFIG.form.refCodePlaceholder} mask={CONFIG.form.refCodeMask} maskChar={CONFIG.form.refCodeMaskChar} />
                        <div className="help-block">{refCodeDescription}</div>
                    </div>
                </div>
                <div className="rnmlfm__confirm__footer">
                    <button type="button" disabled={!submitEnable} onClick={() => this.handleRefCodeConfirm()} className={"btn btn-primary " + submitClass}>Подтвердить</button>
                    <button type="button" onClick={() => this.handleRefCodeSkip()} className="btn btn-default">Пропустить</button>
                </div>

            </div>
        );
    }

    //STEP 5

    renderSuccessReservation() {

        return (
            <div className="rnmlfm__confirm">
                <div className="rnmlfm__confirm__header">
                    <div className="rnmlfm__confirm__header__img">
                    </div>
                    <div className="rnmlfm__confirm__header__value">
                        Ваше бронирование<br />
                        успешно отправлено!
                    </div>
                </div>
                <ReservationInfoLayout
                    className={'rnmlfm'}
                    place={this.getReservation().establishment}
                    date={moment(this.getReservation().date).format('D MMMM, \r dddd')}
                    time={this.getReservation().time}
                    people={this.getReservation().people}
                />
                <div className="rnmlfm__confirm__body">
                    <div className="rnmlfm__confirm__body__text">Ожидайте SMS подтверждения</div>
                </div>
                <div className="rnmlfm__confirm__footer rnmlfm__confirm__footer--center">
                    <button type="button" onClick={() => this.confirmReservationModal()} className="btn btn-success rnmlfm__confirm__footer__button-thanks">Спасибо</button>
                </div>
            </div>
        );
    }




    /*
      ===== END RENDER RESERVATION =====
    */

    render() {

        if (DEVELOPMENT) console.log('EstablishmentPage => ReservationBanquetModalView: Render');

        return (
            <BootstrapModalWindow
                ref="reservationBanquetModal"
                modalId="reservationBanquetModal"
                onCancel={this.handleReservationModalCancel.bind(this)}
                onConfirm={this.confirmReservationModal.bind(this)}
                onShown={this.handleReservationModalShown.bind(this)}
                onHidden={this.handleReservationModalHidden.bind(this)}
                bodyClass="modal-body--no-padding"
                sizeClass="modal-size--md-sm"
            >
                {this.renderReservation()}
            </BootstrapModalWindow>
        );
    }

}

ReservationBanquetModalView.propTypes = {
    reservation: PropTypes.shape({}).isRequired,
    place: PropTypes.string.isRequired,
    placeId: PropTypes.string.isRequired,
};

const mapStateToProps = ({ ui, establishments }) => ({
    reservation: ui.reservationBanquet,
    tempBanquetScheduleTime: establishments.tempBanquetScheduleTime
});

export default connect(mapStateToProps)(ReservationBanquetModalView);