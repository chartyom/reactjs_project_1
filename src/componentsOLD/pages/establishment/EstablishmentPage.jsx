import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import { withRouter } from 'react-router';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import PlaceInsideView from './PlaceInsideView';
import AboutView from './AboutView';
import GalleryView from './GalleryView';
import ReviewsView from './ReviewsView';
import MapView from './MapView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';
//import BannerBitkomAppWidget from '../../widgets/BannerBitkomApp';
import CONFIG from '../../config';
import { loadScript, cropText } from '../../../utils/Tools';
import ReservationModalView from './ReservationModalView';
import ReservationBanquetModalView from './ReservationBanquetModalView';

export const LINK_PAGE = '';
export const LINK_MASK = '/:type/:name/:id';

class EstablishmentPage extends PureComponent {

  static readyOnActions(dispatch, params) {
    return Promise.all([
      dispatch(establishmentsAction.getById(params.id)),
    ]);
  }


  componentDidMount() {
    EstablishmentPage.readyOnActions(this.props.dispatch, this.props.params);
  }

  /**
   * Загрузка скриптов с проверкой загрузки
   */
  Loader() {
    loadScript("https://maps.googleapis.com/maps/api/js?key=" + CONFIG.map.key)
      .then(() => {
        this.initGoogleMap();
      })
      .catch(e => { if (DEVELOPMENT) console.log(e); });
  }

  getData() {
    return this.props.establishment;
  }

  renderContentLoading() {
    let layout = (classes, headerTitle) => (
      <section className={classes}>
        <div className="header-container header-container_bottom-line header-container--paragraph">
          <div className="center">
            <div className="full-width fl fl-in-di">
              <div>
                <h3 className="header-container__header">{headerTitle}</h3>
              </div>
            </div>
          </div>
        </div>
        <div className="center">
          <LoaderView />
        </div>
      </section>
    );

    return (
      <div>
        <section className="place-inside-main-block">
          <LoaderView />
        </section>
        {layout("place-full-information", "О заведении")}
        {layout("gallery", "Галерея")}
        {layout("reviews", "Отзывы")}
      </div>
    );
  }

  onUpdate() {
    this.props.dispatch(establishmentsAction.getById(this.props.params.id));
  }


  /*
    ===== BEGIN GOOGLE MAP =====
  */

  /*
   Инициализация google карты
   */
  initGoogleMap() {
    var location = this.getData().address.location;

    const POSITION = new google.maps.LatLng(location.lat, location.lon);

    var map = new google.maps.Map(document.getElementById('map-place'), {
      center: POSITION,
      scrollwheel: false,
      zoom: 16,
      styles: CONFIG.map.style,
      zoomControl: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false
    });

    CustomMarker.prototype = new google.maps.OverlayView();

    var marker = new CustomMarker(POSITION, map, this.getData().type, this.getData().status);

    function CustomMarker(latlng, map, type, status) {
      this.latlng_ = latlng;
      this.type_ = type || 'restaurant';
      this.status_ = status || 'close';
      this.setMap(map);
    }

    CustomMarker.prototype.draw = function () {
      var me = this;
      var div = this.div_;
      if (!div) {
        div = this.div_ = document.createElement('DIV');
        div.style.border = "none";
        div.style.position = "absolute";
        div.style.paddingLeft = "0px";
        div.style.cursor = 'pointer';
        //you could/should do most of the above via styling the class added below
        div.classList.add('map-marker', 'map-marker_status--' + this.status_, 'map-marker_type--' + this.type_);

        google.maps.event.addDomListener(div, "click", function (event) {
          google.maps.event.trigger(me, "click");
        });

        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
      }

      var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
      if (point) {
        div.style.left = point.x + 'px';
        div.style.top = point.y + 'px';
      }
    };

    CustomMarker.prototype.remove = function () {
      if (this.div_) {
        this.div_.parentNode.removeChild(this.div_);
        this.div_ = null;
      }
    };

  }

  /*
    ===== END GOOGLE MAP =====
  */


  handleAddToFavorites() {
    // if (document.all) window.external.addFavorite(window.location.href, this.getSiteTitle() + ' - Битком24');
    if (document.all && !window.opera) {
      if (typeof window.external == "object") {
        window.external.AddFavorite(document.location, document.title); return false;
      } else return false;
    } else {
      var ua = navigator.userAgent.toLowerCase();
      var isWebkit = (ua.indexOf('webkit') != - 1);
      var isMac = (ua.indexOf('mac') != - 1);

      if (isWebkit || isMac) {
        alert('Нажмите сочетание клавиш:    "' + (isMac ? 'Command/Cmd' : 'CTRL') + ' + D" - для добавления сайта в закладки');

        return false;
      }
      else {
        x.href = document.location;
        x.title = document.title;
        x.rel = "sidebar";
        return true;
      }
    }
  }

  /*
  ===== BEGIN RENDER ESTABLISHMNENTS =====
  */

  getSiteTitle() {
    return this.getData() && this.getData().name ? this.getData().mapType + ' ' + this.getData().name : '';
  }

  renderEstablishment() {
    const establishment = this.getData();

    if (
      !establishment ||
      establishment.readyState === establishmentsAction.ESTABLISHMENT_FETCHING
    ) {
      return this.renderContentLoading();
    }

    if (establishment.readyState === establishmentsAction.ESTABLISHMENT_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('Failed to fetch => show error: ', establishment.error);
      switch (establishment.error.statusCode) {
        case 404:
          return <ErrorPageView />;
        case 500:
          return <BadServer />;
        default:
          return <BadConnection />;
      }
    }

    this.Loader();

    const PAGE_NAME = this.getSiteTitle();
    const PAGE_DESCRIPTION = this.getData() && this.getData().longDesc ? cropText(this.getData().longDesc, 255) : '';
    // const PAGE_KEYWORDS = this.getData() && this.getData().name ? this.getData().name : '';

    return (
      <div>
        <Helmet
          title={PAGE_NAME}
          meta={[
            { name: 'description', content: PAGE_DESCRIPTION },
          ]}
        />
        <PlaceInsideView
          establishment={establishment}
          params={this.props.params}
          handleAddToFavorites={this.handleAddToFavorites.bind(this)} />
        <AboutView establishment={establishment} />
        <GalleryView photos={establishment.images} />
        <ReviewsView establishment={establishment} params={this.props.params} />
        <MapView establishment={establishment} />
        <ReservationModalView
          place={establishment.name}
          placeId={establishment.id}
          schedule={establishment.scheduleRes}
          availableDates={establishment.availableDates} />
        <ReservationBanquetModalView
          place={establishment.name}
          placeId={establishment.id}
          schedule={establishment.scheduleRes}
          availableDates={establishment.availableDates}
          banquetPrice={establishment.banketPrice} />
      </div>
    );
  }



  /*
  ===== END RENDER ESTABLISHMNENTS =====
  */


  render() {

    if (DEVELOPMENT) console.log('EstablishmentPage: Render');

    return this.renderEstablishment();
  }
};


EstablishmentPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  params: PropTypes.shape({
    id: PropTypes.string.isRequired,
  }).isRequired
};


const mapStateToProps = ({ establishments }, { params }) => ({
  establishment: establishments.byId[params.id]
});

export default withRouter(connect(mapStateToProps)(EstablishmentPage));