import React, { PureComponent, PropTypes } from 'react';
import { Link } from 'react-router';
import { getNumEnding } from '../../../utils/Tools';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import LazyLoad from '../../elements/LazyLoad';
import PhotoSwipeGallery from '../../widgets/PhotoSwipeGallery';
import BootstrapModalWindow from '../../modalWindows/BootstrapModalWindow';

class GallaryView extends PureComponent {


	/*
	  ===== BEGIN RENDER GALLERY =====
	*/

	renderGallery() {
		let templateItem = (photo, index) => (
			<figure key={index} itemProp="associatedMedia" className="ps-gallery-all__item" itemScope itemType="http://schema.org/ImageObject">
				<a href={photo} className="ps-gallery-all__item__link" target="_blank" itemProp="contentUrl">
					<LazyLoad itemProp="thumbnail" className="ps-gallery-all__item__link__img" dataSrc={photo} alt={photo} />
				</a>
			</figure>
		);


		let photos = this.props.photos || null;
		if (photos)
			return <PhotoSwipeGallery photos={photos} className={'ps-gallery-all'} templateItem={templateItem} />;
		return;
	}

	/*
	  ===== END RENDER GALLERY =====
	*/

	/*
	  ===== BEGIN MODAL GALLERY =====
	*/

	//actions
	openGalleryModal() {
		this.refs.galleryModal.open();
	}
	closeGalleryModal() {
		this.refs.galleryModal.close();
	}

	//handle actions
	handleGalleryModalCancel() {
		this.refs.galleryModal.close();
	}
	handleGalleryModalShown() {

	}

	// render
	renderGalleryModal() {
		return (
			<BootstrapModalWindow
				ref="galleryModal"
				title="Галерея"
				sizeClass="modal-lg"
				onCancel={this.handleGalleryModalCancel.bind(this)}
				onConfirm={this.closeGalleryModal.bind(this)}
				onShown={this.handleGalleryModalShown.bind(this)}
				bodyClass="modal-body--no-padding-bottom"
			>
				{this.renderGallery()}
			</BootstrapModalWindow>
		);
	}
	/*
	  ===== END MODAL GALLERY =====
	*/


	renderPhotosView() {
		let photosView = (
			<div className="gallery__content-empty">
				Фотографии данного заведения еще не добавлены
			</div>
		);
		if (this.props.photos) {
			let photosMore = '';
			if (this.props.photos.length > 5) {
				let photosCount = this.props.photos.length - 5;
				let photosValue = getNumEnding(photosCount, ['фотография', "фотографии", "фотографий"]);
				photosMore = (
					<div onClick={() => this.openGalleryModal()} className="gallery__last-thumbnail-wrap">
						<LazyLoad className="gallery__single-thumbnail" dataSrc={this.props.photos[5]} alt={this.props.photos[5]} />
						<div className="gallery__more-photos-cover fl fl-jc-c fl-ai-c">
							<div className="gallery__more-photos-note-wrap fl fl-ai-fs">
								<p className="gallery__more-photos-note">Еще {photosCount} {photosValue}</p>
							</div>
						</div>
					</div>
				);
			}



			let templateItem = (photo, index) => (
				<figure key={index} itemProp="associatedMedia" className="gallery__item" itemScope itemType="http://schema.org/ImageObject">
					<a href={photo} className="gallery__item__link" itemProp="contentUrl">
						<LazyLoad itemProp="thumbnail" className="gallery__item__link__img" dataSrc={photo} alt={photo} />
					</a>
				</figure>
			);



			photosView = (
				<div className="gallery__thumbnails-wrapper full-width clearfix">
					{photosMore}
					<PhotoSwipeGallery photos={this.props.photos} count={5} className={'ps-gallery-prev'} templateItem={templateItem} />
				</div>
			);
		}
		return photosView;
	}



	render() {
		return (
			<section className="gallery">
				<div className="header-container header-container_bottom-line header-container--paragraph">
					<div className="center">
						<div className="full-width fl fl-in-di">
							<div>
								<h3 className="header-container__header">Галерея</h3>
							</div>
						</div>
					</div>
				</div>
				<div className="center">
					{this.renderPhotosView()}
				</div>
				{this.renderGalleryModal()}
			</section>

		);
	}

};

/*
GallaryView.propTypes = {
	photos: PropTypes.array
};*/

export default GallaryView;