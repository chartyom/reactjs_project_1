import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as uiAction from '../../../actions/uiAction';
import { loadScript, getNumEnding } from '../../../utils/Tools';
import moment from 'moment';
import CONFIG from '../../config';
import InputElement from 'react-input-mask';
import ScheduleTimeListLayout from '../../layouts/ScheduleTimeListLayout';
import LoaderView from '../../widgets/LoaderView';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import { reservationTablePlaceBottom, reservationBanquetPlaceBottom } from '../../widgets/Analytics/TargetsStore';

class ReviewsReservationView extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            pikaday: null
        };
    }

    componentDidMount() {
        loadScript("/temp/js/pikaday.js")
            .then(() => this.initDatePicker())
            .catch(e => { if (DEVELOPMENT) console.log(e); });
    }

    getReservation() {
        return this.props.reservation;
    }

    handleSetReservation(d) {
        this.props.dispatch(uiAction.setReservation(d));
    }

    handleSetScheduleTime(d) {
        this.props.dispatch(establishmentsAction.setScheduleTime(this.props.params.id, d));
    }

    /**
     * Задаёт дату
     * @param {date} date 
     */
    handleSetDate(date) {
        //Предотвращает излишние изменения состояния
        //Преобразование через moment потому, что date имеет значение начала дня, а this.getReservation().date текущее время дня
        if (moment(date).format('MMDD') != moment(this.getReservation().date).format('MMDD')) {
            this.handleSetReservation({ date: date, time: false, bonus: false, bonusType: false });
            this.handleSetScheduleTime(moment(date).format('YYYY-MM-DD'));
        }
    }

    /**
     * Задаёт количество персон
     * @param {event} e
     */
    handleSetPeople(e) {
        //Предотвращает излишние изменения состояния
        if (e.target.value != this.getReservation().people)
            this.handleSetReservation({ people: e.target.value });
    }


    /**
     * Задаёт время
     * @param {string} time 
     * @param {number} bonus 
     * @param {string} bonusType 
     */
    handleSetTime(time, bonus, bonusType) {
        //Предотвращает излишние изменения состояния
        if (time === this.getReservation().time) {
            this.handleSetReservation({ time: false, bonus: false, bonusType: false });
        } else {
            this.handleSetReservation({ time: time, bonus: bonus, bonusType: bonusType });
        }
    }

    initPikaday(opt) {
        this.setState({
            pikaday: new Pikaday(opt)
        });
    }

    getPikaday() {
        return this.state.pikaday;
    }

    initDatePicker() {

        const c = document.getElementsByClassName('rcrv-piker_window');
        while (c.length > 0) {
            c[0].parentNode.removeChild(c[0]);
        }

        const setDate = (date) => this.handleSetDate(date);
        const getDate = () => this.getReservation().date;
        //массив с датами начала брони
        var getAvailableDates = this.props.availableDates;
        const w = document.getElementById('rcrv-piker_temp');
        const t = document.getElementById('rcrv-piker_trigger');
        if (!w || !t) return;
        // Config
        var datepickerConfig = {
            field: w,
            trigger: t,
            format: 'MMMM',
            theme: 'rcrv-piker_window',
            showDaysInNextAndPreviousMonths: true,
            minDate: moment().toDate(),
            maxDate: moment().add(28, 'days').toDate(),
            firstDay: 1,
            onSelect: function () {
                if (!w) return;
                setDate(this._d);
            },
            onOpen: function () {
                this.setDate(getDate());
            },
            disableDayFn: function (theDay) {
                var i = getAvailableDates.map(function (obj) {
                    return obj.value;
                }).indexOf(moment(theDay).format('YYYY-MM-DD'));
                return i === -1;
            },
            i18n: {
                previousMonth: 'Предыдущий месяц',
                nextMonth: 'Следующий месяц',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
            }
        };
        this.initPikaday(datepickerConfig);

        //по дефолту устанавливаем на ближайшую дату
        this.getPikaday().setDate(getAvailableDates[0].value);
    }

    openReservationModal() {
        reservationTablePlaceBottom();
        $('#reservationModal').modal('show');
    }

    //Время бронирования
    getScheduleList() {
        //Вывод нового списка времени бронирования
        if (this.props.tempScheduleTime.list)
            return this.props.tempScheduleTime.list;
        return this.props.schedule;
    }

    //Хранимый объект хранения
    getScheduleTime() {
        return this.props.tempScheduleTime;
    }


    //График
    renderSchedule() {
        if (this.props.schedule && this.props.availableDates) {
            let classes = '';
            var reservationTime = null;
            if (this.getReservation().time) {
                classes = ' time_wrapper__select';
                reservationTime = this.getReservation().time;
            }
            if (this.getScheduleTime().readyState === establishmentsAction.ESTABLISHMENT_SCHEDULE_TIME_FETCHING)
                return <LoaderView />;
            //Отображение списка кнопок со временем
            return <ScheduleTimeListLayout
                classes={"time_wrapper time_wrapper_row-6 full-width fl" + classes}
                scheduleTimeList={this.getScheduleList()}
                reservationTime={reservationTime}
                handleSetTime={this.handleSetTime.bind(this)} />;
        }
        return;
    }

    //Output select
    renderSelectButtonPersons() {
        let options = [];
        for (var i = 1; i <= CONFIG.reservation.peopleMax; i++) {
            options.push(i);
        }
        return (
            <select className="form-control date-and-people-block__people " value={this.getReservation().people} onChange={(e) => this.handleSetPeople(e)}>
                {options.map(option => (
                    <option key={option} value={option}>{option} {getNumEnding(option, ['персона', 'персоны', 'персон'])} </option>
                ))}
            </select>
        );

    }

    render() {

        if (DEVELOPMENT) console.log('EstablishmentPage => ReviewsReservationView: Render');

        const date = moment(this.getReservation().date).format('dddd, D MMM');

        return (
            <div className="calendar-wrap fl fl-dr-col fl-in-di">
                <div className="calendar-wrap__body calendar-wrap__body_borders">
                    <div className="date-and-people-block full-width fl fl-in-di nwr">
                        <div id="rcrv-piker_trigger" className="date-and-people-block__date fl fl-ai-fe" >
                            <div id="rcrv-piker" className="form-control form-control--date-right date-and-people-block__datepicker date-and-people-block__stable" >{date}</div>
                            <input id="rcrv-piker_temp" type="text" hidden />
                        </div>
                        <div className="date-and-people-block__man-quantity">
                            {this.renderSelectButtonPersons()}
                        </div>
                    </div>
                    <div className="calendar-wrap__body__time">
                        <input className="dn" defaultChecked type="radio" name="reviews-reservation-time-extending" id="reviews-reservation-time-non-extended" />
                        <input className="dn" type="radio" name="reviews-reservation-time-extending" id="reviews-reservation-time-extended" />
                        {this.renderSchedule()}

                        <label className="calendar-wrap__change-time-button calendar-wrap__change-time-button_right-arrow calendar-wrap__to-extend-time" htmlFor="reviews-reservation-time-extended">другое время</label>
                        <label className="calendar-wrap__change-time-button calendar-wrap__change-time-button_left-arrow calendar-wrap__to-reduce-time" htmlFor="reviews-reservation-time-non-extended">свернуть</label>
                    </div>
                </div>

                <a href="javascript:;" onClick={() => this.openReservationModal()} className="reservation-button">Забронировать</a>
            </div>
        );
    }
}

const mapStateToProps = ({ ui, establishments }) => ({
    reservation: ui.reservation,
    tempScheduleTime: establishments.tempScheduleTime
});

export default connect(mapStateToProps)(ReviewsReservationView);