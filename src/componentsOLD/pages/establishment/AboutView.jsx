import React, { PureComponent, PropTypes } from 'react';
import { Link } from 'react-router';
import { getNumEnding } from '../../../utils/Tools';
import LazyLoad from '../../elements/LazyLoad';
import { reservationBanquetPlaceHeader } from '../../widgets/Analytics/TargetsStore';

class AboutView extends PureComponent {


	constructor(props) {
		super(props);
		this.state = {
			isShowAdvantages: false,
		};
		this.handleIsShowAdvantages = this.handleIsShowAdvantages.bind(this);
	}


	handleIsShowAdvantages() {
		if (this.state.isShowAdvantages) {
			return this.setState({ isShowAdvantages: false });
		}
		this.setState({ isShowAdvantages: true });
	}

	handleGetRightDayOfWeekById(i) {
		switch (i) {
			case 0:
				return 'Пн';
			case 1:
				return 'Вт';
			case 2:
				return 'Ср';
			case 3:
				return 'Чт';
			case 4:
				return 'Пт';
			case 5:
				return 'Сб';
			default:
				return 'Вс';
		}
	}

	openReservationModal() {
		reservationBanquetPlaceHeader();
		$('#reservationBanquetModal').modal('show');
	}

	render() {
		const { establishment } = this.props;
		let advantages = establishment.tags,
			description = establishment.longDesc,
			workingTime = establishment.scheduleWork,
			isBanquet = establishment.pointsOfAgreement.banquet,
			advantagesView = '',
			advantagesMoreView = '',
			workingTimeView = '',
			banquetButtonView = '';
		if (advantages) {
			advantagesView = advantages.map((advantage, index) => {
				return (
					<div key={index} className="single-advantage">
						<div className="single-advantage__icon-wrap">
							<LazyLoad className="single-advantage__icon" alt={advantage.name} width='30' height='30' dataSrc={advantage.img} />
						</div>
						<p className="single-advantage__note">{advantage.name}</p>
					</div>
				);
			});
			if (advantages.length > 7) {
				var btntext = this.state.isShowAdvantages ? "Свернуть" : "Показать еще";
				advantagesMoreView = (
					<div className="place-advantages__button_center">
						<button className="btn btn-appo" type="button" onClick={this.handleIsShowAdvantages}>
							{btntext}
						</button>
					</div>
				);
			}
		}

		if (workingTime) {

			let workingTimeViewFirst = (
				<div>
					{workingTimeView = workingTime.map((time, index) => {
						if (index > 3) return false;
						return (
							<p key={index} className="working-hours__schedule-item">{this.handleGetRightDayOfWeekById(index)}: <span className="working-hours__schedule-hours">{time.time}</span></p>
						);
					})}
				</div>
			);
			let workingTimeViewSecond = (
				<div>
					{workingTimeView = workingTime.map((time, index) => {
						if (index <= 3) return false;
						return (
							<p key={index} className="working-hours__schedule-item">{this.handleGetRightDayOfWeekById(index)}: <span className="working-hours__schedule-hours">{time.time}</span></p>
						);
					})}
				</div>
			);

			workingTimeView = (
				<div className="working-hours__schedule-item-wrap fl fl-in-di nwr">
					{workingTimeViewFirst}
					{workingTimeViewSecond}
				</div>
			);
		}

		if (isBanquet) {
			banquetButtonView = (
				<a className="header-container__additional-buttons" onClick={()=>this.openReservationModal()} href="javascript:;">Забронировать банкет</a>
			);
		}

		/*
			AdvantageToggleClass
		*/
		var AdvantageToggleClass = this.state.isShowAdvantages ? '' : ' place-advantages__container_hidden';

		return (
			<section className="place-full-information" id="goto-about">
				<input className="dn" defaultChecked type="radio" name="about-place" id="about-place-description" />
				<div className="header-container header-container_bottom-line header-container--paragraph">
					<div className="center">
						<div className="full-width fl fl-in-di">
							<div>
								<h3 className="header-container__header">О заведении</h3>
								<ul className="header-container__links">
									<li className="header-container__link-item">
										<label className="header-container__link about-place-description-label" htmlFor="about-place-description">
											Описание
									</label>
									</li>
								</ul>
							</div>
							<div>
								{banquetButtonView}
							</div>
						</div>
					</div>
				</div>
				<div className="center">
					<div className="variable-block about-place-description">
						<div className="place-advantages" >
							<div className={"fl full-width" + AdvantageToggleClass}>
								{advantagesView}
							</div>
							{advantagesMoreView}
						</div>
						<div className="working-hours fl nwr">
							<div className="ordinary-note">
								<p className="ordinary-note__text">{description}</p>
							</div>
							<div className="working-hours__schedule fl">
								<div>
									<p className="working-hours__schedule-header">Часы работы</p>
								</div>
								{workingTimeView}
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	};
};

// AboutView.propTypes = {
// 	establishment: PropTypes.shape({
// 		name: PropTypes.string.isRequired,
// 		id: PropTypes.string.isRequired,
// 		link: PropTypes.string.isRequired,
// 		type: PropTypes.string.isRequired,
// 	}).isRequired
// };

export default AboutView;