import React from 'react';
import moment from 'moment';
import { Link } from 'react-router';
import { LINK_PAGE as ESTABLISHMENT_LINK_PAGE } from '../../pages/establishment/EstablishmentPage';
import LazyLoad from '../../elements/LazyLoad';

const MapView = ({ establishment }) => {

	function openReservationModal() {
		$('#reservationModal').modal('show');
	};
	var place = establishment;
	var id = place.id,
		image = place.img,
		label = place.label,
		link = ESTABLISHMENT_LINK_PAGE + '/' + place.mapType + '/' + place.name + '/' + place.id,
		rating = place.rating,
		shortDescription = place.shortDesc,
		name = place.name,
		price = place.price,
		priceName = '₽', // TODO нужно исправлять на сервере
		underground = place.address.metro && place.address.metro.name ? place.address.metro.name : null,
		trend = place.trend,
		isActive = place.isActive, // true || false - Работа Битком24 с заведением
		crowd = place.croud, // Битком || Загружено || Свободно
		currentWork = place.currentWork, // с 12:00 || до 20:00 || Закрыто || 24
		status = establishment.status,
		statusName = establishment.statusName,
		isReservation = establishment.pointsOfAgreement.reservation
		;

	/*
	  LABEL
	*/
	var
		labelView = '';

	if (label && label.color && label.name) {
		var labelColor = label.color,
			labelName = label.name,
			labelType = label.type,
			labelCC = '', //COLOR CLASS
			labelTC = ''; //TYPE CLASS

		if (labelColor == 'greenAlert') {
			labelCC = " label_green";
		} else if (labelColor == 'redAlert') {
			labelCC = " label_red";
		} else if (labelColor == 'yellowAlert') {
			labelCC = " label_yellow";
		}

		if (labelType == 'check') {
			labelTC = " label_type--check";
		} else if (labelType == 'alert') {
			labelTC = " label_type--alert";
		}

		labelView = (
			<p className={"label" + labelCC + labelTC}>
				<span>{labelName}</span>
			</p>
		);
	}


	/*
	  ADDRESS
	*/

	var undergroundView = '';
	if (underground) {
		undergroundView = (
			<p className="metro">{underground}</p>
		);
	}

	/*
	  TREND
	*/
	var trendClass = '';
	if (trend == "up") {
		trendClass = " place-attributes__rating_up";
	} else if (trend == "down") {
		trendClass = "  place-attributes__rating_down";
	}

	var renderReservationButton = '';
	if (isReservation) {
		renderReservationButton = (
			<button type="button" onClick={() => openReservationModal()} className="full-width reserve-button btn btn-danger">Забронировать</button>
		);
	}

	let renderMap = (
		<div className="place place_white-bg">
			<div className="place__image-block">
				<LazyLoad className="place__thumbnail" dataSrc={image} alt={name} />
				{labelView}
				<p className={"status  status_load-" + (status)}><span>{statusName}</span></p>
			</div>
			<div className="place-attributes">
				<div className="fl fl-in-di nwr">
					<div>
						<div className="place-attributes__header">{name}</div>
						<p className="place-attributes__subheader">{shortDescription}</p>
					</div>
					<div>
						<p className={"place-attributes__rating" + trendClass}><span>{rating}</span></p>
					</div>
				</div>
				<div className="fl fl-in-di">
					<div>
						<p className="place-attributes__middle-check fl fl-it-cnt"><span>{price} <i className="currency currency_rub currency_regular ">{priceName}</i></span></p>
						{undergroundView}
					</div>
				</div>
				{renderReservationButton}
			</div>
		</div>
	);

	return (
		<section className="place-view-map" id="goto-map">
			<div className="header-container  header-container--paragraph">
				<div className="center">
					<h3 className="header-container__header">Карта</h3>
				</div>
			</div>
			<div className="place-on-map">
				<div id="map-place" className="map__itsef">
				</div>
				<div className="center">
					{renderMap}
				</div>
			</div>
		</section>
	);
};

export default MapView;