import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as uiAction from '../../../actions/uiAction';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import { loadScript, getNumEnding } from '../../../utils/Tools';
import moment from 'moment';
import CONFIG from '../../config';
import ScheduleTimeListLayout from '../../layouts/ScheduleTimeListLayout';
import LoaderView from '../../widgets/LoaderView';
import { reservationTablePlaceHeader } from '../../widgets/Analytics/TargetsStore';

class PlaceInsideReservationView extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            pikaday: null
        };
    }

    componentDidMount() {
        loadScript("/temp/js/pikaday.js")
            .then(() => this.initDatePicker())
            .catch(e => { if (DEVELOPMENT) console.log(e); });
    }

    /*
      ===== BEGIN GERRERS =====
    */
    
    getReservation() {
        return this.props.reservation;
    }

    //Хранимый объект хранения
    getScheduleTime() {
        return this.props.tempScheduleTime;
    }

    //Время бронирования
    getScheduleList() {
        //Вывод нового списка времени бронирования
        if (this.props.tempScheduleTime.list)
            return this.props.tempScheduleTime.list;
        return this.props.schedule;
    }
    
    /*
      ===== END GERRERS =====
    */


    /*
      ===== BEGIN HANDLE ACTION =====
    */

    handleSetReservation(d) {
        this.props.dispatch(uiAction.setReservation(d));
    }

    handleSetScheduleTime(d) {
        this.props.dispatch(establishmentsAction.setScheduleTime(this.props.params.id, d));
    }

    /**
     * Задаёт дату
     * @param {date} date 
     */
    handleSetDate(date) {
        //Предотвращает излишние изменения состояния
        //Преобразование через moment потому, что date имеет значение начала дня, а this.getReservation().date текущее время дня
        if (moment(date).format('MMDD') != moment(this.getReservation().date).format('MMDD')) {
            this.handleSetReservation({ date: date, time: false, bonus: false, bonusType: false });
            this.handleSetScheduleTime(moment(date).format('YYYY-MM-DD'));
        }

    }

    /**
     * Задаёт количество персон
     * @param {event} e
     */
    handleSetPeople(e) {
        //Предотвращает излишние изменения состояния
        if (e.target.value != this.getReservation().people)
            this.handleSetReservation({ people: e.target.value });
    }


    /**
     * Задаёт время
     * @param {string} time 
     * @param {number} bonus 
     * @param {string} bonusType 
     */
    handleSetTime(time, bonus, bonusType) {
        //Предотвращает излишние изменения состояния
        if (time === this.getReservation().time) {
            this.handleSetReservation({ time: false, bonus: false, bonusType: false });
        } else {
            this.handleSetReservation({ time: time, bonus: bonus, bonusType: bonusType });
        }
    }


    /*
        ===== BEGIN HANDLE PIKADAY =====
    */


    initPikaday(opt) {
        this.setState({
            pikaday: new Pikaday(opt)
        });
    }

    getPikaday() {
        return this.state.pikaday;
    }

    initDatePicker() {

        const c = document.getElementsByClassName('dapb-piker_window');
        while (c.length > 0) {
            c[0].parentNode.removeChild(c[0]);
        }

        const setDate = (date) => this.handleSetDate(date);
        const getDate = () => this.getReservation().date;
        //массив с датами начала брони
        var getAvailableDates = this.props.availableDates;
        const w = document.getElementById('dapb-piker_temp');
        const t = document.getElementById('dapb-piker_trigger');
        if (!w || !t) return;
        // Config
        var datepickerConfig = {
            field: w,
            trigger: t,
            theme: 'dapb-piker_window',
            showDaysInNextAndPreviousMonths: true,
            minDate: moment().toDate(),
            maxDate: moment().add(28, 'days').toDate(),
            firstDay: 1,
            onSelect: function () {
                if (!w) return;
                setDate(this._d);
            },
            disableDayFn: function (theDay) {
                var i = getAvailableDates.map(function (obj) {
                    return obj.value;
                }).indexOf(moment(theDay).format('YYYY-MM-DD'));
                return i === -1;
            },
            onOpen: function () {
                this.setDate(getDate());
            },
            i18n: {
                previousMonth: 'Предыдущий месяц',
                nextMonth: 'Следующий месяц',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
            }
        };
        this.initPikaday(datepickerConfig);

        //по дефолту устанавливаем на ближайшую дату
        this.getPikaday().setDate(getAvailableDates[0].value);
    }

      
    /*
      ===== END PIKADAY =====
    */

    openReservationModal() {
        reservationTablePlaceHeader();
        $('#reservationModal').modal('show');
    }

    //График
    renderSchedule() {
        if (this.getScheduleList() && this.props.availableDates) {
            var classes = '';
            var reservationTime = null;
            if (this.getReservation().time) {
                classes = ' time_wrapper__select';
                reservationTime = this.getReservation().time;
            }
            if (this.getScheduleTime().readyState === establishmentsAction.ESTABLISHMENT_SCHEDULE_TIME_FETCHING)
                return <LoaderView />;
            //Отображение списка кнопок со временем
            return <ScheduleTimeListLayout
                classes={"time_wrapper time_wrapper_row-4 full-width fl" + classes}
                scheduleTimeList={this.getScheduleList()}
                reservationTime={reservationTime}
                handleSetTime={this.handleSetTime.bind(this)} />;
        }
        return;
    }

    //Output select
    renderSelectButtonPersons() {
        let options = [];
        for (var i = 1; i <= CONFIG.reservation.peopleMax; i++) {
            options.push(i);
        }
        return (
            <select className="form-control date-and-people-block__people " value={this.getReservation().people} onChange={(e) => this.handleSetPeople(e)}>
                {options.map(option => (
                    <option key={option} value={option}>{option} {getNumEnding(option, ['персона', 'персоны', 'персон'])} </option>
                ))}
            </select>
        );

    }

    render() {

        if (DEVELOPMENT) console.log('EstablishmentPage => PlaceInsideReservationView: Render');

        const date = moment(this.getReservation().date).format('dddd, D MMM');

        return (
            <selection>
                <div className="calendar-wrap">
                    <div className="calendar-wrap__body">
                        <div className="date-and-people-block full-width fl fl-in-di nwr">
                            <div id="dapb-piker_trigger" className="date-and-people-block__date fl fl-ai-fe" >
                                <div id="dapb-piker" className="form-control form-control--date-right date-and-people-block__datepicker " >{date}</div>
                                <input id="dapb-piker_temp" type="text" hidden />
                            </div>
                            <div className="date-and-people-block__man-quantity">
                                {this.renderSelectButtonPersons()}
                            </div>
                        </div>
                        <div className="calendar-wrap__body__time">
                            <input className="dn" defaultChecked type="radio" name="time-extending" id="time-non-extended" />
                            <input className="dn" type="radio" name="time-extending" id="time-extended" />
                            {this.renderSchedule()}

                            <label className="calendar-wrap__change-time-button calendar-wrap__change-time-button_right-arrow calendar-wrap__to-extend-time" htmlFor="time-extended">другое время</label>
                            <label className="calendar-wrap__change-time-button calendar-wrap__change-time-button_left-arrow calendar-wrap__to-reduce-time" htmlFor="time-non-extended">свернуть</label>
                        </div>
                    </div>
                    <a href="javascript:;" onClick={() => this.openReservationModal()} className="reservation-button">Забронировать</a>

                </div>
            </selection>
        );
    }
}

const mapStateToProps = ({ ui, establishments }) => ({
    reservation: ui.reservation,
    tempScheduleTime: establishments.tempScheduleTime
});

export default connect(mapStateToProps)(PlaceInsideReservationView);