import React, { PureComponent, PropTypes } from 'react';
import moment from 'moment';
import { Link } from 'react-router';
import { getNumEnding } from '../../../utils/Tools';

import BootstrapModalWindow from '../../modalWindows/BootstrapModalWindow';
import ReviewsReservationView from './ReviewsReservationView';
import ReviewsReservationBanquetView from './ReviewsReservationBanquetView';

class ReviewsView extends PureComponent {

	closeReviewsModal() {
		this.refs.reviewsModal.close();
	}
	openReviewsModal() {
		this.refs.reviewsModal.open();
	}

	getData() {
		return this.props.establishment;
	}

	getReviews() {
		return this.props.establishment.feedbacks;
	}

	getSchedule() {
		return this.props.establishment.scheduleRes;
	}

	getAvailableDates() {
		return this.props.establishment.availableDates;
	}

	getBanquetPrice() {
		return this.props.establishment.banketPrice || null;
	}

	renderReviewsPrevList() {
		if (DEVELOPMENT) console.log('EstablishmentPage => ContentView => ReviewsView: Render');
		return (
			<div className="reviews-wrapper">
				{this.getReviews().map((review, index) => {
					var username = review.user && review.user.firstName ? review.user.firstName : 'Гость';
					var ratingView = review.rating
						? (
							<p className="single-review__rating">{review.rating}</p>
						)
						: '';
					var date = review.createdAt ? review.createdAt : '';
					var color = 'grey';
					var rating = parseFloat(review.rating);
					if (rating > 4.5) color = 'green';
					if (rating <= 4.5) color = 'orange';
					if (rating <= 2.5) color = 'red';

					return (
						<div key={index} className={"single-review " + color + "-review"} >
							<div className="single-review__review-wrap full-width">
								<p className="single-review__review">{review.text}</p>
							</div>
							<div className="single-review__parameters full-width fl fl-in-di fl-ai-c">
								<div className="single-review__main-review-info fl fl-ai-c">
									{ratingView}
									<p className="single-review__username">{username}</p>
								</div>
								<div>
									<p className="single-review__sent-time">{date}</p>
								</div>
							</div>
						</div>
					);
				})}
			</div>
		);
	}

	renderReviewsModalList() {
		return (
			<div className="full-reviews__others-reviews-wrapper">
				{this.getReviews().map((review, index) => {
					var username = review.user && review.user.firstName ? review.user.firstName : 'Гость';
					var ratingView = review.rating
						? (
							<p className="single-review__rating">{review.rating}</p>
						)
						: '';
					var date = review.createdAt ? review.createdAt : '';
					var color = 'grey';
					var rating = parseFloat(review.rating);
					if (rating > 4.5) color = 'green';
					if (rating <= 4.5) color = 'orange';
					if (rating <= 2.5) color = 'red';

					return (
						<div key={index} className={"single-review " + color + "-review"} >
							<div className="single-review__review-wrap full-width">
								<p className="single-review__review">{review.text}</p>
							</div>
							<div className="single-review__parameters full-width fl fl-in-di fl-ai-c">
								<div className="single-review__main-review-info fl fl-ai-c">
									{ratingView}
									<p className="single-review__username">{username}</p>
								</div>
								<div>
									<p className="single-review__sent-time">{date}</p>
								</div>
							</div>
						</div>
					);
				})}
			</div>
		);
	}

	renderReiews() {
		if (this.getReviews() && this.getReviews().length > 0)
			return (
				<div className="half-width padd-rig-50">
					{this.renderReviewsPrevList()}
					<input className="dn" type="checkbox" id="revies-popup-launcher" />
					<label onClick={() => this.openReviewsModal()} htmlFor="revies-popup-launcher" className="reviews__more-button">Смотреть отзывы</label>

					<BootstrapModalWindow
						ref="reviewsModal"
						title="Отзывы"
						onCancel={() => this.closeReviewsModal()}>
						{this.renderReviewsModalList()}
					</BootstrapModalWindow>
				</div>
			);
		return (
			<div className="half-width reviews__content-empty">Отзывов еще нет</div>
		);
	}

	renderReservationSchedule() {
		return (
			<div className="time_wrapper full-width fl fl-in-di">
				{this.getSchedule().map((time, index) => {
					if (index > 17) return false;
					time.status = time.status ? time.status : 'green';
					if (time.reward) {
						return (
							<div key={index} className={"time-point time-point_" + time.status + " time-point_with-bonus fl fl-jc-c fl-ai-c"}>
								<p className="time-point__time">{time.time}</p>
								<p className="time-point__bonus-note fl fl-ai-c fl-jc-c">{time.reward}</p>
								<a href="#" className="abs-link"></a>
							</div>
						);
					}
					return (
						<div key={index} className={"time-point time-point_" + time.status + " fl fl-jc-c fl-ai-c"}>
							<p className="time-point__time">{time.time}</p>
							<a href="#" className="abs-link"></a>
						</div>
					);
				})}
			</div>
		);
	}

	renderReservation() {

		var reservationTable = (
			<div className="calendar-wrap reviews__content-empty">Бронирование столиков в данном заведении не осуществляется</div>
		);

		var reservationBanquet = '';


		if (this.getData().pointsOfAgreement.reservation) {
			reservationTable = <ReviewsReservationView
				schedule={this.getSchedule()}
				availableDates={this.getAvailableDates()}
				params={this.props.params} />;
		}

		if (this.getData().pointsOfAgreement.banquet) {
			reservationBanquet = <ReviewsReservationBanquetView
				schedule={this.getSchedule()}
				banquetPrice={this.getBanquetPrice()}
				availableDates={this.props.establishment.availableDates}
				params={this.props.params}
				place={this.props.establishment.name}
				placeId={this.props.establishment.id}
			/>;
		}


		return (
			<div className="half-width fl fl-jc-c">
				{reservationTable}
				{reservationBanquet}
			</div>
		);

	}

	renderReservationsButton() {

		var reservationBanquet = '';

		if (this.getData().pointsOfAgreement.banquet) {
			reservationBanquet = (
				<li className="header-container__link-item">
					<label className="header-container__link place-banquet-label" htmlFor="place-banquet">
						Банкет
					</label>
				</li>
			);
		}

		return (
			<ul className="header-container__links">
				<li className="header-container__link-item">
					<label className="header-container__link place-reservation-label" htmlFor="place-reservation">
						Столик
					</label>
				</li>
				{reservationBanquet}
			</ul>
		);
	}

	render() {
		return (
			<section className="reviews">
				<input className="dn" type="radio" name="reservation" id="place-reservation" defaultChecked />
				<input className="dn" type="radio" name="reservation" id="place-banquet" />
				<div className="header-container header-container_bottom-line header-container--paragraph">
					<div className="center">
						<div className="full-width fl fl-in-di">
							<div className="half-width">
								<h3 className="header-container__header">Отзывы</h3>
								{/*<a className="header-container__additional-buttons header-container__additional-buttons_filled" href="#">Оставить отзыв</a>*/}
							</div>
							<div className="half-width fl">
								<h3 className="header-container__header">Бронирование</h3>
								{this.renderReservationsButton()}
							</div>
						</div>
					</div>
				</div>
				<div className="center">
					<div className="fl nwr">

						{this.renderReiews()}
						{this.renderReservation()}

					</div>
				</div>


			</section>
		);
	}
};

/*
ReviewsView.propTypes = {
	establishment: PropTypes.shape({
		name: PropTypes.string.isRequired,
		id: PropTypes.string.isRequired,
		link: PropTypes.string.isRequired,
		type: PropTypes.string.isRequired,
	}).isRequired
};*/

export default ReviewsView;