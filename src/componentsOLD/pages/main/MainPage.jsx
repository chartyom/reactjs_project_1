import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import SearchSectionHeader from '../../headers/SearchSectionHeader';
import SelectionsView from './SelectionsView';
import CategoriesView from './CategoriesView';
import BannerBitkomAppWidget from '../../widgets/BannerBitkomApp';
import EstablishmentsView from './EstablishmentsView';

export const LINK_PAGE = '/';
export const LINK_MASK = '/';

class MainPage extends PureComponent {

  static readyOnActions(dispatch, params) {
    return Promise.all([
      //dispatch(establishmentsAction.getWithChecked()),
    ]);
  }

  render() {

    if (DEVELOPMENT) console.log('MainPage: Render');

    const PAGE_NAME = 'Лучшие рестораны, бары и кафе Москвы';
    const PAGE_DESCRIPTION = 'Битком24 - Лучшие рестораны, бары и кафе Москвы. Умный сервис по выбору и бронированию ресторанов, баров и кафе';
    const PAGE_KEYWORDS = 'битком, битком24, bitkom, bitkom24, bitcom, bitcom24, лучшие, рестораны, бары, кафе, москва, заказ, столик, стол, бронирование, банкет, меню, отзывы, адрес, бургеры, суши, веранда, панорама, ревизорро, летучая, доставка, скидки, акции, мероприятия, заказать, телефон, часы работы';

    return (
      <div>
        <Helmet
          title={PAGE_NAME}
          meta={[
            { name: 'description', content: PAGE_DESCRIPTION },
            { name: 'Keywords', content: PAGE_KEYWORDS },
          ]}
        />
        <SearchSectionHeader />
        <SelectionsView />
        <CategoriesView />
        <EstablishmentsView />
        <BannerBitkomAppWidget />
      </div>
    );
  }
};

export default MainPage;