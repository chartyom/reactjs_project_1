import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { getNumEnding } from '../../../utils/Tools';
import LazyLoad from '../../elements/LazyLoad';
import { LINK_PAGE } from '../search/SearchPage';

const CategoriesListView = ({ categories, amount }) => {
  amount = amount || 10;
  return (
    <div className="content-container fl fl-in-di ">
      {categories.map(function (category, i) {

        if ((i + 1) > amount) return false;

        var img = '/images/categories/restaurant.png';
        switch (category.name) {
          case 'Ресторан':
            img = '/images/categories/restaurant.png';
            break;
          case 'Кафе':
            img = '/images/categories/cafe.png';
            break;
          case 'Клуб':
            img = '/images/categories/club.png';
            break;
          case 'Винотека':
            img = '/images/categories/vinoteka.png';
            break;
          case 'Бар':
            img = '/images/categories/bar.png';
            break;
          case 'Кальянная':
            img = '/images/categories/hookah.png';
            break;
        }

        let textRightEnding = getNumEnding(category.count, ['заведение', 'заведения', 'заведений']);
        return (
          <div key={i} className="category fl fl-it-cnt fl-js-cnt">
            <LazyLoad className="category__thumbnail" dataSrc={img} alt={category.name} />
            <div className="category__content">
              <h4 className="category__header">{category.name}</h4>
              <p className="category__quantity">{category.count} {textRightEnding}</p>
            </div>
            <Link to={{ pathname: LINK_PAGE, query: { types: category.name } }} className="abs-link" />
          </div>
        );
      })}
    </div>
  );
};

CategoriesListView.propTypes = {
  categories: PropTypes.array.isRequired,
  amount: PropTypes.number.isRequired,
};

export default CategoriesListView;