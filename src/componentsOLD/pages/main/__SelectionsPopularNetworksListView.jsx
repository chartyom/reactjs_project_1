import React from 'react';
import { Link } from 'react-router';
import { LINK_PAGE as SELECTIONS_LINK_PAGE } from '../selection/SelectionPage';
import { getNumEnding } from '../../../utils/Tools';

export default ({ selections }) => (
  <div className="content-container fl fl-in-di">
    {selections.map(function (selection) {
      let textRightEnding = getNumEnding(selection.count, ['заведение', 'заведения', 'заведений']);
      return (
        <div key={selection.id} className="popular fl fl-dr-col fl-in-di fl-ai-c">
          <img className="popular__logo" src={selection.img} />
          <div>
            <p className="poopular__name">{selection.name}</p>
            <p className="popular__quantity">{selection.count} {textRightEnding}</p>
          </div>
          <Link to={SELECTIONS_LINK_PAGE + '/' + selection.id} className="abs-link"></Link>
        </div>
      );
    })}
  </div>
);
