import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import * as selectionsAction from '../../../actions/selectionsAction';
import SelectionsListView from '../../widgets/SelectionsView/SelectionsListView';
import { LINK_PAGE as SELECTIONS_LINK_PAGE } from '../selections/SelectionsPage';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';

class SelectionsView extends PureComponent {

  componentDidMount() {
    //Загрузка подборок "Популярное"
    this.props.dispatch(selectionsAction.getPopular());
  }

  renderSelections() {
    const { selectionsPopular } = this.props;

    if (
      selectionsPopular.readyState === selectionsAction.SELECTIONS_INVALID ||
      selectionsPopular.readyState === selectionsAction.SELECTIONS_POPULAR_FETCHING
    ) {
      return <LoaderView />;
    }

    if (selectionsPopular.readyState === selectionsAction.SELECTIONS_POPULAR_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('Failed to fetch => show error: ', selectionsPopular.error);
      switch (selectionsPopular.error.error) {
        case 404:
          return <ErrorPageView />;
        case 500:
          return <BadServer />;
        default:
          return <BadConnection />;
      }
    }
    return (
      <div className="center">
        <div className="content-container without-flex-spacing fl">
          <SelectionsListView selections={selectionsPopular.list} amount={9} beginMini={3} classes={'fl'} />
        </div>
        <div className="button-wrapper button-wrapper_center">
          <Link className="button-wrapper__button button-wrapper__button_red-hover" to={SELECTIONS_LINK_PAGE}>Показать еще</Link>
        </div>
      </div>
    );
  };


  render() {

    if (DEVELOPMENT) console.log('MainPage => SelectionsView: Render');
    return (
      <section className="main-container" id="selections-main">
        <div className="header-container header-container_bottom-line">
          <div className="center">
            <h3 className="header-container__header">Подборки</h3>
            <ul className="header-container__links">
              <li className="header-container__link-item">
                <a className="header-container__link header-container__link_active" href="#selections-main">Популярное</a>
              </li>
            </ul>
          </div>
        </div>
        {this.renderSelections()}
      </section>
    );
  }
};


SelectionsView.propTypes = {
  selectionsPopular: PropTypes.shape({
    readyState: PropTypes.string.isRequired,
    list: PropTypes.array
  }).isRequired
};

const mapStateToProps = ({ selections }) => ({
  selectionsPopular: selections.popular,
});

export default connect(mapStateToProps)(SelectionsView);