import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as categoriesAction from '../../../actions/categoriesAction';
import CategoriesListView from './CategoriesListView';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';

class CategoriesView extends PureComponent {

  componentDidMount() {
    this.props.dispatch(categoriesAction.get());
  }

  renderCategories() {
    const categories = this.props.categories;

    if (
      categories.readyState === categoriesAction.CATEGORIES_INVALID ||
      categories.readyState === categoriesAction.CATEGORIES_FETCHING
    ) {
      return <LoaderView />;
    }

    if (categories.readyState === categoriesAction.CATEGORIES_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('Failed to fetch => show error: ', categories.error);
      switch (categories.error.errorCode) {
        case 404:
          return <ErrorPageView />;
        case 500:
          return <BadServer />;
        default:
          return <BadConnection />;
      }
    }
    return <CategoriesListView categories={categories.list} amount={5} />;
  };

  render() {

    if (DEVELOPMENT) console.log('MainPage => CategoriesView: Render');

    return (
      <section className="main-container main-container_gray">
        <div className="header-container">
          <div className="center">
            <h3 className="header-container__header">По категориям</h3>
          </div>
        </div>
        <div className="center">
          {this.renderCategories()}
        </div>

      </section>
    );
  }
};


CategoriesView.propTypes = {
  categories: PropTypes.shape({
    readyState: PropTypes.string.isRequired,
    list: PropTypes.array
  }).isRequired
};

const mapStateToProps = ({ categories }) => ({
  categories: categories.main
});

export default connect(mapStateToProps)(CategoriesView);