import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import * as selectionsAction from '../../../actions/selectionsAction';
import EstablishmentsListView from '../../widgets/EstablishmentsView/EstablishmentsListView';
import { LINK_PAGE as ESTABLISHMENTS_LINK_PAGE } from '../establishments/EstablishmentsPage';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';

class EstablishmentsView extends PureComponent {

  componentDidMount() {
    //Загрузка подборок "Популярное"
    this.props.dispatch(selectionsAction.getById('57f761c8dc8289001ea28d86'));
  }

  renderPopularEstablishments() {
    const establishments = this.props.establishments;

    if (
      !establishments ||
      establishments.readyState === selectionsAction.SELECTION_FETCHING
    ) {
      return <LoaderView />;
    }

    if (establishments.readyState === selectionsAction.SELECTION_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('Failed to fetch => show error: ', establishments.error);
      switch (establishments.error.error) {
        case 404:
          return <ErrorPageView />;
        case 500:
          return <BadServer />;
        default:
          return <BadConnection />;
      }
    }

    return (
      <div className="center">
        <EstablishmentsListView establishments={establishments.list} amount={8} classes={'content-container fl place__list--col-4'} />
        <div className="button-wrapper button-wrapper_center">
          <Link className="button-wrapper__button button-wrapper__button_red-hover" to={ESTABLISHMENTS_LINK_PAGE}>Показать все</Link>
        </div>
      </div>
    );
  };

  render() {

    if (DEVELOPMENT) console.log('MainPage => EstablishmentsView: Render');

    return (
      <section className="main-container" id="establishments-main">
        <div className="header-container header-container_bottom-line">
          <div className="center">
            <h3 className="header-container__header">Заведения</h3>
            <ul className="header-container__links">
              <li className="header-container__link-item">
                <a className="header-container__link header-container__link_active" href="#establishments-main">Популярные</a>
              </li>
              {/*<li className="header-container__link-item">
                <a className="header-container__link" href="#establishments-main">Рядом</a>
              </li>*/}
            </ul>
          </div>
        </div>
        {this.renderPopularEstablishments()}
      </section>
    );
  }
};


EstablishmentsView.propTypes = {
  dispatch: PropTypes.func.isRequired,
  // establishments: PropTypes.shape({
  //   readyState: PropTypes.string.isRequired,
  //   list: PropTypes.array,
  // }).isRequired
};

const mapStateToProps = ({ selections }) => ({
  establishments: selections.byId['57f761c8dc8289001ea28d86']
});

export default connect(mapStateToProps)(EstablishmentsView);