import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import * as selectionsAction from '../../../actions/selectionsAction';
import SelectionsPopularNetworksListView from './SelectionsPopularNetworksListView';
import { LINK_PAGE as STOCKS_LINK_PAGE } from '../stocks/StocksPage';

class SelectionsPopularNetworksView extends PureComponent {

  componentDidMount() {
    //Загрузка подборок "Популярное"
    this.props.dispatch(selectionsAction.getPopularNetworksWithChecked());
  }

  renderNetworks() {
    const {selections} = this.props;
    if (
      selections.readyState === selectionsAction.SELECTIONS_INVALID ||
      selections.readyState === selectionsAction.SELECTIONS_POPULAR_NETWORKS_FETCHING
    ) {
      return <p>Loading...</p>;
    }

    if (selections.readyState === selectionsAction.SELECTIONS_POPULAR_NETWORKS_FETCH_FAILED) {
      return <p>Failed to fetch</p>;
    }
    return <SelectionsPopularNetworksListView selections={selections.list} />;
  };

  render() {

    if (DEVELOPMENT) console.log('MainPage->SelectionsPopularNetworksView: Render');

    return (
      <section className="main-container main-container_gray">
        <div className="header-container">
          <div className="center fl fl-ai-c">
            <h3 className="header-container__header">Популярные сети</h3>
          </div>
        </div>
        <div className="center">
          {this.renderNetworks()}
        </div>
      </section>
    );
  }
};


SelectionsPopularNetworksView.propTypes = {
  selections: PropTypes.shape({
    readyState: PropTypes.string.isRequired,
    list: PropTypes.array
  }).isRequired
};

const mapStateToProps = ({ selections }) => ({
  selections: selections.popularNetworks
});

export default connect(mapStateToProps)(SelectionsPopularNetworksView);