import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import * as selectionsAction from '../../../actions/selectionsAction';
import HeaderView from './HeaderView';
//import HeaderDescriptionView from './HeaderDescriptionView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';

class SelectionView extends PureComponent {

	getSelection() {
		return this.props.selection;
	}

	renderSelection() {
		const selection = this.getSelection();

		if (
			!selection ||
			selection.readyState === selectionsAction.SELECTION_FETCHING
		) {
			return <HeaderView />;
		}

		if (selection.readyState === selectionsAction.SELECTION_FETCH_FAILED) {
			if (DEVELOPMENT) console.log('Failed to fetch => show error: ', selection.error);
			switch (selection.error.error) {
				case 404:
					return <ErrorPageView />;
				case 500:
					return <BadServer />;
				default:
					return <BadConnection />;
			}
		}

		const PAGE_NAME = this.getSelection().name ? this.getSelection().name : '';
		const PAGE_DESCRIPTION = 'Подборка ' + PAGE_NAME + ' - Битком24. Лучшие рестораны, бары и кафе Москвы.';
		const PAGE_KEYWORDS = 'битком, битком24, bitkom, bitkom24, bitcom, bitcom24, лучшие, рестораны, бары, кафе, москва, заказ, столик, стол, бронирование, меню, отзывы, адрес, бургеры, суши, веранда, панорама, ревизорро, летучая, скидки, акции, мероприятия, заказать, телефон, ' + PAGE_NAME;

		return (
			<section>
				<Helmet
					title={PAGE_NAME}
					meta={[
						{ name: 'description', content: PAGE_DESCRIPTION },
						{ name: 'Keywords', content: PAGE_KEYWORDS },
					]}
				/>
				<HeaderView selection={selection} />
				{/*<HeaderDescriptionView selection={selection} />*/}
			</section>
		);
	};

	render() {

		if (DEVELOPMENT) console.log('SelectionPage => SelectionView: Render');

		return this.renderSelection();
	}
};


SelectionView.propTypes = {
	dispatch: PropTypes.func.isRequired,
	selection: PropTypes.shape({
		readyState: PropTypes.string,
		name: PropTypes.string,
	})
};


const mapStateToProps = ({ selections }, { params }) => ({
	selection: selections.byId[params.id]
});


export default connect(mapStateToProps)(SelectionView);