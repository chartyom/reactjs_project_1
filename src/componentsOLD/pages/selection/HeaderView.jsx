import React from 'react';
import { getNumEnding } from '../../../utils/Tools';
import LoaderView from '../../widgets/LoaderView';

const HeaderView = ({ selection }) => {

  if (selection) {
    let textRightEnding = getNumEnding(selection.length, ['заведение', 'заведения', 'заведений']);
    let style = { backgroundImage: 'url(' + selection.header + ')' };
    return (
      <section style={style} className="preview-note fl fl-dr-col fl-ai-c fl-in-di">
        <p className="choice choice_gold">{selection.type}</p>
        <h1 className="preview-note__header">{selection.name}</h1>
        <div className="quantity">
          <p>{selection.length} {textRightEnding}</p>
        </div>
      </section>
    );
  }
  return (
    <section className="preview-note fl fl-dr-col fl-ai-c fl-in-di">
      <p className="choice choice_gold">Выбор Битком</p>
      <LoaderView />
      <div className="quantity">
        <p>loading...</p>
      </div>
    </section>
  );
};

export default HeaderView;