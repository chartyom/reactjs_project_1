import React from 'react';

export default HeaderDescriptionView = ({ selection }) => {
  if (selection && selection.description)
    return (
      <section className="before-content-note">
        <div className="center">
          <div className="ordinary-note">
            <p className="ordinary-note__text">{selection.description}</p>
          </div>
        </div>
      </section>
    );
  return '';
};