import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import SearchSectionMiniHeader from '../../headers/SearchSectionMiniHeader';
import SelectionView from './SelectionView';
import EstablishmentsView from './EstablishmentsView';
//import FiltresView from './FiltresView';
import { getById } from '../../../actions/selectionsAction';
//import { getBySelectionId } from '../../../actions/establishmentsAction';

export const LINK_PAGE = '/selection';
export const LINK_MASK = '/selection/:id';

class SelectionPage extends PureComponent {

  static readyOnActions(dispatch, params) {
    return Promise.all([
      dispatch(getById(params.id)),
      //dispatch(getBySelectionId(params.id, 1, 9))
    ]);
  }

  componentDidMount() {
    SelectionPage.readyOnActions(this.props.dispatch, this.props.params);
  }


  render() {

    if (DEVELOPMENT) console.log('SelectionPage: Render');

    return (
      <div>
        <SearchSectionMiniHeader />
        <SelectionView params={this.props.params} />
        <section>
          <div className="header-container header-container_bottom-line header-container--paragraph">
            <div className="center">
              <h3 className="header-container__header">Заведения</h3>
            </div>
          </div>
          <div className="center">
            <EstablishmentsView params={this.props.params} />
          </div>
        </section>
      </div>
    );
  }
};


SelectionPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  params: PropTypes.shape({
    id: PropTypes.string.isRequired,
  }).isRequired,
};



export default connect()(SelectionPage);