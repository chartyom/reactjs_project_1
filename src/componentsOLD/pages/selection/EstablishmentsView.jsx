import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as selectionsAction from '../../../actions/selectionsAction';
import EstablishmentsListView from '../../widgets/EstablishmentsView/EstablishmentsListView';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';

class EstablishmentsView extends PureComponent {
	/*
		constructor(props) {
			super(props);
			this.state = { pageId: 2 };
		}
	
		setNextPage() {
			this.setState({
				pageId: this.state.pageId + 1
			});
		}
	
		buttonLoadEstablishments() {
			const establishments = this.props.establishments;
			if (establishments.readyState === establishmentsAction.ESTABLISHMENTS_TEMP_FETCHED) {
				this.setNextPage();
			}
			this.props.dispatch(establishmentsAction.getBySelectionId(this.props.params.id, this.state.pageId, 9));
		}
	
		renderButtonLoadEstablishments() {
			const establishments = this.props.establishments;
			if (
				establishments.list &&
				establishments.list.length <= establishments.count &&
				establishments.readyState !== establishmentsAction.ESTABLISHMENTS_TEMP_MORE_FETCHING &&
				establishments.readyState !== establishmentsAction.ESTABLISHMENTS_TEMP_FETCH_FAILED
			) {
				return (
					<div className="more-places">
						<a className="more-places-button" href="javascript:;" onClick={() => this.buttonLoadEstablishments()}>
							<span>больше заведений</span>
						</a>
					</div>
				);
			}
		}*/
	/*
		renderLoader() {
			const establishments = this.props.establishments;
			if (establishments.readyState === establishmentsAction.ESTABLISHMENTS_TEMP_MORE_FETCHING) {
				return <LoaderView />;
			}
		}*/

	renderEstablishments() {
		const selection = this.props.selection || {};

		if (
			!selection.list || 
			selection.readyState === selectionsAction.SELECTION_FETCHING
		) {
			return <LoaderView />;
		}

		if (selection.readyState === selectionsAction.SELECTION_FETCH_FAILED) {
			if (DEVELOPMENT) console.log('Failed to fetch => show error: ', selection.error);
			switch (selection.error.errorCode) {
				case 404:
					return <ErrorPageView />;
				case 500:
					return <BadServer />;
				default:
					return <BadConnection />;
			}
		}

		return <EstablishmentsListView establishments={selection.list} classes={'fl place__list--col-4'} />;
	};

	render() {

		if (DEVELOPMENT) console.log('SelectionPage => EstablishmentsView: Render');

		return (
			<div className="clearfix">
				{this.renderEstablishments()}
			</div>
		);
		/*		
		return (
			<div>
				<div className="clearfix">
					{this.renderEstablishments()}
				</div>
				{this.renderLoader()}
				{this.renderButtonLoadEstablishments()}
			</div>
		);
		 */
	}
};


EstablishmentsView.propTypes = {
	selection: PropTypes.shape({
		list: PropTypes.array
	})
};

// const mapStateToProps = ({ establishments }) => ({
// 	establishments: establishments.temp
// });


const mapStateToProps = ({ selections }, { params }) => ({
	selection: selections.byId[params.id]
});

export default connect(mapStateToProps)(EstablishmentsView);
