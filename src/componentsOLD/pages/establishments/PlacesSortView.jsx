import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import CONFIG from '../../config';
import * as searchAction from '../../../actions/searchAction';
import * as establishmentsAction from '../../../actions/establishmentsAction';

/**
 * @param {string} sortBy
 * @param {string} sortDir
 */
class PlaceSortView extends PureComponent {

  handleSetSort(e) {
    var v = e.target.value;
    if (this.props.handleSetSort)
      this.props.handleSetSort(v);
  }

  handleSetTypeSort() {
    if (this.props.handleSetTypeSort)
      this.props.handleSetTypeSort();
  }

  renderButtonTypeSort() {
    if (this.props.sortDir == 'desc') {
      return (
        <button type="button" title="По возрастанию" className="btn btn-default_border" onClick={this.handleSetTypeSort.bind(this)}><i className="fa fa-sort-amount-asc" aria-hidden="true"></i>
        </button>
      );
    }
    return (
      <button type="button" title="По убыванию" className="btn btn-default_border" onClick={this.handleSetTypeSort.bind(this)}><i className="fa fa-sort-amount-desc" aria-hidden="true"></i>
      </button>
    );
  }

  handleGetText(v) {
    switch (v) {
      case 'croud':
        return 'По загруженности';
      case 'rating':
        return 'По популярности';
      case 'price':
        return 'По чеку';
    }
  }

  render() {
    return (
      <div className="header-container__sort fl nwr fl-ai-c">
        <select className="form-control" title="Выбор параметра сортировки" id="selectSort" value={this.props.sortBy} onChange={(e) => this.handleSetSort(e)}>
          {CONFIG.placesSort.by.map((a, i) => (
            <option key={i} value={a} >{this.handleGetText(a)}</option>
          ))}
        </select>
        {this.renderButtonTypeSort()}
      </div>
    );
  }
};

export default connect()(PlaceSortView);