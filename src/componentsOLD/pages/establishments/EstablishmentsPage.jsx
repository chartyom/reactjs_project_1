import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import SearchSectionMiniHeader from '../../headers/SearchSectionMiniHeader';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import EstablishmentsView from './EstablishmentsView';

export const LINK_PAGE = '/places';
export const LINK_MASK = '/places(/:pageId)';

class EstablishmentsPage extends PureComponent {

  static readyOnActions(dispatch, params, query) {
    var obj = {};
    if (query.sort) {
      var a = query.sort.split(',');
      if (a instanceof Array && a[0] && a[1]) {
        //Нормализация данных
        if ((a[0] == 'croud' || a[0] == 'rating' || a[0] == 'price') && (a[1] == 'asc' || a[1] == 'desc')) {
          obj.sort = {};
          obj.sort.by = a[0];
          obj.sort.dir = a[1];
        }
      }
    }
    return Promise.all([
      dispatch(establishmentsAction.get(params.pageId, 32, obj)),
    ]);
  }

  render() {

    if (DEVELOPMENT) console.log('EstablishmentsPage: Render');

    const PAGE_NAME = 'Рестораны, бары, кафе и клубы';
    const PAGE_DESCRIPTION = 'Рестораны, бары кафе и клубы - Битком24. Лучшие рестораны, бары и кафе Москвы.';
    const PAGE_KEYWORDS = 'битком, битком24, bitkom, bitkom24, bitcom, bitcom24, лучшие, рестораны, бары, кафе, москва, заказ, столик, стол, бронирование, меню, отзывы, адрес, бургеры, суши, веранда, панорама, ревизорро, летучая, скидки, акции, мероприятия, заказать, телефон, большой, брассерия мост, о2 lounge, uilliams, nobu, twins, сыроварня, уголёк, Vаниль, русский, sixty, dr.живаго, воронеж, black thai, white rabbit';

    return (
      <div>
        <Helmet
          title={PAGE_NAME}
          meta={[
            { name: 'description', content: PAGE_DESCRIPTION },
            { name: 'Keywords', content: PAGE_KEYWORDS },
          ]}
        />
        <SearchSectionMiniHeader />
        <EstablishmentsView params={this.props.params} query={this.props.location.query} />
      </div>
    );
  }
};


export default EstablishmentsPage;