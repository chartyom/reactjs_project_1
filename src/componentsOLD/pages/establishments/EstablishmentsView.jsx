import React, { PureComponent, PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import EstablishmentsListView from '../../widgets/EstablishmentsView/EstablishmentsListView';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';
import PaginationElement from '../../elements/pagination';
import { LINK_PAGE } from './EstablishmentsPage';
import PlacesSortView from './PlacesSortView';
import CONFIG from '../../config';

/**
 * @param {object} params
 * @param {object} query
 */
class EstablishmentsView extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      sortBy: CONFIG.placesSort.byDefault,
      sortDir: CONFIG.placesSort.dirDefault,
    };
  }

  componentDidMount() {
    this.fetch(this.props.params.pageId);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.params.pageId !== this.props.params.pageId)
      this.fetch(this.props.params.pageId);
    if (prevProps.query.sort !== this.props.query.sort) {
      this.fetch(this.props.params.pageId, false);
    }
  }

  fetch(pageId, cache) {
    var obj = {};
    if (this.props.query.sort) {
      var a = this.props.query.sort.split(',');
      if (a instanceof Array && a[0] && a[1]) {
        //Нормализация данных
        if ((a[0] == 'croud' || a[0] == 'rating' || a[0] == 'price') && (a[1] == 'asc' || a[1] == 'desc')) {
          obj.sort = {};
          obj.sort.by = a[0];
          obj.sort.dir = a[1];
          this.setState({ sortBy: a[0], sortDir: a[1] });
        }
      }
    }
    this.props.dispatch(establishmentsAction.get(pageId, 32, obj, cache));
  }

  renderEstablishments() {
    const { establishments } = this.props;
    if (
      establishments.readyState === establishmentsAction.ESTABLISHMENTS_INVALID ||
      establishments.readyState === establishmentsAction.ESTABLISHMENTS_TEMP_FETCHING
    ) {
      return <LoaderView />;
    }

    if (establishments.readyState === establishmentsAction.ESTABLISHMENTS_TEMP_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('Failed to fetch => show error: ', establishments.error);
      switch (establishments.error.statusCode) {
        case 404:
          return <ErrorPageView />;
        case 500:
          return <BadServer />;
        default:
          return <BadConnection />;
      }
    }
    return <EstablishmentsListView establishments={establishments.list} amount={32} classes={'content-container content-container_without-padd fl place__list--col-4'} />;
  };
  renderPagination() {
    const { establishments } = this.props;
    if (
      establishments.readyState === establishmentsAction.ESTABLISHMENTS_TEMP_FETCHED &&
      establishments.count
    )
      return <PaginationElement link={LINK_PAGE} thisPageId={this.props.params.pageId} count={establishments.count} amount={32} />;
  };

  handleSetSort(v) {
    this.props.router.push({ pathname: LINK_PAGE, query: { sort: v + ',' + this.state.sortDir } });
  }

  handleSetTypeSort() {
    var dir = 'asc';
    if (this.state.sortDir == 'asc')
      dir = 'desc';

    this.props.router.push({ pathname: LINK_PAGE, query: { sort: this.state.sortBy + ',' + dir } });
  }

  render() {
    return (
      <div className="main-container">
        <div className="header-container center">
          <div className="fl-in-di full-width">
            <h3 className="header-container__header header-container__header_big-font">Заведения</h3>
            <PlacesSortView
              handleSetSort={this.handleSetSort.bind(this)}
              handleSetTypeSort={this.handleSetTypeSort.bind(this)}
              sortBy={this.state.sortBy}
              sortDir={this.state.sortDir} />
          </div>
        </div>
        <div className="center">
          {this.renderEstablishments()}
        </div>
        {this.renderPagination()}
      </div>
    );
  }
};


EstablishmentsView.propTypes = {
  dispatch: PropTypes.func.isRequired,
  establishments: PropTypes.shape({
    readyState: PropTypes.string.isRequired,
    list: PropTypes.array,
    count: PropTypes.number
  }).isRequired
};

const mapStateToProps = ({ establishments }) => ({
  establishments: establishments.temp
});

export default connect(mapStateToProps)(withRouter(EstablishmentsView));