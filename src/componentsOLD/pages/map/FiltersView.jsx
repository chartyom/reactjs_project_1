import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const FiltersView = () => (
  <section className="center filters-block">
    <div className="filters-wrap fl fl-ai-cnt nwr">
      <div className="filters-wrap__header">
        <p>Фильтр</p>
      </div>
      <div className="filters-wrap__filters fl fl-ai-fs fl-in-di">
        <div className="filters-wrap__filter-categories">
          <a className="single-filter-category" href="#">Тип заведения</a>
          <a className="single-filter-category" href="#">Кухня</a>
          <a className="single-filter-category" href="#">Метро</a>
          <a className="single-filter-category" href="#">Цены</a>
          <a className="single-filter-category" href="#">Время работы</a>
          <a className="single-filter-category" href="#">Особенности</a>
        </div>
      </div>
    </div>
  </section>
);

export default FiltersView;