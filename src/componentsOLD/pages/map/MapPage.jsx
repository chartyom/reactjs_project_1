import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import SearchSectionMiniHeader from '../../headers/SearchSectionMiniHeader';
import SearchView from './SearchView';
import { loadScript } from '../../../utils/Tools';
import CONFIG from '../../config';
import MapView from './MapView';

export const LINK_PAGE = '/map';
export const LINK_MASK = '/map';

class MapPage extends PureComponent {

  // static readyOnActions(dispatch, params) {
  //   return Promise.all([
  //     //dispatch(establishmentsAction.getWithChecked()),
  //   ]);
  // }

  constructor(props) {
    super(props);
    this.state = {
      searchValue: null,
    };
  }

  render() {

    const PAGE_NAME = 'Карта заведений';
    const PAGE_DESCRIPTION = 'Битком24 - Лучшие рестораны, бары и кафе Москвы, поиск по чеку, кухне, метро, меню, детскому меню, времени работы, особенностям. Подберем самый близкий ресторан';
    const PAGE_KEYWORDS = 'битком, битком24, bitkom, bitkom24, bitcom, bitcom24, лучшие, рестораны, бары, кафе, москва, заказ, столик, стол, бронирование, банкет, меню, отзывы, адрес, найти, поиск, самый близкий, геолокация, быстрый';

    if (DEVELOPMENT) console.log('MapPage: Render');

    return (
      <section className="map">
        <Helmet
          title={PAGE_NAME}
          meta={[
            { name: 'description', content: PAGE_DESCRIPTION },
            { name: 'Keywords', content: PAGE_KEYWORDS },
          ]}
        />
        <MapView />
        <SearchView />
      </section>
    );
  }
};


export default MapPage;