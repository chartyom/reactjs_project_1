import React, { PropTypes, PureComponent } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import * as searchAction from '../../../actions/searchAction';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import BadConnection from '../../widgets/BadConnection';
import BadServer from '../../widgets/BadServer';
import ErrorPageView from '../../widgets/ErrorPageView';
import { LINK_PAGE as ESTABLISHMENT_LINK_PAGE } from '../establishment/EstablishmentPage';
import LazyLoad from '../../elements/LazyLoad';

/**
 * @param {object} places
 */
class SearchView extends PureComponent {

	constructor(props) {
		super(props);
		this.state = {
			removeButton: false,
			preView: false,
		};

		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleSetClearValue = this.handleSetClearValue.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.handleSearchValue = this.handleSearchValue.bind(this);
		//this.handleShowPreview = this.handleShowPreview.bind(this);
		this.handleHidePreview = this.handleHidePreview.bind(this);
	}

	componentDidUpdate(prevProps) {
		if (prevProps.placeMoreInfo.id !== this.props.placeMoreInfo.id && this.props.placeMoreInfo.id !== null) {
			this.setState({ preView: true });
		}
	}


	componentWillUnmount() {
		this.props.dispatch(searchAction.setValue(''));
	}

	handleShowPreview(obj) {
		var selectElement = document.getElementsByClassName('marker-id' + obj.id);
		var staticmarkers = document.getElementsByClassName('map-marker');
		[].forEach.call(staticmarkers, function (d, i) {
			d.classList.remove('map-marker--bounceWave');
		});
		selectElement[0].classList.add("map-marker--bounceWave");
		this.props.dispatch(searchAction.setPlaceInfoMore(obj));
		this.setState({ preView: true });
	}


	renderList() {
		let loaderView = '';
		const { places } = this.props;
		if (places.readyState === establishmentsAction.ESTABLISHMENTS_SEARCH_FETCHING) {
			loaderView = <div className="map__results-list__loader"><LoaderView /></div>;
		}

		if (places.readyState === establishmentsAction.ESTABLISHMENTS_SEARCH_FETCH_FAILED) {
			if (DEVELOPMENT) console.log('FAILED FETCH => ERROR:', places.error);
			switch (places.error.statusCode) {
				case 404:
					return <ErrorPageView />;
				case 500:
					return <BadServer />;
				default:
					return <BadConnection />;
			}
		}
		if (places.list && places.list.length > 0)
			return (
				<div className="map__results">
					<div className="map__results-list">
						{loaderView}
						{places.list.map((place, i) => {
							var id = place.id,
								image = place.img,
								link = ESTABLISHMENT_LINK_PAGE + '/' + place.mapType + '/' + place.name + '/' + place.id,
								rating = place.rating,
								shortDescription = place.shortDesc,
								name = place.name,
								price = place.price,
								priceName = '₽', // TODO нужно исправлять на сервере
								underground = place.address && place.address.metro && place.address.metro.name ? place.address.metro.name : null,
								position = place.address.location,
								trend = place.trend,
								isActive = place.isActive, // true || false - Работа Битком24 с заведением
								crowd = place.croud, // Битком || Загружено || Свободно
								currentWork = place.currentWork, // с 12:00 || до 20:00 || Закрыто || 24
								status = place.status,
								statusName = place.statusName;


							/*
								STATUS
							*/

							var status = currentWork == 'Закрыто' ? currentWork : crowd,
								statusClass = '';
							if (currentWork == 'Закрыто') {
								statusClass = 'status__load-closed';
							} else if (crowd == 'Загружено') {
								statusClass = 'status_load-middle';
							} else if (crowd == 'Битком') {
								statusClass = 'status_load-hight';
							} else if (crowd == 'Свободно') {
								statusClass = 'status_load-low';
							}


							/*
								ADDRESS
							*/

							var undergroundView = '';
							if (underground) {
								undergroundView = (
									<p className="metro">{underground}</p>
								);
							}

							/*
								TREND
							*/
							var trendClass = '';
							if (trend == "up") {
								trendClass = " place-attributes__rating_up";
							} else if (trend == "down") {
								trendClass = "  place-attributes__rating_down";
							}
							var latLng = new google.maps.LatLng(
								position.lat,
								position.lon
							);
							return (
								<div key={i} className="map__item" onClick={() => this.handleShowPreview({
									id: id,
									image: image,
									title: name,
									rating: rating,
									kitchen: shortDescription,
									price: price,
									underground: underground,
									statusValue: status,
									status: statusClass,
									url: link,
									position: latLng
								})}>
									<div className="map__item-image place__image-block">
										<LazyLoad className="place__thumbnail" dataSrc={image} alt={name} />
									</div>
									<div className="place-attributes">
										<div className="map__item-top">
											<div>
												<p className="place-attributes__header map__item-header">{name}</p>
												<p className="place-attributes__subheader">{shortDescription}</p>
											</div>
											<div>
												<p className={"place-attributes__rating" + trendClass}><span>{rating}</span></p>
											</div>
										</div>
										<div className="map__item-bottom">
											<p className="place-attributes__middle-check"><span>{price} <i className="currency currency_rub currency_regular ">{priceName}</i></span></p>
											{undergroundView}
											<p className={"status  " + statusClass}><span>{status}</span></p>
										</div>
									</div>
								</div>
							);
						})}
					</div>
				</div>
			);
		return (
			<div className="map__results"><div> По вашему запросу ничего не найдено </div></div>
		);

	}


	handleHidePreview() {
		var staticmarkers = document.getElementsByClassName('map-marker');
		[].forEach.call(staticmarkers, function (d, i) {
			d.classList.remove('map-marker--bounceWave');
		});
		this.setState({ preView: false });
	}

	renderPreview() {
		if (this.props.placeMoreInfo.readyState === searchAction.SEARCH_MAIN_PLACE_INFO_MORE_CHANGED) {

			/*
				ADDRESS
			*/

			var undergroundView = '';
			if (this.props.placeMoreInfo.underground) {
				undergroundView = (
					<p className="metro">{this.props.placeMoreInfo.underground}</p>
				);
			}
			return (
				<div className="map__place">
					<div className="map__item-image place__image-block">
						<img className="place__thumbnail" src={this.props.placeMoreInfo.image} alt={this.props.placeMoreInfo.title} />
						<span className="map__place-back" onClick={this.handleHidePreview}><i className="fa fa-angle-left" aria-hidden="true"></i> назад</span>
					</div>
					<div className="place-attributes">
						<div className="map__item-top">
							<div>
								<Link to={this.props.placeMoreInfo.url} className="place-attributes__header map__item-header">{this.props.placeMoreInfo.title}</Link>
								<p className="place-attributes__subheader">{this.props.placeMoreInfo.kitchen}</p>
							</div>
							<div>
								<p className="place-attributes__rating"><span>{this.props.placeMoreInfo.rating}</span></p>
							</div>
						</div>
						<div className="map__item-bottom">
							<p className="place-attributes__middle-check"><span>{this.props.placeMoreInfo.price} <i className="currency currency_rub currency_regular ">₽</i></span></p>
							{undergroundView}
							<p className={"status " + this.props.placeMoreInfo.status}><span>{this.props.placeMoreInfo.statusValue}</span></p>
						</div>
						<Link to={this.props.placeMoreInfo.url} className="map__full-view-link btn btn-success btn-block">В карточку заведения</Link>
					</div>


				</div>
			);
		}
		return <div className="map__place map__item" />;
	}

	//HANDLE SUBMIT

	handleSubmit() {
		var q = this.refs.mapSearchInput.value;
		this.props.dispatch(searchAction.setValue(q));
		var obj = {};
		obj.q = q;
		this.props.dispatch(establishmentsAction.getForSearch(1, 50, obj));
	}

	handleKeyUp(e) {
		if (e.keyCode === 13) {
			this.handleSubmit();
		}
	}

	//HANDLE SET CLEAR SEARCH VALUE

	handleSetClearValue() {
		this.setState({ removeButton: false });
		this.refs.mapSearchInput.value = '';
		this.refs.mapSearchInput.focus();
		this.props.dispatch(searchAction.setValue(''));
	}

	//HANDLE SEARCH VALUE

	handleSearchValue(e) {
		const v = e.target.value;
		if (v.length > 0) {
			if (this.state.removeButton === false)
				return this.setState({ removeButton: true });
		} else {
			this.props.dispatch(searchAction.setValue(''));
			this.setState({ removeButton: false });
		}
	}

	render() {
		var buttonRemoveClass = !this.state.removeButton ? ' dn' : '';
		var previewActiveClass = this.state.preView ? ' map__left_place' : '';
		return (
			<section>
				<div className="center">
					<div className={"map__left" + previewActiveClass}>
						<div className="map__search">
							<div className="map__search-input map-search">
								<input type="text" ref="mapSearchInput" name="search" className="map-search__input" onKeyUp={this.handleKeyUp} onChange={this.handleSearchValue} placeholder="Введите название заведения" />
								<button type="button" className={"map-search__remove" + buttonRemoveClass} onClick={this.handleSetClearValue} ></button>
								<button type="button" className="map-search__search" onClick={this.handleSubmit}></button>
							</div>
							{this.renderList()}
						</div>
						{this.renderPreview()}
					</div>
				</div>
			</section>
		);
	}
};

const mapStateToProps = ({ establishments, search }) => ({
	places: establishments.search,
	value: search.main.value,
	placeMoreInfo: search.main.placeMoreInfo
});

export default connect(mapStateToProps)(SearchView);