import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import * as establishmentsAction from '../../../actions/establishmentsAction';
import * as searchAction from '../../../actions/searchAction';
import SearchSectionMiniHeader from '../../headers/SearchSectionMiniHeader';
import { loadScript } from '../../../utils/Tools';
import CONFIG from '../../config';
import { LINK_PAGE as ESTABLISHMENT_LINK_PAGE } from '../establishment/EstablishmentPage';

let markers = [];

class MapView extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      timeouts: [],
      timeoutsFetch: [],
      map: null,
    };

  }

  setTimeout() {
    var t = this.state.timeouts;
    t.push(setTimeout.apply(null, arguments));
    this.setState({ timeouts: t });
  }

  clearTimeouts() {
    this.state.timeouts.forEach(clearTimeout);
  }

  setTimeoutFetch() {
    var t = this.state.timeoutsFetch;
    t.push(setTimeout.apply(null, arguments));
    this.setState({ timeoutsFetch: t });
  }
  clearTimeoutsFetch() {
    this.state.timeoutsFetch.forEach(clearTimeout);
  }

  componentWillUnmount() {
    this.clearTimeouts();
    this.clearTimeoutsFetch();
    this.handleClearMarkers();
    google.maps.event.clearListeners(this.state.map, 'bounds_changed');
    //this.setState({ map: null });
  }

  componentDidMount() {
    loadScript("https://maps.googleapis.com/maps/api/js?key=" + CONFIG.map.key)
      .then(() => {
        this.initGoogleMap();
      })
      .then(() => {
        this.handleFetched();
      })
      .then(() => {
        this.handleEventListener();
      })
      .catch(e => console.log(e));
  }

  componentDidUpdate(prevProps) {

    //Приоритет 1
    if (this.props.value && prevProps.value !== this.props.value && this.props.value.length > 0) {
      this.clearTimeouts();
      this.handleClearMarkers();
      return;
    }

    if (this.props.value == '' && prevProps.value !== this.props.value) {
      this.clearTimeouts();
      this.handleClearMarkers();
      setTimeout(function () {
        this.handleFetch(this.state.map.getBounds().getNorthEast().toJSON(), this.state.map.getBounds().getSouthWest().toJSON());
      }.bind(this), 500);
      return;
    }

    // if (prevProps.places.readyState !== this.props.places.readyState && this.props.places.readyState === establishmentsAction.ESTABLISHMENTS_SEARCH_FETCHED && this.props.value.length === 0) {
    //   return this.handleFetched();
    // }

    if (prevProps.placeMoreInfo.id !== this.props.placeMoreInfo.id && this.props.placeMoreInfo.readyState === searchAction.SEARCH_MAIN_PLACE_INFO_MORE_CHANGED) {
      this.state.map.setCenter(this.props.placeMoreInfo.position);
      this.state.map.setZoom(17);
    }

    if (prevProps.places.readyState !== this.props.places.readyState && this.props.places.readyState === establishmentsAction.ESTABLISHMENTS_SEARCH_FETCHED) {
      return this.handleFetched();
    }
  }

  initGoogleMap() {
    const POSITION = { lat: this.props.locationCity.lat - 0.005, lng: this.props.locationCity.lng - 0.03 };
    var map = new google.maps.Map(document.getElementById('map'), {
      center: POSITION,
      scrollwheel: true,
      zoom: 13,
      zoomControl: true,
      styles: CONFIG.map.style,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false
    });

    this.setState({ map: map });
  }

  handleFetched() {
    var q = this.props.value;
    //if (q) this.handleClearMarkers();

    if (this.props.places.list && this.props.places.list.length > 0) {

      CustomMarker.prototype = new google.maps.OverlayView();

      function CustomMarker(latlng, map, type, status, id) {
        this.latlng_ = latlng;
        this.type_ = type || 'restaurant';
        this.status_ = status || 'close';
        this.id_ = id || 'default';
        this.setMap(map);
      }

      CustomMarker.prototype.draw = function () {
        var me = this;
        var div = this.div_;
        if (!div) {
          div = this.div_ = document.createElement('DIV');
          div.style.border = "none";
          div.style.position = "absolute";
          div.style.paddingLeft = "0px";
          div.style.cursor = 'pointer';
          //you could/should do most of the above via styling the class added below
          div.classList.add('map-marker', 'map-marker_status--' + this.status_, 'map-marker_type--' + this.type_, 'map-marker--hover', 'bounce', 'marker-id' + this.id_);

          google.maps.event.addDomListener(div, "click", function (event) {
            var staticmarkers = document.getElementsByClassName('map-marker');
            [].forEach.call(staticmarkers, function (d, i) {
              d.classList.remove('map-marker--bounceWave');
            });
            event.target.classList.add("map-marker--bounceWave");
            google.maps.event.trigger(me, "click");
          });

          var panes = this.getPanes();
          panes.overlayImage.appendChild(div);
        }

        var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
        if (point) {
          div.style.left = point.x + 'px';
          div.style.top = point.y + 'px';
        }
      };

      CustomMarker.prototype.remove = function () {
        if (this.div_) {
          this.div_.parentNode.removeChild(this.div_);
          this.div_ = null;
        }
      };

      //this.handleClearMarkers();
      for (var i = 0; i < this.props.places.list.length; i++) {
        this.addMarkerWithTimeout(this.props.places.list[i], CustomMarker, i * 50);
      }

    }

  }

  handleShowPreview(obj) {
    this.props.dispatch(searchAction.setPlaceInfoMore(obj));
  }

  addMarkerWithTimeout(item, CustomMarker, timeout) {
    if (markers.map(i => (i.id)).indexOf(item.id) >= 0) return false;
    console.log(item)
    if (item.currentWork && item.croud) {
      var statusName = item.currentWork == 'Закрыто'
        ? item.currentWork
        : item.croud;
      item.statusName = statusName;

      var status = '';

      switch (statusName) {
        case "Закрыто":
          status = 'close';
          break;
        case "Загружено":
          status = 'middle';
          break;
        case "Битком":
          status = 'hight';
          break;
        case "Свободно":
          status = 'low';
          break;
      }
      if(!item.isActive) status = 'close';

      item.status = status;
    }

    var statusValue = item.currentWork == 'Закрыто' ? item.currentWork : item.croud,
      statusClass = '';
    if (item.currentWork == 'Закрыто') {
      statusClass = 'status__load-closed';
    } else if (item.croud == 'Загружено') {
      statusClass = 'status_load-middle';
    } else if (item.croud == 'Битком') {
      statusClass = 'status_load-hight';
    } else if (item.croud == 'Свободно') {
      statusClass = 'status_load-low';
    }

    //TYPE
    if (item.mapType) {

      var type = '';

      switch (item.mapType) {
        case "Ресторан":
          type = 'restaurant';
          break;
        case "Бар":
          type = 'bar';
          break;
        case "Клуб":
          type = 'club';
          break;
        case "Кафе":
          type = 'cafe';
          break;
        case "Винотека":
          type = 'vinoteka';
          break;
        case "Кальянная":
          type = 'hookah';
          break;
      }

      item.type = type;
    }
    var latLng = new google.maps.LatLng(
      item.address.location.lat,
      item.address.location.lon
    );

    markers.push({
      id: item.id,
      f: null
    });
    this.setTimeout(function () {
      var id = markers.map(i => (i.id)).indexOf(item.id);
      markers[id].f = new CustomMarker(
        latLng,
        this.state.map,
        item.type,
        item.status,
        item.id
      );
      markers[id].f.addListener('click', function () {
        this.handleShowPreview({
          id: item.id,
          image: item.img,
          title: item.name,
          rating: item.rating,
          kitchen: item.shortDesc,
          price: item.price,
          underground: item.address && item.address.metro && item.address.metro.name ? item.address.metro.name : null,
          statusValue: statusValue,
          status: statusClass,
          url: ESTABLISHMENT_LINK_PAGE + '/' + item.mapType + '/' + item.name + '/' + item.id,
          position: latLng
        });
        this.state.map.setZoom(17);
        this.state.map.setCenter(latLng);
      }.bind(this));
    }.bind(this), timeout);

  }

  handleEventListener() {
    google.maps.event.addListener(this.state.map, 'bounds_changed', function () {
      this.clearTimeoutsFetch();
      this.setTimeoutFetch(function () {
        this.handleFetch(this.state.map.getBounds().getNorthEast().toJSON(), this.state.map.getBounds().getSouthWest().toJSON());
      }.bind(this), 700);
    }.bind(this));
  }

  handleClearMarkers() {
    for (var i = 0; i < markers.length; i++) {
      if (markers[i] && markers[i].f)
        markers[i].f.setMap(null);
    }
    markers = [];
  }


  handleFetch(NorthEast, SouthWest) {
    var q = this.props.value;
    if (!q) {
      var obj = {};
      obj.bounding = {
        top: {
          lat: NorthEast.lat,
          lon: SouthWest.lng
        },
        bottom: {
          lat: SouthWest.lat,
          lon: NorthEast.lng,
        }
      };
      this.props.dispatch(establishmentsAction.getForSearch(1, 50, obj));
    }
  }

  render() {

    if (DEVELOPMENT) console.log('MapView: Render');
    return <div id="map" className="map__itsef"></div>;
  }
};


MapView.propTypes = {
  dispatch: PropTypes.func.isRequired,
};


const mapStateToProps = ({ authorization, establishments, search }) => ({
  locationCity: authorization.locationCity,
  places: establishments.search,
  value: search.main.value,
  placeMoreInfo: search.main.placeMoreInfo
});

export default connect(mapStateToProps)(MapView);