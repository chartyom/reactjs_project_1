import React, { PureComponent, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { loadScript } from '../../../utils/Tools';
import CONFIG from '../../config';
import { Link } from 'react-router';
import FeedbackSmallFormWidget  from '../../widgets/FeedbackSmallForm';

export const LINK_PAGE = '/contacts';
export const LINK_MASK = '/contacts';

class ContactsPage extends PureComponent {

  // static readyOnActions(dispatch, params) {
  //   return Promise.all([
  //   ]);
  // }

  componentDidMount() {
    this.Loader();
  }



  /**
   * Загрузка скриптов с проверкой загрузки
   */
  Loader() {
    loadScript("https://maps.googleapis.com/maps/api/js?key=" + CONFIG.map.key)
      .then(() => {
        this.initGoogleMap();
      })
      .catch(e => { if (DEVELOPMENT) console.log(e); });
  }


  /*
    ===== BEGIN GOOGLE MAP =====
  */

  /*
   Инициализация google карты
   */
  initGoogleMap() {
    const POSITION = new google.maps.LatLng(55.766757, 37.686247);

    var map = new google.maps.Map(document.getElementById('contacts-page_map'), {
      center: POSITION,
      scrollwheel: false,
      zoom: 16,
      styles: CONFIG.map.style,
      zoomControl: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false
    });

    CustomMarker.prototype = new google.maps.OverlayView();

    var marker = new CustomMarker(POSITION, map);

    function CustomMarker(latlng, map) {
      this.latlng_ = latlng;
      this.setMap(map);
    }

    CustomMarker.prototype.draw = function () {
      var me = this;
      var div = this.div_;
      if (!div) {
        div = this.div_ = document.createElement('DIV');
        div.style.border = "none";
        div.style.position = "absolute";
        div.style.paddingLeft = "0px";
        div.style.cursor = 'pointer';
        //you could/should do most of the above via styling the class added below
        div.classList.add('map-marker', 'map-marker_theme--arrow');

        google.maps.event.addDomListener(div, "click", function (event) {
          google.maps.event.trigger(me, "click");
        });

        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
      }

      var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
      if (point) {
        div.style.left = point.x + 'px';
        div.style.top = point.y + 'px';
      }
    };

    CustomMarker.prototype.remove = function () {
      if (this.div_) {
        this.div_.parentNode.removeChild(this.div_);
        this.div_ = null;
      }
    };

  }

  /*
    ===== END GOOGLE MAP =====
  */

  render() {

    if (DEVELOPMENT) console.log('ContactsPage: Render');

		const PAGE_NAME = 'Контактная информация';
		const PAGE_DESCRIPTION = 'Битком24 - Лучшие рестораны, бары и кафе Москвы. Умный сервис по выбору и бронированию ресторанов, баров и кафе. Телефоны, адрес и форма обратной связи.';
		const PAGE_KEYWORDS = 'битком, битком24, bitkom, bitkom24, bitcom, bitcom24, заказ, столик, стол, бронирование, банкет, заказать, телефон, часы работы, почта, обратная связь, написать';

    return (
      <section>
        <Helmet
					title={PAGE_NAME}
					meta={[
						{ name: 'description', content: PAGE_DESCRIPTION },
						{ name: 'Keywords', content: PAGE_KEYWORDS },
					]}
				/>
        <div className="header-container header-container_content-page header-container_content-page_image">
          <div className="center">
            <div className="full-width fl fl-in-di">
              <div>
                <ul className="breadcrumbs breadcrumbs_banner">
                  <li><Link to="/">Главная</Link></li>
                  <li><span>/</span></li>
                  <li><span>Контакты</span></li>
                </ul>
                <h1 className="header-container__header">Контакты</h1>
              </div>
            </div>
          </div>
        </div>
        <div className="center">
          <div className="content-container contacts-cnt">
            <div className="full-width fl fl-in-di">
              <div className="left">
                <div className="contact-blck contact-blck_info">
                  <div className="header-container header-container_bottom-line header-container--paragraph">
                    <h3 className="header-container__header">Контактная информация</h3>
                  </div>

                  <div className="contact-blck__contact phone">
                    <a className="phone__phone-number fl fl-ai-c" href="tel:+79035597489">8 (903) 559-74-89 </a>
                    <p className="phone__phone-note">По вопросам сотрудничества</p>
                  </div>
                  <div className="contact-blck__contact email-address">
                    <a className="email-address__name fl fl-ai-c" href="mailto:info@bitkom24.ru">info@bitkom24.ru</a>
                    <p className="email-address__note">По вопросам сотрудничества</p>
                  </div>
                  <div className="contact-blck__contact phone">
                    <a className="phone__phone-number fl fl-ai-c" href="tel:+74996382872">8 (499) 638-28-72 </a>
                    <p className="phone__phone-note">Телефон контакт центра</p>
                  </div>
                  <div className="contact-blck__contact email-address">
                    <a className="email-address__name fl fl-ai-c" href="mailto:support@bitkom24.ru">support@bitkom24.ru</a>
                    <p className="email-address__note">Техническая поддержка</p>
                  </div>
                </div>
                <div className="contact-blck contact-blck_office">
                  <div className="header-container header-container_bottom-line header-container--paragraph">
                    <div className="full-width fl fl-in-di fl-ai-b">
                      <h3 className="header-container__header">Офис</h3>
                      <p className="metro">Бауманская</p>
                    </div>
                  </div>
                  <p>105005, г. Москва,  ул. 2-я Бауманская, д. 5, с. 1, пом. 506А</p>
                  <div id="contacts-page_map" className="contact-blck__map"></div>
                </div>
              </div>
              <div className="right">
                <div className="contact-blck contact-blck_law-info">
                  <div className="header-container  header-container--paragraph">
                    <h3 className="header-container__header">Юридическая информация</h3>
                  </div>
                  <p>Общество с ограниченной ответственностью «Битком24» (ООО «Битком»)</p>
                  <p>ИНН 9701046553 <br />КПП 770101001 <br />ОГРН 5147746379523</p>
                </div>
                <FeedbackSmallFormWidget />
              </div>
            </div>
          </div>
        </div>
		  </section>
    );
  }
};

export default ContactsPage;