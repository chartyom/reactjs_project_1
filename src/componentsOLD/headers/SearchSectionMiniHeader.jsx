import React, { PureComponent, PropTypes } from 'react';
import { Link, withRouter } from 'react-router';
import { connect } from 'react-redux';
import SelectSearchElement from '../elements/SelectSearch';
import * as searchAction from '../../actions/searchAction';
import * as establishmentsAction from '../../actions/establishmentsAction';
import LoaderView from '../widgets/LoaderView/LoaderView';
import BadConnection from '../widgets/BadConnection';
import BadServer from '../widgets/BadServer';
import ErrorPageView from '../widgets/ErrorPageView';
import { LINK_PAGE as SEARCH_LINK_PAGE } from '../pages/search/SearchPage';

/**
 * style: ssmg
 * @param {bool} isOpenFilters Состояние открытия фильтров
 * @param {func} handleAfterSubmitAction функция callback при нажатии кнопки найти
 * @param {bool} searchValue Поисковое значение
 * @param {bool} handleFetch (false||true) Требование загрузки заведений (требуется для страницы загрузки)
 * @param {object} query url query значения
 * @param {object} params url params значения
 */
class SearchSectionMiniHeader extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      closeButton: false,
      filterSpace: false,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSetClearValue = this.handleSetClearValue.bind(this);
    this.handleToogleFilters = this.handleToogleFilters.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleSearchValue = this.handleSearchValue.bind(this);
  }

  componentWillMount() {
    if (this.props.isOpenFilters) {
      this.setState({ filterSpace: true });
    }
    //Выполнение на странице поиска
    if (this.props.handleFetch) {
      this.handleBeforeSetFilters();
    }
  }

  componentWillUpdate() {
    this.props.dispatch(searchAction.setValue(''));
  }

  componentDidMount() {
    this.props.dispatch(searchAction.getFilters());
    //Выполнение вне страницы поиска
    if (!this.props.handleFetch) {
      this.props.dispatch(searchAction.removeActiveFilters());
    }
    //Выполнение на странице поиска
    if (this.props.handleFetch) {

      var search = this.props.searchValue ? this.props.searchValue : this.props.value ? this.props.value : '';
      if (search.length > 0) this.setState({ closeButton: true });
      this.setSearchValue(search);
      this.handleFetch();
    }
  }

  componentDidUpdate(prevProps) {

    //Приоритет 1
    if (prevProps.params && this.props.params && prevProps.params.pageId !== this.props.params.pageId) {
      return this.handleFetch();
    }


    //Выполнение на странице поиска
    if (this.props.handleFetch) {
      if (prevProps.query && this.props.query &&
        prevProps.query.kitchens !== this.props.query.kitchens ||
        prevProps.query.types !== this.props.query.types ||
        prevProps.query.tags !== this.props.query.tags ||
        prevProps.query.underground !== this.props.query.underground
      ) {
        this.handleBeforeSetFilters();
        //Убирает сохраненную ранее надпись
        if (prevProps.query && prevProps.query.q && prevProps.query.q !== this.props.query.q)
          this.setSearchValue('');
        return this.handleFetch();
      }


      if (
        prevProps.activeFilters.kitchens.length != this.props.activeFilters.kitchens.length ||
        prevProps.activeFilters.types.length != this.props.activeFilters.types.length ||
        prevProps.activeFilters.tags.length != this.props.activeFilters.tags.length ||
        prevProps.activeFilters.underground.length != this.props.activeFilters.underground.length
      ) {
        this.handleUrl();
        return this.handleFetch();
      }

    }
  }

  /**
   * Установка фильтров из URL строки
   */
  handleBeforeSetFilters() {
    const { query } = this.props;
    if (query) {
      var q = {};
      if (query.kitchens) {
        q.kitchens = query.kitchens.split(',');
      }
      if (query.types) {
        q.types = query.types.split(',');
      }
      if (query.tags) {
        q.tags = query.tags.split(',');
      }
      if (query.underground) {
        q.underground = query.underground.split(',');
      }
      this.props.dispatch(searchAction.setActiveFilters(q));
    }
  }

  getSearchValue() {
    return this.refs.ssmhSearchInput.value || '';
  }

  setSearchValue(v) {
    this.refs.ssmhSearchInput.value = v;
  }

  //HANDLE SEARCH VALUE

  handleSearchValue(e) {
    const v = e.target.value;
    if (v.length > 0) {
      if (this.state.closeButton === false)
        return this.setState({ closeButton: true });
    } else {
      this.setState({ closeButton: false });
    }
  }


  //HANDLE SET CLEAR SEARCH VALUE

  handleSetClearValue() {
    this.setState({ closeButton: false });
    this.refs.ssmhSearchInput.value = '';
    this.refs.ssmhSearchInput.focus();
    this.handleUrl();
  }

  //HANDLE TOOGLE FILTERS

  handleToogleFilters() {
    if (this.state.filterSpace) {
      return this.setState({ filterSpace: false });
    }
    this.setState({ filterSpace: true });
  }

  //HANDLE SUBMIT

  handleSubmit() {
    var { activeFilters } = this.props;

    //Перенаправление на страницу поиска

    this.props.dispatch(searchAction.setValue(this.getSearchValue()));
    this.handleUrl();
    //Сработает только на странице поиска
    this.handleFetch();

  }

  getParamPageId() {
    return this.props.params && this.props.params.pageId ? this.props.params.pageId : 1;
  }

  /**
   * Формирует новый URL при каждом изменении фильтров или значения поискового поля
   * @param {bool} startPageId 
   */
  handleUrl(startPageId) {
    var q = this.getSearchValue();
    var { activeFilters } = this.props;
    var pathname = this.getParamPageId() && this.getParamPageId() != 1 && startPageId ? SEARCH_LINK_PAGE + '/' + this.getParamPageId() : SEARCH_LINK_PAGE;
    var query = {};
    query.q = q;
    if (activeFilters.tags.length > 0) query.tags = activeFilters.tags.toString();
    if (activeFilters.types.length > 0) query.types = activeFilters.types.toString();
    if (activeFilters.kitchens.length > 0) query.kitchens = activeFilters.kitchens.toString();
    if (activeFilters.underground.length > 0) query.underground = activeFilters.underground.toString();
    this.props.router.push({ pathname: pathname, query: query });
  }

  handleFetch() {
    var q = this.getSearchValue();
    var { activeFilters, sort } = this.props;

    var obj = {};
    //TODO надо думать
    obj.q = q;
    obj.filter = {};
    obj.filter.tags = activeFilters.tags;
    obj.filter.types = activeFilters.types;
    obj.filter.kitchens = activeFilters.kitchens;
    obj.filter.metros = activeFilters.underground;
    obj.sort = sort;
    this.props.dispatch(establishmentsAction.getForSearch(this.getParamPageId(), 32, obj));
  }

  handleKeyUp(e) {
    if (e.keyCode === 13) {
      this.handleSubmit();
    }
  }

  hundleFilterKitchens(name) {
    this.props.dispatch(searchAction.setActiveFilter({
      kitchens: name
    }));
    this.handleUrl(true);
    this.handleFetch();
  }
  hundleFilterTypes(name) {
    this.props.dispatch(searchAction.setActiveFilter({
      types: name
    }));
    this.handleUrl(true);
    this.handleFetch();
  }
  hundleFilterTags(name) {
    this.props.dispatch(searchAction.setActiveFilter({
      tags: name
    }));
    this.handleUrl(true);
    this.handleFetch();
  }
  hundleFilterUnderground(name) {
    this.props.dispatch(searchAction.setActiveFilter({
      underground: name
    }));
    this.handleUrl(true);
    this.handleFetch();
  }

  hundleFilterClear() {
    this.props.dispatch(searchAction.removeActiveFilter({
      kitchens: false,
      tags: false,
      types: false,
      underground: false
    }));
  }

  hundleFilterKitchensRemove(name) {
    this.props.dispatch(searchAction.removeActiveFilter({
      kitchens: name
    }));
    this.handleUrl(true);
    this.handleFetch();
  }
  hundleFilterTypesRemove(name) {
    this.props.dispatch(searchAction.removeActiveFilter({
      types: name
    }));
    this.handleUrl(true);
    this.handleFetch();
  }
  hundleFilterTagsRemove(name) {
    this.props.dispatch(searchAction.removeActiveFilter({
      tags: name
    }));
    this.handleUrl(true);
    this.handleFetch();
  }
  hundleFilterUndergroundRemove(name) {
    this.props.dispatch(searchAction.removeActiveFilter({
      underground: name
    }));
    this.handleUrl(true);
    this.handleFetch();
  }

  renderFilters() {
    const { filters } = this.props;

    if (
      filters.readyState === searchAction.SEARCH_INVALID ||
      filters.readyState === searchAction.SEARCH_MAIN_FILTERS_FETCHING
    ) {
      return <LoaderView />;
    }

    if (filters.readyState === searchAction.SEARCH_MAIN_FILTERS_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('Failed to fetch => show error: ', filters.error);
      return (
        <div className="center ssmh__filters__fatch-failure">При загрузке фильтров произошла ошибка</div>
      );
    }

    return (
      <div className="ssmh__filters__inner center ">
        <SelectSearchElement
          data={filters.kitchens}
          value={"Кухня"}
          subValues={["тип", "типа", "типов"]}
          selectData={this.props.activeFilters.kitchens}
          hundleButtonAdd={this.hundleFilterKitchens.bind(this)}
          hundleButtonRemove={this.hundleFilterKitchensRemove.bind(this)}
        />

        <SelectSearchElement
          data={filters.types}
          value={"Заведение"}
          subValues={["тип", "типа", "типов"]}
          selectData={this.props.activeFilters.types}
          hundleButtonAdd={this.hundleFilterTypes.bind(this)}
          hundleButtonRemove={this.hundleFilterTypesRemove.bind(this)}
        />

        <SelectSearchElement
          data={filters.tags}
          value={"Особенности"}
          subValues={["тег", "тега", "тегов"]}
          selectData={this.props.activeFilters.tags}
          hundleButtonAdd={this.hundleFilterTags.bind(this)}
          hundleButtonRemove={this.hundleFilterTagsRemove.bind(this)}
        />

        <SelectSearchElement
          data={filters.metros}
          value={"Метро"}
          subValues={["станция", "станции", "станций"]}
          theme={'underground'}
          selectData={this.props.activeFilters.underground}
          hundleButtonAdd={this.hundleFilterUnderground.bind(this)}
          hundleButtonRemove={this.hundleFilterUndergroundRemove.bind(this)}
        />

      </div>
    );
  }


  render() {

    if (DEVELOPMENT) console.log('SearchSectionMiniHeader: Render');
    var closeClass = this.state.closeButton ? '' : ' dn';
    var filterClass = this.state.filterSpace ? ' ssmh__container__write__settings--active' : '';
    var filterSpaceClass = this.state.filterSpace ? ' ssmh__filters--active' : '';

    return (
      <section className="ssmh">
        <div className="ssmh__container">
          <div className="center fl nwr fl-ai-c">
            <div className="ssmh__container__write">
              <input className="ssmh__container__write__input" ref="ssmhSearchInput" type="text" onChange={this.handleSearchValue} onKeyUp={this.handleKeyUp} placeholder="Заведение, кухня или метро" />
              <button onClick={this.handleSetClearValue} type="button" className={"ssmh__container__write__remove" + closeClass} ></button>
              <button type="button" onClick={this.handleToogleFilters} className={"ssmh__container__write__settings" + filterClass}></button>
            </div>
            <button type="button" className="ssmh__container__find-button" onClick={this.handleSubmit}>Найти</button>
          </div>
          <div className={"ssmh__filters" + filterSpaceClass}>
            {this.renderFilters()}
          </div>
        </div>

      </section>
    );
  }
};

const mapStateToProps = ({ search }) => ({
  filters: search.main.filters,
  value: search.main.value,
  activeFilters: search.main.activeFilters,
  sort: search.main.sort
});


export default connect(mapStateToProps)(withRouter(SearchSectionMiniHeader));