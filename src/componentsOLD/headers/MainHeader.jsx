import React from 'react';
import { Link } from 'react-router';

import { LINK_PAGE as MAIN_LINK_PAGE } from '../pages/main/MainPage';
import { LINK_PAGE as SELECTIONS_LINK_PAGE } from '../pages/selections/SelectionsPage';
import { LINK_PAGE as ESTABLISMENTS_LINK_PAGE } from '../pages/establishments/EstablishmentsPage';
import { LINK_PAGE as MAP_LINK_PAGE } from '../pages/map/MapPage';

// import GeoLocation from '../../utils/GeoLocationHelpers';

class MainHeader extends React.Component {

  render() {

    if (DEVELOPMENT) console.log('MainHeader: Render');

    let classes = this.props.classes || '';
    return (
      <header className={"main-header " + classes} >
        <div className="main-header__inner center">
          <ul className="main-header__left">
            <li className="main-header__left__item">
              <Link to={MAIN_LINK_PAGE} className="main-header__logo">
                <img src="/images/Logo.png" alt="Битком24" />
              </Link>
            </li>
            <li className="main-header__left__item">
              <div className="main-header__city" >Москва</div>
            </li>
          </ul>
          <ul className="main-header__main-menu">
            <li className="main-header__main-menu__item">
              <Link to={ESTABLISMENTS_LINK_PAGE} className="main-header__main-menu__link" activeClassName="active">Заведения</Link>
            </li>
            <li className="main-header__main-menu__item">
              <Link to={SELECTIONS_LINK_PAGE} className="main-header__main-menu__link" activeClassName="active" >Подборки</Link>
            </li>
            <li className="main-header__main-menu__item">
              <Link to={MAP_LINK_PAGE} className="main-header__main-menu__link" activeClassName="active" >Карта</Link>
            </li>
          </ul>
          <ul className="main-header__right">
            <li className="main-header__right__item">
              <div className="main-header__phone">
                <a className="main-header__phone__number" href={"tel:" + this.props.numberPhone}>{this.props.numberPhoneMask}</a>
                <p className="main-header__phone__note">Бесплатное бронирование</p>
              </div>
            </li>
          </ul>
        </div>
      </header>
    );
  }
};

export default MainHeader;