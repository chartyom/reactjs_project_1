import React, { PureComponent, PropTypes } from 'react';
import { Link } from 'react-router';
/**
 * @param {array} tags список элементов
 * @param {array} selectData список используемых элементов
 * @param {number} amount количество выводимых записей
 */

class TagsListView extends PureComponent {

	handleSelectData(v) {
		var a = this.props.selectData;
		var i = a.indexOf(v);
		if (i === -1) {
			if (this.props.hundleButtonAdd)
				this.props.hundleButtonAdd(v);
		} else {
			if (this.props.hundleButtonRemove)
				this.props.hundleButtonRemove(v);
		}
	}
	render() {
		return (
			<div className="ssmainh__container__select__tags sstagsh">
				{this.props.tags.map((tag, i) => {
					if(this.props.amount && i >= this.props.amount) return;
					const selected = tag.selected ? true : false;
					let activeClass = this.props.selectData.indexOf(tag) >= 0 ? " sstagsh__item_active" : "";
					return (
						<button
							key={i}
							type="button"
							className={"sstagsh__item" + activeClass}
							onClick={() => this.handleSelectData(tag)} >
							{tag}
						</button>
					);
				})
				}
			</div>
		);
	}
};


TagsListView.propTypes = {
	tags: PropTypes.array.isRequired
};

export default TagsListView;