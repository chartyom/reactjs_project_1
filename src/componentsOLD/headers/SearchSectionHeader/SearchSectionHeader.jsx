import React, { PureComponent, PropTypes } from 'react';
import { Link, withRouter } from 'react-router';
import { connect } from 'react-redux';
import * as searchAction from '../../../actions/searchAction';
import TagsListView from './TagsListView';
import LoaderView from '../../widgets/LoaderView/LoaderView';
import SelectSearchElement from '../../elements/SelectSearch';
import { LINK_PAGE as SEARCH_LINK_PAGE } from '../../pages/search/SearchPage';
/**
 * 
 * @param {func} handleAfterSubmitAction функция callback при нажатии кнопки найти
 * @param {bool} searchValue Поисковое значение
 */
class SearchSectionHeader extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      closeButton: false,
      filterSpace: false,
    };

    // this.handleGoClick = this.handleGoClick.bind(this);
    // this.handleKeyUp = this.handleKeyUp.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSetClearValue = this.handleSetClearValue.bind(this);
    this.handleToogleFilters = this.handleToogleFilters.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleSearchValue = this.handleSearchValue.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(searchAction.getFilters());
    //Выполнение вне страницы поиска
    if (!this.props.handleFetch) {
      this.props.dispatch(searchAction.removeActiveFilters());
    }
  }


  getSearchValue() {
    return this.refs.ssmainhSearchInput.value || '';
  }

  setSearchValue(v) {
    this.refs.ssmainhSearchInput.value = v;
  }


  //HANDLE SEARCH VALUE

  handleSearchValue(e) {
    const v = e.target.value;
    if (v.length > 0) {
      if (this.state.closeButton === false)
        return this.setState({ closeButton: true });
    } else {
      this.setState({ closeButton: false });
    }
  }


  //HANDLE SET CLEAR SEARCH VALUE

  handleSetClearValue() {
    this.setState({ closeButton: false });
    this.refs.ssmainhSearchInput.value = '';
    this.refs.ssmainhSearchInput.focus();
  }

  //HANDLE TOOGLE FILTERS

  handleToogleFilters() {
    if (this.state.filterSpace) {
      return this.setState({ filterSpace: false });
    }
    this.setState({ filterSpace: true });
  }

  //HANDLE SUBMIT

  handleSubmit() {
    var { activeFilters } = this.props;
    var q = this.getSearchValue();
    //Перенаправление на страницу поиска
    var query = {};
    query.q = q;
    if (activeFilters.tags.length > 0) query.tags = activeFilters.tags.toString();
    if (activeFilters.types.length > 0) query.types = activeFilters.types.toString();
    if (activeFilters.kitchens.length > 0) query.kitchens = activeFilters.kitchens.toString();
    if (activeFilters.underground.length > 0) query.underground = activeFilters.underground.toString();
    this.props.router.push({ pathname: SEARCH_LINK_PAGE, query: query });

    this.props.dispatch(searchAction.setValue(q));

  }

  handleFetch() {
    var { activeFilters } = this.props;
    var obj = {};
    obj.q = this.getSearchValue();
    obj.filter = {};
    obj.filter.tags = activeFilters.tags;
    obj.filter.types = activeFilters.types;
    obj.filter.kitchens = activeFilters.kitchens;
    obj.filter.metros = activeFilters.underground;
    this.props.dispatch(establishmentsAction.get(this.props.params.pageId, 32, obj, false));
  }

  handleKeyUp(e) {
    if (e.keyCode === 13) {
      this.handleSubmit();
    }
  }


  /*
    === BEGIN HANDLE FILTERS ===
  */

  hundleFilterKitchens(name) {
    this.props.dispatch(searchAction.setActiveFilter({
      kitchens: name
    }));
  }
  hundleFilterTypes(name) {
    this.props.dispatch(searchAction.setActiveFilter({
      types: name
    }));
  }
  hundleFilterTags(name) {
    this.props.dispatch(searchAction.setActiveFilter({
      tags: name
    }));
  }
  hundleFilterUnderground(name) {
    this.props.dispatch(searchAction.setActiveFilter({
      underground: name
    }));
  }

  hundleFilterKitchensRemove(name) {
    this.props.dispatch(searchAction.removeActiveFilter({
      kitchens: name
    }));
  }
  hundleFilterTypesRemove(name) {
    this.props.dispatch(searchAction.removeActiveFilter({
      types: name
    }));
  }
  hundleFilterTagsRemove(name) {
    this.props.dispatch(searchAction.removeActiveFilter({
      tags: name
    }));
  }
  hundleFilterUndergroundRemove(name) {
    this.props.dispatch(searchAction.removeActiveFilter({
      underground: name
    }));
  }



  renderFilters() {
    const { filters } = this.props;

    if (
      filters.readyState === searchAction.SEARCH_INVALID ||
      filters.readyState === searchAction.SEARCH_MAIN_FILTERS_FETCHING
    ) {
      return <LoaderView />;
    }

    if (filters.readyState === searchAction.SEARCH_MAIN_FILTERS_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('Failed to fetch => show error: ', filters.error);
      return (
        <div className="center">При загрузке фильтров произошла ошибка</div>
      );
    }

    //TODO  NEED SEE
    /*var sD = [];
    if(this.props.selectData) {
      sD = this.props.selectData.split(',');
    }*/

    return (
      <div className="ssmainh__filters__inner center">
        <SelectSearchElement
          data={filters.kitchens}
          value={"Кухня"}
          subValues={["тип", "типа", "типов"]}
          selectData={this.props.activeFilters.kitchens}
          hundleButtonAdd={this.hundleFilterKitchens.bind(this)}
          hundleButtonRemove={this.hundleFilterKitchensRemove.bind(this)}
        />

        <SelectSearchElement
          data={filters.types}
          value={"Заведение"}
          subValues={["тип", "типа", "типов"]}
          selectData={this.props.activeFilters.types}
          hundleButtonAdd={this.hundleFilterTypes.bind(this)}
          hundleButtonRemove={this.hundleFilterTypesRemove.bind(this)}
        />

        <SelectSearchElement
          data={filters.tags}
          value={"Особенности"}
          subValues={["тег", "тега", "тегов"]}
          selectData={this.props.activeFilters.tags}
          hundleButtonAdd={this.hundleFilterTags.bind(this)}
          hundleButtonRemove={this.hundleFilterTagsRemove.bind(this)}
        />

        <SelectSearchElement
          data={filters.metros}
          value={"Метро"}
          subValues={["станция", "станции", "станций"]}
          theme={'underground'}
          selectData={this.props.activeFilters.underground}
          hundleButtonAdd={this.hundleFilterUnderground.bind(this)}
          hundleButtonRemove={this.hundleFilterUndergroundRemove.bind(this)}
        />

      </div>
    );
  }

  /*
    === END HANDLE FILTERS ===
  */

  renderTags() {
    const { filters } = this.props;

    if (
      filters.readyState === searchAction.SEARCH_INVALID ||
      filters.readyState === searchAction.SEARCH_MAIN_FILTERS_FETCHING
    ) {
      return <div></div>;
    }


    if (filters.readyState === searchAction.SEARCH_MAIN_FILTERS_FETCH_FAILED) {
      if (DEVELOPMENT) console.log('Failed to fetch => show error: ', filters.error);
      return (
        <div></div>
      );
    }

    return <TagsListView
      tags={filters.tags}
      amount={7}
      selectData={this.props.activeFilters.tags}
      hundleButtonAdd={this.hundleFilterTags.bind(this)}
      hundleButtonRemove={this.hundleFilterTagsRemove.bind(this)} />;
  }

  render() {

    if (DEVELOPMENT) console.log('SearchSectionHeader: Render');
    var closeClass = this.state.closeButton ? '' : ' dn';
    var filterClass = this.state.filterSpace ? ' ssmainh__container__select__openFilters--active' : '';
    var filterSpaceClass = this.state.filterSpace ? ' ssmainh__filters--active' : '';

    return (
      <section className="ssmainh">
        <div className="ssmainh__container center">
          <div className="ssmainh__container__header">
            <h1>Рестораны, бары и кафе в Москве</h1>
          </div>
          <div className="fl nwr fl-ai-c">
            <div className="ssmainh__container__write">
              <input className="ssmainh__container__write__input" ref="ssmainhSearchInput" type="text" onChange={this.handleSearchValue} onKeyUp={this.handleKeyUp} placeholder="Заведение, кухня или метро" />
              <button onClick={this.handleSetClearValue} type="button" className={"ssmainh__container__write__remove" + closeClass} ></button>
            </div>
            <button type="button" className="ssmainh__container__find-button" onClick={this.handleSubmit}>Найти</button>
          </div>
          <div className="ssmainh__container__select fl fl-in-di">
            {this.renderTags()}
            <button type="button" onClick={this.handleToogleFilters} className={"ssmainh__container__select__openFilters" + filterClass}>Фильтр</button>
          </div>
          <div className={"ssmainh__filters" + filterSpaceClass}>
            {this.renderFilters()}
          </div>
        </div>
      </section>
    );
  }
};

SearchSectionHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  filters: PropTypes.shape({
    readyState: PropTypes.string.isRequired
  }).isRequired,
  value: PropTypes.string,
  activeFilters: PropTypes.shape({
    readyState: PropTypes.string.isRequired,
    kitchens: PropTypes.array,
    types: PropTypes.array,
    tags: PropTypes.array,
    underground: PropTypes.array
  }).isRequired,
};


const mapStateToProps = ({ search }) => ({
  filters: search.main.filters,
  value: search.main.value,
  activeFilters: search.main.activeFilters
});

export default connect(mapStateToProps)(withRouter(SearchSectionHeader));