function connectConfigLink() {
    return process.env.NODE_ENV === 'development'
    ? {
        link: "https://dev.bitkom.su"
    }
    : {
        link: "https://bitkom.info"
    };
}

export default connectConfigLink();