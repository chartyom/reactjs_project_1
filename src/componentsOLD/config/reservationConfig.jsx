export default {
    peopleMin: 1,
    peopleMax: 30,
    peopleDefault: 2,
    banquetPeopleMin: 8,
    banquetPeopleMax: 150,
    banquetPeopleDefault: 8,
    limitDay: 28,
    banquetMinDay: 2,
    banquetMaxDay: 28,
    format: [
        "Корпоративный праздник (классический/тематический)",
        "Презентация",
        "Свадьба",
        "День рождение",
        "Юбилей",
        "Дискотека",
        "Кейтеринг",
        "Семинар/тренинг",
    ]
};