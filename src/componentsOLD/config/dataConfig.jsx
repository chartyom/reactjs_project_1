export default {
    cacheTime: 600,
    searchFiltersCacheTime: 3600, // Кеш поисковых фильтров - 1 час
};