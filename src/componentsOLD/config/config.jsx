import CONFIG_MAP from './mapConfig';
import CONFIG_SOCIAL from './socialConfig';
import CONFIG_RESERVATION from './reservationConfig';
import CONFIG_FORM from './formConfig';
import CONFIG_CONNECT from './connectConfig';
import CONFIG_DATA from './dataConfig';
import CONFIG_PLACES_SORT from './placesSortConfig';

export default {
    map: CONFIG_MAP,
    social: CONFIG_SOCIAL,
    reservation: CONFIG_RESERVATION,
    form: CONFIG_FORM,
    connect: CONFIG_CONNECT,
    data: CONFIG_DATA,
    placesSort: CONFIG_PLACES_SORT
};