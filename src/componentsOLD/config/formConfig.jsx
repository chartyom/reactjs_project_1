export default {

    phoneNumberRegExp: /(\+?\d{1,5})? \(\d{3}\) \d{3}-\d{2}-\d{2}/i,
    phoneNumberMask: '+7 (999) 999-99-99',
    phoneNumberPlaceholder: '+7 (___) ___-__-__',
    phoneNumberCodeRegExp: /\d{5}/,
    phoneNumberCodeMask: '99999',
    phoneNumberCodeMaskChar: '_',
    phoneNumberCodePlaceholder: '_____',
    refCodeMask: '******',
    refCodeMaskChar: '_',
    refCodePlaceholder: '______',

};