export default [
    {
        id: 'vk',
        name: "Вконтакте",
        link: "https://vk.com/bitkom24"
    },
    {
        id: 'instagram',
        name: "Инстаграм",
        link: "https://www.instagram.com/bitkom24"
    },
    {
        id: 'facebook',
        name: "Facebook",
        link: "https://www.fb.com/bitkom24.ru"
    }
];