import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import LazyLoad from '../elements/LazyLoad';
import { LINK_PAGE as ESTABLISHMENTS_LINK_PAGE } from '../pages/establishments/EstablishmentsPage';
import { LINK_PAGE as SELECTIONS_LINK_PAGE } from '../pages/selections/SelectionsPage';
import { LINK_PAGE as MAP_LINK_PAGE } from '../pages/map/MapPage';
import { LINK_PAGE as CONTACTS_LINK_PAGE } from '../pages/contacts/ContactsPage';
import { LINK_PAGE as SEARCH_LINK_PAGE } from '../pages/search/SearchPage';

export default class MainFooter extends React.Component {

  date() {
    let mdate = new Date();
    return mdate.getFullYear();
  }

  render() {
    return (
      <footer className="footer">
        <div className="header-container header-container_bottom-line">
          <div className="center">
            <div className="footer__social-media-links">
              <p className="footer__sm-clue">Битком 24 в социальных сетях</p>
              <a className="sm-link sm-vk" target="_blank" href="https://vk.com/bitkom24">Вконтакте</a>
              <a className="sm-link sm-instagram" target="_blank" href="https://www.instagram.com/bitkom24 ">Instagram</a>
              <a className="sm-link sm-facebook" target="_blank" href="https://www.fb.com/bitkom24.ru ">Facebook</a>
            </div>
          </div>
        </div>
        <div className="center">
          <div className="full-width footer__logo-name fl fl-ai-c">
            <Link to="/" className="footer__logo fl fl-ai-c" >
              <img className="footer__logo-img" src="/images/Logo.png" alt="Битком24" />
            </Link>
            <div className="footer__city">
              <a className="city" href="#">Москва</a>
            </div>
          </div>
          <div className="full-width footer__content fl fl-in-di">
            <div className="fl fl-ai-fs">
              <div className="phone">
                <a className="phone__phone-number fl fl-ai-c" href={"tel:" + this.props.numberPhone}>{this.props.numberPhoneMask}</a>
                <p className="phone__phone-note">бронировать бесплатно</p>
              </div>
              <div className="footer__menu">
                <ul>
                  <li><Link to={ESTABLISHMENTS_LINK_PAGE} className="footer__menu-item" activeClassName="active" >Заведения</Link></li>
                  <li><Link to={SELECTIONS_LINK_PAGE} className="footer__menu-item" activeClassName="active">Подборки</Link></li>
                </ul>
              </div>
            </div>
            <div className="footer__menu">
              <ul>
                <li><Link to={{ pathname: SEARCH_LINK_PAGE, query: { types: 'Ресторан' } }} className="footer__menu-item" activeClassName="active">Ресторан</Link></li>
                <li><Link to={{ pathname: SEARCH_LINK_PAGE, query: { types: 'Кафе' } }} className="footer__menu-item" activeClassName="active">Кафе</Link></li>
                <li><Link to={{ pathname: SEARCH_LINK_PAGE, query: { types: 'Бар' } }} className="footer__menu-item" activeClassName="active">Бар</Link></li>
                <li><Link to={{ pathname: SEARCH_LINK_PAGE, query: { types: 'Клуб' } }} className="footer__menu-item" activeClassName="active">Клуб</Link></li>
                <li><Link to={{ pathname: SEARCH_LINK_PAGE, query: { types: 'Винотека' } }} className="footer__menu-item" activeClassName="active">Винотека</Link></li>
                <li><Link to={{ pathname: SEARCH_LINK_PAGE, query: { types: 'Кальянная' } }} className="footer__menu-item" activeClassName="active">Кальянная</Link></li>
                {/*<li><Link to={MAP_LINK_PAGE} className="footer__menu-item" activeClassName="active">Карта</Link></li>
                <li><Link to="/" className="footer__menu-item" activeClassName="active">Афиша событий</Link></li>
                <li><Link to="/" className="footer__menu-item" activeClassName="active">Акции</Link></li>*/}
              </ul>
            </div>
            <div className="footer__menu">
              <ul>
                {/*<li><Link to={ABOUT_LINK_PAGE} className="footer__menu-item" activeClassName="active">О проекте</Link></li>*/}
                <li><Link to={CONTACTS_LINK_PAGE} className="footer__menu-item" activeClassName="active">Контакты</Link></li>
              </ul>
            </div>
            <div>
              <p className="footer__download-app-note">Приложение <span className="footer__download-app-note-bold">Битком 24</span></p>
              <a className="download-app-link" target="_blank" href="https://redirect.appmetrica.yandex.com/serve/457394471315723481">
                <LazyLoad dataSrc="/images/appstore.png" alt="Скачать приложение Битком24 в AppStore" />
              </a>
              <a className="download-app-link" target="_blank" href="https://redirect.appmetrica.yandex.com/serve/385337361753463199">
                <LazyLoad dataSrc="/images/playstore.png" alt="Скачать приложение Битком24 в GooglePlay" />
              </a>
            </div>
          </div>
        </div>
        <div className="copyright">
          <div className="center">
            <p className="copyright__note">© Битком 24, {this.date()}</p>
            {/*<a href="#" className="copyright__link">Оферта</a>*/}
            <a href='/terms' className="copyright__link" >Пользовательское соглашение</a>
          </div>
        </div>

      </footer>
    );
  }
}
