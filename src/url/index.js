export default {
    home: '/',
    homeMask: '/',
    signIn: '/signIn',
    signInMask: '/signIn',
    profile: '/profile',
    profileMask: '/profile',
    signOut: '/signOut',
    signOutMask: '/signOut',
    authorize: '/authorize',
    authorizeMask: '/authorize',
    registration: '/registration',
    registrationMask: '/registration',
}