
export const UI_DEFAULT = 'UI_DEFAULT'
export const UI_SET_REFERRER_URL = 'UI_SET_REFERRER_URL'
export const UI_SET_PHONE = 'UI_SET_PHONE'

/**
 * 
 * @param {String} url 
 */
export const setReferrerUrl = (url) => {
	return (dispatch, getState) => {
		dispatch({ type: UI_SET_REFERRER_URL, referrerUrl: url });
	}
}

/**
 * 
 * @param {String} url 
 */
export const setPhone = (phone) => {
	return (dispatch, getState) => {
		dispatch({ type: UI_SET_PHONE, phone });
	}
}
