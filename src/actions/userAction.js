import API from '../api'
import Storage from '../utils/Storage'
import * as AuthHelper from '../utils/AuthHelper'

/**
 * 
 * @return {Bool} 
 */
export const signOut = () => {
  const { access_token, token_type, refresh_token } = AuthHelper.get()
  return API.user.revokeToken(token_type, access_token, refresh_token).then((res) => {

    if (res.error) {
      console.log('error:', res.error, res.error_description)
      return false
    }

    AuthHelper.remove()

    return true
  }).catch((e) => console.log(e))
}


/**
 * Авторизация && Аутентификация пользователя
 * @param {Object} object
 * @param {Bool} object.allowed 
 * 
 * @return {Bool} 
 */
export const authorize = ({ allowed }) => {

  const { access_token, token_type, refresh_token } = AuthHelper.get()
  const uri = window.location.pathname + window.location.search + '&allowed=' + allowed

  return API.user.authorize(uri, token_type, access_token, refresh_token).then((res) => {
    if (res.error) {
      console.log('error:', res.error, res.error_description)
      return false
    }
    return true
  }).catch((e) => console.log(e))
}


/**
 * 
 * @param {String} phone 
 * 
 * @return {Promise<>} 
 */
export const checkPhone = (phone) => {
  return API.user.checkPhone(phone)
}

/**
 * 
 * @param {String} code 
 * 
 * @return {Promise<>} 
 */
export const checkReferrerCode = (code) => {
  return API.user.checkReferrerCode(code)
}

/**
 * 
 * @param {String} email 
 * 
 * @return {Promise<>} 
 */
export const checkEmail = (email) => {
  return API.user.checkEmail(email)
}



/**
 * 
 * @param {String} key 
 * 
 * @return {Promise<>} 
 */
export const regenerateCode = (key) => {
  return API.user.regenerateCode(key)
}


/**
 * 
 * @param {String} key 
 * @param {String} code
 * 
 * @return {Promise<>} 
 */
export const registration = (key, code) => {
  return API.user.registration(key, code)
}


/**
 * 

 * 
 * @return {Promise<>} 
 */
export const registrationSendSMS = ({ phone, code, firstName, lastName, email }) => {
  return API.user.registrationSendSMS(phone, code, firstName, lastName, email)
}

/**
 * 
 * 
 * @return {Promise<>} 
 */
export const authenticateSendSMS = (phone) => {
  return API.user.authenticateSendSMS(phone)
}


/**
 * 
 * 
 * @return {Promise<>} 
 */
export const authenticate = (phone, code) => {
  return API.user.authenticate(phone, code)
}


/**
 * Сохранение Авторизации && Аутентификации пользователя
 * @param {Object} r 
 * 
 */
export const saveToken = (r) => {
  AuthHelper.save(r)
}
