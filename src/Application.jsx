import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import App from './layouts/App'


//Polyfills for old browsers
require('es6-promise').polyfill()
require('isomorphic-fetch')

//Styles
import './styles/app.scss';

if (DEVELOPMENT) console.log('environment:', ENVIRONMENT);

const store = configureStore(window.__INITIAL_STATE__);
ReactDOM.render((
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
), document.getElementById('app'))