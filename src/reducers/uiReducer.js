import {
    UI_DEFAULT,
    UI_SET_REFERRER_URL,
    UI_SET_PHONE,
} from '../actions/uiAction';


export default function (state = {
    referrerUrl: null,
    phone: null
}, action) {
    switch (action.type) {
        case UI_SET_REFERRER_URL:
            return {
                ...state,
                referrerUrl: action.referrerUrl
            };
        case UI_SET_PHONE:
            return {
                ...state,
                phone: action.phone
            };
        default:
            return state;
    }
}

