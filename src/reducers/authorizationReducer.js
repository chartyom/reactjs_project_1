import {
  AUTH_DEFAULT,

  AUTH_LOCATION_CHANGE,
  AUTH_LOCATION_CHANGED,

  AUTH_LOCATION_CITY_CHANGE,
  AUTH_LOCATION_CITY_CHANGED,

  AUTH_LOCALIZATION_CHANGE,
  AUTH_LOCALIZATION_CHANGED,

  AUTH_UNAUTHORIZED,
  AUTH_AUTHORIZATION,
  AUTH_AUTHORIZED,
  AUTH_AUTHORIZATION_FAILED
} from '../actions/authorizationAction';


export default function (state = {
  authorization: {
    readyState: AUTH_UNAUTHORIZED,
    token: null,
    refreshToken: null,
    dateEnd: null,
  },
  location: {
    readyState: AUTH_DEFAULT,
    lat: null,
    lng: null,
  },
  locationCity: {
    readyState: AUTH_DEFAULT,
    id: 1,
    name: 'Москва',
    cityCase: ['Москва', 'Москву', 'Москве'],
    numberPhoneMask: "8 (499) 638-28-72",
    numberPhone: "+74996382872",
    lat: 55.75222,
    lng: 37.61556,
  },
  localization: {
    readyState: AUTH_DEFAULT,
    is: 'RU_ru'
  }
}, action) {
  switch (action.type) {

    case AUTH_LOCATION_CHANGED:
      return {
        ...state,
        location: {
          readyState: AUTH_LOCATION_CHANGED,
          lat: action.latitude,
          lng: action.longitude,
        }
      };

    case AUTH_LOCATION_CITY_CHANGE:
      return {
        ...state,
        locationCity: {
          ...state.location,
          readyState: AUTH_LOCATION_CITY_CHANGE
        }
      };
    case AUTH_LOCATION_CITY_CHANGED:
      return {
        ...state,
        locationCity: {
          readyState: AUTH_LOCATION_CITY_CHANGED,
          cityName: action.cityName,
          cityId: action.cityId,
          lat: action.latitude,
          lng: action.longitude,
        }
      };

    case AUTH_AUTHORIZATION:
      return {
        ...state,
        authorization: {
          readyState: AUTH_AUTHORIZATION,
        }
      };
    case AUTH_AUTHORIZED:
      return {
        ...state,
        authorization: {
          readyState: AUTH_AUTHORIZED,
          token: action.token,
          refreshToken: action.refreshToken,
          dateEnd: action.dateEnd,
        }
      };
    case AUTH_AUTHORIZATION_FAILED:
      return {
        ...state,
        authorization: {
          readyState: AUTH_AUTHORIZATION_FAILED,
          error: action.error
        }
      };
    default:
      return state;
  }
}

