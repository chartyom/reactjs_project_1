import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import URL from '../url'
import { connect } from 'react-redux';
import ThemeMain from '../layouts/ThemeMain'
import RegistrationForm from '../components/RegistrationForm'

class RegistrationPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            redirectToReferrer: false,
        };
        this.handleSetReferrer = this.handleSetReferrer.bind(this)
    }

    handleSetReferrer(s) {
        this.setState({ redirectToReferrer: s })
    }

    render() {
        let { from } = this.props.location.state || { from: { pathname: '/' } }
        if (this.props.referrerUrl) from = this.props.referrerUrl

        if (this.state.redirectToReferrer) {
            return (
                <Redirect to={from} />
            )
        }

        return (
            <ThemeMain>
                <RegistrationForm handleSetReferrer={this.handleSetReferrer} phone={this.props.phone} />
            </ThemeMain>
        )
    }
}

const mapStateToProps = ({ ui }) => ({
    referrerUrl: ui.referrerUrl,
    phone: ui.phone,
});

export default connect(mapStateToProps)(RegistrationPage)