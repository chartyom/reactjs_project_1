import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import URL from '../url'
import ThemeMain from '../layouts/ThemeMain'

class HomePage extends Component {
    componentWillMount() {
        window.location.replace('//bitkom24.ru');
    }
    render() {
        return (<div>Переадресация на основной сайт: bitkom24.ru</div>)
        // return (
        //     <ThemeMain>
        //         <Link to={URL.home}> Home </Link>
        //         <Link to={URL.profile}> Profile </Link>
        //         <Link to={URL.signIn}> SignIn </Link>
        //         <Link to={URL.signOut}> signOut </Link>
        //         <br />
        //         <Link to='/authorize?client_id=1&redirect_uri=http://localhost:3001/callback&response_type=token&state=123&scope=user_profile'> token </Link>
        //         <Link to='/authorize?client_id=1&redirect_uri=http://localhost:3001/callback&response_type=code&state=123&scope=user_profile'> code </Link>
        //     </ThemeMain>
        // )
    }
}

export default HomePage;