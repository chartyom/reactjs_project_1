import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import URL from '../url'
import * as UserAction from '../actions/UserAction'

class SignOutPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            redirectToReferrer: false,
        };
    }

    componentDidMount() {
        this.handleSignOut()
    }

    handleSignOut() {
        UserAction.signOut()
            .then(function (status) {
                this.setState({ redirectToReferrer: true })
            }.bind(this))
    }

    render() {
        const { redirectToReferrer } = this.state

        if (redirectToReferrer) {
            return (
                <Redirect to={URL.home} />
            )
        }
        return (
            <div>
                Please, Wait
            </div>
        )
    }
}

export default SignOutPage;