import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux';
import URL from '../url'
import * as uiAction from '../actions/uiAction'
import { isAuthenticated } from '../utils/AuthHelper'
import ThemeMain from '../layouts/ThemeMain'
import AccessPermissionPanel from '../components/AccessPermissionPanel'

class AuthorizePage extends Component {
    render() {

        const uri = window.location.pathname + window.location.search
        if (!this.props.referrerUrl) this.props.dispatch(uiAction.setReferrerUrl(uri));

        if (!isAuthenticated()) {
            return <Redirect to={URL.signIn} />
        }

        return (
            <ThemeMain>
                <AccessPermissionPanel referrerUrl={uri} />
            </ThemeMain>
        )
    }


}
const mapStateToProps = ({ ui }) => ({
    referrerUrl: ui.referrerUrl
});

export default connect(mapStateToProps)(AuthorizePage)
