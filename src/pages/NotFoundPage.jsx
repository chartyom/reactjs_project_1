import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import URL from '../url'
import ThemeMain from '../layouts/ThemeMain'

class NotFoundPage extends Component {
    render() {
        return (
            <ThemeMain>
                <div>
                    <h1>Страница не найдена.</h1>
                    <p>Возможно эта страница была удалена или никогда не создавалась!</p>
                    <Link to={URL.home} className="button">На главную</Link>
                </div>
            </ThemeMain>
        )
    }
}

export default NotFoundPage;