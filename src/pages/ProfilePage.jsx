import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import URL from '../url'

class ProfilePage extends Component {
    render() {
        return (
            <div>
                <Link to={URL.home}> Home </Link>
                <Link to={URL.profile}> Profile </Link>
                <Link to={URL.signIn}> SignIn </Link>
                <Link to={URL.signOut}> signOut </Link>
            </div>
        )
    }
}

export default ProfilePage;