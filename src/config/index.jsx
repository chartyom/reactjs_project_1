export default {
    connect: {
        main: DEVELOPMENT ? "http://localhost:3001" : "http://localhost:3001",
        oauth: DEVELOPMENT ? "http://localhost:3001" : "http://localhost:3001",
    },
    clientId: 1,
    clientVersion: '0.1.1'
};