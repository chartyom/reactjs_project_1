import User from './User'
import Client from './Client'
import Rules from './Rules'

export default {
    user: User,
    client: Client,
    rules: Rules
}