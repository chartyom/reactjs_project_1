import CONFIG from '../config'
import * as UrlHelper from '../utils/UrlHelper'
import * as AuthHelper from '../utils/AuthHelper'

export default {
    /**
     * 
     * @param {String} scope
     */
    getList: (scope) => {
        return AuthHelper.fetchAuth(CONFIG.connect.oauth + '/rules', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: UrlHelper.handleBody({
                scope
            })
        }).then(r => r.json())
    },
}

