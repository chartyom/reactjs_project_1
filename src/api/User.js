import CONFIG from '../config'
import * as UrlHelper from '../utils/UrlHelper'
import Storage from '../utils/Storage'
import * as AuthHelper from '../utils/AuthHelper'
const URLENCODED = 'application/x-www-form-urlencoded'



export default {

    authenticateSendSMS: (phone) => {
        return fetch(CONFIG.connect.oauth + '/authenticate/sendSMS', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: UrlHelper.handleBody({
                phone
            }),
        }).then(r => r.json())
    },
    authenticate: (phone, code) => {
        return fetch(CONFIG.connect.oauth + '/authenticate', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: UrlHelper.handleBody({
                phone, code
            }),
        }).then(r => r.json())
    },
    revokeToken: (tokenType, accessToken, refreshToken) => {

        return fetch(CONFIG.connect.oauth + '/token/revoke', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': tokenType + ' ' + accessToken
            },
            body: UrlHelper.handleBody({
                accessToken: accessToken,
                refreshToken: refreshToken || null,
            })
        }).then(r => r.json())
    },
    authorize: (uri, tokenType, accessToken, refreshToken) => {

        return fetch(CONFIG.connect.oauth + uri, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': tokenType + ' ' + accessToken
            },
        }).then(r => r.json())
    },
    checkPhone: (phone) => {
        return fetch(CONFIG.connect.oauth + '/check/phone', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: UrlHelper.handleBody({
                phone: phone,
            }),
        }).then(r => r.json())
    },
    checkEmail: (email) => {
        return fetch(CONFIG.connect.oauth + '/check/email', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: UrlHelper.handleBody({
                email: email,
            }),
        }).then(r => r.json())
    },
    checkReferrerCode: (code) => {
        return fetch(CONFIG.connect.oauth + '/check/referrerCode', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: UrlHelper.handleBody({
                code: code,
            }),
        }).then(r => r.json())
    },


    /**
     * 
     */
    registrationSendSMS: (phone, code, firstName, lastName, email) => {
        return fetch(CONFIG.connect.oauth + '/registration/sendSMS', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: UrlHelper.handleBody({
                phone, code, firstName, lastName, email
            }),
        }).then(r => r.json())
    },
    registration: (key, code) => {
        return fetch(CONFIG.connect.oauth + '/registration', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: UrlHelper.handleBody({
                key, code
            }),
        }).then(r => r.json())
    },
    regenerateCode: (key) => {
        return fetch(CONFIG.connect.oauth + '/regenerateCode', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: UrlHelper.handleBody({
                key
            }),
        }).then(r => r.json())
    },
}
