import CONFIG from '../config'
import UrlHelper from '../utils/UrlHelper'
import * as AuthHelper from '../utils/AuthHelper'

export default {
    getName: (id) => {
        return AuthHelper.fetchAuth(CONFIG.connect.oauth + '/client/' + id, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).then(r => r.json())
    },
}

