import React from 'react';
import InputElement from 'react-input-mask'

class Field extends React.Component {
    render() {
        const {
            disabled,
            className,
            type,
            mask,
            maskChar,
            ...props
        } = this.props;
        const CN = className ? ' ' + className : '';
        if (!mask) {
            return (
                <div className='form__field-parent'>
                    <input
                        {...props}
                        className={'form__field' + CN}
                        type={type || "text"}
                        disabled={disabled}
                    />
                </div>
            )
        } else {
            return (
                <div className='form__field-parent'>
                    < InputElement
                        {...props }
                        mask={mask || "+7 (999) 999-99-99"}
                        maskChar={maskChar || "_"}
                        className={'form__field' + CN}
                        type={type || "text"}
                        disabled={disabled}
                    />
                </div>
            )
        }
    }
}

export default Field;