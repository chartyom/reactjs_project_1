import React from 'react';

class Button extends React.Component {

    render() {
        const {
            disabled,
            className,
            type,
            ...props
        } = this.props;
        const CN = className ? ' ' + className : '';

        return (
            <button
                {...props }
                className={'button' + CN}
                type={type || "button"}
                disabled={disabled}
            />
        )

    }
}

export default Button;