import React from 'react';
import InputElement from 'react-input-mask'

class FieldHelp extends React.Component {

    render() {
        const {
            className,
            children,
        } = this.props;
        const CN = className ? ' ' + className : '';
        if (children)
            return (
                <div className={'form__help' + CN} >
                    {children}
                </div>
            )
        return <div />
    }
}

export default FieldHelp;