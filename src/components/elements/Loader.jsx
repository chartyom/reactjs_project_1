import React from 'react';

/**
 * 
 * @param {Object} params
 * @param {String} params.size ( small || middle || big )
 * @param {String} params.color ( black || grey || orange || white)
 * @param {String} params.position ( center )
 */
export default ({ size, color, position}) => (
  <div className={
    "loader loader--size-" + (size ? size : 'middle') +
    " loader--color-" + (color ? color : 'black')+
    " loader--position-" + (position ? position : 'default')}>
    <div className="loader__main" ></div>
  </div>
);
