import React from 'react';

class FormGroup extends React.Component {

    render() {
        const {
            status,
            center,
            className,
            children,
            indent,
            ...props
        } = this.props;



        let CN = className ? ' ' + className : '';
        let style = {}
        if (status === 'danger') CN += ' has_danger'
        if (status === 'success') CN += ' has_success'
        if (status === 'warning') CN += ' has_warning'
        if (status === 'loading') CN += ' has_loading'
        if (center) CN += ' pos_center'
        if (indent) style = { marginBottom: indent }

        return (
            <div
                {...props}
                className={'form__group' + CN}
                style={style}
            >
                {children}
            </div>
        )
    }
}

export default FormGroup;