import React from 'react';
import InputElement from 'react-input-mask'

class Label extends React.Component {

    render() {
        const {
            className,
            require,
            children
        } = this.props;
        const CN = className ? ' ' + className : '';
        let MR = '';

        if (children) {
            if (require) MR = <span className="require" title="Обязательное поле">*</span>
            return (
                <label className={'form__label' + CN} >
                    {children}{MR}
                </label>
            )
        }
        return <div />
    }
}

export default Label;