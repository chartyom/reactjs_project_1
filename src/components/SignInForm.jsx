import React, { PureComponent } from 'react'
import { Link, Redirect } from 'react-router-dom'
import URL from '../url'
import * as userAction from '../actions/UserAction'
import * as uiAction from '../actions/uiAction'
import { FormGroup, Field, FieldHelp, Button } from '../components/elements'
import matchesValidator from 'validator/lib/matches'
import { connect } from 'react-redux';

var checkPhoneTimer
/**
 * 
 * @param {Function} handleSetReferrer
 * @param {String} phone
 */
class SignInForm extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            step: 1,
            passwordDescription: null,
            passwordStatus: null,
            paswordValue: null,
            phoneDescription: null,
            phoneStatus: null,
            phoneValue: null,
            keyValue: null,
            loading: false,
        };
        this.handleStepPhoneSuccess = this.handleStepPhoneSuccess.bind(this)
        this.handleStepPasswordSuccess = this.handleStepPasswordSuccess.bind(this)
        this.handlePhoneValid = this.handlePhoneValid.bind(this)
        this.handlePasswordValid = this.handlePasswordValid.bind(this)
        this.handleStepPhoneReturn = this.handleStepPhoneReturn.bind(this)
        this.handleRegenerateCode = this.handleRegenerateCode.bind(this)
    }

    componentWillMount() {
        if (this.props.phone) {
            return this.setState({ phoneStatus: 'success', phoneValue: this.props.phone, phoneDescription: null });
        }
    }

    handlePhoneValid(e) {
        const v = e.target.value;
        clearTimeout(checkPhoneTimer)
        if (matchesValidator(v, /(\+?\d{1,5})? \(\d{3}\) \d{3}-\d{2}-\d{2}/i)) {
            checkPhoneTimer = setTimeout(function () {
                if (this.state.phoneStatus !== 'loading') this.setState({ phoneStatus: 'loading', phoneDescription: null });
                //XHR
                // setTimeout(function () {
                //     return this.setState({ phoneStatus: 'success', phoneValue: v, phoneDescription: null });
                // }.bind(this), 2000)
                userAction.checkPhone(v).then((res) => {
                    if (!res || res.error) {
                        throw new Error(res.error)
                    }
                    if (res.phoneExist) {
                        return this.setState({ phoneStatus: 'success', phoneValue: v, phoneDescription: null });
                    } else {
                        this.props.dispatch(uiAction.setPhone(v))
                        return this.setState({ phoneStatus: 'danger', phoneValue: null, phoneDescription: 'Пользователь с таким номером телефона не существует. ' });
                    }

                }).catch((e) => {
                    return this.setState({ phoneStatus: 'danger', phoneValue: null, phoneDescription: 'На сервере произошла ошибка попробуйте войти позже' });
                })

            }.bind(this), 500)
        } else {
            return this.setState({ phoneStatus: null });
        }
    }

    handlePasswordValid(e) {
        const v = e.target.value;
        if (v.length > 0) {
            return this.setState({ passwordStatus: null, passwordValue: v });
        }
        return this.setState({ passwordValue: v });
    }

    handleStepPhoneSuccess() {
        if (this.state.phoneValue && this.state.phoneStatus === 'success') {
            let state = {}
            state.step = 2
            this.setState({ loading: true })
            return userAction.authenticateSendSMS(this.state.phoneValue).then((res) => {
                if (!res || res.error) {
                    throw new Error(res.error)
                }
                if (res.success) {
                    state.loading = false
                    return this.setState(state);
                }
            }).catch((e) => {
                this.setState({ phoneStatus: 'danger', phoneDescription: 'В результате отправки возникла ошибка', loading: false })
                return console.log(e);
            })
        } else {
            this.setState({ phoneStatus: 'danger', phoneDescription: 'Введите свой номер телефона' })
        }

    }

    handleStepPhoneReturn() {
        this.setState({ step: 1 })
    }

    /*
        PASSOWRD
    */

    handlePasswordValid(e) {
        const v = e.target.value;
        if (matchesValidator(v, /\d{5}/)) {
            if (this.state.passwordStatus !== 'loading') this.setState({ passwordStatus: 'loading', passwordDescription: null })
            //XHR
            return userAction.authenticate(this.state.phoneValue, v).then((res) => {
                if (res.error) {
                    if (res.error === 'key_not_found')
                        return this.setState({ passwordStatus: 'danger', passwordDescription: 'Время истекло, нажмите кнопку "Повторить отправку СМС"' })
                    if (res.error === 'invalid_code')
                        return this.setState({ passwordStatus: 'danger', passwordDescription: 'Код введен не верно' })
                    throw new Error(res.error)
                }
                if (res.success) {
                    userAction.saveToken(res)
                    return this.props.handleSetReferrer(true)
                }
            }).catch((e) => {
                this.setState({ passwordStatus: 'danger', passwordDescription: 'В результате отправки возникла ошибка' })
                return console.log(e);
            })
        }
        return this.setState({ passwordStatus: null, passwordDescription: null });
    }

    handleStepPasswordSuccess() {
        if (this.state.passwordStatus !== 'success') {
            return this.setState({ passwordStatus: 'danger', passwordValue: null, passwordDescription: 'Введите код подтверждения из СМС сообщения' });
        }
    }

    handleRegenerateCode() {
        //XHR
        this.setState({ loading: true })
        return userAction.regenerateCode(this.state.phoneValue).then((res) => {
            if (!res || res.error) {
                //TODO need modal tooltip
                if (res.error === 'key_not_found')
                    return this.setState({ passwordStatus: 'danger', passwordDescription: 'Время истекло, войдите заново', loading: false })

                if (res.error === 'limit_exceeded')
                    return this.setState({ passwordStatus: 'danger', passwordDescription: 'Превышен лимит отправок СМС', loading: false })

                throw new Error(res.error)
            }
            if (res.success) {
                return this.setState({ loading: false })
            }
        }).catch((e) => {
            this.setState({ passwordStatus: 'danger', passwordDescription: 'В результате отправки возникла ошибка', loading: false })
            return console.log(e);
        })
    }

    render() {
        return (
            <div className="signInForm">
                <div className="signInForm__avatar">
                    <div className="signInForm__avatar__user"></div>
                </div>
                {this.state.step === 1 ? this.renderPhoneStep() : ''}
                {this.state.step === 2 ? this.renderPasswordStep() : ''}

            </div>
        )
    }

    renderPhoneStep() {
        var buttonDisabled = false
        if (this.state.phoneStatus === 'loading') buttonDisabled = true
        return (
            <div className="signInForm__main">
                <FormGroup status={this.state.phoneStatus} center indent={8}>
                    <Field mask="+7 (999) 999-99-99" placeholder="+7 (___) ___-__-__" type="tel" onChange={this.handlePhoneValid} defaultValue={this.props.phone} />
                    <FieldHelp>{this.state.phoneDescription}</FieldHelp>
                </FormGroup>

                <Button className="warning block" onClick={this.handleStepPhoneSuccess} disabled={buttonDisabled}>Далее</Button>
                <div className="signInForm__link">
                    <Link to={URL.registration} className="button link">Регистрация</Link>
                </div>
            </div>
        )
    }

    renderPasswordStep() {
        var buttonDisabled = false
        if (this.state.passwordStatus === 'loading') buttonDisabled = true
        return (
            <div className="signInForm__main">
                <FormGroup status={this.state.passwordStatus} center indent={8}>
                    <Field mask="99999" type="tel" placeholder="Введите код из СМС" onChange={this.handlePasswordValid} />
                    <FieldHelp>{this.state.passwordDescription}</FieldHelp>
                </FormGroup>

                <Button className="warning block" onClick={this.handleStepPasswordSuccess} disabled={buttonDisabled}>Войти</Button>
                <Button className="default block" onClick={this.handleStepPhoneReturn} disabled={buttonDisabled}>Вернуться</Button>

                <div className="signInForm__link">
                    <Button className="link" onClick={this.handleRegenerateCode} disabled={buttonDisabled}>Повторить отправку СМС</Button>
                </div>
            </div>
        )
    }
}

export default connect()(SignInForm)