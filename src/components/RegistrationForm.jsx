import React, { PureComponent } from 'react'
import { Link, Redirect, Route } from 'react-router-dom'
import URL from '../url'
import * as userAction from '../actions/userAction'
import * as uiAction from '../actions/uiAction'
import { FormGroup, Field, FieldHelp, Button, Label } from '../components/elements'
import isEmailValidator from 'validator/lib/isEmail';
import matchesValidator from 'validator/lib/matches'
import { connect } from 'react-redux';

var checkReferrerCodeTimer, checkPhoneTimer, checkFirstnameTimer, checkLastnameTimer, checkEmailTimer

/**
 * 
 * @param {Function} handleSetReferrer
 * @param {String} phone
 */
class RegistrationForm extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            //steps
            step: 1,
            passwordDescription: null,
            passwordStatus: null,
            paswordValue: null,
            phoneDescription: null,
            phoneStatus: null,
            phoneValue: null,
            referrerCodeDescription: null,
            referrerCodeStatus: null,
            referrerCodeValue: null,
            firstnameDescription: null,
            firstnameStatus: null,
            firstnameValue: null,
            lastnameDescription: null,
            lastnameStatus: null,
            lastnameValue: null,
            emailDescription: null,
            emailStatus: null,
            emailValue: null,
            keyValue: null,
            loading: false,
        };
        this.handleStepPhoneSuccess = this.handleStepPhoneSuccess.bind(this)
        this.handleReferrerCodeValid = this.handleReferrerCodeValid.bind(this)
        this.handlePhoneValid = this.handlePhoneValid.bind(this)
        this.handleStepPhoneReturn = this.handleStepPhoneReturn.bind(this)

        this.handleStepMoreSuccess = this.handleStepMoreSuccess.bind(this)
        this.handleFirtnameValid = this.handleFirtnameValid.bind(this)
        this.handleLastnameValid = this.handleLastnameValid.bind(this)
        this.handleEmailValid = this.handleEmailValid.bind(this)
        this.handleStepMoreReturn = this.handleStepMoreReturn.bind(this)

        this.handleStepPasswordSuccess = this.handleStepPasswordSuccess.bind(this)
        this.handlePasswordValid = this.handlePasswordValid.bind(this)
        this.handleRegenerateCode = this.handleRegenerateCode.bind(this)

    }

    componentWillMount() {

        //firstly
        let state = {}
        if (this.props.phone) {
            state.phoneValue = this.props.phone
            state.phoneStatus = 'success'
            this.setState(state)
        }

    }

    // componentDidMount() {

    //     //firstly
    //     let state = {}
    //     if (this.props.phone) {
    //         state.phoneValue = this.props.phone
    //         state.phoneStatus = 'success'
    //         this.setState(state)
    //     }

    // }

    /*
        PHONE
    */

    handlePhoneValid(e) {
        const v = e.target.value;
        clearTimeout(checkPhoneTimer)
        if (matchesValidator(v, /(\+?\d{1,5})? \(\d{3}\) \d{3}-\d{2}-\d{2}/i)) {
            checkPhoneTimer = setTimeout(function () {
                if (this.state.phoneStatus !== 'loading') this.setState({ phoneStatus: 'loading' });
                //XHR
                userAction.checkPhone(v).then((res) => {
                    if (!res || res.error) {
                        throw new Error(res.error)
                    }
                    if (res.phoneExist) {
                        this.props.dispatch(uiAction.setPhone(v))
                        return this.setState({ phoneStatus: 'danger', phoneValue: null, phoneDescription: 'Пользователь с таким номером телефона уже существует. ' });
                    } else {
                        return this.setState({ phoneStatus: 'success', phoneValue: v, phoneDescription: null });
                    }

                }).catch((e) => {
                    return this.setState({ phoneStatus: 'danger', phoneValue: null, phoneDescription: 'На сервере произошла ошибка попробуйте зарегистрироваться позже' });
                })
            }.bind(this), 500)
        } else {
            return this.setState({ phoneStatus: null });
        }
    }


    /*
        REFERRER CODE
    */
    handleReferrerCodeValid(e) {
        const v = e.target.value;

        clearTimeout(checkReferrerCodeTimer)
        if (v.length > 0) {
            checkReferrerCodeTimer = setTimeout(function () {
                if (this.state.referrerCodeStatus !== 'loading') this.setState({ referrerCodeStatus: 'loading' });
                //XHR

                userAction.checkReferrerCode(v).then((res) => {
                    if (!res || res.error) {
                        throw new Error(res.error)
                    }
                    if (res.referrerExist) {
                        return this.setState({ referrerCodeStatus: 'success', referrerCodeValue: v, referrerCodeDescription: 'Вас пригласил(-а) ' + (res.firstName || '') + ' ' + (res.lastName || '') });
                    } else {
                        return this.setState({ referrerCodeStatus: 'danger', referrerCodeValue: null, referrerCodeDescription: 'Код приглашения введен не верно. Введите другой код приглашения или нажмите кнопку "Далее" для продолжения регистрации' });
                    }

                }).catch((e) => {
                    return this.setState({ referrerCodeStatus: 'danger', referrerCodeValue: null, phoneDescription: 'На сервере произошла ошибка попробуйте зарегистрироваться позже' });
                })

            }.bind(this), 500)
        } else {
            return this.setState({ referrerCodeStatus: null });
        }
    }


    handleStepPhoneSuccess() {
        let state = {}
        if (this.state.phoneValue && this.state.phoneStatus === 'success') {
            state.step = 2
        } else {
            state.phoneStatus = 'danger'
            state.phoneDescription = 'Введите номер телефона'
        }
        if (this.state.referrerCodeStatus !== 'success') {
            state.referrerCodeStatus = null
            state.referrerCodeDescription = null
        }

        this.setState(state)
    }

    handleStepPhoneReturn() {
        this.setState({ step: 1 })
    }



    /*
        FIRSTNAME
    */
    handleFirtnameValid(e) {
        const v = e.target.value;
        clearTimeout(checkFirstnameTimer)
        checkFirstnameTimer = setTimeout(function () {
            if (v.length > 1)
                return this.setState({ firstnameStatus: 'success', firstnameValue: v, firstnameDescription: null });
            return this.setState({ firstnameStatus: 'danger', firstnameValue: null, firstnameDescription: 'Введенное имя слишком короткое' });
        }.bind(this), 500)
    }

    /*
        LASTNAME
    */
    handleLastnameValid(e) {
        const v = e.target.value;
        clearTimeout(checkLastnameTimer)
        checkLastnameTimer = setTimeout(function () {
            if (v.length > 1)
                return this.setState({ lastnameStatus: 'success', lastnameValue: v, lastnameDescription: null });
            return this.setState({ lastnameStatus: 'danger', lastnameValue: null, lastnameDescription: 'Введенная фамилия слишком короткая' });
        }.bind(this), 500)
    }


    /*
        EMAIL
    */
    handleEmailValid(e) {
        const v = e.target.value;
        clearTimeout(checkEmailTimer)
        if (v.length > 0) {
            checkEmailTimer = setTimeout(function () {
                if (isEmailValidator(v)) {
                    if (this.state.emailStatus !== 'loading') this.setState({ emailStatus: 'loading', emailDescription: null });
                    //XHR
                    userAction.checkEmail(v).then((res) => {
                        if (!res || res.error) {
                            throw new Error(res.error)
                        }
                        if (res.emailExist) {
                            this.props.dispatch(uiAction.setPhone(v))
                            return this.setState({ emailStatus: 'danger', emailValue: null, emailDescription: 'Введенный вами e-mail уже используется. Укажите другой e-mail или нажите на кнопку "Зарегистрироваться" для завершения регистрации' });
                        } else {
                            return this.setState({ emailStatus: 'success', emailValue: v, emailDescription: null });
                        }

                    }).catch((e) => {
                        return this.setState({ emailStatus: 'danger', emailValue: null, emailDescription: 'На сервере произошла ошибка попробуйте зарегистрироваться позже' });
                    })

                } else {
                    return this.setState({ emailStatus: 'danger', emailValue: null, emailDescription: 'Введен не корректный e-mail' });
                }
            }.bind(this), 500)
        } else {
            return this.setState({ emailStatus: null, emailValue: null, emailDescription: null });
        }
    }


    handleStepMoreSuccess(e) {

        let state = {}
        if (
            this.state.firstnameStatus !== 'success'
        ) {
            state.firstnameStatus = 'danger'
            state.firstnameDescription = 'Введите ваше имя'
        }

        if (
            this.state.lastnameStatus !== 'success'
        ) {
            state.lastnameStatus = 'danger'
            state.lastnameDescription = 'Введите вашу фамилию'
        }

        if (
            this.state.firstnameValue &&
            this.state.lastnameValue &&
            this.state.emailStatus !== 'danger'
        ) {
            state.step = 3
            this.setState({ loading: true })
            return userAction.registrationSendSMS({
                phone: this.state.phoneValue,
                firstName: this.state.firstnameValue,
                lastName: this.state.lastnameValue,
                email: this.state.emailValue,
                code: this.state.referrerCodeValue
            }).then((res) => {
                if (!res || res.error) {
                    throw new Error(res.error)
                }
                if (res.key) {
                    state.key = res.key
                    state.loading = false
                    return this.setState(state);
                }
            }).catch((e) => {
                this.setState({ firstnameStatus: 'danger', firstnameDescription: 'В результате отправки возникла ошибка', loading: false })
                return console.log(e);
            })
        }

        this.setState(state)

    }

    handleStepMoreReturn() {
        this.setState({ step: 2 })
    }




    /*
        PASSOWRD
    */

    handlePasswordValid(e) {
        const v = e.target.value;
        if (matchesValidator(v, /\d{5}/)) {
            if (this.state.passwordStatus !== 'loading') this.setState({ passwordStatus: 'loading', passwordDescription: null })
            //XHR
            return userAction.registration(this.state.key, v).then((res) => {
                if (res.error) {
                    if (res.error === 'key_not_found')
                        return this.setState({ passwordStatus: 'danger', passwordDescription: 'Время истекло, нажмите кнопку "Повторить отправку СМС"' })
                    if (res.error === 'invalid_code')
                        return this.setState({ passwordStatus: 'danger', passwordDescription: 'Код введен не верно' })
                    throw new Error(res.error)
                }
                console.log(res)
                if (res.success) {
                    userAction.saveToken(res)
                    return this.props.handleSetReferrer(true)
                }
            }).catch((e) => {
                this.setState({ passwordStatus: 'danger', passwordDescription: 'В результате отправки возникла ошибка' })
                return console.log(e);
            })
        }
        return this.setState({ passwordStatus: null, passwordDescription: null });
    }

    handleStepPasswordSuccess() {
        if (this.state.passwordStatus !== 'success') {
            return this.setState({ passwordStatus: 'danger', passwordValue: null, passwordDescription: 'Введите код подтверждения из СМС сообщения' });
        }
    }


    handleRegenerateCode() {
        //XHR
        this.setState({ loading: true })
        return userAction.regenerateCode(this.state.key).then((res) => {
            if (!res || res.error) {
                if (res.error === 'key_not_found')
                    return this.setState({ passwordStatus: 'danger', passwordDescription: 'Время истекло, пройдите регистрацию заново', loading: false })
                if (res.error === 'limit_exceeded')
                    return this.setState({ passwordStatus: 'danger', passwordDescription: 'Превышен лимит отправок СМС', loading: false })
                throw new Error(res.error)
            }
            if (res.success) {
                return this.setState({ loading: false })
            }
        }).catch((e) => {
            this.setState({ passwordStatus: 'danger', passwordDescription: 'В результате отправки возникла ошибка', loading: false })
            return console.log(e);
        })
    }

    render() {
        return (
            <div className="RegistrationForm">
                <h1 className="RegistrationForm__title">Регистрация</h1>
                {this.state.step === 1 ? this.renderPhoneStep() : ''}
                {this.state.step === 2 ? this.renderMoreStep() : ''}
                {this.state.step === 3 ? this.renderPasswordStep() : ''}
            </div>
        )
    }

    renderPhoneStep() {
        var buttonDisabled = false
        if (this.state.phoneStatus === 'loading' || this.state.referrerCodeStatus === 'loading') buttonDisabled = true
        return (
            <div className="RegistrationForm__main">
                <FormGroup status={this.state.phoneStatus} >
                    <Label require >Номер телефона</Label>
                    <Field mask="+7 (999) 999-99-99" placeholder="+7 (___) ___-__-__" type="tel" onChange={this.handlePhoneValid} defaultValue={this.state.phoneValue} />
                    <FieldHelp>{this.state.phoneDescription}</FieldHelp>
                </FormGroup>
                <FormGroup status={this.state.referrerCodeStatus} >
                    <Label >Код приглашение</Label>
                    <Field placeholder="Введите код приглашения" type="text" onChange={this.handleReferrerCodeValid} defaultValue={this.state.referrerCodeValue} />
                    <FieldHelp>{this.state.referrerCodeDescription}</FieldHelp>
                </FormGroup>

                <Button className="warning block" onClick={this.handleStepPhoneSuccess} disabled={buttonDisabled}>Далее</Button>

                <div className="RegistrationForm__link">
                    <Link to={URL.signIn} className="button link">Войти</Link>
                </div>
            </div>
        )
    }

    renderMoreStep() {
        var buttonDisabled = false
        if (this.state.emailStatus === 'loading' || this.state.loading) buttonDisabled = true
        return (
            <div className="RegistrationForm__main">
                <FormGroup status={this.state.firstnameStatus} >
                    <Label require >Имя</Label>
                    <Field placeholder="Введите ваше имя" onChange={this.handleFirtnameValid} defaultValue={this.state.firstnameValue} />
                    <FieldHelp>{this.state.firstnameDescription}</FieldHelp>
                </FormGroup>
                <FormGroup status={this.state.lastnameStatus}>
                    <Label require >Фамилия</Label>
                    <Field placeholder="Введите вашу фамилию" onChange={this.handleLastnameValid} defaultValue={this.state.lastnameValue} />
                    <FieldHelp>{this.state.lastnameDescription}</FieldHelp>
                </FormGroup>
                <FormGroup status={this.state.emailStatus}>
                    <Label >E-mail</Label>
                    <Field placeholder="Введите ваш e-mail" onChange={this.handleEmailValid} defaultValue={this.state.emailValue} />
                    <FieldHelp>{this.state.emailDescription}</FieldHelp>
                </FormGroup>

                <Button className={"warning block" + (this.state.loading ? ' loading' : '')} onClick={this.handleStepMoreSuccess} disabled={buttonDisabled}>Зарегистрироваться</Button>
                <Button className="default block" onClick={this.handleStepPhoneReturn} disabled={buttonDisabled}>Вернуться</Button>
            </div>
        )
    }

    renderPasswordStep() {
        var buttonDisabled = false
        if (this.state.passwordStatus === 'loading') buttonDisabled = true
        return (
            <div className="RegistrationForm__main">
                <FormGroup status={this.state.passwordStatus} center indent={8}>
                    <Field mask="99999" type="tel" placeholder="Введите код из СМС" onChange={this.handlePasswordValid} />
                    <FieldHelp>{this.state.passwordDescription}</FieldHelp>
                </FormGroup>

                <Button className="warning block" onClick={this.handleStepPasswordSuccess} disabled={buttonDisabled}>Войти</Button>
                <Button className="default block" onClick={this.handleStepMoreReturn} disabled={buttonDisabled}>Вернуться</Button>


                <div className="RegistrationForm__link">
                    <Button className="link" onClick={this.handleRegenerateCode}>Повторить отправку СМС</Button>
                </div>
            </div>
        )
    }
}

export default connect()(RegistrationForm)