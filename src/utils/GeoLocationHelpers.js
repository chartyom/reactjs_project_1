import ClientStorage from './LocalStorageHelpers';

const GeoLocation = {
    /**
     * получить геолокацию
     * 
     * get(function (err, position) {
     * 
     *  if (err) return false;
     * 
     *  console.log(position);
     * 
     * });
     * @param {function} callback 
     */
    get: function (callback) {
        if (!this.getEnable()) {
            //error_code: 0 - Браузер не потдерживает геолокацию
            return callback({ code: 0, message: "Browser dont support geolocation" });
        };
        function success(position) {
            GeoLocation.setNotifyAsWrite(true);
            callback(null, { latitude: position.coords.latitude, longitude: position.coords.longitude });
        }
        function error(error) {
            //error_code: 1 - Не удалось получить местоположение
            callback(error);
        }
        navigator.geolocation.getCurrentPosition(success, error);
    },

    getEnable(){
        return !!navigator.geolocation;
    },
    /**
     * Проверка на показ уведомления о включении геолокации пользователем 
     * @return {boolean} 
     */
    getNotifyAsWrite() {
        if (ClientStorage.getItem('geolocation')) return true;
        return false;
    },

    /**
     * Задать включение или отключение геолокации пользователем 
     * @param {boolean} param  
     */
    setNotifyAsWrite(param) {
        param = param || true;
        ClientStorage.setItem('geolocation', param);
    }
};

export default GeoLocation;