
function LSEnable() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}

function cookieEnable() {
    return navigator.cookieEnabled;
}

const Cookie = {
    getItem: function (name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    },
    setItem: function (name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    },
    removeItem: function (name) {
        this.setItem(name, "", {
            expires: -1
        });
    }
};


export default {
    getItem: function (key) {
        if (LSEnable()) {
            return localStorage.getItem(key);
        } else if (cookieEnable()) {
            return Cookie.getItem(key);
        }
        return undefined;
    },
    setItem: function (key, value, params) {
        if (LSEnable()) {
            localStorage.setItem(key, value);
        } else if (cookieEnable()) {
            expires = params && params.expires ? params.expires : 2592000
            Cookie.setItem(key, value, { expires: expires });
        }
    },
    removeItem: function (key) {
        if (LSEnable()) {
            localStorage.removeItem(key);
        } else if (cookieEnable()) {
            Cookie.removeItem(key);
        }
    },


};