import { browserHistory } from 'react-router';
import {AUTH_UNAUTHENTICATED} from '../actions/authAction';

export function isAuthorized(props) {
    return props.auth.authenticate.is;
}

export function onlyAuthorized(props, link) {
    if (typeof window === 'object') {
        /*
         В момент проверки авторизации пользователя authenticate.readyState == 'AUTH_AUTHENTICATE'.
         Если пользователь авторизован, то readyState изменится c AUTH_AUTHENTICATE на AUTH_AUTHENTICATED, а если пользователь не авторизован, то на AUTH_UNAUTHENTICATED.
         Если пользователь не авторизован, то его перенаправляет на страницу авторизации.
       */
        if (!props.auth.authenticate.is && props.auth.authenticate.readyState === AUTH_UNAUTHENTICATED) {
            const _link = link || '/login';
            browserHistory.push(_link);
        }
    }
}

export function isNotAuthorized(props) {
    return !props.auth.authenticate.is;
}

export function onlyNotAuthorized(props, link) {
    if (typeof window === 'object') {
        if (props.auth.authenticate.is) {
            browserHistory.push('/profile');
        }
    }
}

export function getUserInfo(props) {
    return props.auth.authenticate;
}