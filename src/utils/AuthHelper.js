import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Storage from './Storage'
import URL from '../url'

export const isAuthenticated = () => {
    const { access_token, expires_at, token_type, refresh_token } = get()

    if (access_token && expires_at && expires_at.setSeconds(expires_at.getSeconds() - 25200) < Date.now()) {
        if (DEVELOPMENT) console.log('isAuthenticated: access_token expired')
        if (refresh_token) {
            if (DEVELOPMENT) console.log('isAuthenticated: refresh_token exist')
            return refreshToken(refresh_token).then(d => {
                if (d && d.access_token) {
                    if (DEVELOPMENT) console.log('isAuthenticated: save new token')
                    return save(d)
                }
                if (DEVELOPMENT) console.log('isAuthenticated: empty token')
                return false
            }).then(b => {
                if (DEVELOPMENT) console.log('isAuthenticated: new query')
                if (b) isAuthenticated()
            })
        }
    }
    if (access_token && token_type)
        return true
    return false
}

/**
 * @return {Object<access_token,expires_at,token_type,refresh_token>} 
 */
export const get = () => {
    var t = Storage.getItem('expires_at')
    return {
        access_token: Storage.getItem('access_token'),
        expires_at: t ? new Date(t) : null,
        token_type: Storage.getItem('token_type'),
        refresh_token: Storage.getItem('refresh_token')
    }
}

export const save = ({ access_token, expires_in, token_type, refresh_token }) => {
    Storage.setItem('access_token', access_token)
    var expires = new Date()
    expires.setSeconds(expires.getSeconds() + expires_in);
    Storage.setItem('expires_at', expires)
    Storage.setItem('token_type', token_type)
    if (refresh_token) Storage.setItem('refresh_token', refresh_token)
    return true
}

export const remove = () => {
    Storage.setItem('access_token', '')
    Storage.setItem('expires_at', '')
    Storage.setItem('token_type', '')
    Storage.setItem('refresh_token', '')
    return true
}

export const RouteAuth = ({ component, redirect, ...rest }) => {
    const authenticated = isAuthenticated()
    redirect = redirect || URL.signIn
    return (
        <Route {...rest} render={(props) => {
            return authenticated ?
                (React.createElement(component, { ...props, authenticated })) :
                (<Redirect to={redirect} />);
        }} />
    )
};

export const RouteUnauth = ({ component, redirect, ...rest }) => {
    const authenticated = isAuthenticated()
    redirect = redirect ? redirect : URL.profile
    return (
        <Route {...rest} render={(props) => {
            return authenticated ?
                (<Redirect to={redirect} />) :
                (React.createElement(component, { ...props, authenticated }));
        }} />
    )
};

const refreshToken = (refresh_token) => {
    return fetch(CONFIG.connect.oauth + '/token', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: UrlHelper.handleBody({
            grant_type: 'refresh_token',
            client_id: CONFIG.clientId,
            refresh_token: refresh_token
        }),
    })
        .then(y => y.json())
}

export const fetchAuth = (url, params) => {

    let { access_token, expires_at, token_type, refresh_token } = get()

    if (access_token)
        params.headers['Authorization'] = token_type + ' ' + access_token

    if (access_token && expires_at && expires_at.setSeconds(expires_at.getSeconds() - 25200) < Date.now()) {
        if (DEVELOPMENT) console.log('fetchAuth: access_token expired')
        if (refresh_token) {
            if (DEVELOPMENT) console.log('fetchAuth: refresh_token exist')
            return refreshToken(refresh_token).then(d => {
                if (d && d.access_token) {
                    if (DEVELOPMENT) console.log('fetchAuth: save new token')
                    return save(d)
                }
                if (DEVELOPMENT) console.log('fetchAuth: empty token')
                return false
            }).then(b => {
                if (DEVELOPMENT) console.log('fetchAuth: new query')
                if (b) fetchAuth(url, params)
            })
        }
    }

    if (DEVELOPMENT) console.log('fetchAuth: params', params)
    return fetch(url, params)
}