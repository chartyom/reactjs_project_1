import React from 'react'
import { Link } from 'react-router-dom'
import URL from '../url'

class ThemeMain extends React.Component {
    date() {
        const d = new Date();
        return d.getFullYear();
    }
    render() {
        return (
            <div className="theme_main">
                <div className="theme_main__header">
                    <Link to={URL.home} className="theme_main__header__logotype" title="На главную"></Link>
                </div>
                <div className="theme_main__article">
                    {this.props.children}
                </div>
                <div className="theme_main__footer">
                    <div className="theme_main__footer__copytight">
                        © 2016 - {this.date()} Битком 24
                    </div>
                </div>
            </div>
        )
    }
}

export default ThemeMain
