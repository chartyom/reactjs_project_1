import React from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import Helmet from 'react-helmet';
import NotFoundPage from '../pages/NotFoundPage'
import HomePage from '../pages/HomePage'
import SignInPage from '../pages/SignInPage'
import SignOutPage from '../pages/SignOutPage'
import ProfilePage from '../pages/ProfilePage'
import AuthorizePage from '../pages/AuthorizePage'
import RegistrationPage from '../pages/RegistrationPage'
import URL from '../url'
import { RouteAuth, RouteUnauth } from '../utils/AuthHelper'

const App = () => (
    <div className="App">
        <Helmet
            htmlAttributes={{ lang: "ru" }}
            title="Битком 24"
            titleTemplate="%s - Битком 24"
        />
        <Switch>
            <Route exact path={URL.home} component={HomePage} />
            {/*<RouteUnauth exact path={URL.signIn} component={SignInPage} />*/}
            <RouteUnauth path={URL.registration} component={RegistrationPage} />
            <RouteAuth exact path={URL.signOut} component={SignOutPage} />
            {/*<RouteAuth exact path={URL.profile} component={ProfilePage} />*/}
            <Route exact path={URL.authorize} component={AuthorizePage} />
            <Route component={NotFoundPage} />
        </Switch>
    </div>
)
export default App
