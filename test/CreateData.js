#! /usr/bin/env node

var db = require('redis').createClient();

db.multi()
  .hmset('users:username', {
    id: 'username',
    username: 'username',
    password: 'password'
  })
  .hmset('clients:client', {
    clientId: 'client',
    clientSecret: 'secret'
  })
  .sadd('clients:client:grant_types', [
    'password',
    'refresh_token'
  ])
  .hmset('users:Artyom', {
    id: 'Artyom',
    username: 'Artyom',
    password: 'Artyom'
  })
  .hmset('clients:BitkomWebApp', {
    clientId: 'BitkomWebApp',
    clientSecret: 'secret'
  })
  .sadd('clients:BitkomWebApp:grant_types', [
    'password',
    'refresh_token',
    'client_credentials'
  ])
  .exec(function (errs) {
    if (errs) {
      console.error(errs[0].message);
      return process.exit(1);
    }

    console.log('Client and user added successfully');
    process.exit();
  });
