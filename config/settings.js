module.exports = {
    oauth: {
        accessTokenLifetime: 432000, // 5 дней. Время жизни созданного access_token в секундах  (second*minute*hour*day) 60*60*24*5 
        refreshTokenLifetime: 31536000, // 1 год. Время жизни созданного refresh_token в секундах  (second*minute*hour*day) 60*60*24*365  
        authorizationCodeLifetime: 600 // 10 мин. Время жизни созданного authorization_code в секундах (default = 5 minutes).
    }
}