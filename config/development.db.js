module.exports = {
    // https://github.com/vitaly-t/pg-promise/wiki/Connection-Syntax#configuration-object
    pg: {
        host: 'localhost',
        port: 5432,
        database: 'oauth',
        user: 'postgres',
        password: 'postgres',
        poolSize: 10
    },
    //https://www.npmjs.com/package/redis#options-object-properties
    redis: {
        host: 'localhost'
    },
}