
const resolve = (urls, apiVer = '') => {
    let resolved = {}
    for (let prop of Object.keys(urls)) {
        let url = urls[prop][env]
        let path = urls[prop].path || ''
        let api = (prop !== 'db' && prop !== 'memcached' && apiVer) ? `/${apiVer}` : ''
        let protocol = (prop === 'db') ? 'mongodb://' : 'http://'
        if (prop === 'memcached') protocol = ''
        resolved[prop] = protocol + url + api + path
    }
    return resolved
}

const envModel = () => {
    let env = process.env.ENV ? process.env.ENV.toLowerCase() : 'local'
    switch (env) {
        case 'development':
            env = 'env'
            break;
        case 'production':
            env = 'prod'
            break;
    }
    return env
}

const env = envModel();

module.exports = {
    settings: require('./settings'),
    server: DEVELOPMENT ? require('./development.server') : require('./production.server'),
    db: DEVELOPMENT ? require('./development.db') : require('./production.db'),
    DEFAULT_CACHE_TIME: 300,
    REFERRER_CODE_LENGTH: 6,
    /*url: resolve({
        memcached: { local: 'localhost:11211', dev: 'memcached:11211', prod: 'memcached:11211' },
        db: { local: 'localhost:27017', dev: 'mongo:27017', prod: 'mongo:27017', path: '/users' },
        file: { local: 'localhost:5050', dev: 'file', prod: 'file' },
        feedback: { local: 'localhost:5010', dev: 'feedback', prod: 'feedback' },
        restaurant: { local: 'localhost:5000', dev: 'restaurant', prod: 'restaurant' },
        reservation: { local: 'localhost:6060', dev: 'reservation', prod: 'reservation' },
        bonus: { local: 'localhost:7070', dev: 'bonus', prod: 'bonus' },
        sms: { local: 'localhost:3000', dev: 'sms', prod: 'sms' }
    })*/

    url: resolve({
        // memcached: { local: 'localhost:11211', dev: 'memcached:11211', prod: 'memcached:11211' },
        memcached: { local: '78.155.196.235:30432', dev: 'memcached:11211', prod: 'memcached:11211' },
        // db: { local: 'localhost:27017', dev: 'mongo:27017', prod: 'mongo:27017', path: '/users' },
        db: { local: '78.155.196.235:31736', dev: 'mongo:27017', prod: 'mongo:27017', path: '/users' },
        // file: { local: 'localhost:5050', dev: 'file', prod: 'file' },
        file: { local: '78.155.196.235:30087', dev: 'file', prod: 'file' },
        // feedback: { local: 'localhost:5010', dev: 'feedback', prod: 'feedback' },
        feedback: { local: '78.155.196.235:32127', dev: 'feedback', prod: 'feedback' },
        // restaurant: { local: 'localhost:5000', dev: 'restaurant', prod: 'restaurant' },
        restaurant: { local: '78.155.196.235:31771', dev: 'restaurant', prod: 'restaurant' },
        // reservation: { local: 'localhost:6060', dev: 'reservation', prod: 'reservation' },
        reservation: { local: '78.155.196.235:32424', dev: 'reservation', prod: 'reservation' },
        // bonus: { local: 'localhost:7070', dev: 'bonus', prod: 'bonus' },
        bonus: { local: '78.155.196.235:32702', dev: 'bonus', prod: 'bonus' },
        // sms: { local: 'localhost:3000', dev: 'sms', prod: 'sms' },
        sms: { local: '78.155.196.235:31509', dev: 'sms', prod: 'sms' },
        user: { local: '78.155.196.235:30026', dev: 'user', prod: 'user' }
    })
}

