/**
 * HELP: 
 * https://www.npmjs.com/package/redis
 */
var config = require('../config'),
    redis = require('redis'),
    bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);

var client = redis.createClient(config.db.redis)

client.on('ready', onDatabaseConnected)
client.on('end', onDatabaseDisconnected)
client.on('error', onDatabaseError)

module.exports = client;


function onDatabaseConnected() {
    console.log('Redis connected to database')
}

function onDatabaseDisconnected() {
    console.log('Redis disconnecting from database:')
}

function onDatabaseError(err) {
    console.log(`Redis connection has an error: ${err}`)
}