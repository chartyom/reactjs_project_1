/**
 * HELP: 
 * http://mongodb.github.io/node-mongodb-native/2.2/api/Db.html
 * http://mongoosejs.com/docs/connections.html
 */

const config = require('../config');
const mongoose = require('mongoose');

const connection = function () {
    mongoose.Promise = require('bluebird');
    var url = "mongodb://" + config.db.mongo.host + ":" + config.db.mongo.port + "/" + config.db.mongo.database
    var options = {
        server: {
            poolSize: 10,
            reconnectTries: Number.MAX_VALUE
        }
    }
    mongoose.connect(url, options)
    var a = mongoose.connection
    a.on('connected', onDatabaseConnected)
    a.on('disconnected', onDatabaseDisconnected)
    a.on('error', onDatabaseError)
    this.connect = a
}

module.exports = connection()


function onDatabaseConnected() {
    console.log('Mongo connected to database:', config.db.mongo.database)
}

function onDatabaseDisconnected() {
    console.log('Mongo disconnecting from database:', config.db.mongo.database)
    //mongoose.connect(dbUri, options)
}

function onDatabaseError(err) {
    console.log(`Mongo connection has an error: ${err}`)
    //mongoose.disconnect()
}

