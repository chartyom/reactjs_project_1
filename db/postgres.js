/*
 * HELP: 
 * https://www.npmjs.com/package/pg-promise
 * http://vitaly-t.github.io/pg-promise/module-pg-promise.html
 * 
 */


var Promise = require('bluebird'),
    options = {
        promiseLib: Promise,
        connect: (client, dc, isFresh) => {
            var cp = client.connectionParameters;
            console.log("Postgres connected to database:", cp.database);
        },
        disconnect: (client, dc) => {
            var cp = client.connectionParameters;
            console.log("Postgres disconnecting from database:", cp.database);
        },
        query: (e) => {
            console.log('Postgres query: "' + e.query + '"');
        }
    },
    config = require('../config'),
    pgp = require('pg-promise')(options);

module.exports = pgp(config.db.pg);