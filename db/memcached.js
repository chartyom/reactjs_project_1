const config = require('../config')
const Memcached = require('memcached')
const memcached = new Memcached(config.url.memcached)
memcached.on('failure', function( details ){ console.error( "Server " + details.server + "went down due to: " + details.messages.join( '' ) ) });
memcached.on('reconnecting', function( details ){ console.log( "Total downtime caused by server " + details.server + " :" + details.totalDownTime + "ms")});
memcached.on('issue', function( details ){ console.log( "issue")});
memcached.on('reconnect', function( details ){ console.log( "reconnect")});
module.exports = memcached