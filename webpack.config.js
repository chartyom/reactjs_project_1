const NODE_ENV = (process.env.NODE_ENV || 'development');
const webpack = require('webpack');
const path = require("path");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
//const CompressionPlugin = require("compression-webpack-plugin");

console.log('Webpack start with NODE_ENV:', NODE_ENV);

module.exports = [

    /*
        ===== BEGIN app =====
    */

    {
        entry: {
            'vendor.app': [
                'react',
                'react-dom',
                'react-router',
                'react-redux',
                'redux-thunk',
                'redux',
                'isomorphic-fetch',
                'es6-promise',
                'react-helmet'
            ],
            app: './src/Application.jsx',
        },
        output: {
            path: path.resolve(__dirname, 'public/temp'),
            filename: NODE_ENV == 'development' ? 'js/[name].js' : 'js/[name].[chunkhash:5].js',
        },
        // автозапуск после внесения изменений
        watch: NODE_ENV == 'development' ? true : false,
        watchOptions: {
            // задержка в мс запуска программы после сохранения файла с изменениями
            aggregateTimeout: 600
        },
        // различные способы проверки (debug) кода
        devtool: NODE_ENV == 'development' ? 'eval' : false,
        // Поиск require модулей 
        resolve: {
            modules: ['node_modules', 'src'],
            //extensions: ['', '.js', '.jsx', '.css', '.styl']
            extensions: ['.jsx', ".js", ".json"],
        },
        // Поиск loader модулей
        // resolveLoader: {
        //     modulesDirectories: ['node_modules'],
        //     moduleTemplates: ['*-loader', '*'],
        //     extensions: ['', '.js'],
        // },
        plugins: [
            //добавляет глобальные переменные в клиентскую часть
            new webpack.DefinePlugin({
                ENVIRONMENT: JSON.stringify(NODE_ENV),
                DEVELOPMENT: NODE_ENV == 'development' ? true : false,
                'process.env': {
                    NODE_ENV: JSON.stringify(NODE_ENV)
                }
            }),
            // extract vendor and webpack's module manifest
            new webpack.optimize.CommonsChunkPlugin({
                names: ['vendor.app', 'manifest.app'],
                minChunks: Infinity
            }),
            // extract common modules from all the chunks (requires no 'name' property)
            new webpack.optimize.CommonsChunkPlugin({
                async: true,
                children: true,
                minChunks: 4
            }),
            //Файл manifest.json
            new ManifestPlugin({
                fileName: '../../config/files.app.json',
                publicPath: 'temp/'
            }),
            //Подключение глобальных переменных для jquery
            new webpack.ProvidePlugin({
                //$: "jquery/dist/jquery.slim.min",
                //jQuery: "jquery/dist/jquery.slim.min"
                //React: "react/dist/react.min"
            }),
            //Минимизирует код
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
            // new webpack.LoaderOptionsPlugin({
            //     minimize: true,
            //     debug: false
            // }),
            new webpack.optimize.UglifyJsPlugin({
                minimize: NODE_ENV == 'development' ? false : true,
                compress: {
                    warnings: false,
                    drop_console: false,
                }
            }),

            new ExtractTextPlugin({
                filename: NODE_ENV == 'development' ? 'css/[name].css' : 'css/[name].[contenthash:5].css',
            }),
            // new CompressionPlugin({
            //     asset: "[path].gz[query]",
            //     algorithm: "gzip",
            //     test: /\.(js)$/,
            //     threshold: 10240,
            //     minRatio: 0.8
            // })
        ],
        module: {
            rules: [
                // rules for modules (configure loaders, parser options, etc.)
                {
                    test: /\.jsx?$/,
                    // include: [
                    //     path.resolve(process.cwd(), 'src/components')
                    // ],
                    exclude: /(node_modules)/,
                    loader: 'babel-loader',
                    query: {
                        presets: ["es2015", "stage-0", "react"]
                    }
                },
                {
                    test: /\.scss$/,
                    loader: ExtractTextPlugin.extract({
                        use: ['css-loader', "sass-loader"],
                        fallback: "style-loader"
                    }),
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    loaders: [
                        {
                            loader: 'file-loader',
                            query: {
                                //useRelativePath: process.env.NODE_ENV === "production",
                                name: '[name].[ext]?[hash:5]',
                                outputPath: 'images/',
                                publicPath: '/temp/'
                            }
                        },
                        {
                            loader: 'image-webpack-loader',
                            query: {
                                progressive: true,
                                optimizationLevel: 7,
                                interlaced: false,
                                pngquant: {
                                    quality: '65-90',
                                    speed: 4
                                },
                                mozjpeg: {
                                    quality: 65
                                },
                            }
                        },
                    ]
                    //'file-loader?hash=sha512&digest=hex&name=~/.local/share/Trash/[hash].[ext]',
                    // 'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false', {
                    //     loader: 'image-webpack-loader',
                    // }


                },
                {
                    test: /\.(eot|ttf|woff|woff2)$/,
                    loader: 'file-loader',
                    query: {
                        name: '[name].[ext]?[hash:5]',
                        outputPath: 'fonts/',
                        publicPath: '/temp/',
                    }
                }
            ]
        }
    },

    /*
        ===== END app =====
    */

];