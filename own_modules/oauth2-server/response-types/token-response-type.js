'use strict';

/**
 * Module dependencies.
 */

var BearerTokenType = require('../token-types/bearer-token-type');
var InvalidArgumentError = require('../errors/invalid-argument-error');
var is = require('../validator/is');
var url = require('url');
var tokenUtil = require('../utils/token-util');
var TokenModel = require('../models/token-model');

/**
 * Constructor.
 */

function TokenResponseType(options) {

  if (!options.model) {
    throw new InvalidArgumentError('Missing parameter: `model`');
  }

  if (!options.accessTokenLifetime) {
    throw new InvalidArgumentError('Missing parameter: `accessTokenLifetime`');
  }

  if (!options.model.saveToken) {
    throw new InvalidArgumentError('Invalid argument: model does not implement `saveToken()`');
  }

  this.model = options.model
  this.scopeDefault = options.scopeDefault
  this.accessTokenLifetime = options.accessTokenLifetime
  this.refreshTokenLifetime = options.refreshTokenLifetime
  this.allowExtendedTokenAttributes = options.allowExtendedTokenAttributes
  this.data = {}
}

/**
 * Handle code response type.
 */

TokenResponseType.prototype.handle = function (request, uri, client, user, scope, state) {
  if (!request) {
    throw new InvalidArgumentError('Missing parameter: `request`');
  }

  if (!client) {
    throw new InvalidArgumentError('Missing parameter: `client`');
  }

  if (!user) {
    throw new InvalidArgumentError('Missing parameter: `user`');
  }

  return this.saveToken(user, client, scope, state)
};




/**
 * Generate access token.
 */

TokenResponseType.prototype.generateAccessToken = async function (client, user, scope) {
  if (this.model.generateAccessToken) {
    return await this.model.generateAccessToken(client, user, scope)
      .then(function (accessToken) {
        return accessToken || tokenUtil.generateRandomToken();
      });
  }

  return tokenUtil.generateRandomToken();
};

/**
 * Generate refresh token.
 */

TokenResponseType.prototype.generateRefreshToken = async function (client, user, scope) {
  if (this.model.generateRefreshToken) {
    return await this.model.generateRefreshToken(client, user, scope)
      .then(function (refreshToken) {
        return refreshToken || tokenUtil.generateRandomToken();
      });
  }

  return tokenUtil.generateRandomToken();
};

/**
 * Get access token expiration date.
 */

TokenResponseType.prototype.getAccessTokenExpiresAt = function () {
  var expires = new Date();

  expires.setSeconds(expires.getSeconds() + this.accessTokenLifetime);

  return expires;
};

/**
 * Get refresh token expiration date.
 */

TokenResponseType.prototype.getRefreshTokenExpiresAt = function () {
  var expires = new Date();

  expires.setSeconds(expires.getSeconds() + this.refreshTokenLifetime);

  return expires;
};


/**
 * Save token.
 */

TokenResponseType.prototype.saveToken = async function (user, client, scope, state) {

  var accessToken = await this.generateAccessToken(client, user, scope)
  var refreshToken = await this.generateRefreshToken(client, user, scope)
  var accessTokenExpiresAt = this.getAccessTokenExpiresAt()
  var refreshTokenExpiresAt = this.getRefreshTokenExpiresAt()



  var token = {
    accessToken: accessToken,
    accessTokenExpiresAt: accessTokenExpiresAt,
    scope: scope
  };

  //Check access for refreshToken
  if (this.isClientAccessRefreshToken(client)) {
    token.refreshToken = refreshToken;
    token.refreshTokenExpiresAt = refreshTokenExpiresAt;
  }

  var model = new TokenModel({
    accessToken,
    accessTokenExpiresAt,
    refreshToken: token.refreshToken,
    scope,
    user,
    client,
    state
  }, { allowExtendedTokenAttributes: this.allowExtendedTokenAttributes });
  this.data = new BearerTokenType(model.accessToken, model.accessTokenLifetime, model.refreshToken, model.scope, model.customAttributes).valueOf();


  return await this.model.saveToken(token, client, user);
};


TokenResponseType.prototype.isClientAccessRefreshToken = function (client) {
  if (client.grants) {
    return (client.grants.indexOf('refresh_token') >= 0);
  } else {
    return false;
  }
};

/**
 * Build success redirect uri.
 */

TokenResponseType.prototype.buildSuccessRedirectUri = function (redirectUri) {
  if (!redirectUri) {
    throw new InvalidArgumentError('Missing parameter: `redirectUri`');
  }

  var uri = url.parse(redirectUri, true);

  uri.hash = '#' + this.buildObjectToQueryString(this.data);

  // uri.search = null;
  return uri;
};

/**
 * Build error redirect uri.
 */

TokenResponseType.prototype.buildErrorRedirectUri = function (redirectUri, error) {
  if (!redirectUri) {
    throw new InvalidArgumentError('Missing parameter: `redirectUri`');
  }

  var uri = url.parse(redirectUri);

  var jsonError = {
    error: error.name
  }

  if (error.message) {
    jsonError.error_description = error.message;
  }

  if (this.data.state) {
    jsonError.state = this.data.state;
  }

  uri.hash = '#' + this.buildObjectToQueryString(jsonError);

  return uri;
};

TokenResponseType.prototype.setState = function (state) {
  this.data.state = state
}


TokenResponseType.prototype.buildObjectToQueryString = function (json) {
  return Object.keys(json).map(function (key) {
    return encodeURIComponent(key) + '=' +
      encodeURIComponent(json[key]);
  }).join('&');
}


/**
 * Export constructor.
 */

module.exports = TokenResponseType;
