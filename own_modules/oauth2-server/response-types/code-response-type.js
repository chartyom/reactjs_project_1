'use strict';

/**
 * Module dependencies.
 */

var InvalidArgumentError = require('../errors/invalid-argument-error');
var is = require('../validator/is');
var url = require('url');

/**
 * Constructor.
 */

function CodeResponseType(options) {

  if (!options.model.saveAuthorizationCode) {
    throw new InvalidArgumentError('Invalid argument: model does not implement `saveAuthorizationCode()`');
  }

  if (!options.authorizationCodeLifetime) {
    throw new InvalidArgumentError('Missing parameter: `authorizationCodeLifetime`');
  }

  this.model = options.model
  this.scopeDefault = options.scopeDefault
  this.authorizationCodeLifetime = options.authorizationCodeLifetime
  this.data = {}
}

/**
 * Handle code response type.
 */

CodeResponseType.prototype.handle = function (request, uri, client, user, scope, state) {
  if (!request) {
    throw new InvalidArgumentError('Missing parameter: `request`');
  }

  if (!uri) {
    throw new InvalidArgumentError('Missing parameter: `uri`');
  }

  if (!client) {
    throw new InvalidArgumentError('Missing parameter: `client`');
  }

  if (!user) {
    throw new InvalidArgumentError('Missing parameter: `user`');
  }

  return this.saveAuthorizationCode(scope, state, client, uri, user)
};



/**
 * Generate authorization code.
 */

CodeResponseType.prototype.generateAuthorizationCode = function () {
  if (this.model.generateAuthorizationCode) {
    return this.model.generateAuthorizationCode();
  }
  return tokenUtil.generateRandomToken();
};

/**
 * Get authorization code lifetime.
 */

CodeResponseType.prototype.getAuthorizationCodeLifetime = function () {
  var expires = new Date();

  expires.setSeconds(expires.getSeconds() + this.authorizationCodeLifetime);
  return expires;
};


/**
 * Save authorization code.
 */

CodeResponseType.prototype.saveAuthorizationCode = async function (scope, state, client, redirectUri, user) {

  var authorizationCode = await this.generateAuthorizationCode()
  var expiresAt = this.getAuthorizationCodeLifetime()

  this.data.code = authorizationCode;
  this.data.state = state;

  var code = {
    authorizationCode: authorizationCode,
    expiresAt: expiresAt,
    redirectUri: redirectUri,
    scope: scope
  };
  return await this.model.saveAuthorizationCode(code, client, user);
};

CodeResponseType.prototype.setState = function (state) {
  this.data.state = state
}

/**
 * Build success redirect uri.
 */

CodeResponseType.prototype.buildSuccessRedirectUri = function (redirectUri) {
  if (!redirectUri) {
    throw new InvalidArgumentError('Missing parameter: `redirectUri`');
  }

  var uri = url.parse(redirectUri, true);

  uri.query.code = this.data.code;
  uri.search = null;
  if (this.data.state) {
    uri.query.state = this.data.state;
  }
  return uri;
};

/**
 * Build error redirect uri.
 */

CodeResponseType.prototype.buildErrorRedirectUri = function (redirectUri, error) {
  if (!redirectUri) {
    throw new InvalidArgumentError('Missing parameter: `redirectUri`');
  }

  var uri = url.parse(redirectUri);

  uri.query = {
    error: error.name
  };

  if (error.message) {
    uri.query.error_description = error.message;
  }

  if (this.data.state) {
    uri.query.state = this.data.state;
  }

  return uri;
};

/**
 * Export constructor.
 */

module.exports = CodeResponseType;
