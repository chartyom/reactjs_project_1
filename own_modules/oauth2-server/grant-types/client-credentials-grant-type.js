'use strict';

/**
 * Module dependencies.
 */

var AbstractGrantType = require('./abstract-grant-type');
var InvalidArgumentError = require('../errors/invalid-argument-error');
var InvalidGrantError = require('../errors/invalid-grant-error');
var util = require('util');

/**
 * Constructor.
 */

function ClientCredentialsGrantType(options) {
  options = options || {};

  if (!options.model) {
    throw new InvalidArgumentError('Missing parameter: `model`');
  }

  if (!options.model.getUserFromClient) {
    throw new InvalidArgumentError('Invalid argument: model does not implement `getUserFromClient()`');
  }

  if (!options.model.saveToken) {
    throw new InvalidArgumentError('Invalid argument: model does not implement `saveToken()`');
  }

  AbstractGrantType.call(this, options);
}

/**
 * Inherit prototype.
 */

util.inherits(ClientCredentialsGrantType, AbstractGrantType);

/**
 * Handle client credentials grant.
 *
 * @see https://tools.ietf.org/html/rfc6749#section-4.4.2
 */

ClientCredentialsGrantType.prototype.handle = async function (request, client) {
  if (!request) {
    throw new InvalidArgumentError('Missing parameter: `request`');
  }

  if (!client) {
    throw new InvalidArgumentError('Missing parameter: `client`');
  }

  var scope = await this.getScope(request);
  var user = await this.getUserFromClient(client);
  return await this.saveToken(user, client, scope);

};

/**
 * Retrieve the user using client credentials.
 */

ClientCredentialsGrantType.prototype.getUserFromClient = async function (client) {
  return await this.model.getUserFromClient(client)
    .then(function (user) {
      if (!user) {
        throw new InvalidGrantError('Invalid grant: user credentials are invalid');
      }

      return user;
    });
};

/**
 * Save token.
 */

ClientCredentialsGrantType.prototype.saveToken = async function (user, client, scope) {

  var scope = await this.validateScope(user, client, scope)
  var accessToken = await this.generateAccessToken(client, user, scope)
  var accessTokenExpiresAt = await this.getAccessTokenExpiresAt(client, user, scope)

  var token = {
    accessToken: accessToken,
    accessTokenExpiresAt: accessTokenExpiresAt,
    scope: scope
  };

  return await this.model.saveToken(token, client, user);

};

/**
 * Export constructor.
 */

module.exports = ClientCredentialsGrantType;
