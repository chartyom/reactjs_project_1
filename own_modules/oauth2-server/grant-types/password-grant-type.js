'use strict';

/**
 * Module dependencies.
 */

var AbstractGrantType = require('./abstract-grant-type');
var InvalidArgumentError = require('../errors/invalid-argument-error');
var InvalidGrantError = require('../errors/invalid-grant-error');
var InvalidRequestError = require('../errors/invalid-request-error');
var Promise = require('bluebird');
var is = require('../validator/is');
var util = require('util');

/**
 * Constructor.
 */

function PasswordGrantType(options) {
  options = options || {};

  if (!options.model) {
    throw new InvalidArgumentError('Missing parameter: `model`');
  }

  if (!options.model.getUser) {
    throw new InvalidArgumentError('Invalid argument: model does not implement `getUser()`');
  }

  if (!options.model.saveToken) {
    throw new InvalidArgumentError('Invalid argument: model does not implement `saveToken()`');
  }

  AbstractGrantType.call(this, options);
}

/**
 * Inherit prototype.
 */

util.inherits(PasswordGrantType, AbstractGrantType);

/**
 * Retrieve the user from the model using a username/password combination.
 *
 * @see https://tools.ietf.org/html/rfc6749#section-4.3.2
 */

PasswordGrantType.prototype.handle = async function (request, client) {
  if (!request) {
    throw new InvalidArgumentError('Missing parameter: `request`');
  }

  if (!client) {
    throw new InvalidArgumentError('Missing parameter: `client`');
  }

  var scope = this.getScope(request)
  var user = await this.getUser(request)
  return await this.saveToken(user, client, scope)

};

/**
 * Get user using a username/password combination.
 */

PasswordGrantType.prototype.getUser = async function (request) {
  if (!request.body.username) {
    throw new InvalidRequestError('Missing parameter: `username`');
  }

  if (!request.body.password) {
    throw new InvalidRequestError('Missing parameter: `password`');
  }

  if (!is.uchar(request.body.username)) {
    throw new InvalidRequestError('Invalid parameter: `username`');
  }

  if (!is.uchar(request.body.password)) {
    throw new InvalidRequestError('Invalid parameter: `password`');
  }

  return await this.model.getUser(request.body.username, request.body.password)
    .then(function (user) {
      if (!user) {
        throw new InvalidGrantError('Invalid grant: user credentials are invalid');
      }

      return user;
    });
};

/**
 * Save token.
 */

PasswordGrantType.prototype.saveToken = async function (user, client, scope) {

  //Authenticate with Password dont need rules
  //var scope = await this.validateScope(user, client, scope)
  var accessToken = await this.generateAccessToken(client, user, scope)
  var refreshToken = await this.generateRefreshToken(client, user, scope)
  var accessTokenExpiresAt = this.getAccessTokenExpiresAt()
  var refreshTokenExpiresAt = this.getRefreshTokenExpiresAt()

  var token = {
    accessToken: accessToken,
    accessTokenExpiresAt: accessTokenExpiresAt,
    scope: client.scope
  };

  //Check access for refreshToken
  if (this.isClientAccessRefreshToken(client) || this.isUserOwnerClient(client)) {
    token.refreshToken = refreshToken;
    token.refreshTokenExpiresAt = refreshTokenExpiresAt;
  }

  return await this.model.saveToken(token, client, user);
};

/**
 * Export constructor.
 */

module.exports = PasswordGrantType;
